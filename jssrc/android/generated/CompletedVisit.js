function addWidgetsCompletedVisit() {
    CompletedVisit.setDefaultUnit(kony.flex.DP);
    var flxCompletedContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxCompletedContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxCompletedContainer.setDefaultUnit(kony.flex.DP);
    var flxCompletedHeaderContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxCompletedHeaderContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknHeaderBg",
        "top": "0dp",
        "width": "100.00%"
    }, {}, {});
    flxCompletedHeaderContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "30dp",
        "id": "imgBurgerIcon",
        "isVisible": true,
        "left": "12dp",
        "skin": "slImage",
        "src": "burgermenu.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBurgerMenu = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "40dp",
        "id": "btnBurgerMenu",
        "isVisible": true,
        "left": "7dp",
        "onClick": AS_NamedActions_0ee594e17b72488ba950418f5daec2ad,
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblHeaderCompleted = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderCompleted",
        "isVisible": true,
        "left": "54dp",
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblWhiteLargeSkin",
        "text": kony.i18n.getLocalizedString("common.label.completedJobs"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCompletedHeaderContainer.add(imgBurgerIcon, btnBurgerMenu, lblHeaderCompleted);
    var flxCompletedCusName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxCompletedCusName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknHeaderPropertyTitleBgSkin",
        "top": "55dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCompletedCusName.setDefaultUnit(kony.flex.DP);
    var lblCompletedCustomerName = new kony.ui.Label({
        "centerY": "50.00%",
        "id": "lblCompletedCustomerName",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblWhiteNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCompletedCusName.add(lblCompletedCustomerName);
    var flxCompletedDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxCompletedDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "90dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCompletedDetails.setDefaultUnit(kony.flex.DP);
    var tabCompletedCustomerDetails = new kony.ui.TabPane({
        "activeFocusSkin": "sknTabPaneRedInactive",
        "activeSkin": "sknTabPaneRedInactive",
        "activeTabs": [6],
        "bottom": "0dp",
        "id": "tabCompletedCustomerDetails",
        "inactiveSkin": "sknTabPaneRedBg",
        "isVisible": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "left": "10dp",
        "onTabClick": AS_TabPane_8803b48f198749f9922ce3f5c4b5f99a,
        "right": "10dp",
        "top": "0dp",
        "viewConfig": {
            "collapsibleViewConfig": {
                "collapsedImage": "icon_plus_big.png",
                "collapsedimage": "icon_plus_big.png",
                "expandedImage": "icon_minus_big.png",
                "expandedimage": "icon_minus_big.png",
                "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                "imageposition": "right",
                "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                "tabnamealignment": "left",
                "toggleTabs": true,
                "toggletabs": true
            },
            "collapsibleviewconfig": {
                "collapsedImage": "icon_plus_big.png",
                "collapsedimage": "icon_plus_big.png",
                "expandedImage": "icon_minus_big.png",
                "expandedimage": "icon_minus_big.png",
                "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                "imageposition": "right",
                "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                "tabnamealignment": "left",
                "toggleTabs": true,
                "toggletabs": true
            },
            "pageViewConfig": {
                "needPageIndicator": true
            },
            "tabViewConfig": {
                "headerPosition": constants.TAB_HEADER_POSITION_TOP,
                "image1": "tableftarrow.png",
                "image2": "tabrightarrow.png"
            },
        },
        "viewType": constants.TABPANE_VIEW_TYPE_COLLAPSIBLEVIEW
    }, {
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "tabHeaderHeight": 200
    });
    var tabCompletedCustomerInfo = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "290dp",
        "id": "tabCompletedCustomerInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Customer Information",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCompletedCustomerInfo.setDefaultUnit(kony.flex.DP);
    var flxCompletedCustomerInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxCompletedCustomerInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxCompletedCustomerInfo.setDefaultUnit(kony.flex.DP);
    var flxNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "slFbox",
        "top": "5dp",
        "width": "94%",
        "zIndex": 10
    }, {}, {});
    flxNameContainer.setDefaultUnit(kony.flex.DP);
    var lblCustomerNumber = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCustomerNumber",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 1,
        "skin": "lblBlkBld120",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine = new kony.ui.Label({
        "bottom": "0%",
        "centerY": "88.89%",
        "height": "1dp",
        "id": "lblLine",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLineBlk",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNameContainer.add(lblCustomerNumber, lblLine);
    var flxAddressContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "74dp",
        "id": "flxAddressContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "slFbox",
        "top": "35dp",
        "width": "94%",
        "zIndex": 10
    }, {}, {});
    flxAddressContainer.setDefaultUnit(kony.flex.DP);
    var flxAddressLeftContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxAddressLeftContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0%",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxAddressLeftContainer.setDefaultUnit(kony.flex.DP);
    var imgLocIcon = new kony.ui.Image2({
        "centerX": "50.00%",
        "centerY": "50%",
        "height": "30dp",
        "id": "imgLocIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "navigate.png",
        "width": "30dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAddressLeftContainer.add(imgLocIcon);
    var flxAddressRightContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "64dp",
        "id": "flxAddressRightContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "12%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxAddressRightContainer.setDefaultUnit(kony.flex.DP);
    var lblAddress = new kony.ui.Label({
        "id": "lblAddress",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 2,
        "skin": "lblBlk100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "5dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPostcode = new kony.ui.Label({
        "id": "lblPostcode",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 2,
        "skin": "lblBlk100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "47dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAddressRightContainer.add(lblAddress, lblPostcode);
    var lblLine1 = new kony.ui.Label({
        "bottom": "0%",
        "height": "1dp",
        "id": "lblLine1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLineBlk",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "69dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAddressContainer.add(flxAddressLeftContainer, flxAddressRightContainer, lblLine1);
    var flxDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "176dp",
        "id": "flxDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "slFbox",
        "top": "109dp",
        "width": "94%",
        "zIndex": 10
    }, {}, {});
    flxDetailsContainer.setDefaultUnit(kony.flex.DP);
    var lblName = new kony.ui.Label({
        "height": "25dp",
        "id": "lblName",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 1,
        "skin": "lblBlk100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDesingnation = new kony.ui.Label({
        "height": "25dp",
        "id": "lblDesingnation",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblBlk100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPhoneContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxPhoneContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0dp",
        "skin": "slFbox",
        "top": "50dp",
        "zIndex": 1
    }, {}, {});
    flxPhoneContainer.setDefaultUnit(kony.flex.DP);
    var flxPhoneLeftContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPhoneLeftContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxPhoneLeftContainer.setDefaultUnit(kony.flex.DP);
    var imgPhoneIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "30dp",
        "id": "imgPhoneIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "call.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPhoneLeftContainer.add(imgPhoneIcon);
    var flxPhoneRightContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPhoneRightContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "12%",
        "skin": "slFbox",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxPhoneRightContainer.setDefaultUnit(kony.flex.DP);
    var lblPhone = new kony.ui.Label({
        "centerY": "50%",
        "height": "28dp",
        "id": "lblPhone",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblEmailSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPhoneRightContainer.add(lblPhone);
    flxPhoneContainer.add(flxPhoneLeftContainer, flxPhoneRightContainer);
    var flxMobileContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxMobileContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0dp",
        "skin": "slFbox",
        "top": "92dp",
        "zIndex": 1
    }, {}, {});
    flxMobileContainer.setDefaultUnit(kony.flex.DP);
    var flxMobileLeftContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxMobileLeftContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxMobileLeftContainer.setDefaultUnit(kony.flex.DP);
    var imgMobileIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "30dp",
        "id": "imgMobileIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "mobile.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMobileLeftContainer.add(imgMobileIcon);
    var flxMobileRightContaine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxMobileRightContaine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "12%",
        "skin": "slFbox",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxMobileRightContaine.setDefaultUnit(kony.flex.DP);
    var lblMobile = new kony.ui.Label({
        "centerY": "50%",
        "height": "28dp",
        "id": "lblMobile",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblEmailSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMobileRightContaine.add(lblMobile);
    flxMobileContainer.add(flxMobileLeftContainer, flxMobileRightContaine);
    var flxEmailContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxEmailContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0dp",
        "skin": "slFbox",
        "top": "134dp",
        "zIndex": 1
    }, {}, {});
    flxEmailContainer.setDefaultUnit(kony.flex.DP);
    var flxEmailLeftContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxEmailLeftContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxEmailLeftContainer.setDefaultUnit(kony.flex.DP);
    var imgEmailIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "30dp",
        "id": "imgEmailIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "email.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxEmailLeftContainer.add(imgEmailIcon);
    var flxEmailRightContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxEmailRightContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "12%",
        "skin": "slFbox",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxEmailRightContainer.setDefaultUnit(kony.flex.DP);
    var lblEmail = new kony.ui.Label({
        "centerY": "50%",
        "height": "28dp",
        "id": "lblEmail",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblEmailSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEmailRightContainer.add(lblEmail);
    flxEmailContainer.add(flxEmailLeftContainer, flxEmailRightContainer);
    flxDetailsContainer.add(lblName, lblDesingnation, flxPhoneContainer, flxMobileContainer, flxEmailContainer);
    flxCompletedCustomerInfo.add(flxNameContainer, flxAddressContainer, flxDetailsContainer);
    tabCompletedCustomerInfo.add(flxCompletedCustomerInfo);
    tabCompletedCustomerDetails.addTab("tabCompletedCustomerInfo", "  Customer Information", null, tabCompletedCustomerInfo, null);
    var tabCompletedRiskAsst = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "170dp",
        "id": "tabCompletedRiskAsst",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Site Risk Assessment",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCompletedRiskAsst.setDefaultUnit(kony.flex.DP);
    var FlxCompletedRiskAsst = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "FlxCompletedRiskAsst",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    FlxCompletedRiskAsst.setDefaultUnit(kony.flex.DP);
    var lblSRAPlaceholder = new kony.ui.Label({
        "id": "lblSRAPlaceholder",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblGrayPlaceHolderSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 2, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txaSiteRiskAssessment = new kony.ui.TextArea2({
        "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
        "bottom": "10dp",
        "focusSkin": "sknTxtAreaDisabledVNote",
        "id": "txaSiteRiskAssessment",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
        "left": "10dp",
        "numberOfVisibleLines": 3,
        "right": "10dp",
        "skin": "sknTxtAreaVNote",
        "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
        "top": "10dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [2, 2, 2, 2],
        "paddingInPixel": false
    }, {});
    FlxCompletedRiskAsst.add(lblSRAPlaceholder, txaSiteRiskAssessment);
    tabCompletedRiskAsst.add(FlxCompletedRiskAsst);
    tabCompletedCustomerDetails.addTab("tabCompletedRiskAsst", "  Site Risk Assessment", null, tabCompletedRiskAsst, null);
    var tabCompletedServiceNotes = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "170dp",
        "id": "tabCompletedServiceNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Service Notes",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCompletedServiceNotes.setDefaultUnit(kony.flex.DP);
    var flxCompletedServiceNotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxCompletedServiceNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    flxCompletedServiceNotes.setDefaultUnit(kony.flex.DP);
    var lblServiceNotesPlaceholder = new kony.ui.Label({
        "id": "lblServiceNotesPlaceholder",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblGrayPlaceHolderSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 2, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txaServiceNotes = new kony.ui.TextArea2({
        "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
        "bottom": "10dp",
        "focusSkin": "sknTxtAreaDisabledVNote",
        "id": "txaServiceNotes",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
        "left": "10dp",
        "numberOfVisibleLines": 3,
        "right": "10dp",
        "skin": "sknTxtAreaVNote",
        "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
        "top": "10dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [2, 2, 2, 2],
        "paddingInPixel": false
    }, {});
    flxCompletedServiceNotes.add(lblServiceNotesPlaceholder, txaServiceNotes);
    tabCompletedServiceNotes.add(flxCompletedServiceNotes);
    tabCompletedCustomerDetails.addTab("tabCompletedServiceNotes", "  Service Notes", null, tabCompletedServiceNotes, null);
    var tabCompletedServiceCovers = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "230dp",
        "id": "tabCompletedServiceCovers",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Service Covers",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCompletedServiceCovers.setDefaultUnit(kony.flex.DP);
    var flxCompletedServiceCovers = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "clipBounds": true,
        "id": "flxCompletedServiceCovers",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "flxWhiteBgWithBorderSkin",
        "top": "10dp",
        "zIndex": 10
    }, {}, {});
    flxCompletedServiceCovers.setDefaultUnit(kony.flex.DP);
    var segService = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [
            [{
                    "lblTitle": ""
                },
                [{
                    "lblBottom": "",
                    "lblTop": ""
                }]
            ]
        ],
        "groupCells": false,
        "id": "segService",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "right": "0dp",
        "rowTemplate": flxSegCommonList,
        "scrollingEvents": {},
        "sectionHeaderTemplate": flxHeaderSegCommonList,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxHeaderSegCommonList": "flxHeaderSegCommonList",
            "flxSegCommonList": "flxSegCommonList",
            "lblBottom": "lblBottom",
            "lblTitle": "lblTitle",
            "lblTop": "lblTop"
        },
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNoServiceCovers = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblNoServiceCovers",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblNoRecordsSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCompletedServiceCovers.add(segService, lblNoServiceCovers);
    tabCompletedServiceCovers.add(flxCompletedServiceCovers);
    tabCompletedCustomerDetails.addTab("tabCompletedServiceCovers", "  Service Covers", null, tabCompletedServiceCovers, null);
    var tabCompletedHistory = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "230dp",
        "id": "tabCompletedHistory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  History",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCompletedHistory.setDefaultUnit(kony.flex.DP);
    var flxCompletedHistory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "clipBounds": true,
        "id": "flxCompletedHistory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "flxWhiteBgWithBorderSkin",
        "top": "10dp",
        "zIndex": 20
    }, {}, {});
    flxCompletedHistory.setDefaultUnit(kony.flex.DP);
    var segCustomerHistory = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [
            [{
                    "lblTitle": ""
                },
                [{
                    "lblTop": ""
                }]
            ]
        ],
        "groupCells": false,
        "id": "segCustomerHistory",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "right": "0dp",
        "rowTemplate": flxSegCommonSingleRow,
        "scrollingEvents": {},
        "sectionHeaderTemplate": flxHeaderSegCommonList,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxHeaderSegCommonList": "flxHeaderSegCommonList",
            "flxSegCommonSingleRow": "flxSegCommonSingleRow",
            "lblTitle": "lblTitle",
            "lblTop": "lblTop"
        }
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNoHistory = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblNoHistory",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblNoRecordsSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCompletedHistory.add(segCustomerHistory, lblNoHistory);
    tabCompletedHistory.add(flxCompletedHistory);
    tabCompletedCustomerDetails.addTab("tabCompletedHistory", "  History", null, tabCompletedHistory, null);
    var tabCompletedPreparations = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "230dp",
        "id": "tabCompletedPreparations",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": " Preparations",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCompletedPreparations.setDefaultUnit(kony.flex.DP);
    var flxCompletedPreparationsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "290dp",
        "id": "flxCompletedPreparationsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCompletedPreparationsContainer.setDefaultUnit(kony.flex.DP);
    var lblPreprationHead = new kony.ui.Label({
        "id": "lblPreprationHead",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "lblBlackNormalBoldSkin",
        "text": "Preparations",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblQuatity = new kony.ui.Label({
        "id": "lblQuatity",
        "isVisible": true,
        "left": "71%",
        "skin": "lblBlackNormalBoldSkin",
        "text": "Quantity",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var PreprationsCmb1 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb1",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknListBoxNormal",
        "top": "35dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var PreprationsCmb2 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb2",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknListBoxNormal",
        "top": "73dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var PreprationsCmb3 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb3",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknListBoxNormal",
        "top": "111dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var PreprationsCmb4 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb4",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknListBoxNormal",
        "top": "149dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var PreprationsCmb5 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb5",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknListBoxNormal",
        "top": "186dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb1 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb1",
        "isVisible": true,
        "left": "71%",
        "skin": "sknListBoxNormal",
        "top": "35dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb2 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb2",
        "isVisible": true,
        "left": "71%",
        "skin": "sknListBoxNormal",
        "top": "73dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb3 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb3",
        "isVisible": true,
        "left": "71%",
        "skin": "sknListBoxNormal",
        "top": "111dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb4 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb4",
        "isVisible": true,
        "left": "71.03%",
        "skin": "sknListBoxNormal",
        "top": "149dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb5 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb5",
        "isVisible": true,
        "left": "71%",
        "skin": "sknListBoxNormal",
        "top": "186dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    flxCompletedPreparationsContainer.add(lblPreprationHead, lblQuatity, PreprationsCmb1, PreprationsCmb2, PreprationsCmb3, PreprationsCmb4, PreprationsCmb5, quatityCmb1, quatityCmb2, quatityCmb3, quatityCmb4, quatityCmb5);
    tabCompletedPreparations.add(flxCompletedPreparationsContainer);
    tabCompletedCustomerDetails.addTab("tabCompletedPreparations", " Preparations", null, tabCompletedPreparations, null);
    var tabCompletedVisitNotes = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "220dp",
        "id": "tabCompletedVisitNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": " Visit Notes",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCompletedVisitNotes.setDefaultUnit(kony.flex.DP);
    var flexCompletedVnotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flexCompletedVnotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexCompletedVnotes.setDefaultUnit(kony.flex.DP);
    var lblVisitNotePlaceholder = new kony.ui.Label({
        "height": "30dp",
        "id": "lblVisitNotePlaceholder",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "lblGrayPlaceHolderSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtAreaVisitNote = new kony.ui.TextArea2({
        "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
        "height": "80dp",
        "id": "txtAreaVisitNote",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
        "left": "10dp",
        "numberOfVisibleLines": 3,
        "right": "10dp",
        "skin": "sknTxtAreaVNote",
        "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
        "top": "10dp"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [2, 2, 2, 2],
        "paddingInPixel": false
    }, {});
    var flxCapturedImgContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "clipBounds": true,
        "height": "110dp",
        "id": "flxCapturedImgContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "flxWhiteBgWithBorderSkin",
        "top": "100dp",
        "zIndex": 1
    }, {}, {});
    flxCapturedImgContainer.setDefaultUnit(kony.flex.DP);
    var imgCapturedPhoto = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "89.24%",
        "id": "imgCapturedPhoto",
        "isVisible": true,
        "skin": "slImage",
        "src": "noimage.png",
        "width": "96.97%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCapturedImgContainer.add(imgCapturedPhoto);
    flexCompletedVnotes.add(lblVisitNotePlaceholder, txtAreaVisitNote, flxCapturedImgContainer);
    tabCompletedVisitNotes.add(flexCompletedVnotes);
    tabCompletedCustomerDetails.addTab("tabCompletedVisitNotes", " Visit Notes", null, tabCompletedVisitNotes, null);
    var tabCompletedSignature = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "210dp",
        "id": "tabCompletedSignature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Signature",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCompletedSignature.setDefaultUnit(kony.flex.DP);
    var FlexCompletedmainSignature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "FlexCompletedmainSignature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "100.00%",
        "zIndex": 1
    }, {}, {});
    FlexCompletedmainSignature.setDefaultUnit(kony.flex.DP);
    var lblCustomerNamePlaceholder = new kony.ui.Label({
        "height": "30dp",
        "id": "lblCustomerNamePlaceholder",
        "isVisible": true,
        "left": "10dp",
        "skin": "lblGrayPlaceHolderSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtCustomerNameSig = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "30dp",
        "id": "txtCustomerNameSig",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "10dp",
        "right": "10dp",
        "secureTextEntry": false,
        "skin": "txtNormalSkin",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 2, 1],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var FlexSignhere = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "FlexSignhere",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "flxWhiteBgWithBorderSkin",
        "top": "50dp",
        "zIndex": 1
    }, {}, {});
    FlexSignhere.setDefaultUnit(kony.flex.DP);
    var lblSignHere = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "28dp",
        "id": "lblSignHere",
        "isVisible": true,
        "skin": "lblGrayLargeSkin",
        "text": "Sign here",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgSignature = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "40%",
        "height": "100dp",
        "id": "imgSignature",
        "isVisible": false,
        "skin": "slImage",
        "src": "imagedrag.png",
        "width": "300dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSignDate = new kony.ui.Label({
        "bottom": "3dp",
        "centerX": "50%",
        "height": "28dp",
        "id": "lblSignDate",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "lblGrayPlaceHolderSkin",
        "text": "15/6/2016",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexSignhere.add(lblSignHere, imgSignature, lblSignDate);
    var signatureReasoncmb = new kony.ui.ListBox({
        "height": "40dp",
        "id": "signatureReasoncmb",
        "isVisible": true,
        "left": "10dp",
        "masterData": [
            ["lb1", "No Signature Reason"],
            ["lb2", "Customer not available"],
            ["lb3", "Customer refused to sign"]
        ],
        "right": "10dp",
        "selectedKey": "lb1",
        "selectedKeyValue": ["lb1", "No Signature Reason"],
        "skin": "sknListBoxNormal",
        "top": "160dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    FlexCompletedmainSignature.add(lblCustomerNamePlaceholder, txtCustomerNameSig, FlexSignhere, signatureReasoncmb);
    tabCompletedSignature.add(FlexCompletedmainSignature);
    tabCompletedCustomerDetails.addTab("tabCompletedSignature", "  Signature", null, tabCompletedSignature, null);
    var tabCompletedTime = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100dp",
        "id": "tabCompletedTime",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Time",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCompletedTime.setDefaultUnit(kony.flex.DP);
    var flxCompletedTimeContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxCompletedTimeContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxCompletedTimeContainer.setDefaultUnit(kony.flex.DP);
    var lblVisitStartTime = new kony.ui.Label({
        "id": "lblVisitStartTime",
        "isVisible": true,
        "left": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "Start Time:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStartTimeValue = new kony.ui.Label({
        "id": "lblStartTimeValue",
        "isVisible": true,
        "left": "50%",
        "right": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "-:--",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "46%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEndTime = new kony.ui.Label({
        "id": "lblEndTime",
        "isVisible": true,
        "left": "50%",
        "right": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "-:--",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "39dp",
        "width": "46%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblVisitEndTime = new kony.ui.Label({
        "id": "lblVisitEndTime",
        "isVisible": true,
        "left": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "End Time  :",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "39dp",
        "width": "45%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDuration = new kony.ui.Label({
        "id": "lblDuration",
        "isVisible": true,
        "left": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "Duration      :",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDurationValue = new kony.ui.Label({
        "id": "lblDurationValue",
        "isVisible": true,
        "left": "50%",
        "right": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "-:--",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70dp",
        "width": "46%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCompletedTimeContainer.add(lblVisitStartTime, lblStartTimeValue, lblEndTime, lblVisitEndTime, lblDuration, lblDurationValue);
    tabCompletedTime.add(flxCompletedTimeContainer);
    tabCompletedCustomerDetails.addTab("tabCompletedTime", "  Time", null, tabCompletedTime, null);
    flxCompletedDetails.add(tabCompletedCustomerDetails);
    flxCompletedContainer.add(flxCompletedHeaderContainer, flxCompletedCusName, flxCompletedDetails);
    var flxBurgerMenuContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "-50%",
        "clipBounds": true,
        "id": "flxBurgerMenuContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_0af4e55072b7436b9c31e57c1ec48b17,
        "skin": "flxBurgerMenuShadowSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBurgerMenuContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerMenuContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerMenuContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "80%"
    }, {}, {});
    flxBurgerMenuContentContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerTopContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxBurgerTopContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxRedBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxBurgerTopContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerMenu = new kony.ui.Image2({
        "height": "32dp",
        "id": "imgBurgerMenu",
        "isVisible": false,
        "left": "8%",
        "skin": "slImage",
        "src": "burgermenu.png",
        "top": "7%",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAppVersion = new kony.ui.Label({
        "id": "lblAppVersion",
        "isVisible": true,
        "right": "10dp",
        "skin": "lblLightGrayTinySkin",
        "text": "v1.0.0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgAppLogo = new kony.ui.Image2({
        "centerY": "45%",
        "height": "29dp",
        "id": "imgAppLogo",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "rentokillogo.png",
        "width": "138dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxUserDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxUserDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_da53288bd8d047e3a24f5917c5ed23ba,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUserDetailsContainer.setDefaultUnit(kony.flex.DP);
    var imgUserIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgUserIcon",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "usericon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUserEmail = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblUserEmail",
        "isVisible": true,
        "left": "20%",
        "maxNumberOfLines": 2,
        "skin": "lblWhiteNormalSkin",
        "text": "adam.mcintyre@rentokil-initial.com",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "76%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxUserDetailsContainer.add(imgUserIcon, lblUserEmail);
    flxBurgerTopContainer.add(imgBurgerMenu, lblAppVersion, imgAppLogo, flxUserDetailsContainer);
    var flxBurgerBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "32%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBurgerBottomContainer.setDefaultUnit(kony.flex.DP);
    var segMenuListOne = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "imgBtnIcon": "settings.png",
            "lblItem": "Label"
        }],
        "groupCells": false,
        "height": "225dp",
        "id": "segMenuListOne",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_a86ad15c3344435f9fc981732db12b4e,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegMenuListTop,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxRowSegMenuListTop": "flxRowSegMenuListTop",
            "imgBtnIcon": "imgBtnIcon",
            "lblItem": "lblItem"
        },
        "width": "100%"
    }, {
        "padding": [0, 1, 0, 0],
        "paddingInPixel": false
    }, {});
    var segMenuListTwo = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "imgBtnIcon": "",
            "lblItem": ""
        }],
        "groupCells": false,
        "height": "90dp",
        "id": "segMenuListTwo",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_001b3e171a0e4f5b946fd4779a211e97,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegMenuList,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "60%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxRowSegMenuList": "flxRowSegMenuList",
            "imgBtnIcon": "imgBtnIcon",
            "lblItem": "lblItem"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLastSync = new kony.ui.Label({
        "bottom": "30dp",
        "id": "lblLastSync",
        "isVisible": false,
        "left": "4%",
        "skin": "lblDarkGrayTinySkin",
        "text": "Date last transferred:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastSyncDate = new kony.ui.Label({
        "bottom": "25dp",
        "id": "lblLastSyncDate",
        "isVisible": true,
        "left": "4%",
        "skin": "lblDarkGrayTinySkin",
        "text": "11/05/2016 15:30:12",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBurgerBottomContainer.add(segMenuListOne, segMenuListTwo, lblLastSync, lblLastSyncDate);
    flxBurgerMenuContentContainer.add(flxBurgerTopContainer, flxBurgerBottomContainer);
    flxBurgerMenuContainer.add(flxBurgerMenuContentContainer);
    CompletedVisit.add(flxCompletedContainer, flxBurgerMenuContainer);
};

function CompletedVisitGlobals() {
    CompletedVisit = new kony.ui.Form2({
        "addWidgets": addWidgetsCompletedVisit,
        "enabledForIdleTimeout": false,
        "id": "CompletedVisit",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_2fb3e6c77635413ca91300dd1cbda116,
        "preShow": AS_Form_4c166880f72546d0b18526ea07c5d2f0,
        "skin": "slForm"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_1713b6cd7e414e63bef91146436cbc73,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
    CompletedVisit.info = {
        "kuid": "c1efd43832cc436ea217c43c5583de5d"
    };
};