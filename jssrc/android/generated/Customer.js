function addWidgetsCustomer() {
    Customer.setDefaultUnit(kony.flex.DP);
    var flxCutomerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxCutomerContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxCutomerContainer.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxHeaderContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknHeaderBg",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxHeaderContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "30dp",
        "id": "imgBurgerIcon",
        "isVisible": true,
        "left": "12dp",
        "skin": "slImage",
        "src": "burgermenu.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBurgerMenu = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "40dp",
        "id": "btnBurgerMenu",
        "isVisible": true,
        "left": "7dp",
        "onClick": AS_NamedActions_0ee594e17b72488ba950418f5daec2ad,
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblHeaderTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "left": "54dp",
        "maxNumberOfLines": 1,
        "skin": "lblWhiteLargeSkin",
        "text": kony.i18n.getLocalizedString("common.label.customer"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_NONE,
        "width": "53%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgStartVisit = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgStartVisit",
        "isVisible": true,
        "right": "64dp",
        "skin": "slImage",
        "src": "startvisit.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnStartVisit = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "50dp",
        "id": "btnStartVisit",
        "isVisible": true,
        "onClick": AS_FlexContainer_8ee136b6189f45eeafa0a9970378bc31,
        "right": "59dp",
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStartdayIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgStartdayIcon",
        "isVisible": true,
        "right": "12dp",
        "skin": "slImage",
        "src": "startday.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnStartday = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "50dp",
        "id": "btnStartday",
        "isVisible": true,
        "onClick": AS_NamedActions_98246b17def84ae4a557741194b3ce3f,
        "right": "7dp",
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderContainer.add(imgBurgerIcon, btnBurgerMenu, lblHeaderTitle, imgStartVisit, btnStartVisit, imgStartdayIcon, btnStartday);
    var flxPropertyNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxPropertyNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknHeaderPropertyTitleBgSkin",
        "top": "55dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    flxPropertyNameContainer.setDefaultUnit(kony.flex.DP);
    var lblPropertyName = new kony.ui.Label({
        "centerY": "50.00%",
        "id": "lblPropertyName",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "lblWhiteNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "94%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPropertyNameContainer.add(lblPropertyName);
    var flxPropertyContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxPropertyContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0%",
        "skin": "flxWhiteBgSkin",
        "top": "85dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxPropertyContainer.setDefaultUnit(kony.flex.DP);
    var tabCustomerDetails = new kony.ui.TabPane({
        "activeFocusSkin": "sknTabPaneRedInactive",
        "activeSkin": "sknTabPaneRedInactive",
        "activeTabs": [1],
        "bottom": "75dp",
        "centerX": "49.97%",
        "id": "tabCustomerDetails",
        "inactiveSkin": "sknTabPaneRedBg",
        "isVisible": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "left": "0dp",
        "right": "0dp",
        "top": "5dp",
        "viewConfig": {
            "collapsibleViewConfig": {
                "collapsedImage": "icon_plus_big.png",
                "collapsedimage": "icon_plus_big.png",
                "expandedImage": "icon_minus_big.png",
                "expandedimage": "icon_minus_big.png",
                "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                "imageposition": "right",
                "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                "tabnamealignment": "left",
                "toggleTabs": true,
                "toggletabs": true
            },
            "collapsibleviewconfig": {
                "collapsedImage": "icon_plus_big.png",
                "collapsedimage": "icon_plus_big.png",
                "expandedImage": "icon_minus_big.png",
                "expandedimage": "icon_minus_big.png",
                "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                "imageposition": "right",
                "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                "tabnamealignment": "left",
                "toggleTabs": true,
                "toggletabs": true
            },
            "pageViewConfig": {
                "needPageIndicator": true
            },
            "tabViewConfig": {
                "headerPosition": constants.TAB_HEADER_POSITION_TOP,
                "image1": "tableftarrow.png",
                "image2": "tabrightarrow.png"
            },
        },
        "viewType": constants.TABPANE_VIEW_TYPE_COLLAPSIBLEVIEW,
        "width": "96%"
    }, {
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "tabHeaderHeight": 200
    });
    var tabCustomerInfo = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "290dp",
        "id": "tabCustomerInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Customer Information",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabCustomerInfo.setDefaultUnit(kony.flex.DP);
    var flxCustomerInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxCustomerInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxCustomerInfo.setDefaultUnit(kony.flex.DP);
    var flxNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "slFbox",
        "top": "5dp",
        "width": "94%",
        "zIndex": 10
    }, {}, {});
    flxNameContainer.setDefaultUnit(kony.flex.DP);
    var lblCustomerNumber = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCustomerNumber",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 1,
        "skin": "lblBlkBld120",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLine = new kony.ui.Label({
        "bottom": "0%",
        "centerY": "88.89%",
        "height": "1dp",
        "id": "lblLine",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLineBlk",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNameContainer.add(lblCustomerNumber, lblLine);
    var flxAddressContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "74dp",
        "id": "flxAddressContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "slFbox",
        "top": "35dp",
        "width": "94%",
        "zIndex": 10
    }, {}, {});
    flxAddressContainer.setDefaultUnit(kony.flex.DP);
    var flxAddressLeftContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxAddressLeftContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_6a32e70dbbfc4d0eb2f14dfe56b7d208,
        "skin": "flxWhiteBgSkin",
        "top": "0%",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxAddressLeftContainer.setDefaultUnit(kony.flex.DP);
    var imgLocIcon = new kony.ui.Image2({
        "centerX": "50.00%",
        "centerY": "50%",
        "height": "30dp",
        "id": "imgLocIcon",
        "isVisible": true,
        "onTouchStart": AS_Image_b257e883c2b94cd6993904636aaad026,
        "skin": "slImage",
        "src": "navigate.png",
        "width": "30dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAddressLeftContainer.add(imgLocIcon);
    var flxAddressRightContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxAddressRightContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "12%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxAddressRightContainer.setDefaultUnit(kony.flex.DP);
    var lblAddress = new kony.ui.Label({
        "id": "lblAddress",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 2,
        "skin": "lblBlk100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "5dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPostcode = new kony.ui.Label({
        "id": "lblPostcode",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 1,
        "skin": "lblBlk100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "47dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAddressRightContainer.add(lblAddress, lblPostcode);
    var lblLine1 = new kony.ui.Label({
        "bottom": "0%",
        "height": "1dp",
        "id": "lblLine1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLineBlk",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAddressContainer.add(flxAddressLeftContainer, flxAddressRightContainer, lblLine1);
    var flxDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "176dp",
        "id": "flxDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "slFbox",
        "top": "111dp",
        "width": "94%",
        "zIndex": 10
    }, {}, {});
    flxDetailsContainer.setDefaultUnit(kony.flex.DP);
    var lblName = new kony.ui.Label({
        "height": "25dp",
        "id": "lblName",
        "isVisible": true,
        "left": "0dp",
        "maxNumberOfLines": 1,
        "skin": "lblBlk100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDesingnation = new kony.ui.Label({
        "height": "25dp",
        "id": "lblDesingnation",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblBlk100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPhoneContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxPhoneContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_002187e08e6f4127bdb8aa7a1b924f29,
        "right": "0dp",
        "skin": "slFbox",
        "top": "50dp",
        "zIndex": 1
    }, {}, {});
    flxPhoneContainer.setDefaultUnit(kony.flex.DP);
    var flxPhoneLeftContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPhoneLeftContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxPhoneLeftContainer.setDefaultUnit(kony.flex.DP);
    var imgPhoneIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "30dp",
        "id": "imgPhoneIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "call.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPhoneLeftContainer.add(imgPhoneIcon);
    var flxPhoneRightContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPhoneRightContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "12%",
        "skin": "slFbox",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxPhoneRightContainer.setDefaultUnit(kony.flex.DP);
    var lblPhone = new kony.ui.Label({
        "centerY": "50%",
        "height": "28dp",
        "id": "lblPhone",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblEmailSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPhoneRightContainer.add(lblPhone);
    flxPhoneContainer.add(flxPhoneLeftContainer, flxPhoneRightContainer);
    var flxMobileContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxMobileContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_d7ecb8b152414afbab76c20b5e63dfe6,
        "right": "0dp",
        "skin": "slFbox",
        "top": "92dp",
        "zIndex": 1
    }, {}, {});
    flxMobileContainer.setDefaultUnit(kony.flex.DP);
    var flxMobileLeftContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxMobileLeftContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxMobileLeftContainer.setDefaultUnit(kony.flex.DP);
    var imgMobileIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "30dp",
        "id": "imgMobileIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "mobile.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMobileLeftContainer.add(imgMobileIcon);
    var flxMobileRightContaine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxMobileRightContaine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "12%",
        "skin": "slFbox",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxMobileRightContaine.setDefaultUnit(kony.flex.DP);
    var lblMobile = new kony.ui.Label({
        "centerY": "50%",
        "height": "28dp",
        "id": "lblMobile",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblEmailSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMobileRightContaine.add(lblMobile);
    flxMobileContainer.add(flxMobileLeftContainer, flxMobileRightContaine);
    var flxEmailContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxEmailContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_4b45b4748224474ea557cd12d3c44a7d,
        "right": "0dp",
        "skin": "slFbox",
        "top": "134dp",
        "zIndex": 1
    }, {}, {});
    flxEmailContainer.setDefaultUnit(kony.flex.DP);
    var flxEmailLeftContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxEmailLeftContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxEmailLeftContainer.setDefaultUnit(kony.flex.DP);
    var imgEmailIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "30dp",
        "id": "imgEmailIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "email.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxEmailLeftContainer.add(imgEmailIcon);
    var flxEmailRightContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxEmailRightContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "12%",
        "skin": "slFbox",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxEmailRightContainer.setDefaultUnit(kony.flex.DP);
    var lblEmail = new kony.ui.Label({
        "centerY": "50%",
        "height": "28dp",
        "id": "lblEmail",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblEmailSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEmailRightContainer.add(lblEmail);
    flxEmailContainer.add(flxEmailLeftContainer, flxEmailRightContainer);
    flxDetailsContainer.add(lblName, lblDesingnation, flxPhoneContainer, flxMobileContainer, flxEmailContainer);
    flxCustomerInfo.add(flxNameContainer, flxAddressContainer, flxDetailsContainer);
    tabCustomerInfo.add(flxCustomerInfo);
    tabCustomerDetails.addTab("tabCustomerInfo", "  Customer Information", null, tabCustomerInfo, null);
    var tabRiskAsst = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "290dp",
        "id": "tabRiskAsst",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Site Risk Assessment",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabRiskAsst.setDefaultUnit(kony.flex.DP);
    var FlxRiskAsst = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "FlxRiskAsst",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    FlxRiskAsst.setDefaultUnit(kony.flex.DP);
    var lblSRAPlaceholder = new kony.ui.Label({
        "id": "lblSRAPlaceholder",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "lblGrayPlaceHolderSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 2, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txaSiteRiskAssessment = new kony.ui.TextArea2({
        "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_SENTENCES,
        "focusSkin": "sknTxtAreaVNote",
        "height": "220dp",
        "id": "txaSiteRiskAssessment",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
        "left": "10dp",
        "numberOfVisibleLines": 3,
        "onDone": AS_TextArea_6cd6d51b579e40449ae00c499700158e,
        "onTextChange": AS_TextField_b15e86ae0cba4e26905bab47ad0d0195,
        "right": "10dp",
        "skin": "sknTxtAreaVNote",
        "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [2, 2, 2, 2],
        "paddingInPixel": false
    }, {
        "onBeginEditing": AS_TextArea_b2e87630af274c3a844f26ed179f885b,
        "onEndEditing": AS_TextArea_a73ad3460ce6487489c2a43aa9c1906f
    });
    var flxSelectSRA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "clipBounds": true,
        "height": "35dp",
        "id": "flxSelectSRA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "onClick": AS_FlexContainer_c7b8af903bc54bb88127b7e815ef882f,
        "skin": "flxGrayBgWithBorderSkin",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxSelectSRA.setDefaultUnit(kony.flex.DP);
    var imgSelectSRA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgSelectSRA",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "down.png",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSelectSRA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSelectSRA",
        "isVisible": true,
        "left": "5dp",
        "maxNumberOfLines": 1,
        "skin": "lblGrayNormalSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSelectSRA.add(imgSelectSRA, lblSelectSRA);
    var imgEditAndDone = new kony.ui.Image2({
        "bottom": "10dp",
        "height": "35dp",
        "id": "imgEditAndDone",
        "isVisible": true,
        "onTouchStart": AS_Image_b20b32c771674b5db53722bfff53e24f,
        "right": "10dp",
        "skin": "slImage",
        "src": "edit.png",
        "width": "35dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnEditAndDone = new kony.ui.Button({
        "bottom": "10dp",
        "focusSkin": "btnTransparentSkin",
        "height": "35dp",
        "id": "btnEditAndDone",
        "isVisible": true,
        "onClick": AS_Button_84a1645ccffc496b858461022eddaca2,
        "right": "10dp",
        "skin": "btnTransparentSkin",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlxRiskAsst.add(lblSRAPlaceholder, txaSiteRiskAssessment, flxSelectSRA, imgEditAndDone, btnEditAndDone);
    tabRiskAsst.add(FlxRiskAsst);
    tabCustomerDetails.addTab("tabRiskAsst", "  Site Risk Assessment", null, tabRiskAsst, null);
    var tabServiceNotes = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "290dp",
        "id": "tabServiceNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Service Notes",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabServiceNotes.setDefaultUnit(kony.flex.DP);
    var flxServiceNotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxServiceNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    flxServiceNotes.setDefaultUnit(kony.flex.DP);
    var lblServiceNotesPlaceholder = new kony.ui.Label({
        "id": "lblServiceNotesPlaceholder",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblGrayPlaceHolderSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 2, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txaServiceNotes = new kony.ui.TextArea2({
        "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
        "bottom": "10dp",
        "focusSkin": "sknTxtAreaDisabledVNote",
        "id": "txaServiceNotes",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
        "left": "10dp",
        "numberOfVisibleLines": 3,
        "right": "10dp",
        "skin": "sknTxtAreaVNote",
        "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
        "top": "10dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [2, 2, 2, 2],
        "paddingInPixel": false
    }, {});
    flxServiceNotes.add(lblServiceNotesPlaceholder, txaServiceNotes);
    tabServiceNotes.add(flxServiceNotes);
    tabCustomerDetails.addTab("tabServiceNotes", "  Service Notes", null, tabServiceNotes, null);
    var tabServiceCovers = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "290dp",
        "id": "tabServiceCovers",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Service Covers",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabServiceCovers.setDefaultUnit(kony.flex.DP);
    var flxServiceCovers = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "clipBounds": true,
        "id": "flxServiceCovers",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "flxWhiteBgWithBorderSkin",
        "top": "10dp",
        "zIndex": 10
    }, {}, {});
    flxServiceCovers.setDefaultUnit(kony.flex.DP);
    var segService = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [
            [{
                    "lblTitle": ""
                },
                [{
                    "lblBottom": "",
                    "lblTop": ""
                }]
            ]
        ],
        "groupCells": false,
        "id": "segService",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "right": "0dp",
        "rowTemplate": flxSegCommonList,
        "scrollingEvents": {},
        "sectionHeaderTemplate": flxHeaderSegCommonList,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxHeaderSegCommonList": "flxHeaderSegCommonList",
            "flxSegCommonList": "flxSegCommonList",
            "lblBottom": "lblBottom",
            "lblTitle": "lblTitle",
            "lblTop": "lblTop"
        },
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNoServiceCovers = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblNoServiceCovers",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblNoRecordsSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxServiceCovers.add(segService, lblNoServiceCovers);
    tabServiceCovers.add(flxServiceCovers);
    tabCustomerDetails.addTab("tabServiceCovers", "  Service Covers", null, tabServiceCovers, null);
    var tabHistory = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "290dp",
        "id": "tabHistory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  History",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabHistory.setDefaultUnit(kony.flex.DP);
    var flxHistory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "clipBounds": true,
        "id": "flxHistory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "skin": "flxWhiteBgWithBorderSkin",
        "top": "10dp",
        "zIndex": 20
    }, {}, {});
    flxHistory.setDefaultUnit(kony.flex.DP);
    var segCustomerHistory = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [
            [{
                    "lblTitle": ""
                },
                [{
                    "lblTop": ""
                }]
            ]
        ],
        "groupCells": false,
        "id": "segCustomerHistory",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "right": "0dp",
        "rowTemplate": flxSegCommonSingleRow,
        "scrollingEvents": {},
        "sectionHeaderTemplate": flxHeaderSegCommonList,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxHeaderSegCommonList": "flxHeaderSegCommonList",
            "flxSegCommonSingleRow": "flxSegCommonSingleRow",
            "lblTitle": "lblTitle",
            "lblTop": "lblTop"
        }
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNoHistory = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblNoHistory",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblNoRecordsSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHistory.add(segCustomerHistory, lblNoHistory);
    tabHistory.add(flxHistory);
    tabCustomerDetails.addTab("tabHistory", "  History", null, tabHistory, null);
    var flxStartVisitBtnContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxStartVisitBtnContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_8ee136b6189f45eeafa0a9970378bc31,
        "skin": "slFbox",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    flxStartVisitBtnContainer.setDefaultUnit(kony.flex.DP);
    var ImageStartVisit = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "60dp",
        "id": "ImageStartVisit",
        "isVisible": true,
        "skin": "slImage",
        "src": "startvisit.png",
        "width": "60dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStartVisitBtnContainer.add(ImageStartVisit);
    flxPropertyContainer.add(tabCustomerDetails, flxStartVisitBtnContainer);
    flxCutomerContainer.add(flxHeaderContainer, flxPropertyNameContainer, flxPropertyContainer);
    var flxBurgerMenuContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "-50%",
        "clipBounds": true,
        "id": "flxBurgerMenuContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_d0afaa6188384f2eb8c89840cf60e162,
        "skin": "flxBurgerMenuShadowSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBurgerMenuContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerMenuContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerMenuContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "80%"
    }, {}, {});
    flxBurgerMenuContentContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerTopContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxBurgerTopContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxRedBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxBurgerTopContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerMenu = new kony.ui.Image2({
        "height": "32dp",
        "id": "imgBurgerMenu",
        "isVisible": false,
        "left": "8%",
        "skin": "slImage",
        "src": "burgermenu.png",
        "top": "7%",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAppVersion = new kony.ui.Label({
        "id": "lblAppVersion",
        "isVisible": true,
        "right": "10dp",
        "skin": "lblLightGrayTinySkin",
        "text": "v1.0.0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgAppLogo = new kony.ui.Image2({
        "centerY": "45%",
        "height": "29dp",
        "id": "imgAppLogo",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "rentokillogo.png",
        "width": "138dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxUserDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxUserDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_a126a59110c14305ba412620d3e8e770,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUserDetailsContainer.setDefaultUnit(kony.flex.DP);
    var imgUserIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgUserIcon",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "usericon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUserEmail = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblUserEmail",
        "isVisible": true,
        "left": "20%",
        "maxNumberOfLines": 2,
        "skin": "lblWhiteNormalSkin",
        "text": "adam.mcintyre@rentokil-initial.com",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "76%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxUserDetailsContainer.add(imgUserIcon, lblUserEmail);
    flxBurgerTopContainer.add(imgBurgerMenu, lblAppVersion, imgAppLogo, flxUserDetailsContainer);
    var flxBurgerBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "32%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBurgerBottomContainer.setDefaultUnit(kony.flex.DP);
    var segMenuListOne = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "imgBtnIcon": "settings.png",
            "lblItem": "Label"
        }],
        "groupCells": false,
        "height": "225dp",
        "id": "segMenuListOne",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_553bb9e3acd84154bf440fc4c74fae29,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegMenuListTop,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxRowSegMenuListTop": "flxRowSegMenuListTop",
            "imgBtnIcon": "imgBtnIcon",
            "lblItem": "lblItem"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var segMenuListTwo = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "actionId": "",
            "imgBtnIcon": "",
            "lblItem": ""
        }],
        "groupCells": false,
        "height": "90dp",
        "id": "segMenuListTwo",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_1ee653556cec4da38edc7a78cfe92f34,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegMenuList,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "60%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxRowSegMenuList": "flxRowSegMenuList",
            "imgBtnIcon": "imgBtnIcon",
            "lblItem": "lblItem"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLastSync = new kony.ui.Label({
        "bottom": "30dp",
        "id": "lblLastSync",
        "isVisible": false,
        "left": "4%",
        "skin": "lblDarkGrayTinySkin",
        "text": "Date last transferred:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastSyncDate = new kony.ui.Label({
        "bottom": "25dp",
        "id": "lblLastSyncDate",
        "isVisible": true,
        "left": "4%",
        "skin": "lblDarkGrayTinySkin",
        "text": "11/05/2016 15:30:12",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBurgerBottomContainer.add(segMenuListOne, segMenuListTwo, lblLastSync, lblLastSyncDate);
    flxBurgerMenuContentContainer.add(flxBurgerTopContainer, flxBurgerBottomContainer);
    flxBurgerMenuContainer.add(flxBurgerMenuContentContainer);
    var flxSRAContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSRAContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "onClick": AS_NamedActions_98246b17def84ae4a557741194b3ce3f,
        "skin": "flxMapCallOut",
        "top": "0%",
        "width": "100%"
    }, {}, {});
    flxSRAContainer.setDefaultUnit(kony.flex.DP);
    var FlxSRA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "40%",
        "id": "FlxSRA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10%",
        "right": "10%",
        "skin": "flxWhiteBgWithBorderSkin"
    }, {}, {});
    FlxSRA.setDefaultUnit(kony.flex.DP);
    var flxHdrView = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxHdrView",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxHeaderBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxHdrView.setDefaultUnit(kony.flex.DP);
    var lblHdr = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHdr",
        "isVisible": true,
        "left": "5dp",
        "right": "5dp",
        "skin": "lblWhiteNormalBoldSkin",
        "text": kony.i18n.getLocalizedString("common.app.select"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        }
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHdrView.add(lblHdr);
    var checkBxSRA = new kony.ui.CheckBoxGroup({
        "height": "135dp",
        "id": "checkBxSRA",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "checkBxBlk120",
        "top": "50dp",
        "zIndex": 1
    }, {
        "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
        "padding": [2, 2, 2, 2],
        "paddingInPixel": false
    }, {
        "tickedImage": "checkbox.png",
        "untickedImage": "uncheckbox.png"
    });
    var flxBottomView = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxBottomView",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "slFbox",
        "width": "250dp",
        "zIndex": 1
    }, {}, {});
    flxBottomView.setDefaultUnit(kony.flex.DP);
    var btnCancel = new kony.ui.Button({
        "centerX": "22%",
        "centerY": "50%",
        "focusSkin": "btnCancelSkin",
        "height": "40dp",
        "id": "btnCancel",
        "isVisible": true,
        "left": "13dp",
        "onClick": AS_Button_a081a6b22511443ba1d79870e79935a8,
        "skin": "btnCancelSkin",
        "text": kony.i18n.getLocalizedString("common.label.cancel"),
        "top": "7dp",
        "width": "120dp"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnConfirm = new kony.ui.Button({
        "centerX": "78%",
        "centerY": "50%",
        "focusSkin": "btnNormalSkin",
        "height": "40dp",
        "id": "btnConfirm",
        "isVisible": true,
        "left": "13dp",
        "onClick": AS_Button_5291ce94cd984252aeb3ff9acc44ac81,
        "skin": "btnNormalSkin",
        "text": kony.i18n.getLocalizedString("common.label.confirm"),
        "top": "7dp",
        "width": "120dp"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBottomView.add(btnCancel, btnConfirm);
    FlxSRA.add(flxHdrView, checkBxSRA, flxBottomView);
    flxSRAContainer.add(FlxSRA);
    Customer.add(flxCutomerContainer, flxBurgerMenuContainer, flxSRAContainer);
};

function CustomerGlobals() {
    Customer = new kony.ui.Form2({
        "addWidgets": addWidgetsCustomer,
        "enabledForIdleTimeout": false,
        "id": "Customer",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_2a97f627436c43deb579adfcb73c9d04,
        "preShow": AS_Form_1be9783649f14cac84c157c4e9295bbe,
        "skin": "slForm"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_d0e9569948564331b7720834eb14639b,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
    Customer.info = {
        "kuid": "366f2167c681470e9458506c1b7096bb"
    };
};