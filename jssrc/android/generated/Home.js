function addWidgetsHome() {
    Home.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxHeaderContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxHeaderBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxHeaderContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "30dp",
        "id": "imgBurgerIcon",
        "isVisible": true,
        "left": "12dp",
        "skin": "slImage",
        "src": "burgermenu.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBurgerMenu = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "40dp",
        "id": "btnBurgerMenu",
        "isVisible": true,
        "left": "7dp",
        "onClick": AS_NamedActions_0ee594e17b72488ba950418f5daec2ad,
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblHeaderTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "left": "54dp",
        "maxNumberOfLines": 1,
        "skin": "lblWhiteLargeSkin",
        "text": "Work View",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_NONE,
        "width": "53%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgCalendarIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgCalendarIcon",
        "isVisible": true,
        "right": "64dp",
        "skin": "slImage",
        "src": "calendarblank.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnCalendar = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentTinyTextSkin",
        "height": "50dp",
        "id": "btnCalendar",
        "isVisible": true,
        "onClick": AS_Button_1135915f8f9a4d5fb8d9448ee518b935,
        "right": "59dp",
        "skin": "btnTransparentTinyTextSkin",
        "text": "17",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 1, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStartdayIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgStartdayIcon",
        "isVisible": true,
        "right": "12dp",
        "skin": "slImage",
        "src": "startday.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnStartday = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "50dp",
        "id": "btnStartday",
        "isVisible": true,
        "onClick": AS_Button_f0ddc6bd50f945598a90677049332a24,
        "right": "7dp",
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderContainer.add(imgBurgerIcon, btnBurgerMenu, lblHeaderTitle, imgCalendarIcon, btnCalendar, imgStartdayIcon, btnStartday);
    var flxSALabelContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxSALabelContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxGary",
        "top": "55dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSALabelContainer.setDefaultUnit(kony.flex.DP);
    var lblServiceAreaCode = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblServiceAreaCode",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "right": "10dp",
        "skin": "lblWhiteNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "3dp",
        "width": "94%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSALabelContainer.add(lblServiceAreaCode);
    var flxMenuContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "flxMenuContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxRedBgSkin",
        "top": "85dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMenuContainer.setDefaultUnit(kony.flex.DP);
    var btnSchedule = new kony.ui.Button({
        "bottom": "0dp",
        "focusSkin": "btnTransparentSkin",
        "id": "btnSchedule",
        "isVisible": true,
        "left": "0%",
        "onClick": AS_Button_c4bd39d43e194ea4815246c8e9f4a814,
        "skin": "btnSelectedSkin",
        "text": "Schedule",
        "top": "0dp",
        "width": "49.80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLineSeperatorRight = new kony.ui.Label({
        "height": "100%",
        "id": "lblLineSeperatorRight",
        "isVisible": true,
        "left": "49.80%",
        "skin": "lblWhiteBgSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "0.30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnMap = new kony.ui.Button({
        "bottom": "0dp",
        "focusSkin": "btnTransparentSkin",
        "id": "btnMap",
        "isVisible": true,
        "left": "50.10%",
        "onClick": AS_Button_53fcfede0ab14a6095a8c346f8790660,
        "skin": "btnTransparentSkin",
        "text": "Map",
        "top": "0dp",
        "width": "49.90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMenuContainer.add(btnSchedule, lblLineSeperatorRight, btnMap);
    var flxContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "120dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContentContainer.setDefaultUnit(kony.flex.DP);
    var flxScheduleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxScheduleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScheduleContainer.setDefaultUnit(kony.flex.DP);
    var segHomeContentList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [
            [{
                    "lblTitle": ""
                },
                [{
                    "dateStrKey": "",
                    "imgJobTypeIcon": "",
                    "imgLeftArrowIcon": "",
                    "isGPSPresent": "false",
                    "isMobileNumberPresent": "false",
                    "isPhoneNumberPresent": "false",
                    "lblItemBottom": "",
                    "lblItemMiddle": "",
                    "lblItemTop": "",
                    "sectionIndex": "",
                    "visitStatus": ""
                }]
            ]
        ],
        "groupCells": false,
        "height": "92%",
        "id": "segHomeContentList",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_a1f824de671849849182b9e916eaa2a9,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegHomeContentList,
        "scrollingEvents": {},
        "sectionHeaderTemplate": flxHeaderSegHomeList,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxHeaderSegHomeList": "flxHeaderSegHomeList",
            "flxLeft": "flxLeft",
            "flxMiddle": "flxMiddle",
            "flxRight": "flxRight",
            "flxRowSegHomeContentList": "flxRowSegHomeContentList",
            "imgImpIcon": "imgImpIcon",
            "imgJobTypeIcon": "imgJobTypeIcon",
            "imgLeftArrowIcon": "imgLeftArrowIcon",
            "lblItemBottom": "lblItemBottom",
            "lblItemMiddle": "lblItemMiddle",
            "lblItemTop": "lblItemTop",
            "lblTitle": "lblTitle"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNoScheduledJobs = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblNoScheduledJobs",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblNoRecordsSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCompletedList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxCompletedList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "92%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCompletedList.setDefaultUnit(kony.flex.DP);
    var flxCompletedJobsHeaderContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxCompletedJobsHeaderContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_d60b46c86b1e4f75963b67ce5e63f969,
        "skin": "flxRedBgBorderedSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCompletedJobsHeaderContainer.setDefaultUnit(kony.flex.DP);
    var lblCompletedJobs = new kony.ui.Label({
        "centerY": "50.28%",
        "height": "30dp",
        "id": "lblCompletedJobs",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "lblWhiteNormalSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgToggle = new kony.ui.Image2({
        "centerY": "50%",
        "height": "30dp",
        "id": "imgToggle",
        "isVisible": true,
        "right": "10dp",
        "skin": "slImage",
        "src": "scrollup.png",
        "width": "30dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCompletedJobsHeaderContainer.add(lblCompletedJobs, imgToggle);
    var segHomeCompletedList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [{
            "imgJobTypeIcon": "",
            "imgLeftArrowIcon": "",
            "lblItemBottom": "",
            "lblItemMiddle": "",
            "lblItemTop": "",
            "sectionIndex": ""
        }],
        "groupCells": false,
        "id": "segHomeCompletedList",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_cc1feb89d9c44c4eb106328cbcba09c3,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegHomeContentList,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "40dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxLeft": "flxLeft",
            "flxRight": "flxRight",
            "flxRowSegHomeContentList": "flxRowSegHomeContentList",
            "imgJobTypeIcon": "imgJobTypeIcon",
            "imgLeftArrowIcon": "imgLeftArrowIcon",
            "lblItemBottom": "lblItemBottom",
            "lblItemMiddle": "lblItemMiddle",
            "lblItemTop": "lblItemTop"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNoCompletedJobs = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblNoCompletedJobs",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblNoRecordsSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCompletedList.add(flxCompletedJobsHeaderContainer, segHomeCompletedList, lblNoCompletedJobs);
    flxScheduleContainer.add(segHomeContentList, lblNoScheduledJobs, flxCompletedList);
    var flxMapContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMapContentContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxMapContentContainer.setDefaultUnit(kony.flex.DP);
    var flxMapContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": true,
        "id": "flxMapContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxHeaderBgSkin",
        "top": "0%",
        "width": "100%"
    }, {}, {});
    flxMapContainer.setDefaultUnit(kony.flex.DP);
    var mapLocation = new kony.ui.Map({
        "bottom": "0%",
        "calloutTemplate": flxMapCallouts,
        "calloutWidth": 80,
        "defaultPinImage": "pinb.png",
        "id": "mapLocation",
        "isVisible": true,
        "left": "0dp",
        "onPinClick": AS_Map_7f62b3251817427cb08c7e256dd96d52,
        "onSelection": AS_Map_a1c68f4a52b84d1d8880d5ec7112cd6c,
        "provider": constants.MAP_PROVIDER_GOOGLE,
        "top": "0%",
        "widgetDataMapForCallout": {
            "flxMapCallouts": "flxMapCallouts",
            "lblAddress": "lblAddress",
            "lblPostCode": "lblPostCode",
            "lblPropertyCode": "lblPropertyCode",
            "lblPropertyName": "lblPropertyName"
        },
        "width": "100%"
    }, {}, {
        "mode": constants.MAP_VIEW_MODE_NORMAL,
        "showZoomControl": true,
        "zoomLevel": 1
    });
    flxMapContainer.add(mapLocation);
    var flxButtonContainers = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "25dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxButtonContainers",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0px",
        "skin": "slFbox",
        "width": "250dp"
    }, {}, {});
    flxButtonContainers.setDefaultUnit(kony.flex.DP);
    var btnMyLocation = new kony.ui.Button({
        "centerX": "23%",
        "centerY": "50%",
        "focusSkin": "btnNormalSkin",
        "height": "40dp",
        "id": "btnMyLocation",
        "isVisible": true,
        "onClick": AS_Button_3ba86f90592f4e2e9451e6f5e3fa1e59,
        "skin": "btnCancelSkin",
        "text": "My Location",
        "width": "120dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnShowHere = new kony.ui.Button({
        "centerX": "77%",
        "centerY": "50%",
        "focusSkin": "btnNormalSkin",
        "height": "40dp",
        "id": "btnShowHere",
        "isVisible": true,
        "left": "72dp",
        "onClick": AS_Button_4e76765ad63e426996f48f489b0a6292,
        "skin": "btnNormalSkin",
        "text": "Show Here",
        "top": "419dp",
        "width": "120dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxButtonContainers.add(btnMyLocation, btnShowHere);
    flxMapContentContainer.add(flxMapContainer, flxButtonContainers);
    flxContentContainer.add(flxScheduleContainer, flxMapContentContainer);
    flxMainContainer.add(flxHeaderContainer, flxSALabelContainer, flxMenuContainer, flxContentContainer);
    var flxBurgerMenuContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "-50%",
        "clipBounds": true,
        "id": "flxBurgerMenuContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_d0afaa6188384f2eb8c89840cf60e162,
        "skin": "flxBurgerMenuShadowSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBurgerMenuContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerMenuContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerMenuContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "80%"
    }, {}, {});
    flxBurgerMenuContentContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerTopContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxBurgerTopContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxRedBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxBurgerTopContainer.setDefaultUnit(kony.flex.DP);
    var lblAppVersion = new kony.ui.Label({
        "id": "lblAppVersion",
        "isVisible": true,
        "right": "10dp",
        "skin": "lblLightGrayTinySkin",
        "text": "v1.0.0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgAppLogo = new kony.ui.Image2({
        "centerY": "45%",
        "height": "29dp",
        "id": "imgAppLogo",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "rentokillogo.png",
        "width": "138dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgBurgerMenu = new kony.ui.Image2({
        "height": "32dp",
        "id": "imgBurgerMenu",
        "isVisible": false,
        "left": "8%",
        "skin": "slImage",
        "src": "burgermenu.png",
        "top": "7%",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxUserDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxUserDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_a126a59110c14305ba412620d3e8e770,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUserDetailsContainer.setDefaultUnit(kony.flex.DP);
    var imgUserIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgUserIcon",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "usericon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUserEmail = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblUserEmail",
        "isVisible": true,
        "left": "20%",
        "maxNumberOfLines": 2,
        "skin": "lblWhiteNormalSkin",
        "text": "adam.mcintyre@rentokil-initial.com",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "76%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxUserDetailsContainer.add(imgUserIcon, lblUserEmail);
    flxBurgerTopContainer.add(lblAppVersion, imgAppLogo, imgBurgerMenu, flxUserDetailsContainer);
    var flxBurgerBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "32%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBurgerBottomContainer.setDefaultUnit(kony.flex.DP);
    var segMenuListOne = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "imgBtnIcon": "settings.png",
            "lblItem": "Label"
        }],
        "groupCells": false,
        "height": "225dp",
        "id": "segMenuListOne",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_553bb9e3acd84154bf440fc4c74fae29,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegMenuListTop,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxRowSegMenuListTop": "flxRowSegMenuListTop",
            "imgBtnIcon": "imgBtnIcon",
            "lblItem": "lblItem"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var segMenuListTwo = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "actionId": "",
            "imgBtnIcon": "",
            "lblItem": ""
        }],
        "groupCells": false,
        "height": "135dp",
        "id": "segMenuListTwo",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_1ee653556cec4da38edc7a78cfe92f34,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegMenuList,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "55%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxRowSegMenuList": "flxRowSegMenuList",
            "imgBtnIcon": "imgBtnIcon",
            "lblItem": "lblItem"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLastSync = new kony.ui.Label({
        "bottom": "30dp",
        "id": "lblLastSync",
        "isVisible": false,
        "left": "4%",
        "skin": "lblDarkGrayTinySkin",
        "text": "Date last transferred:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastSyncDate = new kony.ui.Label({
        "bottom": "25dp",
        "id": "lblLastSyncDate",
        "isVisible": true,
        "left": "4%",
        "skin": "lblDarkGrayTinySkin",
        "text": "11/05/2016 15:30:12",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBurgerBottomContainer.add(segMenuListOne, segMenuListTwo, lblLastSync, lblLastSyncDate);
    flxBurgerMenuContentContainer.add(flxBurgerTopContainer, flxBurgerBottomContainer);
    flxBurgerMenuContainer.add(flxBurgerMenuContentContainer);
    Home.add(flxMainContainer, flxBurgerMenuContainer);
};

function HomeGlobals() {
    Home = new kony.ui.Form2({
        "addWidgets": addWidgetsHome,
        "enabledForIdleTimeout": false,
        "id": "Home",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_a42d0a23806e4007935ab8298f044e27,
        "preShow": AS_Form_550cd32062fe4269a4cc4d9a88295286,
        "skin": "frmRedBgSkin"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_0976e06f24af482bae5c770db31147d5,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
    Home.info = {
        "kuid": "aad96a0e12c2406ca0b809d7dbc1bf46"
    };
};