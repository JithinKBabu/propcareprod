function addWidgetsLogin() {
    Login.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var imgLogo = new kony.ui.Image2({
        "bottom": "0dp",
        "id": "imgLogo",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "splash.png",
        "top": "0dp",
        "width": "100%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxAppDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "62%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxAppDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAppDetailsContainer.setDefaultUnit(kony.flex.DP);
    var lblAppName = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblAppName",
        "isVisible": true,
        "skin": "lblWhiteLargeSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAppDetailsContainer.add(lblAppName);
    var lblWelcome = new kony.ui.Label({
        "bottom": "18%",
        "centerX": "50%",
        "id": "lblWelcome",
        "isVisible": true,
        "skin": "lblWhiteNormalSkin",
        "text": "Welcome",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgUserIcon = new kony.ui.Image2({
        "bottom": "8%",
        "centerX": "50%",
        "height": "60dp",
        "id": "imgUserIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "usericon.png",
        "width": "60dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUsername = new kony.ui.Label({
        "bottom": "5%",
        "centerX": "50%",
        "id": "lblUsername",
        "isVisible": true,
        "skin": "lblWhiteSmallSkin",
        "text": "Adam McIntyre",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAppVersion = new kony.ui.Label({
        "bottom": "10dp",
        "id": "lblAppVersion",
        "isVisible": true,
        "right": "10dp",
        "skin": "lblLightGrayTinySkin",
        "text": "v1.0.0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMainContainer.add(imgLogo, flxAppDetailsContainer, lblWelcome, imgUserIcon, lblUsername, lblAppVersion);
    Login.add(flxMainContainer);
};

function LoginGlobals() {
    Login = new kony.ui.Form2({
        "addWidgets": addWidgetsLogin,
        "enabledForIdleTimeout": false,
        "id": "Login",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_896471a353a44dafb06e58a669feffc1,
        "preShow": AS_Form_efd395230fb342e0b2c8697b39f6f6a1,
        "skin": "frmRedBgSkin"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_c9db835807c14d8ba6ff23e67b7335c1,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
    Login.info = {
        "kuid": "1888d1cfe469424c9af00b85ca273810"
    };
};