function initializePCMapCallout() {
    flxMapCallouts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "120dp",
        "id": "flxMapCallouts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin",
        "width": "75%"
    }, {}, {});
    flxMapCallouts.setDefaultUnit(kony.flex.DP);
    var lblPropertyName = new kony.ui.Label({
        "id": "lblPropertyName",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlk140",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAddress = new kony.ui.Label({
        "id": "lblAddress",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlk140",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPostCode = new kony.ui.Label({
        "id": "lblPostCode",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlk140",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPropertyCode = new kony.ui.Label({
        "id": "lblPropertyCode",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblBlk140",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "80dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMapCallouts.add(lblPropertyName, lblAddress, lblPostCode, lblPropertyCode);
}