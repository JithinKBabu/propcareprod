function addWidgetsPopupSettings() {
    var hbxHeaderContainer = new kony.ui.Box({
        "id": "hbxHeaderContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbxGrayBgSkin"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [2, 1, 2, 1],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var lblSettingsTitle = new kony.ui.Label({
        "id": "lblSettingsTitle",
        "isVisible": true,
        "skin": "lblWhiteNormalBoldSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        }
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "hExpand": true,
        "margin": [4, 1, 4, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    hbxHeaderContainer.add(lblSettingsTitle);
    var hbxSettingsContentContainer = new kony.ui.Box({
        "id": "hbxSettingsContentContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slHbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var vbxSettingsContentContainer = new kony.ui.Box({
        "id": "vbxSettingsContentContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slVbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [4, 1, 4, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var segSettingsList = new kony.ui.SegmentedUI2({
        "data": [
            [{
                    "lblTitle": "Label"
                },
                [{
                    "imgRadioBtnIcon": "imagedrag.png",
                    "lblItem": "Label"
                }, {
                    "imgRadioBtnIcon": "imagedrag.png",
                    "lblItem": "Label"
                }, {
                    "imgRadioBtnIcon": "imagedrag.png",
                    "lblItem": "Label"
                }]
            ],
            [{
                    "lblTitle": "Label"
                },
                [{
                    "imgRadioBtnIcon": "imagedrag.png",
                    "lblItem": "Label"
                }, {
                    "imgRadioBtnIcon": "imagedrag.png",
                    "lblItem": "Label"
                }, {
                    "imgRadioBtnIcon": "imagedrag.png",
                    "lblItem": "Label"
                }]
            ]
        ],
        "groupCells": false,
        "id": "segSettingsList",
        "isVisible": true,
        "needPageIndicator": true,
        "onRowClick": AS_Segment_70265e8c58c9456ca57a921a28e72082,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": hbxRowSegSelectionList,
        "screenLevelWidget": false,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "sectionHeaderTemplate": hbxHeaderSegSelectionList,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "hbxHeaderSegSelectionList": "hbxHeaderSegSelectionList",
            "hbxRowSegSelectionList": "hbxRowSegSelectionList",
            "imgRadioBtnIcon": "imgRadioBtnIcon",
            "lblItem": "lblItem",
            "lblTitle": "lblTitle"
        }
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    vbxSettingsContentContainer.add(segSettingsList);
    hbxSettingsContentContainer.add(vbxSettingsContentContainer);
    var hbxSettingsContentContainerAlt = new kony.ui.Box({
        "id": "hbxSettingsContentContainerAlt",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slHbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var vbxSettingsContentContainerAlt = new kony.ui.Box({
        "id": "vbxSettingsContentContainerAlt",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slVbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [4, 1, 4, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var lblSource = new kony.ui.Label({
        "id": "lblSource",
        "isVisible": true,
        "skin": "lblGrayNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        }
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [0, 4, 0, 2],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var lbxSourceList = new kony.ui.ListBox({
        "id": "lbxSourceList",
        "isVisible": true,
        "masterData": [
            ["lb1", "Listbox One"],
            ["lb2", "Listbox Two"],
            ["lb3", "Listbox Three"]
        ],
        "skin": "lbxNormalSkin"
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var lblLanguage = new kony.ui.Label({
        "id": "lblLanguage",
        "isVisible": true,
        "skin": "lblGrayNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        }
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [0, 6, 0, 2],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var lbxLanguageList = new kony.ui.ListBox({
        "id": "lbxLanguageList",
        "isVisible": true,
        "masterData": [
            ["lb1", "Listbox One"],
            ["lb2", "Listbox Two"],
            ["lb3", "Listbox Three"]
        ],
        "skin": "lbxNormalSkin"
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    vbxSettingsContentContainerAlt.add(lblSource, lbxSourceList, lblLanguage, lbxLanguageList);
    hbxSettingsContentContainerAlt.add(vbxSettingsContentContainerAlt);
    var hbxFooterContainer = new kony.ui.Box({
        "id": "hbxFooterContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slHbox"
    }, {
        "containerWeight": 100,
        "layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_CENTER,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 2, 0, 2],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    var vbxLeft = new kony.ui.Box({
        "id": "vbxLeft",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slVbox"
    }, {
        "containerWeight": 48,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [4, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    var btnCancelSettings = new kony.ui.Button({
        "focusSkin": "btnCancelSkin",
        "id": "btnCancelSettings",
        "isVisible": true,
        "onClick": AS_Button_2d4a3854ff9e4d22854249998d5b4b79,
        "skin": "btnCancelSkin",
        "text": "Button"
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [4, 4, 4, 4],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    vbxLeft.add(btnCancelSettings);
    var vbxCenter = new kony.ui.Box({
        "id": "vbxCenter",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slVbox"
    }, {
        "containerWeight": 4,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 10, 0, 10],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    vbxCenter.add();
    var vbxRight = new kony.ui.Box({
        "id": "vbxRight",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slVbox"
    }, {
        "containerWeight": 48,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    var btnConfirmSettings = new kony.ui.Button({
        "focusSkin": "btnNormalSkin",
        "id": "btnConfirmSettings",
        "isVisible": true,
        "onClick": AS_Button_c2e14aeedb8e4cffa571cbf6c588b3b1,
        "skin": "btnNormalSkin",
        "text": "Button"
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [4, 4, 4, 4],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    vbxRight.add(btnConfirmSettings);
    hbxFooterContainer.add(vbxLeft, vbxCenter, vbxRight);
    PopupSettings.add(hbxHeaderContainer, hbxSettingsContentContainer, hbxSettingsContentContainerAlt, hbxFooterContainer);
};

function PopupSettingsGlobals() {
    PopupSettings = new kony.ui.Popup({
        "addWidgets": addWidgetsPopupSettings,
        "id": "PopupSettings",
        "isModal": true,
        "skin": "popupWhiteBgSkin",
        "transparencyBehindThePopup": 60
    }, {
        "containerWeight": 80,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "windowSoftInputMode": constants.POPUP_ADJUST_PAN
    });
    PopupSettings.info = {
        "kuid": "6f8e76f95ba444728bd13be148f9bae0"
    };
};