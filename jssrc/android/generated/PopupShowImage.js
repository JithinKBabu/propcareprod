function addWidgetsPopupShowImage() {
    var hbxImageContainer = new kony.ui.Box({
        "id": "hbxImageContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slHbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [2, 2, 2, 2],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var imgPhoto = new kony.ui.Image2({
        "id": "imgPhoto",
        "isVisible": true,
        "skin": "slImage",
        "src": "imagedrag.png"
    }, {
        "containerWeight": 100,
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    hbxImageContainer.add(imgPhoto);
    PopupShowImage.add(hbxImageContainer);
};

function PopupShowImageGlobals() {
    PopupShowImage = new kony.ui.Popup({
        "addWidgets": addWidgetsPopupShowImage,
        "id": "PopupShowImage",
        "isModal": false,
        "skin": "popupWhiteBgSkin",
        "transparencyBehindThePopup": 60
    }, {
        "containerWeight": 80,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "windowSoftInputMode": constants.POPUP_ADJUST_PAN
    });
    PopupShowImage.info = {
        "kuid": "f28eb6ebf0a04c868db1da71f0bf66f1"
    };
};