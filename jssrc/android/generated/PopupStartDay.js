function addWidgetsPopupStartDay() {
    var hbxHeaderContainer = new kony.ui.Box({
        "id": "hbxHeaderContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbxGrayBgSkin"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [2, 1, 2, 1],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var lblTitle = new kony.ui.Label({
        "id": "lblTitle",
        "isVisible": true,
        "skin": "lblWhiteNormalBoldSkin",
        "text": "Start / End Day",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        }
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "hExpand": true,
        "margin": [4, 1, 4, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    hbxHeaderContainer.add(lblTitle);
    var hbxStartDayContainer = new kony.ui.Box({
        "id": "hbxStartDayContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slHbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var btnStartDay = new kony.ui.Button({
        "focusSkin": "btnNormalSkin",
        "id": "btnStartDay",
        "isVisible": true,
        "onClick": AS_Button_18b4d78ea2a14dad94422bb09d049835,
        "skin": "btnNormalSkin",
        "text": "Start Day"
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "hExpand": true,
        "margin": [20, 6, 20, 3],
        "marginInPixel": false,
        "padding": [4, 3, 4, 3],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    hbxStartDayContainer.add(btnStartDay);
    var hbxEndDayContainer = new kony.ui.Box({
        "id": "hbxEndDayContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slHbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var btnEndDay = new kony.ui.Button({
        "focusSkin": "btnDisabledSkin",
        "id": "btnEndDay",
        "isVisible": true,
        "onClick": AS_Button_1cd73dc11c3646498d5c40155611ee69,
        "skin": "btnDisabledSkin",
        "text": "End Day"
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "hExpand": true,
        "margin": [20, 3, 20, 6],
        "marginInPixel": false,
        "padding": [4, 3, 4, 3],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    hbxEndDayContainer.add(btnEndDay);
    PopupStartDay.add(hbxHeaderContainer, hbxStartDayContainer, hbxEndDayContainer);
};

function PopupStartDayGlobals() {
    PopupStartDay = new kony.ui.Popup({
        "addWidgets": addWidgetsPopupStartDay,
        "id": "PopupStartDay",
        "isModal": false,
        "skin": "popupWhiteBgSkin",
        "transparencyBehindThePopup": 60
    }, {
        "containerWeight": 80,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "windowSoftInputMode": constants.POPUP_ADJUST_PAN
    });
    PopupStartDay.info = {
        "kuid": "7ff67e4b15a8492bb6e2a27affe17438"
    };
};