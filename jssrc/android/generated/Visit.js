function addWidgetsVisit() {
    Visit.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainerVisit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxHeaderContainerVisit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxHeaderBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxHeaderContainerVisit.setDefaultUnit(kony.flex.DP);
    var imgBurgerIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "30dp",
        "id": "imgBurgerIcon",
        "isVisible": true,
        "left": "12dp",
        "skin": "slImage",
        "src": "burgermenu.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBurgerMenu = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "40dp",
        "id": "btnBurgerMenu",
        "isVisible": true,
        "left": "7dp",
        "onClick": AS_NamedActions_0ee594e17b72488ba950418f5daec2ad,
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblHeaderVisit = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderVisit",
        "isVisible": true,
        "left": "54dp",
        "maxNumberOfLines": 1,
        "skin": "lblWhiteLargeSkin",
        "text": "Visit Details",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_NONE,
        "width": "53%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgStartVisit = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgStartVisit",
        "isVisible": true,
        "right": "64dp",
        "skin": "slImage",
        "src": "startvisit.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnStartVisit = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "50dp",
        "id": "btnStartVisit",
        "isVisible": true,
        "onClick": AS_NamedActions_98246b17def84ae4a557741194b3ce3f,
        "right": "59dp",
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStartdayIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgStartdayIcon",
        "isVisible": true,
        "right": "12dp",
        "skin": "slImage",
        "src": "startday.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnStartday = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "50dp",
        "id": "btnStartday",
        "isVisible": true,
        "onClick": AS_NamedActions_98246b17def84ae4a557741194b3ce3f,
        "right": "7dp",
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderContainerVisit.add(imgBurgerIcon, btnBurgerMenu, lblHeaderVisit, imgStartVisit, btnStartVisit, imgStartdayIcon, btnStartday);
    var flxPropertyNameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxPropertyNameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknHeaderPropertyTitleBgSkin",
        "top": "55dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    flxPropertyNameContainer.setDefaultUnit(kony.flex.DP);
    var lblPropertyName = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPropertyName",
        "isVisible": true,
        "left": "3%",
        "skin": "lblWhiteNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPropertyNameContainer.add(lblPropertyName);
    var flxMainContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "85dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    flxMainContentContainer.setDefaultUnit(kony.flex.DP);
    var flxTabContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "75dp",
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxTabContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTabContainer.setDefaultUnit(kony.flex.DP);
    var TabPaneVisits = new kony.ui.TabPane({
        "activeFocusSkin": "sknTabPaneRedInactive",
        "activeSkin": "sknTabPaneRedInactive",
        "activeTabs": [1],
        "bottom": "0dp",
        "centerX": "50%",
        "id": "TabPaneVisits",
        "inactiveSkin": "sknTabPaneRedBg",
        "isVisible": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "onTabClick": AS_TabPane_f618d0fc025148dfa80b0aeed20ccae8,
        "top": "0dp",
        "viewConfig": {
            "collapsibleViewConfig": {
                "collapsedImage": "icon_plus_big.png",
                "collapsedimage": "icon_plus_big.png",
                "expandedImage": "icon_minus_big.png",
                "expandedimage": "icon_minus_big.png",
                "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                "imageposition": "right",
                "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                "tabnamealignment": "left",
                "toggleTabs": true,
                "toggletabs": true
            },
            "collapsibleviewconfig": {
                "collapsedImage": "icon_plus_big.png",
                "collapsedimage": "icon_plus_big.png",
                "expandedImage": "icon_minus_big.png",
                "expandedimage": "icon_minus_big.png",
                "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                "imageposition": "right",
                "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                "tabnamealignment": "left",
                "toggleTabs": true,
                "toggletabs": true
            },
            "pageViewConfig": {
                "needPageIndicator": true
            },
            "tabViewConfig": {
                "headerPosition": constants.TAB_HEADER_POSITION_TOP,
                "image1": "tableftarrow.png",
                "image2": "tabrightarrow.png"
            },
        },
        "viewType": constants.TABPANE_VIEW_TYPE_COLLAPSIBLEVIEW,
        "width": "96%",
        "zIndex": 1
    }, {
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "tabHeaderHeight": 200
    });
    var TabPreparations = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "230dp",
        "id": "TabPreparations",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Preparations",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    TabPreparations.setDefaultUnit(kony.flex.DP);
    var flxPreparationsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "230dp",
        "id": "flxPreparationsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPreparationsContainer.setDefaultUnit(kony.flex.DP);
    var lblPreprationHead = new kony.ui.Label({
        "id": "lblPreprationHead",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 1,
        "skin": "lblBlackNormalBoldSkin",
        "text": "Preparations",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "10dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblQuatity = new kony.ui.Label({
        "id": "lblQuatity",
        "isVisible": true,
        "left": "71%",
        "skin": "lblBlackNormalBoldSkin",
        "text": "Quantity",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var PreprationsCmb1 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb1",
        "isVisible": true,
        "left": "10dp",
        "onSelection": AS_ListBox_1cf2a732e5364ff0a41633463aaeae06,
        "skin": "sknListBoxNormal",
        "top": "35dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var PreprationsCmb2 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb2",
        "isVisible": true,
        "left": "10dp",
        "onSelection": AS_ListBox_1cf2a732e5364ff0a41633463aaeae06,
        "skin": "sknListBoxNormal",
        "top": "73dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var PreprationsCmb3 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb3",
        "isVisible": true,
        "left": "10dp",
        "onSelection": AS_ListBox_1cf2a732e5364ff0a41633463aaeae06,
        "skin": "sknListBoxNormal",
        "top": "111dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var PreprationsCmb4 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb4",
        "isVisible": true,
        "left": "10dp",
        "onSelection": AS_ListBox_1cf2a732e5364ff0a41633463aaeae06,
        "skin": "sknListBoxNormal",
        "top": "149dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var PreprationsCmb5 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "PreprationsCmb5",
        "isVisible": true,
        "left": "10dp",
        "onSelection": AS_ListBox_1cf2a732e5364ff0a41633463aaeae06,
        "skin": "sknListBoxNormal",
        "top": "186dp",
        "width": "65%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb1 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb1",
        "isVisible": true,
        "left": "71%",
        "onSelection": AS_ListBox_fdfc9b06a4aa4256b76e5f476ab218bc,
        "skin": "sknListBoxNormal",
        "top": "35dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb2 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb2",
        "isVisible": true,
        "left": "71%",
        "onSelection": AS_ListBox_fdfc9b06a4aa4256b76e5f476ab218bc,
        "skin": "sknListBoxNormal",
        "top": "73dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb3 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb3",
        "isVisible": true,
        "left": "71%",
        "onSelection": AS_ListBox_fdfc9b06a4aa4256b76e5f476ab218bc,
        "skin": "sknListBoxNormal",
        "top": "111dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb4 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb4",
        "isVisible": true,
        "left": "71.03%",
        "onSelection": AS_ListBox_fdfc9b06a4aa4256b76e5f476ab218bc,
        "skin": "sknListBoxNormal",
        "top": "149dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var quatityCmb5 = new kony.ui.ListBox({
        "height": "30dp",
        "id": "quatityCmb5",
        "isVisible": true,
        "left": "71%",
        "onSelection": AS_ListBox_fdfc9b06a4aa4256b76e5f476ab218bc,
        "skin": "sknListBoxNormal",
        "top": "186dp",
        "width": "26%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    flxPreparationsContainer.add(lblPreprationHead, lblQuatity, PreprationsCmb1, PreprationsCmb2, PreprationsCmb3, PreprationsCmb4, PreprationsCmb5, quatityCmb1, quatityCmb2, quatityCmb3, quatityCmb4, quatityCmb5);
    TabPreparations.add(flxPreparationsContainer);
    TabPaneVisits.addTab("TabPreparations", "  Preparations", null, TabPreparations, null);
    var TabVNotes = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "320dp",
        "id": "TabVNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Visit Notes",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    TabVNotes.setDefaultUnit(kony.flex.DP);
    var flexVnotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flexVnotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexVnotes.setDefaultUnit(kony.flex.DP);
    var lblVisitNotePlaceholder = new kony.ui.Label({
        "height": "30dp",
        "id": "lblVisitNotePlaceholder",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "lblGrayPlaceHolderSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtAreaVisitNote = new kony.ui.TextArea2({
        "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_SENTENCES,
        "height": "120dp",
        "id": "txtAreaVisitNote",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
        "left": "10dp",
        "numberOfVisibleLines": 3,
        "onDone": AS_TextField_e9049a86f7d8494b8cd3d9e6b716084c,
        "onTextChange": AS_TextField_b15e86ae0cba4e26905bab47ad0d0195,
        "right": "10dp",
        "skin": "sknTxtAreaVNote",
        "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "94%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [2, 2, 2, 2],
        "paddingInPixel": false
    }, {
        "onBeginEditing": AS_TextArea_e2a7ddc9cf304672958582379b15a160,
        "onEndEditing": AS_TextArea_7410979fe42d48cc96933a3e00c12a68
    });
    var flxCapturedImgContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "clipBounds": true,
        "height": "170dp",
        "id": "flxCapturedImgContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "onClick": AS_FlexContainer_c72baef0520b48acb563d5d6fe52fe52,
        "right": "10dp",
        "skin": "flxWhiteBgWithBorderSkin",
        "top": "140dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxCapturedImgContainer.setDefaultUnit(kony.flex.DP);
    var imgCapturedPhoto = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "96%",
        "id": "imgCapturedPhoto",
        "isVisible": true,
        "skin": "slImage",
        "src": "noimage.png",
        "width": "98%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgVisitPhoto = new kony.ui.Image2({
        "centerX": "91%",
        "centerY": "17%",
        "height": "40dp",
        "id": "imgVisitPhoto",
        "isVisible": true,
        "skin": "slImage",
        "src": "camera.png",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var camVisitPhoto = new kony.ui.Camera({
        "centerX": "91%",
        "centerY": "17%",
        "compressionLevel": 40,
        "height": "60dp",
        "id": "camVisitPhoto",
        "isVisible": true,
        "onCapture": AS_Camera_70895fda3c0b4f1ba48a169561c59b31,
        "scaleFactor": 90,
        "skin": "slCamera",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_PUBLIC,
        "enableOverlay": false,
        "enablePhotoCropFeature": false
    });
    var imgDeleteIcon = new kony.ui.Image2({
        "centerX": "91%",
        "centerY": "82%",
        "height": "40dp",
        "id": "imgDeleteIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "delete.png",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnDeletePhoto = new kony.ui.Button({
        "centerX": "91%",
        "centerY": "82%",
        "focusSkin": "btnTransparentSkin",
        "height": "60dp",
        "id": "btnDeletePhoto",
        "isVisible": true,
        "onClick": AS_Button_809716dce97540f181bccd06ad268eb5,
        "skin": "btnTransparentSkin",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCapturedImgContainer.add(imgCapturedPhoto, imgVisitPhoto, camVisitPhoto, imgDeleteIcon, btnDeletePhoto);
    flexVnotes.add(lblVisitNotePlaceholder, txtAreaVisitNote, flxCapturedImgContainer);
    TabVNotes.add(flexVnotes);
    TabPaneVisits.addTab("TabVNotes", "  Visit Notes", null, TabVNotes, null);
    var TabVSignature = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "250dp",
        "id": "TabVSignature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Signature",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    TabVSignature.setDefaultUnit(kony.flex.DP);
    var FlexmainSignature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "250dp",
        "id": "FlexmainSignature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "100.00%",
        "zIndex": 1
    }, {}, {});
    FlexmainSignature.setDefaultUnit(kony.flex.DP);
    var lblCustomerNamePlaceholder = new kony.ui.Label({
        "height": "30dp",
        "id": "lblCustomerNamePlaceholder",
        "isVisible": true,
        "left": "10dp",
        "skin": "lblGrayPlaceHolderSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtCustomerNameSig = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
        "height": "30dp",
        "id": "txtCustomerNameSig",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "10dp",
        "onDone": AS_TextField_e9049a86f7d8494b8cd3d9e6b716084c,
        "onTextChange": AS_TextField_b15e86ae0cba4e26905bab47ad0d0195,
        "right": "10dp",
        "secureTextEntry": false,
        "skin": "txtNormalSkin",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 2, 1],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "onBeginEditing": AS_TextField_19e74d6da7e94b10bdb0e8e7ae3145d1,
        "onEndEditing": AS_TextField_98cb2c75a3c84e828c6cc0d862ecc3bd,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var FlexSignhere = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "140dp",
        "id": "FlexSignhere",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "onTouchStart": AS_FlexContainer_23e73f624a724c09bde5dde56567bbdb,
        "right": "10dp",
        "skin": "flxWhiteBgWithBorderSkin",
        "top": "50dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    FlexSignhere.setDefaultUnit(kony.flex.DP);
    var lblSignHere = new kony.ui.Label({
        "centerX": "50.03%",
        "centerY": "50.00%",
        "height": "28dp",
        "id": "lblSignHere",
        "isVisible": true,
        "skin": "lblGrayLargeSkin",
        "text": "Sign here",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgSignature = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "40%",
        "height": "100dp",
        "id": "imgSignature",
        "isVisible": false,
        "skin": "slImage",
        "src": "imagedrag.png",
        "width": "300dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSignDate = new kony.ui.Label({
        "bottom": "3dp",
        "centerX": "50%",
        "height": "28dp",
        "id": "lblSignDate",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "lblGrayPlaceHolderSkin",
        "text": "15/6/2016",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexSignhere.add(lblSignHere, imgSignature, lblSignDate);
    var signatureReasoncmb = new kony.ui.ListBox({
        "height": "40dp",
        "id": "signatureReasoncmb",
        "isVisible": true,
        "left": "10dp",
        "masterData": [
            ["lb1", "No Signature Reason"],
            ["lb2", "Customer not available"],
            ["lb3", "Customer refused to sign"]
        ],
        "onSelection": AS_ListBox_453f4c589eb64b889885566208504ec5,
        "right": "10dp",
        "selectedKey": "lb1",
        "selectedKeyValue": ["lb1", "No Signature Reason"],
        "skin": "sknListBoxNormal",
        "top": "200dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var FlexClearSignature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "48dp",
        "id": "FlexClearSignature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_32c515bed78a4e37bda4b834bcad281c,
        "right": "11dp",
        "skin": "slFbox",
        "top": 140,
        "width": "17.45%",
        "zIndex": 4
    }, {}, {});
    FlexClearSignature.setDefaultUnit(kony.flex.DP);
    var imgClearSign = new kony.ui.Image2({
        "centerX": "52%",
        "centerY": "50%",
        "height": "76.39%",
        "id": "imgClearSign",
        "isVisible": true,
        "left": "0dp",
        "right": 0,
        "skin": "slImage",
        "src": "discardvisit.png",
        "top": 10,
        "width": "84.51%",
        "zIndex": 3
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexClearSignature.add(imgClearSign);
    FlexmainSignature.add(lblCustomerNamePlaceholder, txtCustomerNameSig, FlexSignhere, signatureReasoncmb, FlexClearSignature);
    TabVSignature.add(FlexmainSignature);
    TabPaneVisits.addTab("TabVSignature", "  Signature", null, TabVSignature, null);
    var TabVTime = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100dp",
        "id": "TabVTime",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "sknTabRedBg",
        "tabName": "  Time",
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    TabVTime.setDefaultUnit(kony.flex.DP);
    var flxTimeContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxTimeContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxTimeContainer.setDefaultUnit(kony.flex.DP);
    var lblVisitStartTime = new kony.ui.Label({
        "id": "lblVisitStartTime",
        "isVisible": true,
        "left": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "Start Time:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStartTimeValue = new kony.ui.Label({
        "id": "lblStartTimeValue",
        "isVisible": true,
        "left": "50%",
        "right": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "2:00",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "46%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEndTime = new kony.ui.Label({
        "id": "lblEndTime",
        "isVisible": true,
        "left": "50%",
        "right": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "2:00",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "39dp",
        "width": "46%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblVisitEndTime = new kony.ui.Label({
        "id": "lblVisitEndTime",
        "isVisible": true,
        "left": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "End Time  :",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "39dp",
        "width": "45%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDuration = new kony.ui.Label({
        "id": "lblDuration",
        "isVisible": true,
        "left": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "Duration      :",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDurationValue = new kony.ui.Label({
        "id": "lblDurationValue",
        "isVisible": true,
        "left": "50%",
        "right": "10dp",
        "skin": "lblBlackNormalBoldSkin",
        "text": "2:00",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70dp",
        "width": "46%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTimeContainer.add(lblVisitStartTime, lblStartTimeValue, lblEndTime, lblVisitEndTime, lblDuration, lblDurationValue);
    TabVTime.add(flxTimeContainer);
    TabPaneVisits.addTab("TabVTime", "  Time", null, TabVTime, null);
    flxTabContainer.add(TabPaneVisits);
    var flxStopVisitBtnContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxStopVisitBtnContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_102b8999f53c46f196aa0758504d8956,
        "skin": "slFbox",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    flxStopVisitBtnContainer.setDefaultUnit(kony.flex.DP);
    var ImageSaveVisit = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50.00%",
        "height": "60dp",
        "id": "ImageSaveVisit",
        "isVisible": true,
        "skin": "slImage",
        "src": "endvisit.png",
        "width": "60dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStopVisitBtnContainer.add(ImageSaveVisit);
    flxMainContentContainer.add(flxTabContainer, flxStopVisitBtnContainer);
    flxMainContainer.add(flxHeaderContainerVisit, flxPropertyNameContainer, flxMainContentContainer);
    var flxBurgerMenuContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "-50%",
        "clipBounds": true,
        "id": "flxBurgerMenuContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_d0afaa6188384f2eb8c89840cf60e162,
        "skin": "flxBurgerMenuShadowSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBurgerMenuContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerMenuContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerMenuContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "80%"
    }, {}, {});
    flxBurgerMenuContentContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerTopContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxBurgerTopContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxRedBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxBurgerTopContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerMenu = new kony.ui.Image2({
        "height": "32dp",
        "id": "imgBurgerMenu",
        "isVisible": false,
        "left": "8%",
        "skin": "slImage",
        "src": "burgermenu.png",
        "top": "7%",
        "width": "32dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAppVersion = new kony.ui.Label({
        "id": "lblAppVersion",
        "isVisible": true,
        "right": "10dp",
        "skin": "lblLightGrayTinySkin",
        "text": "v1.0.0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgAppLogo = new kony.ui.Image2({
        "centerY": "45%",
        "height": "29dp",
        "id": "imgAppLogo",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "rentokillogo.png",
        "width": "138dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxUserDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxUserDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_a126a59110c14305ba412620d3e8e770,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUserDetailsContainer.setDefaultUnit(kony.flex.DP);
    var imgUserIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgUserIcon",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "usericon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUserEmail = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblUserEmail",
        "isVisible": true,
        "left": "20%",
        "maxNumberOfLines": 2,
        "skin": "lblWhiteNormalSkin",
        "text": "adam.mcintyre@rentokil-initial.com",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "76%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxUserDetailsContainer.add(imgUserIcon, lblUserEmail);
    flxBurgerTopContainer.add(imgBurgerMenu, lblAppVersion, imgAppLogo, flxUserDetailsContainer);
    var flxBurgerBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "32%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBurgerBottomContainer.setDefaultUnit(kony.flex.DP);
    var segMenuListOne = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "imgBtnIcon": "settings.png",
            "lblItem": "Label"
        }],
        "groupCells": false,
        "height": "225dp",
        "id": "segMenuListOne",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_553bb9e3acd84154bf440fc4c74fae29,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegMenuListTop,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxRowSegMenuListTop": "flxRowSegMenuListTop",
            "imgBtnIcon": "imgBtnIcon",
            "lblItem": "lblItem"
        },
        "width": "100%"
    }, {
        "padding": [0, 1, 0, 0],
        "paddingInPixel": false
    }, {});
    var segMenuListTwo = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "actionId": "",
            "imgBtnIcon": "",
            "lblItem": ""
        }],
        "groupCells": false,
        "height": "90dp",
        "id": "segMenuListTwo",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_1ee653556cec4da38edc7a78cfe92f34,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSegMenuList,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "60%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxRowSegMenuList": "flxRowSegMenuList",
            "imgBtnIcon": "imgBtnIcon",
            "lblItem": "lblItem"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLastSync = new kony.ui.Label({
        "bottom": "30dp",
        "id": "lblLastSync",
        "isVisible": false,
        "left": "4%",
        "skin": "lblDarkGrayTinySkin",
        "text": "Date last transferred:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastSyncDate = new kony.ui.Label({
        "bottom": "25dp",
        "id": "lblLastSyncDate",
        "isVisible": true,
        "left": "4%",
        "skin": "lblDarkGrayTinySkin",
        "text": "11/05/2016 15:30:12",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBurgerBottomContainer.add(segMenuListOne, segMenuListTwo, lblLastSync, lblLastSyncDate);
    flxBurgerMenuContentContainer.add(flxBurgerTopContainer, flxBurgerBottomContainer);
    flxBurgerMenuContainer.add(flxBurgerMenuContentContainer);
    Visit.add(flxMainContainer, flxBurgerMenuContainer);
};

function VisitGlobals() {
    Visit = new kony.ui.Form2({
        "addWidgets": addWidgetsVisit,
        "enabledForIdleTimeout": false,
        "id": "Visit",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_c76072e215854b4d8c4dc11fc2493b88,
        "preShow": AS_Form_a4992b0cd14141e18ae535acd38a2c07,
        "skin": "slForm"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_3bfbb233c2514b61a17a82c5e605f34c,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_RESIZE
    });
    Visit.info = {
        "kuid": "b573400090f040c79575f4925f1f14e3"
    };
};