//actions.js file 
function ActionApplicationBecomesForeground() {
    return AS_NamedActions_d2dcc7b6f7154e289d55caa55211d1e1();
}

function AS_NamedActions_d2dcc7b6f7154e289d55caa55211d1e1() {
    return onAppBecomesForeground.call(this);
}

function ActionApplicationWentBackground() {
    return AS_NamedActions_a11425c5727a4eda9ebaa3584a764683();
}

function AS_NamedActions_a11425c5727a4eda9ebaa3584a764683() {
    return onAppWentBackground.call(this);
}

function ActionBurgerMenuClick() {
    return AS_NamedActions_0ee594e17b72488ba950418f5daec2ad();
}

function AS_NamedActions_0ee594e17b72488ba950418f5daec2ad() {
    return onClickOpenMenuButton.call(this);
}

function ActionCallDurationCalculator(eventobject, changedtext) {
    return AS_TextField_21d24d7d26e54519a01ba220fb1303aa(eventobject, changedtext);
}

function AS_TextField_21d24d7d26e54519a01ba220fb1303aa(eventobject, changedtext) {
    return durationCalculator.call(this);
}

function ActionCallSignatureFFI(eventobject, x, y) {
    return AS_FlexContainer_23e73f624a724c09bde5dde56567bbdb(eventobject, x, y);
}

function AS_FlexContainer_23e73f624a724c09bde5dde56567bbdb(eventobject, x, y) {
    return onClickVisitSign.call(this);
}

function ActionCancelConfirmEndVisit() {
    return AS_NamedActions_5bca2952d1fa47f0bd89624fb64f8ea6();
}

function AS_NamedActions_5bca2952d1fa47f0bd89624fb64f8ea6() {
    return cancelEndVisit.call(this);
}

function ActionCancelDiscardVisit() {
    return AS_NamedActions_c156ee6e209b407ba8e1399bae2a9aa7();
}

function AS_NamedActions_c156ee6e209b407ba8e1399bae2a9aa7() {
    return cancelDiscardVisit.call(this);
}

function ActionConfirmChangeEmail() {
    return AS_NamedActions_b4b7c0b7f0f7410390ad9a958ec1e8df();
}

function AS_NamedActions_b4b7c0b7f0f7410390ad9a958ec1e8df() {
    return confirmChangeEmail.call(this);
}

function ActionConfirmEndVisit() {
    return AS_NamedActions_b6bd76881c73404d843141aca4af9ca9();
}

function AS_NamedActions_b6bd76881c73404d843141aca4af9ca9() {
    return doEndVisit.call(this);
}

function ActionConfirmExit() {
    return AS_NamedActions_a0c776d9861f48058d56f1d1b1faf9cd();
}

function AS_NamedActions_a0c776d9861f48058d56f1d1b1faf9cd() {
    return confirmExit.call(this);
}

function ActionConfirmPhotoDeletion() {
    return AS_NamedActions_8638cf89cc7b484cab86d8ef4c8dda23();
}

function AS_NamedActions_8638cf89cc7b484cab86d8ef4c8dda23() {
    return confirmedDeletePhoto.call(this);
}

function ActionConfirmPopupNo() {
    return AS_NamedActions_124aab083d234aaabd0e839d0c335e34();
}

function AS_NamedActions_124aab083d234aaabd0e839d0c335e34() {
    return dismissConfirmPopup.call(this);
}

function ActionConfirmPopupNoBtn() {
    return AS_NamedActions_2a9e9fb4cb65409d9f6f420ff1f74fdc();
}

function AS_NamedActions_2a9e9fb4cb65409d9f6f420ff1f74fdc() {
    return onClickConfirmPopupNoBtn.call(this);
}

function ActionConfirmPopupYesBtn() {
    return AS_NamedActions_e0cc2aea4dde421cb715c8e46cbbcd1a();
}

function AS_NamedActions_e0cc2aea4dde421cb715c8e46cbbcd1a() {
    return onClickConfirmPopupYesBtn.call(this);
}

function ActionCustomerEmail(eventobject, x, y) {
    return AS_Label_ef122fd6446143538ecd820c97ee7f02(eventobject, x, y);
}

function AS_Label_ef122fd6446143538ecd820c97ee7f02(eventobject, x, y) {
    return onClickCustomerEmail.call(this);
}

function ActionCustomerGPS(eventobject, x, y) {
    return AS_Image_b257e883c2b94cd6993904636aaad026(eventobject, x, y);
}

function AS_Image_b257e883c2b94cd6993904636aaad026(eventobject, x, y) {
    return onClickCustomerLoc.call(this);
}

function ActionCustomerPostShow(eventobject) {
    return AS_Form_2a97f627436c43deb579adfcb73c9d04(eventobject);
}

function AS_Form_2a97f627436c43deb579adfcb73c9d04(eventobject) {
    return onPostShowCustomerScreen.call(this);
}

function ActionCustomerPreShow(eventobject) {
    return AS_Form_1be9783649f14cac84c157c4e9295bbe(eventobject);
}

function AS_Form_1be9783649f14cac84c157c4e9295bbe(eventobject) {
    return onPreShowCustomerScreen.call(this);
}

function ActionCustomerSRATextAreaBeginEditing(eventobject) {
    return AS_TextArea_b2e87630af274c3a844f26ed179f885b(eventobject);
}

function AS_TextArea_b2e87630af274c3a844f26ed179f885b(eventobject) {
    return onTextboxBeginEditing.call(this, eventobject);
}

function ActionCustomerSRATextAreaDoneEditing(eventobject) {
    return AS_TextArea_6cd6d51b579e40449ae00c499700158e(eventobject);
}

function AS_TextArea_6cd6d51b579e40449ae00c499700158e(eventobject) {
    return onTextboxEndEditing.call(this, eventobject);
}

function ActionCustomerSRATextAreaEndEditing(eventobject) {
    return AS_TextArea_a73ad3460ce6487489c2a43aa9c1906f(eventobject);
}

function AS_TextArea_a73ad3460ce6487489c2a43aa9c1906f(eventobject) {
    return onTextboxEndEditing.call(this, eventobject);
}

function ActionDeclineChangeEmail() {
    return AS_NamedActions_fb7edb6acb714ed8a00e61bbbb1b0b9e();
}

function AS_NamedActions_fb7edb6acb714ed8a00e61bbbb1b0b9e() {
    return declineChangeEmail.call(this);
}

function ActionDeclineDeletion() {
    return AS_NamedActions_8cc49d006c1240338fe066fde5cc325a();
}

function AS_NamedActions_8cc49d006c1240338fe066fde5cc325a() {
    return declineDeletePhoto.call(this);
}

function ActionDeclineExit() {
    return AS_NamedActions_f8e7b598c81f489ba61c11c7512a618f();
}

function AS_NamedActions_f8e7b598c81f489ba61c11c7512a618f() {
    return declineExit.call(this);
}

function ActionDoDiscardVisit() {
    return AS_NamedActions_1a4862b2d0e340eaa72be27ffdb316e2();
}

function AS_NamedActions_1a4862b2d0e340eaa72be27ffdb316e2() {
    return doDiscardVisit.call(this);
}

function ActionDoExitApplication() {
    return AS_NamedActions_d05ff71cb0564c068ba743f10293ca8f();
}

function AS_NamedActions_d05ff71cb0564c068ba743f10293ca8f() {
    return doExitApplication.call(this);
}

function ActionDurationCalculator(eventobject, changedtext) {
    return AS_TextField_1156aee7b2f844ac908d09fcbe2e5bca(eventobject, changedtext);
}

function AS_TextField_1156aee7b2f844ac908d09fcbe2e5bca(eventobject, changedtext) {
    return durationCalculator.call(this);
}

function ActionEmailCancelBtn(eventobject) {
    return AS_Button_a916f23cd0464776a2d512c7e6478f21(eventobject);
}

function AS_Button_a916f23cd0464776a2d512c7e6478f21(eventobject) {
    return onClickEmailCancelButton.call(this);
}

function ActionEmailConfirmBtn(eventobject) {
    return AS_Button_13c5f75a92d04ffca1aab4dd3c2a4222(eventobject);
}

function AS_Button_13c5f75a92d04ffca1aab4dd3c2a4222(eventobject) {
    return onClickEmailConfirmButton.call(this);
}

function ActionEmailListRowClick() {
    return AS_NamedActions_ac0f5b894f5848a5a313a5e4a1e8fea7();
}

function AS_NamedActions_ac0f5b894f5848a5a313a5e4a1e8fea7() {
    return onEmailListRowClick.call(this);
}

function ActionEnableSRASelection(eventobject, x, y) {
    return AS_Image_902c1200d89c495cb382f73eb29107b4(eventobject, x, y);
}

function AS_Image_902c1200d89c495cb382f73eb29107b4(eventobject, x, y) {
    return onDoneSRASelection.call(this);
}

function ActionEndVisit(eventobject) {
    return AS_FlexContainer_102b8999f53c46f196aa0758504d8956(eventobject);
}

function AS_FlexContainer_102b8999f53c46f196aa0758504d8956(eventobject) {
    return onClickEndVisit.call(this);
}

function ActionForCompletedVisitList(eventobject) {
    return AS_Form_2fb3e6c77635413ca91300dd1cbda116(eventobject);
}

function AS_Form_2fb3e6c77635413ca91300dd1cbda116(eventobject) {
    return onPostShowCompleteVisitScreen.call(this);
}

function ActionHomePostshow(eventobject) {
    return AS_Form_a42d0a23806e4007935ab8298f044e27(eventobject);
}

function AS_Form_a42d0a23806e4007935ab8298f044e27(eventobject) {
    return onPostShowHomeScreen.call(this);
}

function ActionHomePreshow(eventobject) {
    return AS_Form_550cd32062fe4269a4cc4d9a88295286(eventobject);
}

function AS_Form_550cd32062fe4269a4cc4d9a88295286(eventobject) {
    return onPreShowHomeScreen.call(this);
}

function ActionLoginPostshow(eventobject) {
    return AS_Form_896471a353a44dafb06e58a669feffc1(eventobject);
}

function AS_Form_896471a353a44dafb06e58a669feffc1(eventobject) {
    return onPostShowLoginScreen.call(this);
}

function ActionLoginPreshow(eventobject) {
    return AS_Form_efd395230fb342e0b2c8697b39f6f6a1(eventobject);
}

function AS_Form_efd395230fb342e0b2c8697b39f6f6a1(eventobject) {
    return onPreShowLoginScreen.call(this);
}

function ActionMapMenuClick(eventobject) {
    return AS_Button_53fcfede0ab14a6095a8c346f8790660(eventobject);
}

function AS_Button_53fcfede0ab14a6095a8c346f8790660(eventobject) {
    return handleTabSwitch.call(this, false);
}

function ActionMobileCallClick(eventobject, x, y) {
    return AS_FlexContainer_67b48d3870df4eb0add7256b9cee9a02(eventobject, x, y);
}

function AS_FlexContainer_67b48d3870df4eb0add7256b9cee9a02(eventobject, x, y) {
    return onClickMobileCall.call(this);
}

function ActionNavigationClick(eventobject, context) {
    return AS_FlexContainer_0069ad4d54a0495395cb7011e2fe3d8f(eventobject, context);
}

function AS_FlexContainer_0069ad4d54a0495395cb7011e2fe3d8f(eventobject, context) {
    return onClickNavigation.call(this);
}

function ActionOnClickCalendar(eventobject) {
    return AS_Button_1135915f8f9a4d5fb8d9448ee518b935(eventobject);
}

function AS_Button_1135915f8f9a4d5fb8d9448ee518b935(eventobject) {
    return onClickScrollToToday.call(this);
}

function ActionOnClickCompletedJobs(eventobject) {
    return AS_FlexContainer_d60b46c86b1e4f75963b67ce5e63f969(eventobject);
}

function AS_FlexContainer_d60b46c86b1e4f75963b67ce5e63f969(eventobject) {
    return onClickCompletedJobs.call(this);
}

function ActionOnClickCustomerEmail(eventobject) {
    return AS_FlexContainer_4b45b4748224474ea557cd12d3c44a7d(eventobject);
}

function AS_FlexContainer_4b45b4748224474ea557cd12d3c44a7d(eventobject) {
    return onClickCustomerEmail.call(this);
}

function ActionOnClickCustomerLoc(eventobject) {
    return AS_FlexContainer_6a32e70dbbfc4d0eb2f14dfe56b7d208(eventobject);
}

function AS_FlexContainer_6a32e70dbbfc4d0eb2f14dfe56b7d208(eventobject) {
    return onClickCustomerLoc.call(this);
}

function ActionOnClickCustomerMobile(eventobject) {
    return AS_FlexContainer_d7ecb8b152414afbab76c20b5e63dfe6(eventobject);
}

function AS_FlexContainer_d7ecb8b152414afbab76c20b5e63dfe6(eventobject) {
    return onClickCustomerMobile.call(this);
}

function ActionOnClickCustomerPhone(eventobject) {
    return AS_FlexContainer_002187e08e6f4127bdb8aa7a1b924f29(eventobject);
}

function AS_FlexContainer_002187e08e6f4127bdb8aa7a1b924f29(eventobject) {
    return onClickCustomerPhone.call(this);
}

function ActionOnClickDeletePhoto(eventobject) {
    return AS_Button_809716dce97540f181bccd06ad268eb5(eventobject);
}

function AS_Button_809716dce97540f181bccd06ad268eb5(eventobject) {
    return onClickDeletePhoto.call(this);
}

function ActionOnClickEditSRA(eventobject) {
    return AS_Button_84a1645ccffc496b858461022eddaca2(eventobject);
}

function AS_Button_84a1645ccffc496b858461022eddaca2(eventobject) {
    return doHandleSRAEditButton.call(this);
}

function ActionOnClickEndDayFromPopup(eventobject) {
    return AS_Button_1cd73dc11c3646498d5c40155611ee69(eventobject);
}

function AS_Button_1cd73dc11c3646498d5c40155611ee69(eventobject) {
    return onClickEndDayBtn.call(this);
}

function ActionOnClickImgFlex(eventobject) {
    return AS_FlexContainer_7b00f7e96df949309df271c93e937fd9(eventobject);
}

function AS_FlexContainer_7b00f7e96df949309df271c93e937fd9(eventobject) {
    return onClickImageFlx.call(this);
}

function ActionOnClickSelectSRA(eventobject) {
    return AS_FlexContainer_c7b8af903bc54bb88127b7e815ef882f(eventobject);
}

function AS_FlexContainer_c7b8af903bc54bb88127b7e815ef882f(eventobject) {
    return onSRASelectBtnAction.call(this);
}

function ActionOnClickStartDay(eventobject) {
    return AS_Button_f0ddc6bd50f945598a90677049332a24(eventobject);
}

function AS_Button_f0ddc6bd50f945598a90677049332a24(eventobject) {
    return onClickStartDay.call(this);
}

function ActionOnClickStartDayFromPopup(eventobject) {
    return AS_Button_18b4d78ea2a14dad94422bb09d049835(eventobject);
}

function AS_Button_18b4d78ea2a14dad94422bb09d049835(eventobject) {
    return onClickStartDayBtn.call(this);
}

function ActionOnDeviceBackFromCompletedVisit(eventobject) {
    return AS_Form_1713b6cd7e414e63bef91146436cbc73(eventobject);
}

function AS_Form_1713b6cd7e414e63bef91146436cbc73(eventobject) {
    return onDeviceBackButtonClick.call(this);
}

function ActionOnDeviceBackFromCustomer(eventobject) {
    return AS_Form_d0e9569948564331b7720834eb14639b(eventobject);
}

function AS_Form_d0e9569948564331b7720834eb14639b(eventobject) {
    return onDeviceBackButtonClick.call(this);
}

function ActionOnDeviceBackFromHome(eventobject) {
    return AS_Form_0976e06f24af482bae5c770db31147d5(eventobject);
}

function AS_Form_0976e06f24af482bae5c770db31147d5(eventobject) {
    return onDeviceBackButtonClick.call(this);
}

function ActionOnDeviceBackFromLogin(eventobject) {
    return AS_Form_c9db835807c14d8ba6ff23e67b7335c1(eventobject);
}

function AS_Form_c9db835807c14d8ba6ff23e67b7335c1(eventobject) {
    return onDeviceBackButtonClick.call(this);
}

function ActionOnDeviceBackFromVisit(eventobject) {
    return AS_Form_3bfbb233c2514b61a17a82c5e605f34c(eventobject);
}

function AS_Form_3bfbb233c2514b61a17a82c5e605f34c(eventobject) {
    return onDeviceBackButtonClick.call(this);
}

function ActionOnEmailIdClick(eventobject) {
    return AS_FlexContainer_a126a59110c14305ba412620d3e8e770(eventobject);
}

function AS_FlexContainer_a126a59110c14305ba412620d3e8e770(eventobject) {
    return onClickEmailId.call(this);
}

function ActionOnMessagePopupOkBtn() {
    return AS_NamedActions_58737c6ec5c54c469e31511d4e6e7e33();
}

function AS_NamedActions_58737c6ec5c54c469e31511d4e6e7e33() {
    return onClickMessagePopupOkButton.call(this);
}

function ActionOnSelectionNoSignCombo(eventobject) {
    return AS_ListBox_453f4c589eb64b889885566208504ec5(eventobject);
}

function AS_ListBox_453f4c589eb64b889885566208504ec5(eventobject) {
    return updateNoSignatureReasonOnVisitObj.call(this);
}

function ActionOnSelectionPrepQuantityCombo(eventobject) {
    return AS_ListBox_fdfc9b06a4aa4256b76e5f476ab218bc(eventobject);
}

function AS_ListBox_fdfc9b06a4aa4256b76e5f476ab218bc(eventobject) {
    return updatePreparationQuantityOnVisitObj.call(this, eventobject);
}

function ActionOnSelectionPreprationsCombo(eventobject) {
    return AS_ListBox_1cf2a732e5364ff0a41633463aaeae06(eventobject);
}

function AS_ListBox_1cf2a732e5364ff0a41633463aaeae06(eventobject) {
    return updatePreparationOnVisitObj.call(this, eventobject);
}

function ActionOnSettingsBottomSegRowClick(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_1ee653556cec4da38edc7a78cfe92f34(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_1ee653556cec4da38edc7a78cfe92f34(eventobject, sectionNumber, rowNumber) {
    return onClickBottomSegmentRow.call(this);
}

function ActionOnSettingsLayerClick(eventobject) {
    return AS_FlexContainer_d0afaa6188384f2eb8c89840cf60e162(eventobject);
}

function AS_FlexContainer_d0afaa6188384f2eb8c89840cf60e162(eventobject) {
    return closeSettingsMenu.call(this);
}

function ActionOnSettingsTopSegRowClick(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_553bb9e3acd84154bf440fc4c74fae29(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_553bb9e3acd84154bf440fc4c74fae29(eventobject, sectionNumber, rowNumber) {
    return onClickTopSegmentRow.call(this);
}

function ActionOnTextChange(eventobject, changedtext) {
    return AS_TextField_b15e86ae0cba4e26905bab47ad0d0195(eventobject, changedtext);
}

function AS_TextField_b15e86ae0cba4e26905bab47ad0d0195(eventobject, changedtext) {
    return onTextChangeOnInputField.call(this, eventobject);
}

function ActionOnVisitTabClick(eventobject, currentindex, isexpanded) {
    return AS_TabPane_f618d0fc025148dfa80b0aeed20ccae8(eventobject, currentindex, isexpanded);
}

function AS_TabPane_f618d0fc025148dfa80b0aeed20ccae8(eventobject, currentindex, isexpanded) {
    return onTabClick.call(this, eventobject, currentindex, isexpanded);
}

function ActionPhoneCallClick(eventobject, x, y) {
    return AS_FlexContainer_05276165d5e54a3996fc96f107e766e3(eventobject, x, y);
}

function AS_FlexContainer_05276165d5e54a3996fc96f107e766e3(eventobject, x, y) {
    return onClickPhoneCall.call(this);
}

function ActionPopupMessageOk() {
    return AS_NamedActions_d6165b397cf24f7e9fb3a1b351052e5b();
}

function AS_NamedActions_d6165b397cf24f7e9fb3a1b351052e5b() {
    return dismissMessagePopup.call(this);
}

function ActionPostAppInit(eventobject) {
    return AS_AppEvents_1255ca9b508441779247d6b3604da112(eventobject);
}

function AS_AppEvents_1255ca9b508441779247d6b3604da112(eventobject) {
    return onPostAppInit.call(this);
}

function ActionPreAppInit(eventobject) {
    return AS_AppEvents_1c41cd45c4d04d9aa73d1236fd864901(eventobject);
}

function AS_AppEvents_1c41cd45c4d04d9aa73d1236fd864901(eventobject) {
    return onPreAppInit.call(this);
}

function ActionPreshowCompleteVisit(eventobject) {
    return AS_Form_4c166880f72546d0b18526ea07c5d2f0(eventobject);
}

function AS_Form_4c166880f72546d0b18526ea07c5d2f0(eventobject) {
    return onPreShowCompleteDetailsScreen.call(this);
}

function ActionScheduledSegRowClick(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_a1f824de671849849182b9e916eaa2a9(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_a1f824de671849849182b9e916eaa2a9(eventobject, sectionNumber, rowNumber) {
    return onScheduledSegRowClick.call(this);
}

function ActionScheduledTabClick(eventobject) {
    return AS_Button_c4bd39d43e194ea4815246c8e9f4a814(eventobject);
}

function AS_Button_c4bd39d43e194ea4815246c8e9f4a814(eventobject) {
    return handleTabSwitch.call(this, true);
}

function ActionScrollToCompletedJobs(eventobject) {
    return AS_FlexContainer_1bf80dab5dad43d4ac27112f90b9b6cc(eventobject);
}

function AS_FlexContainer_1bf80dab5dad43d4ac27112f90b9b6cc(eventobject) {
    return scrollToCompletedJobs.call(this);
}

function ActionSegCompletedJobsRowClick(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_cc1feb89d9c44c4eb106328cbcba09c3(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_cc1feb89d9c44c4eb106328cbcba09c3(eventobject, sectionNumber, rowNumber) {
    return onCompletedJobsSegRowClick.call(this, null, null);
}

function ActionSettingsCancelBtn(eventobject) {
    return AS_Button_2d4a3854ff9e4d22854249998d5b4b79(eventobject);
}

function AS_Button_2d4a3854ff9e4d22854249998d5b4b79(eventobject) {
    return onCancelSourceLanguageSelection.call(this);
}

function ActionSettingsConfirmBtn(eventobject) {
    return AS_Button_c2e14aeedb8e4cffa571cbf6c588b3b1(eventobject);
}

function AS_Button_c2e14aeedb8e4cffa571cbf6c588b3b1(eventobject) {
    return onConfirmSourceLanguageSelection.call(this);
}

function ActionSettingsListRowClick(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_70265e8c58c9456ca57a921a28e72082(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_70265e8c58c9456ca57a921a28e72082(eventobject, sectionNumber, rowNumber) {
    return onSettingsListRowClick.call(this);
}

function ActionSRACancel(eventobject) {
    return AS_Button_a081a6b22511443ba1d79870e79935a8(eventobject);
}

function AS_Button_a081a6b22511443ba1d79870e79935a8(eventobject) {
    return onCancelSRASelection.call(this);
}

function ActionSRAConfirm(eventobject) {
    return AS_Button_5291ce94cd984252aeb3ff9acc44ac81(eventobject);
}

function AS_Button_5291ce94cd984252aeb3ff9acc44ac81(eventobject) {
    return onConfirmSRASelection.call(this);
}

function ActionStartVisitClick(eventobject, x, y) {
    return AS_FlexContainer_8ee136b6189f45eeafa0a9970378bc31(eventobject, x, y);
}

function AS_FlexContainer_8ee136b6189f45eeafa0a9970378bc31(eventobject, x, y) {
    return onClickStartVisit.call(this);
}

function ActionTabClickcompleted(eventobject, currentindex, isexpanded) {
    return AS_TabPane_8803b48f198749f9922ce3f5c4b5f99a(eventobject, currentindex, isexpanded);
}

function AS_TabPane_8803b48f198749f9922ce3f5c4b5f99a(eventobject, currentindex, isexpanded) {
    return onTabClickCompletedSignature.call(this, eventobject, currentindex, isexpanded);
}

function ActionToDeleteSignature(eventobject) {
    return AS_FlexContainer_73169839a52049e0b9c47289ae1a8eac(eventobject);
}

function AS_FlexContainer_73169839a52049e0b9c47289ae1a8eac(eventobject) {
    return deleteSignature.call(this);
}

function ActionVisitCamCaptureCallback(eventobject) {
    return AS_Camera_3cc9c4e1c66c4f4d802495086c6a2017(eventobject);
}

function AS_Camera_3cc9c4e1c66c4f4d802495086c6a2017(eventobject) {
    return onCameraPhotoCaptureCallback.call(this, null);
}

function ActionVisitCustomerNameBeginEditing(eventobject, changedtext) {
    return AS_TextField_19e74d6da7e94b10bdb0e8e7ae3145d1(eventobject, changedtext);
}

function AS_TextField_19e74d6da7e94b10bdb0e8e7ae3145d1(eventobject, changedtext) {
    return onTextboxBeginEditing.call(this, eventobject);
}

function ActionVisitCustomerNameEndEditing(eventobject, changedtext) {
    return AS_TextField_98cb2c75a3c84e828c6cc0d862ecc3bd(eventobject, changedtext);
}

function AS_TextField_98cb2c75a3c84e828c6cc0d862ecc3bd(eventobject, changedtext) {
    return onTextboxEndEditing.call(this, eventobject);
}

function ActionVisitDoneTextEditing(eventobject, changedtext) {
    return AS_TextField_e9049a86f7d8494b8cd3d9e6b716084c(eventobject, changedtext);
}

function AS_TextField_e9049a86f7d8494b8cd3d9e6b716084c(eventobject, changedtext) {
    return onTextboxEndEditing.call(this, eventobject);
}

function ActionVisitPostShow(eventobject) {
    return AS_Form_c76072e215854b4d8c4dc11fc2493b88(eventobject);
}

function AS_Form_c76072e215854b4d8c4dc11fc2493b88(eventobject) {
    return onPostShowVisitDetailsScreen.call(this);
}

function ActionVisitPreshow(eventobject) {
    return AS_Form_a4992b0cd14141e18ae535acd38a2c07(eventobject);
}

function AS_Form_a4992b0cd14141e18ae535acd38a2c07(eventobject) {
    return onPreShowVisitDetailsScreen.call(this);
}

function ActionVisitTextareaBeginEditing(eventobject) {
    return AS_TextArea_e2a7ddc9cf304672958582379b15a160(eventobject);
}

function AS_TextArea_e2a7ddc9cf304672958582379b15a160(eventobject) {
    return onTextboxBeginEditing.call(this, eventobject);
}

function ActionVisitTextareaEndEditing(eventobject) {
    return AS_TextArea_7410979fe42d48cc96933a3e00c12a68(eventobject);
}

function AS_TextArea_7410979fe42d48cc96933a3e00c12a68(eventobject) {
    return onTextboxEndEditing.call(this, eventobject);
}

function ActionVoidClick() {
    return AS_NamedActions_98246b17def84ae4a557741194b3ce3f();
}

function AS_NamedActions_98246b17def84ae4a557741194b3ce3f() {
    return voidClick.call(this);
}

function AS_AppEvents_84fc983a51c14945bae7ff8bd4fb1314(eventobject) {}

function AS_Button_29c08b88322544f2aaf8d5dce769f7be(eventobject) {
    return showMeHere.call(this);
}

function AS_Button_2b72682cba57432b8cb41f4d82fca28d(eventobject) {
    return showNearByLocations.call(this);
}

function AS_Button_3ba86f90592f4e2e9451e6f5e3fa1e59(eventobject) {
    return showNearByLocations.call(this);
}

function AS_Button_4e76765ad63e426996f48f489b0a6292(eventobject) {
    return showMeHere.call(this);
}

function AS_Button_6616a8a91e6b49c9a0f734a658293a23(eventobject) {
    return onSelectBtnAction.call(this);
}

function AS_Button_d126dc9c7980454787a0d3a3dfbc6565(eventobject) {
    return onSelectBtnAction.call(this);
}

function AS_Button_d34c6722f15e4810b4c80629056f5114(eventobject) {
    return onSelectBtnAction.call(this);
}

function AS_Button_e7ac7f2937064c31a93f619eddd2011b(eventobject) {
    return onSelectBtnAction.call(this);
}

function AS_Camera_70895fda3c0b4f1ba48a169561c59b31(eventobject) {
    return onCameraPhotoCaptureCallback.call(this, eventobject);
}

function AS_FlexContainer_0af4e55072b7436b9c31e57c1ec48b17(eventobject) {
    return closeSettingsMenu.call(this);
}

function AS_FlexContainer_12dd8eee718b42b5a8867ba6f9eeab95(eventobject) {
    return onClickCustomerEmail.call(this);
}

function AS_FlexContainer_29f85c08b0d24d5faa58fa1fa30ed1ae(eventobject) {
    return onClickCustomerLoc.call(this);
}

function AS_FlexContainer_32c515bed78a4e37bda4b834bcad281c(eventobject) {
    return deleteSignature.call(this);
}

function AS_FlexContainer_3c4bad0954054d828c83fb47948073c9(eventobject) {
    return onClickCustomerEmail.call(this);
}

function AS_FlexContainer_45ab146b07d34d8cb5e993908e78e694(eventobject) {
    return onClickCustomerPhone.call(this);
}

function AS_FlexContainer_487ad7b601324bacb7f706059998b75c(eventobject) {
    return onClickCustomerPhone.call(this);
}

function AS_FlexContainer_4af3b0313703462c8865302d8a038ba2(eventobject) {
    return onClickCustomerMobile.call(this);
}

function AS_FlexContainer_63b7ef1d757a44a4949b73d3f183c148(eventobject) {
    return onClickCustomerPhone.call(this);
}

function AS_FlexContainer_66751360c5894f53a1401ec46477db19(eventobject) {
    return onClickCustomerEmail.call(this);
}

function AS_FlexContainer_720f0263c63940f9a239ab050167288a(eventobject) {
    return onClickCustomerLoc.call(this);
}

function AS_FlexContainer_734b9eabd7d440588128431f8c019b36(eventobject) {
    return onClickCustomerEmail.call(this);
}

function AS_FlexContainer_7e47440667344170aa853089b175ecea(eventobject) {}

function AS_FlexContainer_90f3241fb0424e96a5ef5ab8352676bf(eventobject) {
    return onClickCustomerLoc.call(this);
}

function AS_FlexContainer_9d564f2541bf428d824f85609d064dfa(eventobject) {
    return onClickCustomerMobile.call(this);
}

function AS_FlexContainer_a24f985aa1644e51a3435ea1d1d79130(eventobject) {
    return onClickCustomerPhone.call(this);
}

function AS_FlexContainer_a58d4d457ec04cc898ab9b363d331a41(eventobject) {
    return onClickCustomerLoc.call(this);
}

function AS_FlexContainer_bd0224feee5b4c4da0b90131ce11e4af(eventobject) {
    return onClickCustomerMobile.call(this);
}

function AS_FlexContainer_c72baef0520b48acb563d5d6fe52fe52(eventobject) {
    return onClickImageFlx.call(this);
}

function AS_FlexContainer_da53288bd8d047e3a24f5917c5ed23ba(eventobject) {
    return onClickEmailId.call(this);
}

function AS_FlexContainer_fd3371a192c949239ed3014b8f176a82(eventobject) {
    return onClickCustomerMobile.call(this);
}

function AS_Image_b20b32c771674b5db53722bfff53e24f(eventobject, x, y) {
    return onDoneSRASelection.call(this);
}

function AS_Map_393266fbaf3a4847b43e3e17862d06b4(eventobject, location) {
    return onPinClickCallBack.call(this, eventobject, location);
}

function AS_Map_7f62b3251817427cb08c7e256dd96d52(eventobject, location) {
    return onPinClickCallBack.call(this, eventobject, location);
}

function AS_Segment_001b3e171a0e4f5b946fd4779a211e97(eventobject, sectionNumber, rowNumber) {
    return onClickBottomSegmentRow.call(this);
}

function AS_Segment_2da0199dec8a4fe9a1df5747edce20c7(eventobject, sectionNumber, rowNumber) {
    return onScheduledSegRowClick.call(this);
}

function AS_Segment_388135eeaa5b4c8ea63eb57a45ab5602(eventobject, sectionNumber, rowNumber) {
    return onClickBottomSegmentRow.call(this);
}

function AS_Segment_849875a7efa6472ea99b416d78d076b2(eventobject, sectionNumber, rowNumber) {
    return onScheduledSegRowClick.call(this);
}

function AS_Segment_87f7bffd6c6945afaafa75d1a5dca015(eventobject, sectionNumber, rowNumber) {
    return onClickTopSegmentRow.call(this);
}

function AS_Segment_a6579f0fbc4a4a9784757127d65ac1c1(eventobject, sectionNumber, rowNumber) {
    return onScheduledSegRowClick.call(this);
}

function AS_Segment_a86ad15c3344435f9fc981732db12b4e(eventobject, sectionNumber, rowNumber) {
    return onClickTopSegmentRow.call(this);
}

function AS_Segment_c818b75fed6e4d878635439309dcb549(eventobject, sectionNumber, rowNumber) {
    return onScheduledSegRowClick.call(this);
}

function AS_Segment_c9016c422e25452baf9bbecc424d01ca(eventobject, sectionNumber, rowNumber) {
    return onScheduledSegRowClick.call(this);
}

function AS_TextField_4addd3fd4f0f492ca83facc1cd3e404d(eventobject, changedtext) {
    return onTextboxEndEditing.call(this, null);
}

function AS_TextField_8f195acab95a4dd686be26dd7cddfb44(eventobject, changedtext) {
    return onClickDoneGoogleSearchTxtBx.call(this);
}

function AS_TextField_fbb0f49929fd48da822a90a138b915b3(eventobject, changedtext) {}

function onAddFilterAction(eventobject, x, y) {
    return AS_FlexContainer_93f2ac5414a34b4fb52b7ce9865ce082(eventobject, x, y);
}

function AS_FlexContainer_93f2ac5414a34b4fb52b7ce9865ce082(eventobject, x, y) {
    return onClickFlxAddFilter.call(this);
}

function onChkBxAddFiltrSelection(eventobject) {
    return AS_CheckBoxGroup_2580982c2e9043e096eb9c5401fd21bb(eventobject);
}

function AS_CheckBoxGroup_2580982c2e9043e096eb9c5401fd21bb(eventobject) {
    return onSelectChkBxFlxAddFilter.call(this);
}

function onChkBxContTyeSelection(eventobject) {
    return AS_CheckBoxGroup_c89f1f602ed846a58626ea6f3536836a(eventobject);
}

function AS_CheckBoxGroup_c89f1f602ed846a58626ea6f3536836a(eventobject) {
    return onSelectChkBxContractType.call(this);
}

function onChkBxKeyActSelection(eventobject) {
    return AS_CheckBoxGroup_0c9c14e1bf784aaa82d1ee0ed7583c96(eventobject);
}

function AS_CheckBoxGroup_0c9c14e1bf784aaa82d1ee0ed7583c96(eventobject) {
    return onSelectChkBxKeyAccnt.call(this);
}

function onChkBxServiceSelection(eventobject) {
    return AS_CheckBoxGroup_2db4cc7404e6483392345569f1399f00(eventobject);
}

function AS_CheckBoxGroup_2db4cc7404e6483392345569f1399f00(eventobject) {
    return onSelectChkBxServiceCover.call(this);
}

function onChkBxVisitTypeSelection(eventobject) {
    return AS_CheckBoxGroup_4a4c235c22664ca6bf3ce06394654089(eventobject);
}

function AS_CheckBoxGroup_4a4c235c22664ca6bf3ce06394654089(eventobject) {
    return onSelectChkBxVisitType.call(this);
}

function onContentTypeAction(eventobject, x, y) {
    return AS_FlexContainer_3ef5e8acc5c84038829f84789ef59c3b(eventobject, x, y);
}

function AS_FlexContainer_3ef5e8acc5c84038829f84789ef59c3b(eventobject, x, y) {
    return onClickFlxContactType.call(this);
}

function onFilterBtnAcrion(eventobject, x, y) {
    return AS_Image_9840111046884b42800251d9c3a48c9b(eventobject, x, y);
}

function AS_Image_9840111046884b42800251d9c3a48c9b(eventobject, x, y) {
    undefined.show();
}

function onFilterClearAllAction(eventobject) {
    return AS_Button_bfb19c6ed39d4dee9fccedcda7577aa8(eventobject);
}

function AS_Button_bfb19c6ed39d4dee9fccedcda7577aa8(eventobject) {
    return onClickClearFilter.call(this);
}

function onFilterGoAction(eventobject) {
    return AS_Button_48949ce3de0d48089df2c1a7543d1a72(eventobject);
}

function AS_Button_48949ce3de0d48089df2c1a7543d1a72(eventobject) {
    return onClickGoFiltersSchedule.call(this);
}

function onFilterPostShowAction(eventobject) {
    return AS_Form_820ec2aa538140ebac9a7f68f5fd041d(eventobject);
}

function AS_Form_820ec2aa538140ebac9a7f68f5fd041d(eventobject) {
    return onFilterPagePostShow.call(this);
}

function onFilterSelectAllAction(eventobject) {
    return AS_Button_deb8476351184ef19935613a9768140b(eventobject);
}

function AS_Button_deb8476351184ef19935613a9768140b(eventobject) {
    return onClickSelectAllFilters.call(this);
}

function onKeyActAction(eventobject, x, y) {
    return AS_FlexContainer_fafcb73e131b4c4f8a0c837dc28fbfc0(eventobject, x, y);
}

function AS_FlexContainer_fafcb73e131b4c4f8a0c837dc28fbfc0(eventobject, x, y) {
    return onClickFlxKeyAccnt.call(this);
}

function onMapPickClick(eventobject, location) {
    return AS_Map_cdcad6ddea764f8c9799342045ddec84(eventobject, location);
}

function AS_Map_cdcad6ddea764f8c9799342045ddec84(eventobject, location) {
    return onPinClickCallBack.call(this, eventobject, location);
}

function onMyLocationAction(eventobject) {
    return AS_Button_25e2613521ec42f58f5d7716c2e0cda7(eventobject);
}

function AS_Button_25e2613521ec42f58f5d7716c2e0cda7(eventobject) {
    return showNearByLocations.call(this);
}

function onSearchBgAction(eventobject, x, y) {
    return AS_FlexContainer_4f36d8075a514cb083b197a34716e30d(eventobject, x, y);
}

function AS_FlexContainer_4f36d8075a514cb083b197a34716e30d(eventobject, x, y) {
    return handleSearchDisplay.call(this);
}

function onSearchBtnAction(eventobject, x, y) {
    return AS_Image_4b30640617d84cf59123c9a962dcb355(eventobject, x, y);
}

function AS_Image_4b30640617d84cf59123c9a962dcb355(eventobject, x, y) {
    return handleSearchDisplay.call(this);
}

function onServiceCvrAction(eventobject, x, y) {
    return AS_FlexContainer_205abfb9c5b3450bbdb561737ba352a0(eventobject, x, y);
}

function AS_FlexContainer_205abfb9c5b3450bbdb561737ba352a0(eventobject, x, y) {
    return onClickFlxServiceCover.call(this);
}

function onShowHereAction(eventobject) {
    return AS_Button_69998d7d56b24369817f6119a6e6b0d5(eventobject);
}

function AS_Button_69998d7d56b24369817f6119a6e6b0d5(eventobject) {
    return showMeHere.call(this);
}

function onTapofLocationCallout(eventobject, location) {
    return AS_Map_a1c68f4a52b84d1d8880d5ec7112cd6c(eventobject, location);
}

function AS_Map_a1c68f4a52b84d1d8880d5ec7112cd6c(eventobject, location) {
    return navigateToCustomerScreen.call(this, location);
}

function onVisitTypeAction(eventobject, x, y) {
    return AS_FlexContainer_c171ce6406d64fde8d72f8589591f0b2(eventobject, x, y);
}

function AS_FlexContainer_c171ce6406d64fde8d72f8589591f0b2(eventobject, x, y) {
    return onClickFlxVisitType.call(this);
}