function displayMWError() {
    kony.ui.Alert("Middleware Error ", null, "error", null, null);
};

function displaySessionError() {
    kony.ui.Alert("Session Expired .. Please re-login", null, "error", null, null);
};

function displayError(code, msg) {
    // Commented for SWA: kony.ui.Alert("Error Code: "..code .." Message: " ..msg,null,"error",null,null);
    kony.ui.Alert(code + "- " + msg, null, "error", null, null);
};
var mergeHeaders = function(httpHeaders, globalHeaders) {
    for (var attrName in globalHeaders) {
        httpHeaders[attrName] = globalHeaders[attrName];
    }
    return httpHeaders;
};

function appmiddlewareinvoker(inputParam, isBlocking, indicator, datasetID) {
    var url = appConfig.url;
    var sessionIdKey = "cacheid";
    inputParam.appID = appConfig.appId;
    inputParam.appver = appConfig.appVersion;
    inputParam["channel"] = "rc";
    inputParam["platform"] = kony.os.deviceInfo().name;
    if (indicator) {
        inputParam["indicator"] = indicator;
    };
    if (datasetID) {
        inputParam["datasetID"] = datasetID;
    };
    inputParam[sessionIdKey] = sessionID;
    if (globalhttpheaders) {
        if (inputParam["httpheaders"]) {
            inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
        } else {
            inputParam["httpheaders"] = globalhttpheaders;
        };
    };
    var resulttable = _invokeServiceSyncForMF_(url, inputParam, isBlocking);
    if (resulttable) {
        if (resulttable[sessionIdKey]) {
            sessionID = resulttable[sessionIdKey];
        };
    };
    return resulttable;
};

function appmiddlewaresecureinvoker(inputParam, isBlocking, indicator, datasetID) {
    var url = appConfig.secureurl;
    var sessionIdKey = "cacheid";
    inputParam.appID = appConfig.appId;
    inputParam.appver = appConfig.appVersion;
    inputParam["channel"] = "rc";
    inputParam["platform"] = kony.os.deviceInfo().name;
    if (indicator) {
        inputParam["indicator"] = indicator;
    };
    if (datasetID) {
        inputParam["datasetID"] = datasetID;
    };
    inputParam[sessionIdKey] = sessionID;
    if (globalhttpheaders) {
        if (inputParam["httpheaders"]) {
            inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
        } else {
            inputParam["httpheaders"] = globalhttpheaders;
        };
    };
    var resulttable = _invokeServiceSyncForMF_(url, inputParam, isBlocking);
    if (resulttable) {
        if (resulttable[sessionIdKey]) {
            sessionID = resulttable[sessionIdKey];
        };
    };
    return resulttable;
};

function appmiddlewareinvokerasync(inputParam, callBack) {
    var url = appConfig.url;
    var sessionIdKey = "cacheid";
    inputParam.appID = appConfig.appId;
    inputParam.appver = appConfig.appVersion;
    inputParam["channel"] = "rc";
    inputParam["platform"] = kony.os.deviceInfo().name;
    inputParam[sessionIdKey] = sessionID;
    if (globalhttpheaders) {
        if (inputParam["httpheaders"]) {
            inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
        } else {
            inputParam.httpheaders = globalhttpheaders;
        };
    };
    var connHandle = _invokeServiceAsyncForMF_(url, inputParam, callBack);
    return connHandle;
};

function appmiddlewaresecureinvokerasync(inputParam, callBack) {
    var url = appConfig.secureurl;
    var sessionIdKey = "cacheid";
    inputParam.appID = appConfig.appId;
    inputParam.appver = appConfig.appVersion;
    inputParam["channel"] = "rc";
    inputParam["platform"] = kony.os.deviceInfo().name;
    inputParam[sessionIdKey] = sessionID;
    if (globalhttpheaders) {
        if (inputParam["httpheaders"]) {
            inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
        } else {
            inputParam["httpheaders"] = globalhttpheaders;
        };
    };
    var connHandle = _invokeServiceAsyncForMF_(url, inputParam, callBack);
    return connHandle;
};

function mfgetidentityservice(idProviderName) {
    var currentInstance = kony.sdk.getCurrentInstance();
    if (!currentInstance) {
        throw new Exception("INIT_FAILURE", "Please call init before getting identity provider");
    }
    return currentInstance.getIdentityService(idProviderName);
};

function mfintegrationsecureinvokerasync(inputParam, serviceID, operationID, callBack) {
    var url = appConfig.secureurl;
    var sessionIdKey = "cacheid";
    inputParam.appID = appConfig.appId;
    inputParam.appver = appConfig.appVersion;
    inputParam["channel"] = "rc";
    inputParam["platform"] = kony.os.deviceInfo().name;
    inputParam[sessionIdKey] = sessionID;
    if (globalhttpheaders) {
        if (inputParam["httpheaders"]) {
            inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
        } else {
            inputParam["httpheaders"] = mergeHeaders({}, globalhttpheaders);
        };
    };
    kony.print("Async : Invoking service through mobileFabric with url : " + url + " service id : " + serviceID + " operationid : " + operationID + "\n input params" + JSON.stringify(inputParam));
    if (kony.mbaas) {
        kony.mbaas.invokeMbaasServiceFromKonyStudio(url, inputParam, serviceID, operationID, callBack);
    } else {
        alert("Unable to find the mobileFabric SDK for KonyStudio. Please download the SDK from the Kony Cloud Console and add as module to the Kony Project.");
    }
};

function mfintegrationsecureinvokersync(inputParam, serviceID, operationID) {
    var url = appConfig.secureurl;
    var sessionIdKey = "cacheid";
    var resulttable;
    inputParam.appID = appConfig.appId;
    inputParam.appver = appConfig.appVersion;
    inputParam["channel"] = "rc";
    inputParam["platform"] = kony.os.deviceInfo().name;
    inputParam[sessionIdKey] = sessionID;
    if (globalhttpheaders) {
        if (inputParam["httpheaders"]) {
            inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
        } else {
            inputParam["httpheaders"] = mergeHeaders({}, globalhttpheaders);
        };
    };
    kony.print("Invoking service through mobileFabric with url : " + url + " service id : " + serviceID + " operationid : " + operationID + "\n input params" + JSON.stringify(inputParam));
    if (kony.mbaas) {
        resulttable = kony.mbaas.invokeMbaasServiceFromKonyStudioSync(url, inputParam, serviceID, operationID);
        kony.print("Result table for service id : " + serviceID + " operationid : " + operationID + " : " + JSON.stringify(resulttable));
    } else {
        alert("Unable to find the mobileFabric SDK for KonyStudio. Please download the SDK from the Kony Cloud Console and add as module to the Kony Project.");
    }
    return resulttable;
};
_invokeServiceAsyncForMF_ = function(url, inputParam, callBack) {
    var operationID = inputParam["serviceID"];
    if (!operationID) {
        resulttable = kony.net.invokeServiceAsync(url, inputParam, callBack);
    } else {
        var _mfServicesMap_ = {
            "getServiceAreas": {
                "servicename": "ServiceAreasPc",
                "serviceid": "132df26b-f47c-42e6-93bb-bff6859f871c",
                "type": "integration",
                "operationname": "getServiceAreas",
                "operationid": "265221"
            },
            "getPortfolioDetails": {
                "servicename": "PortfoliosPc",
                "serviceid": "2506f97b-db3b-4e6a-aeee-a9d7d4ec810c",
                "type": "integration",
                "operationname": "getPortfolioDetails",
                "operationid": "265224"
            },
            "getPortfolios": {
                "servicename": "PortfoliosPc",
                "serviceid": "2506f97b-db3b-4e6a-aeee-a9d7d4ec810c",
                "type": "integration",
                "operationname": "getPortfolios",
                "operationid": "265225"
            },
            "PropertyContract": {
                "servicename": "PortfoliosPc",
                "serviceid": "2506f97b-db3b-4e6a-aeee-a9d7d4ec810c",
                "type": "integration",
                "operationname": "PropertyContract",
                "operationid": "265226"
            },
            "workDayStart": {
                "servicename": "WorkdayPc",
                "serviceid": "9560ec15-cc90-4af5-97c8-ad704f8b961d",
                "type": "integration",
                "operationname": "workDayStart",
                "operationid": "265229"
            },
            "workDayEnd": {
                "servicename": "WorkdayPc",
                "serviceid": "9560ec15-cc90-4af5-97c8-ad704f8b961d",
                "type": "integration",
                "operationname": "workDayEnd",
                "operationid": "265230"
            },
            "getConfigData": {
                "servicename": "ConfigDataPc",
                "serviceid": "bc9b6213-765d-4982-8a51-c3ef3f03cea7",
                "type": "integration",
                "operationname": "getConfigData",
                "operationid": "265233"
            },
            "getServiceCovers": {
                "servicename": "ServiceCoversPc",
                "serviceid": "ec2753b4-a8c6-4771-940e-5362c7702421",
                "type": "integration",
                "operationname": "getServiceCovers",
                "operationid": "265236"
            },
            "getIcabInstances": {
                "servicename": "iCabInstancePc",
                "serviceid": "9b4350cd-db96-41fa-b347-375b4ac47cb5",
                "type": "integration",
                "operationname": "getIcabInstances",
                "operationid": "265239"
            },
            "getWorkloads": {
                "servicename": "WorkloadsPc",
                "serviceid": "ad01499c-a957-465f-9f9e-4625af645f20",
                "type": "integration",
                "operationname": "getWorkloads",
                "operationid": "265242"
            },
            "closedWork": {
                "servicename": "WorkloadsPc",
                "serviceid": "ad01499c-a957-465f-9f9e-4625af645f20",
                "type": "integration",
                "operationname": "closedWork",
                "operationid": "265243"
            },
            "getEmployees": {
                "servicename": "EmployeesPc",
                "serviceid": "33ddc1e3-d348-4a18-a4b4-f1455a911640",
                "type": "integration",
                "operationname": "getEmployees",
                "operationid": "265246"
            },
            "getWorkHistory": {
                "servicename": "WorkHistoryPc",
                "serviceid": "73b0f311-014d-4923-b319-c656ce27aa5d",
                "type": "integration",
                "operationname": "getWorkHistory",
                "operationid": "265249"
            }
        };
        kony.print("Getting serviceID for : " + operationID);
        var serviceID = _mfServicesMap_[operationID] && _mfServicesMap_[operationID]["servicename"];
        kony.print("Got serviceID for : " + operationID + " : " + serviceID);
        kony.print("Async : Invoking service through mobileFabric with url : " + url + " service id : " + serviceID + " operationid : " + operationID + "\n input params" + JSON.stringify(inputParam));
        if (serviceID && operationID) {
            var url = appConfig.secureurl;
            if (kony.mbaas) {
                kony.mbaas.invokeMbaasServiceFromKonyStudio(url, inputParam, serviceID, operationID, callBack);
            } else {
                alert("Unable to find the mobileFabric SDK for KonyStudio. Please download the SDK from the Kony Cloud Console and add as module to the Kony Project.");
            }
        } else {
            resulttable = kony.net.invokeServiceAsync(url, inputParam, callBack);
        }
    }
};
_invokeServiceSyncForMF_ = function(url, inputParam, isBlocking) {
    var resulttable;
    var operationID = inputParam["serviceID"];
    if (!operationID) {
        resulttable = kony.net.invokeService(url, inputParam, isBlocking);
    } else {
        var _mfServicesMap_ = {
            "getServiceAreas": {
                "servicename": "ServiceAreasPc",
                "serviceid": "132df26b-f47c-42e6-93bb-bff6859f871c",
                "type": "integration",
                "operationname": "getServiceAreas",
                "operationid": "265221"
            },
            "getPortfolioDetails": {
                "servicename": "PortfoliosPc",
                "serviceid": "2506f97b-db3b-4e6a-aeee-a9d7d4ec810c",
                "type": "integration",
                "operationname": "getPortfolioDetails",
                "operationid": "265224"
            },
            "getPortfolios": {
                "servicename": "PortfoliosPc",
                "serviceid": "2506f97b-db3b-4e6a-aeee-a9d7d4ec810c",
                "type": "integration",
                "operationname": "getPortfolios",
                "operationid": "265225"
            },
            "PropertyContract": {
                "servicename": "PortfoliosPc",
                "serviceid": "2506f97b-db3b-4e6a-aeee-a9d7d4ec810c",
                "type": "integration",
                "operationname": "PropertyContract",
                "operationid": "265226"
            },
            "workDayStart": {
                "servicename": "WorkdayPc",
                "serviceid": "9560ec15-cc90-4af5-97c8-ad704f8b961d",
                "type": "integration",
                "operationname": "workDayStart",
                "operationid": "265229"
            },
            "workDayEnd": {
                "servicename": "WorkdayPc",
                "serviceid": "9560ec15-cc90-4af5-97c8-ad704f8b961d",
                "type": "integration",
                "operationname": "workDayEnd",
                "operationid": "265230"
            },
            "getConfigData": {
                "servicename": "ConfigDataPc",
                "serviceid": "bc9b6213-765d-4982-8a51-c3ef3f03cea7",
                "type": "integration",
                "operationname": "getConfigData",
                "operationid": "265233"
            },
            "getServiceCovers": {
                "servicename": "ServiceCoversPc",
                "serviceid": "ec2753b4-a8c6-4771-940e-5362c7702421",
                "type": "integration",
                "operationname": "getServiceCovers",
                "operationid": "265236"
            },
            "getIcabInstances": {
                "servicename": "iCabInstancePc",
                "serviceid": "9b4350cd-db96-41fa-b347-375b4ac47cb5",
                "type": "integration",
                "operationname": "getIcabInstances",
                "operationid": "265239"
            },
            "getWorkloads": {
                "servicename": "WorkloadsPc",
                "serviceid": "ad01499c-a957-465f-9f9e-4625af645f20",
                "type": "integration",
                "operationname": "getWorkloads",
                "operationid": "265242"
            },
            "closedWork": {
                "servicename": "WorkloadsPc",
                "serviceid": "ad01499c-a957-465f-9f9e-4625af645f20",
                "type": "integration",
                "operationname": "closedWork",
                "operationid": "265243"
            },
            "getEmployees": {
                "servicename": "EmployeesPc",
                "serviceid": "33ddc1e3-d348-4a18-a4b4-f1455a911640",
                "type": "integration",
                "operationname": "getEmployees",
                "operationid": "265246"
            },
            "getWorkHistory": {
                "servicename": "WorkHistoryPc",
                "serviceid": "73b0f311-014d-4923-b319-c656ce27aa5d",
                "type": "integration",
                "operationname": "getWorkHistory",
                "operationid": "265249"
            }
        };
        kony.print("Getting serviceID for : " + operationID);
        var serviceID = _mfServicesMap_[operationID] && _mfServicesMap_[operationID]["servicename"];
        kony.print("Got serviceID for : " + operationID + " : " + serviceID);
        kony.print("Invoking service through mobileFabric with url : " + url + " service id : " + serviceID + " operationid : " + operationID + "\n input params" + JSON.stringify(inputParam));
        if (serviceID && operationID) {
            var url = appConfig.secureurl;
            if (kony.mbaas) {
                resulttable = kony.mbaas.invokeMbaasServiceFromKonyStudioSync(url, inputParam, serviceID, operationID);
                kony.print("Result table for service id : " + serviceID + " operationid : " + operationID + " : " + JSON.stringify(resulttable));
            } else {
                alert("Unable to find the mobileFabric SDK for KonyStudio. Please download the SDK from the Kony Cloud Console and add as module to the Kony Project.");
            }
        } else {
            resulttable = kony.net.invokeService(url, inputParam, isBlocking);
        }
    }
    return resulttable;
};
/*
   Sample invocation code
   var inputparam = {};
   inputparam.options = {
       "access": "online",
       "CRUD_TYPE": "get",//get/create..
       "odataurl": "$filter=UserId eq xxx",
       "data" : {a:1,b:2}//in case of create/update
   };
*/
function mfobjectsecureinvokerasync(inputParam, serviceID, objectID, callBack) {
    var options = {
        "access": inputParam.options.access
    };
    var serviceObj = kony.sdk.getCurrentInstance().getObjectService(serviceID, options);
    var CRUD_TYPE = inputParam.options.CRUD_TYPE;
    switch (CRUD_TYPE) {
        case 'get':
            var dataObject = new kony.sdk.dto.DataObject(objectID);
            var headers = inputParam.httpheaders || {};
            if (inputParam.options && inputParam.options.odataurl) dataObject.setOdataUrl(inputParam.options.odataurl.toString());
            options = {
                "dataObject": dataObject,
                "headers": headers
            };
            serviceObj.fetch(options, callBack, callBack);
            break;
        case 'create':
            var dataObject = new kony.sdk.dto.DataObject(objectID);
            var headers = inputParam.httpheaders || {};
            var data = inputParam.options && inputParam.options.data || {};
            var key;
            for (key in data) {
                dataObject.addField(key, data[key]);
            }
            options = {
                "dataObject": dataObject,
                "headers": headers
            };
            serviceObj.create(options, callBack, callBack);
            break;
        case 'update':
            var dataObject = new kony.sdk.dto.DataObject(objectID);
            var headers = inputParam.httpheaders || {};
            var data = inputParam.options && inputParam.options.data || {};
            var key;
            for (key in data) {
                dataObject.addField(key, data[key]);
            }
            options = {
                "dataObject": dataObject,
                "headers": headers
            };
            serviceObj.update(options, callBack, callBack);
            break;
        case 'partialupdate':
            var dataObject = new kony.sdk.dto.DataObject(objectID);
            var headers = inputParam.httpheaders || {};
            var data = inputParam.options && inputParam.options.data || {};
            var key;
            for (key in data) {
                dataObject.addField(key, data[key]);
            }
            options = {
                "dataObject": dataObject,
                "headers": headers
            };
            serviceObj.partialUpdate(options, callBack, callBack);
            break;
        case 'delete':
            var dataObject = new kony.sdk.dto.DataObject(objectID);
            var headers = inputParam.httpheaders || {};
            var data = inputParam.options && inputParam.options.data || {};
            var key;
            for (key in data) {
                dataObject.addField(key, data[key]);
            }
            options = {
                "dataObject": dataObject,
                "headers": headers
            };
            serviceObj.deleteRecord(options, callBack, callBack);
            break;
        default:
    }
};

function appmenuseq() {
    Login.show();
};

function callAppMenu() {
    var appMenu = [
        ["appmenuitemid1", "Item 1", "option1.png", appmenuseq, {}],
        ["appmenuitemid2", "Item 2", "option2.png", appmenuseq, {}],
        ["appmenuitemid3", "Item 3", "option3.png", appmenuseq, {}],
        ["appmenuitemid4", "Item 4", "option4.png", appmenuseq, {}]
    ];
    kony.application.createAppMenu("sampAppMenu", appMenu, "", "");
    kony.application.setCurrentAppMenu("sampAppMenu");
};

function makeCall(eventobject) {
    kony.phone.dial(eventobject.text);
};

function initializeGlobalVariables() {};