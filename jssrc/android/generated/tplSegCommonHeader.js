function initializetplSegCommonHeader() {
    flxHeaderSegCommonList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "25dp",
        "id": "flxHeaderSegCommonList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxLightDarkGrayBgSkin"
    }, {}, {});
    flxHeaderSegCommonList.setDefaultUnit(kony.flex.DP);
    var lblTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblTitle",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "lblWhiteSmallSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeaderSegCommonList.add(lblTitle);
}