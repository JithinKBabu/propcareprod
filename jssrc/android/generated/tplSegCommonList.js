function initializetplSegCommonList() {
    flxSegCommonList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxSegCommonList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0.00%",
        "right": "2%",
        "skin": "flxWhiteBgSkin",
        "top": "-0.17%",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxSegCommonList.setDefaultUnit(kony.flex.DP);
    var lblTop = new kony.ui.Label({
        "id": "lblTop",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 2,
        "right": "10dp",
        "skin": "lblGray95Skin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "6dp"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBottom = new kony.ui.Label({
        "bottom": "6dp",
        "id": "lblBottom",
        "isVisible": true,
        "left": "10dp",
        "maxNumberOfLines": 2,
        "right": "10dp",
        "skin": "lblGray95Skin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSegCommonList.add(lblTop, lblBottom);
}