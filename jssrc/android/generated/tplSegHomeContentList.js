function initializetplSegHomeContentList() {
    flxRowSegHomeContentList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85dp",
        "id": "flxRowSegHomeContentList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin"
    }, {}, {});
    flxRowSegHomeContentList.setDefaultUnit(kony.flex.DP);
    var flxLeft = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxLeft",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxLeft.setDefaultUnit(kony.flex.DP);
    var lblItemTop = new kony.ui.Label({
        "centerY": "20%",
        "id": "lblItemTop",
        "isVisible": true,
        "left": "4%",
        "maxNumberOfLines": 1,
        "skin": "lblBlackNormalBoldSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "96%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItemMiddle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItemMiddle",
        "isVisible": true,
        "left": "4%",
        "maxNumberOfLines": 1,
        "skin": "lblGrayLargeSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItemBottom = new kony.ui.Label({
        "centerY": "80%",
        "id": "lblItemBottom",
        "isVisible": true,
        "left": "4%",
        "maxNumberOfLines": 1,
        "skin": "lblGrayLargeSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLeft.add(lblItemTop, lblItemMiddle, lblItemBottom);
    var flxRight = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxRight",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "88%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxRight.setDefaultUnit(kony.flex.DP);
    var imgLeftArrowIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40dp",
        "id": "imgLeftArrowIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "leftarrow.png",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgJobTypeIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "80%",
        "height": "22dp",
        "id": "imgJobTypeIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "imagedrag.png",
        "width": "22dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxRight.add(imgLeftArrowIcon, imgJobTypeIcon);
    flxRowSegHomeContentList.add(flxLeft, flxRight);
}