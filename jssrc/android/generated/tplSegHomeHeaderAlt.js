function initializetplSegHomeHeaderAlt() {
    flxHeaderSegHomeListAlt = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxHeaderSegHomeListAlt",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxGreenBgSkin"
    }, {}, {});
    flxHeaderSegHomeListAlt.setDefaultUnit(kony.flex.DP);
    var lblTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblTitle",
        "isVisible": true,
        "left": "4%",
        "skin": "lblWhiteNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "92%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeaderSegHomeListAlt.add(lblTitle);
}