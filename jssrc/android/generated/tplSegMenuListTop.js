function initializetplSegMenuListTop() {
    flxRowSegMenuListTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45dp",
        "id": "flxRowSegMenuListTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin"
    }, {}, {});
    flxRowSegMenuListTop.setDefaultUnit(kony.flex.DP);
    var imgBtnIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "30dp",
        "id": "imgBtnIcon",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "settings.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblItem = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItem",
        "isVisible": true,
        "left": "18%",
        "skin": "lblGrayLargeSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRowSegMenuListTop.add(imgBtnIcon, lblItem);
}