if (typeof(SyncConfigDataPc) === "undefined") {
    SyncConfigDataPc = {};
}
//API call will trigger SyncConfigDataPc reset
SyncConfigDataPc.reset = function(successcallback, errorcallback) {
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    kony.sync.scopeReset("SyncConfigDataPc", successcallback, errorcallback);
}
if (typeof(com) === "undefined") {
    com = {};
}
if (typeof(com.pc) === "undefined") {
    com.pc = {};
}
if (typeof(com.pc.employees) === "undefined") {
    com.pc.employees = {};
}
if (typeof(com.pc.employees.SyncEmployeesPc) === "undefined") {
    com.pc.employees.SyncEmployeesPc = {};
}
//API call will trigger SyncEmployeesPc reset
com.pc.employees.SyncEmployeesPc.reset = function(successcallback, errorcallback) {
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    kony.sync.scopeReset("SyncEmployeesPc", successcallback, errorcallback);
}
if (typeof(com) === "undefined") {
    com = {};
}
if (typeof(com.pc) === "undefined") {
    com.pc = {};
}
if (typeof(com.pc.icabinstances) === "undefined") {
    com.pc.icabinstances = {};
}
if (typeof(com.pc.icabinstances.SynciCabInstancesPc) === "undefined") {
    com.pc.icabinstances.SynciCabInstancesPc = {};
}
//API call will trigger SynciCabInstancesPc reset
com.pc.icabinstances.SynciCabInstancesPc.reset = function(successcallback, errorcallback) {
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    kony.sync.scopeReset("SynciCabInstancesPc", successcallback, errorcallback);
}
if (typeof(com) === "undefined") {
    com = {};
}
if (typeof(com.pc) === "undefined") {
    com.pc = {};
}
if (typeof(com.pc.portfolios) === "undefined") {
    com.pc.portfolios = {};
}
if (typeof(com.pc.portfolios.SyncPortfoliosPc) === "undefined") {
    com.pc.portfolios.SyncPortfoliosPc = {};
}
//API call will trigger SyncPortfoliosPc reset
com.pc.portfolios.SyncPortfoliosPc.reset = function(successcallback, errorcallback) {
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    kony.sync.scopeReset("SyncPortfoliosPc", successcallback, errorcallback);
}
if (typeof(com) === "undefined") {
    com = {};
}
if (typeof(com.pc) === "undefined") {
    com.pc = {};
}
if (typeof(com.pc.serviceareas) === "undefined") {
    com.pc.serviceareas = {};
}
if (typeof(com.pc.serviceareas.SyncServiceAreasPc) === "undefined") {
    com.pc.serviceareas.SyncServiceAreasPc = {};
}
//API call will trigger SyncServiceAreasPc reset
com.pc.serviceareas.SyncServiceAreasPc.reset = function(successcallback, errorcallback) {
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    kony.sync.scopeReset("SyncServiceAreasPc", successcallback, errorcallback);
}
if (typeof(com) === "undefined") {
    com = {};
}
if (typeof(com.pc) === "undefined") {
    com.pc = {};
}
if (typeof(com.pc.servicecovers) === "undefined") {
    com.pc.servicecovers = {};
}
if (typeof(com.pc.servicecovers.SyncServiceCoversPc) === "undefined") {
    com.pc.servicecovers.SyncServiceCoversPc = {};
}
//API call will trigger SyncServiceCoversPc reset
com.pc.servicecovers.SyncServiceCoversPc.reset = function(successcallback, errorcallback) {
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    kony.sync.scopeReset("SyncServiceCoversPc", successcallback, errorcallback);
}
if (typeof(SyncWorkDayEndPc) === "undefined") {
    SyncWorkDayEndPc = {};
}
//API call will trigger SyncWorkDayEndPc reset
SyncWorkDayEndPc.reset = function(successcallback, errorcallback) {
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    kony.sync.scopeReset("SyncWorkDayEndPc", successcallback, errorcallback);
}
if (typeof(SyncWorkDayStartPc) === "undefined") {
    SyncWorkDayStartPc = {};
}
//API call will trigger SyncWorkDayStartPc reset
SyncWorkDayStartPc.reset = function(successcallback, errorcallback) {
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    kony.sync.scopeReset("SyncWorkDayStartPc", successcallback, errorcallback);
}
if (typeof(SyncWorkHistoryPc) === "undefined") {
    SyncWorkHistoryPc = {};
}
//API call will trigger SyncWorkHistoryPc reset
SyncWorkHistoryPc.reset = function(successcallback, errorcallback) {
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    kony.sync.scopeReset("SyncWorkHistoryPc", successcallback, errorcallback);
}
if (typeof(com) === "undefined") {
    com = {};
}
if (typeof(com.pc) === "undefined") {
    com.pc = {};
}
if (typeof(com.pc.workloads) === "undefined") {
    com.pc.workloads = {};
}
if (typeof(com.pc.workloads.SyncWorkLoadsPc) === "undefined") {
    com.pc.workloads.SyncWorkLoadsPc = {};
}
//API call will trigger SyncWorkLoadsPc reset
com.pc.workloads.SyncWorkLoadsPc.reset = function(successcallback, errorcallback) {
        if (!kony.sync.isSyncInitialized(errorcallback)) {
            return;
        }
        kony.sync.scopeReset("SyncWorkLoadsPc", successcallback, errorcallback);
    }
    // **********************************Start Scope definition************************
konysyncClientSyncConfig = {
        "ArrayOfSyncScope": [{
            "DataSource": "bc9b6213-765d-4982-8a51-c3ef3f03cea7",
            "ScopeName": "SyncConfigDataPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "actionStandard",
                "Columns": [{
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "actionStandardID",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "actionStandardShortText",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "actionStandardLongText",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "actionDeleted",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }],
                "Pk_Columns": ["countryCode", "businessCode", "actionStandardID"],
                "Relationships": {}
            }, {
                "Name": "issue",
                "Columns": [{
                    "Name": "issueTypeCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueDescription",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["issueTypeCode", "businessCode", "issueCode", "countryCode"],
                "Relationships": {}
            }, {
                "Name": "issueType",
                "Columns": [{
                    "Name": "issueTypeCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueTypeDescription",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["issueTypeCode"],
                "Relationships": {}
            }, {
                "Name": "preparations",
                "Columns": [{
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "preparationCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "preparationName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "preparationMeasureBy",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "preparationUsedFor",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["countryCode", "businessCode", "preparationCode"],
                "Relationships": {}
            }, {
                "Name": "workOrderType",
                "Columns": [{
                    "Name": "isInstallation",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "isFollowUp",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "updateStateOfService",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "isCallOut",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "WorkOrderTypeDescription",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "WorkOrderTypeCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["businessCode", "countryCode", "WorkOrderTypeCode"],
                "Relationships": {}
            }]
        }, {
            "DataSource": "33ddc1e3-d348-4a18-a4b4-f1455a911640",
            "ScopeName": "SyncEmployeesPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "Employees",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeBranchNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeLeavingDate",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeName1",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeNameLast",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeOccupationCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeSms",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeSupervisorCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeWorkEmail",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["businessCode", "countryCode"],
                "Relationships": {}
            }]
        }, {
            "DataSource": "9b4350cd-db96-41fa-b347-375b4ac47cb5",
            "ScopeName": "SynciCabInstancesPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "iCabinstancesPc",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "email",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "iCABSServer",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["businessCode", "countryCode", "email", "iCABSServer"],
                "Relationships": {}
            }]
        }, {
            "DataSource": "2506f97b-db3b-4e6a-aeee-a9d7d4ec810c",
            "ScopeName": "SyncPortfoliosPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "account",
                "Columns": [{
                    "Name": "accountCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "accountName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "nationalAccount",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["accountCode", "businessCode", "countryCode"],
                "Relationships": {
                    "OneToMany": [{
                        "TargetObject": "contract",
                        "Cascade": "true",
                        "RelationshipAttributes": [{
                            "SourceObject_Attribute": "accountCode",
                            "TargetObject_Attribute": "accountCode"
                        }, {
                            "SourceObject_Attribute": "businessCode",
                            "TargetObject_Attribute": "businessCode"
                        }, {
                            "SourceObject_Attribute": "countryCode",
                            "TargetObject_Attribute": "countryCode"
                        }]
                    }]
                }
            }, {
                "Name": "contract",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "contractCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "contractName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "contractTypeCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "accountCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["businessCode", "contractCode", "countryCode"],
                "Relationships": {}
            }, {
                "Name": "property",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "gpsCoordinateX",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "gpsCoordinateY",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyAddressLine1",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyAddressLine2",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyAddressLine3",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyAddressLine4",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyAddressLine5",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "propertyContactMobile",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyContactTelephone",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyPostcode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyContactEmail",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyContactName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyContactPosition",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyContactFax",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }],
                "Pk_Columns": ["businessCode", "countryCode", "propertyCode"],
                "Relationships": {}
            }, {
                "Name": "propertycontract",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "contractCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "csiEnabled",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "propertyServiceNote",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertySpecialInstructions",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["businessCode", "contractCode", "countryCode", "propertyCode"],
                "Relationships": {}
            }, {
                "Name": "servicePoint",
                "Columns": [{
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "contractCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceLineCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceCoverNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "servicePointId",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "servicePointDescription",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "servicePointOrder",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "classedAsInternal",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "detectorId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "physicalId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "physicalIdType",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "quantityCurrent",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "quantityOutstanding",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "siteDivisionLevel",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "siteDivisionId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["countryCode", "businessCode", "contractCode", "propertyCode", "servicePointId"],
                "Relationships": {}
            }]
        }, {
            "DataSource": "132df26b-f47c-42e6-93bb-bff6859f871c",
            "ScopeName": "SyncServiceAreasPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "ServiceAreaPc",
                "Columns": [{
                    "Name": "assignedArea",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "branchNumber",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "email",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeforename1",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeSurname",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "errorDescription",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "errorNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "languageCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceAreaCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceAreaDeleted",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "updateDateTime",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "timestamp"
                }, {
                    "Name": "serviceAreaDescription",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["branchNumber", "businessCode", "countryCode", "serviceAreaCode"],
                "Relationships": {}
            }]
        }, {
            "DataSource": "ec2753b4-a8c6-4771-940e-5362c7702421",
            "ScopeName": "SyncServiceCoversPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "servicecover",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "contractCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceBranchNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "serviceCoverNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "serviceCoverSequence",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "serviceLineCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceLineDescription",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceSpecialInstructions",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["businessCode", "contractCode", "countryCode", "propertyCode", "serviceLineCode"],
                "Relationships": {}
            }]
        }, {
            "DataSource": "9560ec15-cc90-4af5-97c8-ad704f8b961d",
            "ScopeName": "SyncWorkDayEndPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "workDayEnd",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "email",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "languageCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workingDayDateEnd",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workingDayTimeEnd",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "createDateTime",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "lastUpdatedTimeStamp",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "timestamp"
                }],
                "Pk_Columns": ["businessCode", "countryCode", "workingDayDateEnd", "workingDayTimeEnd"],
                "Relationships": {}
            }]
        }, {
            "DataSource": "9560ec15-cc90-4af5-97c8-ad704f8b961d",
            "ScopeName": "SyncWorkDayStartPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "workDayStart",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "email",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "languageCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workingDayDateStart",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workingDayTimeStart",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "createDateTime",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "lastUpdateTimestamp",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "timestamp"
                }],
                "Pk_Columns": ["businessCode", "countryCode", "workingDayDateStart", "workingDayTimeStart"],
                "Relationships": {}
            }]
        }, {
            "DataSource": "73b0f311-014d-4923-b319-c656ce27aa5d",
            "ScopeName": "SyncWorkHistoryPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "workHistory",
                "Columns": [{
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyCode",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workNumber",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "employeeCodePrimary",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueCodeNoSignature",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "customerApproval",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "customerApprovalName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workDateStartActual",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueTypeCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueTypeCodeNoSignature",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "customerApprovalSignature",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["countryCode", "businessCode", "propertyCode", "workNumber"],
                "Relationships": {}
            }]
        }, {
            "DataSource": "ad01499c-a957-465f-9f9e-4625af645f20",
            "ScopeName": "SyncWorkLoadsPc",
            "ScopeDatabaseName": "100004898512bd8be",
            "Strategy": "OTA_SYNC",
            "ScopeTables": [{
                "Name": "treatment",
                "Columns": [{
                    "Name": "treatmentId",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": false,
                    "type": "string"
                }, {
                    "Name": "workPointId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "treatmentBatch",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "treatmentPreparationCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "treatmentQuantity",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "treatmentRecordedDateTime",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["treatmentId"],
                "Relationships": {}
            }, {
                "Name": "work",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeCodePrimary",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workDateStartPlanned",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workId",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workStatus",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "customerApproval",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "customerApprovalName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "customerApprovalSignature",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "blob"
                }, {
                    "Name": "issueCodeNoSignature",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workDateStartActual",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueTypeCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueTypeCodeNoSignature",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "emailDistributionList",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceReceiptEmailSubject",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcard",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "outcardCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardPropertyName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardPropertyAddressLine1",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardPropertyAddressLine2",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardPropertyAddressLine3",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardPropertyAddressLine4",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardPropertyAddressLine5",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardPropertyPostcode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardPropertyContactName",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardPropertyContactTelephone",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "outcardOfficeRef",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "capturedPhoto",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["workId"],
                "Relationships": {
                    "OneToMany": [{
                        "TargetObject": "workAttendance",
                        "Cascade": "true",
                        "RelationshipAttributes": [{
                            "SourceObject_Attribute": "workId",
                            "TargetObject_Attribute": "workId"
                        }, {
                            "SourceObject_Attribute": "businessCode",
                            "TargetObject_Attribute": "businessCode"
                        }, {
                            "SourceObject_Attribute": "countryCode",
                            "TargetObject_Attribute": "countryCode"
                        }]
                    }, {
                        "TargetObject": "workOrder",
                        "Cascade": "true",
                        "RelationshipAttributes": [{
                            "SourceObject_Attribute": "workId",
                            "TargetObject_Attribute": "workId"
                        }, {
                            "SourceObject_Attribute": "businessCode",
                            "TargetObject_Attribute": "businessCode"
                        }, {
                            "SourceObject_Attribute": "countryCode",
                            "TargetObject_Attribute": "countryCode"
                        }]
                    }]
                }
            }, {
                "Name": "workAttendance",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeCodePrimary",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workAttendanceDateTimeStart",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workAttendanceId",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workAttendanceStatus",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workAttendanceTimeDuration",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "workId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["workAttendanceId"],
                "Relationships": {}
            }, {
                "Name": "workOrder",
                "Columns": [{
                    "Name": "businessCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "contractCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "countryCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "quantityPlanned",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "sequenceNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "estimatedEffort",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceCoverNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "serviceLineCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workOrderDateStartPlanned",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workorderId",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workOrderPlannerNotes",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workOrderStatus",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workOrderTypeCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workStatusLoc",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workLocOrder",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "isCompletedVisit",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "boolean"
                }, {
                    "Name": "planVisitNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "issueTypeCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "quantityActual",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workOrderDateStartActual",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workOrderVisitNotes",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "actualEffort",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "csiVisitReference",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "followUpDate",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "followUpTime",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "followUpDuration",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }],
                "Pk_Columns": ["workorderId"],
                "Relationships": {}
            }, {
                "Name": "workPoint",
                "Columns": [{
                    "Name": "workPointId",
                    "Autogenerated": "false",
                    "IsPrimaryKey": true,
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "activationMethod",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "contractCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "propertyCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceLineCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceCoverNumber",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "employeeCodeActual",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "employeeCodePlanned",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "issueTypeCode",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "servicePointId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "serviceQuantityActual",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "serviceQuantityPlanned",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }, {
                    "Name": "workAttendanceId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workOrderId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workId",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workPointActivationDateTime",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "string"
                }, {
                    "Name": "workPointEffort",
                    "Autogenerated": "false",
                    "IsNullable": true,
                    "type": "int"
                }],
                "Pk_Columns": ["workPointId"],
                "Relationships": {}
            }]
        }],
        "ArrayOfDataSource": [{
            "ID": "bc9b6213-765d-4982-8a51-c3ef3f03cea7",
            "type": "JSON"
        }, {
            "ID": "33ddc1e3-d348-4a18-a4b4-f1455a911640",
            "type": "JSON"
        }, {
            "ID": "9b4350cd-db96-41fa-b347-375b4ac47cb5",
            "type": "JSON"
        }, {
            "ID": "2506f97b-db3b-4e6a-aeee-a9d7d4ec810c",
            "type": "JSON"
        }, {
            "ID": "132df26b-f47c-42e6-93bb-bff6859f871c",
            "type": "JSON"
        }, {
            "ID": "ec2753b4-a8c6-4771-940e-5362c7702421",
            "type": "JSON"
        }, {
            "ID": "9560ec15-cc90-4af5-97c8-ad704f8b961d",
            "type": "JSON"
        }, {
            "ID": "73b0f311-014d-4923-b319-c656ce27aa5d",
            "type": "JSON"
        }, {
            "ID": "ad01499c-a957-465f-9f9e-4625af645f20",
            "type": "JSON"
        }],
        "AppID": "100004898512bd8be",
        "Version": "45d8d3f8231da33498fbb00fafb1dd68cb066e74df6285f184d055cb7043ae26"
    }
    //**********************************End Scope definition************************