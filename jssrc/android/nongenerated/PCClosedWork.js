/**  Description : Integration service class **/
function invokeIntegrationService(params, curWorkId) {
    /**  Description : success call back for closed work service **/
    this.createClosedWorkSuccessCallBack = function(closedWorkRes) {
        printMessage("Inside createClosedWorkService - createClosedWorkSuccessCallBack: " + JSON.stringify(closedWorkRes));
        var dummyWorkId = "DUMMY_WORK_ID";
        if (!isEmpty(closedWorkRes) && !isEmpty(closedWorkRes.errorNumber) && parseInt(closedWorkRes.errorNumber) === 0) {
            printMessage("Inside createClosedWorkService - createClosedWorkSuccessCallBack: Successfully closed work: " + closedWorkRes.WorkId);
            gblClosedWorkData.succeededWorkIds.push(closedWorkRes.WorkId);
        } else if (!isEmpty(closedWorkRes) && !isEmpty(closedWorkRes.errorNumber) && closedWorkRes.errorNumber > 20000) {
            printMessage("Inside createClosedWorkService - createClosedWorkSuccessCallBack: iCabs throws an error");
            if (!isEmpty(closedWorkRes.WorkId)) {
                gblClosedWorkData.failedWorkIds.push(closedWorkRes.WorkId);
            } else {
                gblClosedWorkData.failedWorkIds.push(dummyWorkId); /** Here we don't get the details of failed close work. So putting a dummy value in this array to make total service call count equal to the sum of failed and succeeded service calls **/
            }
        } else {
            printMessage("Inside createClosedWorkService - createClosedWorkSuccessCallBack: ESB layer throws an error");
            if (!isEmpty(closedWorkRes.WorkId)) {
                gblClosedWorkData.failedWorkIds.push(closedWorkRes.WorkId);
            } else {
                gblClosedWorkData.failedWorkIds.push(dummyWorkId); /** Here we don't get the details of failed close work. So putting a dummy value in this array to make total service call count equal to the sum of failed and succeeded service calls **/
            }
        }
        initiate15MinSyncCall();
    };
    /**  Description : failure call back for closed work service **/
    this.createClosedWorkFailureCallBack = function(error) {
        printMessage("Inside createClosedWorkService - createClosedWorkFailureCallBack: " + JSON.stringify(error));
        var dummyWorkId = "DUMMY_WORK_ID";
        gblClosedWorkData.failedWorkIds.push(dummyWorkId); /** Here we don't get the details of failed close work. So putting a dummy value in this array to make total service call count equal to the sum of failed and succeeded service calls **/
        initiate15MinSyncCall();
    };
    var integrationClient = null;
    var serviceName = "WorkloadsPc";
    var operationName = "closedWork";
    var headers = null;
    try {
        printMessage("Inside createClosedWorkService - serviceName: " + serviceName);
        printMessage("Inside createClosedWorkService - operationName: " + operationName);
        printMessage("Inside createClosedWorkService - params: " + JSON.stringify(params));
        gblClosedWorkData.processedWorkIds.push(curWorkId);
        integrationClient = KNYMobileFabric.getIntegrationService(serviceName);
        integrationClient.invokeOperation(operationName, headers, params, this.createClosedWorkSuccessCallBack, this.createClosedWorkFailureCallBack);
    } catch (exception) {
        printMessage("Inside createClosedWorkService - Exception: " + exception.message);
    }
}
/** calling the closed work integrated service **/
function createClosedWorkService() {
    printMessage("Inside createClosedWorkService");
    try {
        if (!isEmpty(gblClosedWorkData) && !isEmpty(gblClosedWorkData.works) && gblClosedWorkData.works.length > 0) {
            var totalClosedWorks = gblClosedWorkData.works.length;
            var params = null;
            var wkRecord = null;
            var waRecord = null;
            var woRecord = null;
            var wpRecord = null;
            var workCol = [];
            var workAttendanceCol = [];
            var workOrderCol = [];
            var workPointCol = [];
            var treatmentCol = [];
            var anIntegrationClient = null;
            for (var i = 0; i < totalClosedWorks; i++) {
                params = {};
                params.businessCode = gblClosedWorkData.businessCode;
                params.countryCode = gblClosedWorkData.countryCode;
                params.email = gblClosedWorkData.email;
                params.languageCode = gblClosedWorkData.languageCode;
                params.work = "";
                params.workAttendance = "";
                params.workOrder = "";
                params.workPoint = "";
                params.treatment = "";
                params.client_id = "e4dfc2b9d3fa474dbacbe1d9048745f2";
                params.client_secret = "0055aa2686434b58AEA5F6691321A0A3";
                /*  params.client_id            = "c89acbf594ec450480d9dbf518a0f4fe";
                  params.client_secret        = "0c3d0df4ffc74563BDF5AA7D001F5CAC";  */
                wkRecord = gblClosedWorkData.works[i];
                workCol = [];
                workCol.push(wkRecord);
                params.work = (gblAddEncodeURI === true) ? encodeURI(JSON.stringify(workCol)) : JSON.stringify(workCol);
                printMessage("Inside createClosedWorkService - work - : " + params.work);
                printMessage("Inside createClosedWorkService - work - : " + JSON.stringify(workCol));
                waRecord = gblClosedWorkData.workAttendances[i];
                workAttendanceCol = [];
                workAttendanceCol.push(waRecord);
                if (!isEmpty(workAttendanceCol) && workAttendanceCol.length > 0) {
                    params.workAttendance = (gblAddEncodeURI === true) ? encodeURI(JSON.stringify(workAttendanceCol)) : JSON.stringify(workAttendanceCol);
                }
                printMessage("Inside createClosedWorkService - workAttendance - : " + params.workAttendance);
                printMessage("Inside createClosedWorkService - workAttendance - : " + JSON.stringify(workAttendanceCol));
                woRecord = gblClosedWorkData.workOrders[i];
                workOrderCol = [];
                workOrderCol.push(woRecord);
                if (!isEmpty(workOrderCol) && workOrderCol.length > 0) {
                    params.workOrder = (gblAddEncodeURI === true) ? encodeURI(JSON.stringify(workOrderCol)) : JSON.stringify(workOrderCol);
                }
                printMessage("Inside createClosedWorkService - workOrder - : " + params.workOrder);
                printMessage("Inside createClosedWorkService - workOrder - : " + JSON.stringify(workOrderCol));
                wpRecord = gblClosedWorkData.workPoints[i];
                workPointCol = [];
                workPointCol.push(wpRecord);
                if (!isEmpty(workPointCol) && workPointCol.length > 0) {
                    params.workPoint = (gblAddEncodeURI === true) ? encodeURI(JSON.stringify(workPointCol)) : JSON.stringify(workPointCol);
                }
                printMessage("Inside createClosedWorkService - workPoint - : " + params.workPoint);
                printMessage("Inside createClosedWorkService - workPoint - : " + JSON.stringify(workPointCol));
                treatmentCol = gblClosedWorkData.getTreatmentCol(wpRecord.workPointId);
                if (!isEmpty(treatmentCol) && treatmentCol.length > 0) {
                    params.treatment = (gblAddEncodeURI === true) ? encodeURI(JSON.stringify(treatmentCol)) : JSON.stringify(treatmentCol);
                } else {
                    params.treatment = JSON.stringify([]);
                }
                printMessage("Inside createClosedWorkService - treatment - : " + params.treatment);
                printMessage("Inside createClosedWorkService - treatment - : " + JSON.stringify(treatmentCol));
                anIntegrationClient = new invokeIntegrationService(params, wkRecord.workId);
            }
        } else {
            initiate15MinSyncCall();
        }
    } catch (e) {
        printMessage("Error: " + e);
    }
}
/**  To initiate 15 minutes sync call - Once all close work services are processed **/
function initiate15MinSyncCall() {
    printMessage("Inside createClosedWorkService - initiate15MinSyncCall");
    printMessage("Inside createClosedWorkService - processedWorkIds - count:" + gblClosedWorkData.processedWorkIds.length);
    printMessage("Inside createClosedWorkService - failedWorkIds-count:" + gblClosedWorkData.failedWorkIds.length);
    printMessage("Inside createClosedWorkService - succeededWorkIds-count:" + gblClosedWorkData.succeededWorkIds.length);
    if (gblClosedWorkData.processedWorkIds.length === (gblClosedWorkData.failedWorkIds.length + gblClosedWorkData.succeededWorkIds.length)) {
        printMessage("Inside createClosedWorkService - initiate15MinSyncCall - Call 15minutes sync");
        //gblSyncedWorkIds		= gblClosedWorkData.processedWorkIds;	
        //deleteClosedWorkRecords();
        updateWorkStatusAsCompletedInLocalTbl(gblClosedWorkData.processedWorkIds, gblClosedWorkData.succeededWorkIds);
        start15MinutesSyncScheduler();
    }
}
/** Description : Forming input parameters to closed work service **/
function getClosedWorkData() {
    printMessage("Inside createClosedWorkService - getClosedWorkData");
    try {
        var selectedIcabInstance = gblMyAppDataObject.getIcabInstance();
        gblClosedWorkData = new closeWorkData();
        if (!isEmpty(selectedIcabInstance)) {
            gblClosedWorkData.businessCode = selectedIcabInstance.businessCode;
            gblClosedWorkData.countryCode = selectedIcabInstance.countryCode;
            gblClosedWorkData.email = selectedIcabInstance.email;
        }
        printMessage("Inside createClosedWorkService - getClosedWorkData: " + JSON.stringify(gblClosedWorkData));
        getClosedWorksFromTable();
    } catch (e) {
        printMessage("Error: " + e);
    }
}
/** Success callback - Get closed works from local db */
function success_getClosedWorks(res) {
    printMessage(" Inside success_getClosedWorks ");
    printMessage(" Inside success_getClosedWorks - resultset: " + JSON.stringify(res));
    if (!isEmpty(res) && res.length > 0) {
        var wkRecord = null; // Work
        var waRecord = null; // WorkAttendance
        var woRecord = null; // WorkOrder
        var wpRecord = null; // WorkPoint
        var closedWorksLength = res.length;
        printMessage(" Inside success_getClosedWorks - closedWorksLength: " + closedWorksLength);
        gblClosedWorkData.works = [];
        gblClosedWorkData.workAttendances = [];
        gblClosedWorkData.workOrders = [];
        gblClosedWorkData.workPoints = [];
        gblClosedWorkData.treatments = {};
        gblWorkPointIds = [];
        for (var i = 0; i < closedWorksLength; i++) {
            wkRecord = {};
            wkRecord.workId = res[i].workId;
            printMessage("Inside success_getClosedWorks Signature base64 - length: " + res[i].customerApprovalSignature.length);
            wkRecord.propertyCode = res[i].propertyCode;
            wkRecord.customerApproval = res[i].customerApproval;
            wkRecord.customerApprovalName = res[i].customerApprovalName;
            wkRecord.employeeCodePrimary = res[i].employeeCodePrimary;
            wkRecord.issueTypeCode = res[i].issueTypeCode;
            wkRecord.issueCode = res[i].issueCode;
            wkRecord.workDateStartActual = res[i].workDateStartActual;
            /*    wkRecord.outcard 						= res[i].outcard;
                wkRecord.outcardCode 					= res[i].outcardCode;
                wkRecord.outcardPropertyName 			= res[i].outcardPropertyName;
                wkRecord.outcardPropertyAddressLine1 	= res[i].outcardPropertyAddressLine1;
                wkRecord.outcardPropertyAddressLine2 	= res[i].outcardPropertyAddressLine2;
                wkRecord.outcardPropertyAddressLine3 	= res[i].outcardPropertyAddressLine3;
                wkRecord.outcardPropertyAddressLine4 	= res[i].outcardPropertyAddressLine4;
                wkRecord.outcardPropertyAddressLine5 	= res[i].outcardPropertyAddressLine5;
                wkRecord.outcardPropertyPostcode 		= res[i].outcardPropertyPostcode;
                wkRecord.outcardPropertyContactName 	= res[i].outcardPropertyContactName;
                wkRecord.outcardPropertyContactTelephone= res[i].outcardPropertyContactTelephone;
                wkRecord.outcardOfficeRef 				= res[i].outcardOfficeRef;     */
            if (!isEmpty(res[i].customerApprovalSignature)) {
                wkRecord.issueTypeCodeNoSignature = "";
                wkRecord.issueCodeNoSignature = "";
                // 	wkRecord.customerApprovalSignature 	= res[i].customerApprovalSignature;
                var custSign = ConvertImageJPEG.convertImageType(res[i].customerApprovalSignature);
                custSign = custSign.replace(/\n/g, '');
                wkRecord.customerApprovalSignature = custSign;
            } else {
                wkRecord.issueTypeCodeNoSignature = res[i].issueTypeCodeNoSignature;
                wkRecord.issueCodeNoSignature = res[i].issueCodeNoSignature;
                wkRecord.customerApprovalSignature = "";
            }
            //wkRecord.serviceReceiptHtml 			= res[i].serviceReceiptHtml;
            //wkRecord.workDeleted 					= res[i].workDeleted;
            //wkRecord.updateDateTime 				= res[i].updateDateTime;
            gblClosedWorkData.works.push(wkRecord);
            waRecord = {};
            waRecord.workAttendanceId = res[i].workAttendanceId;
            waRecord.workId = res[i].workId;
            waRecord.employeeCode = res[i].employeeCodePrimary;
            waRecord.workAttendanceDateTimeStart = res[i].workAttendanceDateTimeStart;
            waRecord.workAttendanceTimeDuration = res[i].workAttendanceTimeDuration;
            if (waRecord.workAttendanceTimeDuration === 0) {
                waRecord.workAttendanceTimeDuration = 60; // It shouldn't be 0
            }
            //waRecord.workAttendanceDeleted 		= res[i].workAttendanceDeleted;
            //waRecord.updateDateTime 				= res[i].updateDateTime;
            gblClosedWorkData.workAttendances.push(waRecord);
            woRecord = {};
            woRecord.workOrderId = res[i].workorderId;
            woRecord.workId = res[i].workId;
            woRecord.contractCode = res[i].contractCode;
            woRecord.propertyCode = res[i].propertyCode;
            woRecord.serviceLineCode = res[i].serviceLineCode;
            woRecord.serviceCoverNumber = res[i].serviceCoverNumber;
            woRecord.issueTypeCode = res[i].issueTypeCode;
            woRecord.issueCode = res[i].issueCode;
            woRecord.quantityActual = res[i].quantityPlanned;
            woRecord.workOrderDateStartActual = res[i].workOrderDateStartActual;
            woRecord.workOrderTypeCode = res[i].workOrderTypeCode;
            woRecord.workOrderVisitNotes = res[i].workOrderVisitNotes;
            woRecord.actualEffort = res[i].actualEffort; //parseInt(actualEffort)
            if (woRecord.actualEffort === 0) {
                woRecord.actualEffort = 60; // It shouldn't be 0
            }
            woRecord.planVisitNumber = res[i].planVisitNumber;
            woRecord.csiVisitReference = res[i].csiVisitReference;
            //woRecord.followUpDate 				= res[i].followUpDate; 
            //woRecord.followUpTime 				= res[i].followUpTime;
            //woRecord.followUpDuration 			= res[i].followUpDuration;
            //woRecord.workOrderDeleted 				= res[i].workOrderDeleted;
            //woRecord.updateDateTime 				= res[i].updateDateTime;
            gblClosedWorkData.workOrders.push(woRecord);
            wpRecord = {};
            wpRecord.workPointId = res[i].workPointId;
            gblWorkPointIds.push(res[i].workPointId);
            wpRecord.activationMethod = res[i].activationMethod;
            wpRecord.contractCode = res[i].contractCode;
            wpRecord.propertyCode = res[i].propertyCode;
            wpRecord.serviceLineCode = res[i].serviceLineCode;
            wpRecord.serviceCoverNumber = res[i].serviceCoverNumber;
            wpRecord.employeeCodeActual = res[i].employeeCodeActual;
            wpRecord.employeeCodePlanned = res[i].employeeCodePlanned;
            wpRecord.issueCode = res[i].issueCode;
            wpRecord.issueTypeCode = res[i].issueTypeCode;
            wpRecord.servicePointId = res[i].servicePointId;
            wpRecord.workAttendanceId = res[i].workAttendanceId;
            wpRecord.workOrderId = res[i].workorderId;
            wpRecord.workId = res[i].workId;
            wpRecord.workPointActivationDateTime = res[i].workPointActivationDateTime;
            wpRecord.workPointEffort = res[i].workPointEffort;
            if (wpRecord.workPointEffort === 0) {
                wpRecord.workPointEffort = 60; // It shouldn't be 0
            }
            //wpRecord.serviceQuantityActual 		= res[i].serviceQuantityActual; 
            //wpRecord.serviceQuantityPlanned 		= res[i].serviceQuantityPlanned; 
            //wpRecord.workPointDeleted 			= res[i].workPointDeleted;
            //wpRecord.updateDateTime 				= res[i].updateDateTime;
            gblClosedWorkData.workPoints.push(wpRecord);
        }
        getTreatmentsFromTable();
    } else {
        start15MinutesSyncScheduler();
    }
}
/** Failure callback - Get closed works from local db */
function error_getClosedWorks(error) {
    printMessage(" Inside error_getClosedWorks ");
    printMessage(" Inside error_getClosedWorks - error : " + JSON.stringify(error));
    start15MinutesSyncScheduler();
}
/** Success callback - Get treatments from local db */
function success_getTreatments(res) {
    printMessage(" Inside success_getTreatments ");
    if (!isEmpty(res) && res.length > 0) {
        printMessage("Inside success_getTreatments - res: " + JSON.stringify(res));
        var aRecord = null;
        var wkPointId = "";
        var treatmentLength = res.length;
        printMessage("Inside success_getTreatments - treatmentLength: " + treatmentLength);
        gblClosedWorkData.treatments = {};
        for (var i = 0; i < treatmentLength; i++) {
            aRecord = {};
            aRecord.treatmentId = res[i].treatmentId;
            aRecord.workPointId = res[i].workPointId;
            wkPointId = res[i].workPointId;
            aRecord.treatmentBatch = res[i].treatmentBatch;
            aRecord.treatmentPreparationCode = res[i].treatmentPreparationCode;
            aRecord.treatmentQuantity = res[i].treatmentQuantity;
            aRecord.treatmentRecordedDateTime = res[i].treatmentRecordedDateTime;
            //aRecord.treatmentDeleted 				= res[i].treatmentDeleted;
            //aRecord.updateDateTime 				= res[i].updateDateTime;	            
            if (isEmpty(gblClosedWorkData.treatments[wkPointId])) {
                gblClosedWorkData.treatments[wkPointId] = [];
            }
            gblClosedWorkData.treatments[wkPointId].push(aRecord);
        }
        printMessage("Inside success_getTreatments - gblClosedWorkData.treatments: " + JSON.stringify(gblClosedWorkData.treatments));
    }
    createClosedWorkService();
}
/** Failure callback - Get treatments from local db */
function error_getTreatments() {
    printMessage(" Inside error_getTreatments ");
    createClosedWorkService();
}