/** Show Complete Visit Details screen **/
function showCompleteVisitScreen() {
    gblCompletedVisitFlag = true;
    CompletedVisit.show();
}
/** Complete Visit Details screen - PreShow **/
function onPreShowCompleteDetailsScreen() {
    printMessage("Inside - onPreShowCompleteVisitDetailsScreen");
    refreshi18nCompleteVisit();
    CompletedVisit.tabCompletedCustomerDetails.activeTabs = [0];
    getPreparationsFromLocalTbl();
    getIssueCodesFromLocalTbl(PCConstant.SIGNATURE_ISSUE_TYPE_CODE);
}
/** Complete Visit Details - PostShow **/
function onPostShowCompleteVisitScreen() {
    printMessage("Inside - onPostShowCompleteVisitScreen ");
    printMessage("Inside - onCompletedJobsSegRowClick" + JSON.stringify(gblMyAppDataObject.completedJobs));
    printMessage("Inside - onCompletedJobsSegRowClick selected items" + JSON.stringify(Home.segHomeCompletedList.selectedRowItems));
    var completedItems = gblMyAppDataObject.selCompletedItem;
    //printMessage("Inside - onPostShowCompleteVisitScreen completedItems"+JSON.stringify(completedItems));
    CompletedVisit.lblCompletedCustomerName.text = completedItems.propertyName;
    CompletedVisit.tabCompletedCustomerDetails.lblCustomerNumber.text = completedItems.contractTypeCode + "/" + completedItems.contractCode;
    CompletedVisit.tabCompletedCustomerDetails.lblAddress.text = completedItems.propertyAddress;
    CompletedVisit.tabCompletedCustomerDetails.lblPostcode.text = completedItems.propertyPostcode;
    if (!isEmpty(completedItems.propertyContactName)) {
        CompletedVisit.tabCompletedCustomerDetails.lblName.text = completedItems.propertyContactName;
        CompletedVisit.tabCompletedCustomerDetails.lblName.skin = lblBlk100;
    } else {
        CompletedVisit.tabCompletedCustomerDetails.lblName.text = getI18nString("common.label.notAvailable");
        CompletedVisit.tabCompletedCustomerDetails.lblName.skin = lblNotAvailableSkin;
    }
    if (!isEmpty(completedItems.propertyContactPosition)) {
        CompletedVisit.tabCompletedCustomerDetails.lblDesingnation.text = completedItems.propertyContactPosition;
        CompletedVisit.tabCompletedCustomerDetails.lblDesingnation.skin = lblBlk100;
    } else {
        CompletedVisit.tabCompletedCustomerDetails.lblDesingnation.text = getI18nString("common.label.notAvailable");
        CompletedVisit.tabCompletedCustomerDetails.lblDesingnation.skin = lblNotAvailableSkin;
    }
    if (!isEmpty(completedItems.gpsCoordinateX) && !isEmpty(completedItems.gpsCoordinateY)) {
        CompletedVisit.tabCompletedCustomerDetails.imgLocIcon.src = "navigate.png";
        CompletedVisit.tabCompletedCustomerDetails.flxAddressLeftContainer.setEnabled(true);
    } else {
        CompletedVisit.tabCompletedCustomerDetails.imgLocIcon.src = "navigatedisabled.png";
        CompletedVisit.tabCompletedCustomerDetails.flxAddressLeftContainer.setEnabled(false);
    }
    if (!isEmpty(completedItems.propertyContactTelephone)) {
        CompletedVisit.tabCompletedCustomerDetails.lblPhone.text = completedItems.propertyContactTelephone;
        CompletedVisit.tabCompletedCustomerDetails.lblPhone.skin = phoneSkin;
        CompletedVisit.tabCompletedCustomerDetails.imgPhoneIcon.src = "call.png";
        CompletedVisit.tabCompletedCustomerDetails.flxPhoneContainer.setEnabled(true);
    } else {
        CompletedVisit.tabCompletedCustomerDetails.lblPhone.text = getI18nString("common.label.notAvailable");
        CompletedVisit.tabCompletedCustomerDetails.lblPhone.skin = lblNotAvailableSkin;
        CompletedVisit.tabCompletedCustomerDetails.imgPhoneIcon.src = "calldisabled.png";
        CompletedVisit.tabCompletedCustomerDetails.flxPhoneContainer.setEnabled(false);
    }
    if (!isEmpty(completedItems.propertyContactMobile)) {
        CompletedVisit.tabCompletedCustomerDetails.lblMobile.text = completedItems.propertyContactMobile;
        CompletedVisit.tabCompletedCustomerDetails.lblMobile.skin = phoneSkin;
        CompletedVisit.tabCompletedCustomerDetails.imgMobileIcon.src = "mobile.png";
        CompletedVisit.tabCompletedCustomerDetails.flxMobileContainer.setEnabled(true);
    } else {
        CompletedVisit.tabCompletedCustomerDetails.lblMobile.text = getI18nString("common.label.notAvailable");
        CompletedVisit.tabCompletedCustomerDetails.lblMobile.skin = lblNotAvailableSkin;
        CompletedVisit.tabCompletedCustomerDetails.imgMobileIcon.src = "mobiledisabled.png";
        CompletedVisit.tabCompletedCustomerDetails.flxMobileContainer.setEnabled(false);
    }
    if (!isEmpty(completedItems.propertyContactEmail)) {
        CompletedVisit.tabCompletedCustomerDetails.lblEmail.text = completedItems.propertyContactEmail;
        CompletedVisit.tabCompletedCustomerDetails.lblEmail.skin = lblEmailSkin;
        CompletedVisit.tabCompletedCustomerDetails.imgEmailIcon.src = "email.png";
        CompletedVisit.tabCompletedCustomerDetails.flxEmailContainer.setEnabled(true);
    } else {
        CompletedVisit.tabCompletedCustomerDetails.lblEmail.text = getI18nString("common.label.notAvailable");
        CompletedVisit.tabCompletedCustomerDetails.lblEmail.skin = lblNotAvailableSkin;
        CompletedVisit.tabCompletedCustomerDetails.imgEmailIcon.src = "emaildisabled.png";
        CompletedVisit.tabCompletedCustomerDetails.flxEmailContainer.setEnabled(false);
    }
    if (!isEmpty(completedItems.propertySpecialInstructions)) {
        CompletedVisit.tabCompletedCustomerDetails.txaSiteRiskAssessment.text = completedItems.propertySpecialInstructions;
    }
    if (!isEmpty(completedItems.propertyServiceNote)) {
        CompletedVisit.tabCompletedCustomerDetails.txaServiceNotes.text = completedItems.propertyServiceNote;
    } else {
        CompletedVisit.tabCompletedCustomerDetails.txaServiceNotes.text = "";
    }
    CompletedVisit.tabCompletedCustomerDetails.lblStartTimeValue.text = (!isEmpty(completedItems.visitStartTimeStr)) ? completedItems.visitStartTimeStr : "--:--";
    CompletedVisit.tabCompletedCustomerDetails.lblEndTime.text = (!isEmpty(completedItems.visitEndTimeStr)) ? completedItems.visitEndTimeStr : "--:--";
    CompletedVisit.tabCompletedCustomerDetails.lblDurationValue.text = (!isEmpty(completedItems.duration)) ? completedItems.duration : "--:--";
    if (!isEmpty(completedItems.visitNote)) {
        CompletedVisit.tabCompletedCustomerDetails.txtAreaVisitNote.text = completedItems.visitNote;
    } else {
        CompletedVisit.tabCompletedCustomerDetails.txtAreaVisitNote.text = "";
    }
    CompletedVisit.tabCompletedCustomerDetails.txtCustomerNameSig.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.txtAreaVisitNote.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.txaSiteRiskAssessment.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.txaServiceNotes.setEnabled(false);
    loadDataOnHistorySegment(gblMyAppDataObject.historyDatesList, gblMyAppDataObject.historyObjects);
    printMessage("gblMyAppDataObject.serviceCoverCodeList: " + JSON.stringify(gblMyAppDataObject.serviceCoverCodeList));
    printMessage("gblMyAppDataObject.serviceCoverObjects: " + JSON.stringify(gblMyAppDataObject.serviceCoverObjects));
    loadDataOnServiceCoverSegment(gblMyAppDataObject.serviceCoverCodeList, gblMyAppDataObject.serviceCoverObjects);
    dismissLoading();
    destroyFormsExceptMe(PCConstant.FORM_COMPLETEVISIT); /** Issue fix - Swipe operation on Home segment doesn't work if it is not destroyed here **/
}
/** Complete Visit Details screen - Refresh all i18n **/
function refreshi18nCompleteVisit() {
    printMessage("Inside - refreshi18nCompleteVisit: " + JSON.stringify(gblMyAppDataObject.selCompletedItem));
    var anItem = null;
    var i = 0;
    var sampleMasterDataQuantity = [];
    if (gblPrepQuantityList.length > 0) {
        for (i = 0; i < gblPrepQuantityList.length; i++) {
            anItem = gblPrepQuantityList[i];
            sampleMasterDataQuantity.push([anItem.key, anItem.val]);
        }
    }
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb1.masterData = sampleMasterDataQuantity;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb2.masterData = sampleMasterDataQuantity;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb3.masterData = sampleMasterDataQuantity;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb4.masterData = sampleMasterDataQuantity;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb5.masterData = sampleMasterDataQuantity;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb1.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.PreprationsQuantity1)) ? gblMyAppDataObject.selCompletedItem.PreprationsQuantity1 : "0";
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb2.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.PreprationsQuantity2)) ? gblMyAppDataObject.selCompletedItem.PreprationsQuantity2 : "0";
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb3.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.PreprationsQuantity3)) ? gblMyAppDataObject.selCompletedItem.PreprationsQuantity3 : "0";
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb4.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.PreprationsQuantity4)) ? gblMyAppDataObject.selCompletedItem.PreprationsQuantity4 : "0";
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb5.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.PreprationsQuantity5)) ? gblMyAppDataObject.selCompletedItem.PreprationsQuantity5 : "0";
    ComboAccessDisabled();
}
/** To toggle access of no signature reason combobox **/
function ComboAccessDisabled() {
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb1.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb1.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb1.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb2.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb2.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb2.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb3.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb3.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb3.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb4.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb4.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb4.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb5.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb5.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.quatityCmb5.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb1.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb1.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb1.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb2.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb2.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb2.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb3.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb3.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb3.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb4.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb4.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb4.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb5.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb5.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb5.focusSkin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.setEnabled(false);
    CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.skin = sknListBoxDisabled;
    CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.focusSkin = sknListBoxDisabled;
}
/** Tab click functionality **/
function onTabClickCompletedSignature(eventObject, curIndex, isExpanded) {
    //var visitInProgressObj					= null;
    var completedItems = gblMyAppDataObject.selCompletedItem;
    if (curIndex === 7) { /** Customer signature fetching **/
        // alert('inside onTabClickCompletedSignature'+completedItems.workId);
        showLoading("common.message.loading");
        getCustomerSignatureFromLocalTblCompleted(completedItems.workId);
    }
    if (curIndex === 6) { /** Customer signature fetching **/
        // alert('inside onTabClickCompletedSignature'+completedItems.workId);
        showLoading("common.message.loading");
        getCapturedPhotoFromLocalTblCompleted(completedItems.workId);
        //getCustomerSignatureFromLocalTblCompleted(completedItems.workId);
    }
}

function successCallbackCompletedSignData(resultset) {
    printMessage(" Inside - success_getCustomerSignature - resultset: " + JSON.stringify(resultset));
    var completedItems = gblMyAppDataObject.selCompletedItem;
    if (!isEmpty(resultset) && resultset.length > 0) {
        if (!isEmpty(resultset[0].customerApprovalSignature)) {
            printMessage(" successCallbackCompletedSignData" + resultset[0].customerApprovalSignature);
            CompletedVisit.tabCompletedCustomerDetails.imgSignature.base64 = resultset[0].customerApprovalSignature;
            CompletedVisit.tabCompletedCustomerDetails.lblSignDate.text = completedItems.signedDate;
            CompletedVisit.tabCompletedCustomerDetails.lblSignHere.text = "";
            CompletedVisit.tabCompletedCustomerDetails.txtCustomerNameSig.text = completedItems.customernameSigned;
            CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.selectedKey = PCConstant.COMMON_KEY_SELECT;
            CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.setVisibility(true);
            CompletedVisit.tabCompletedCustomerDetails.imgSignature.setVisibility(true);
            CompletedVisit.tabCompletedCustomerDetails.lblSignDate.setVisibility(false);
            CompletedVisit.tabCompletedCustomerDetails.lblSignHere.setVisibility(false);
            CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.setVisibility(true);
        } else {
            printMessage(" successCallbackCompletedSignData else-->");
            CompletedVisit.tabCompletedCustomerDetails.imgSignature.base64 = "";
            CompletedVisit.tabCompletedCustomerDetails.lblSignDate.text = "";
            CompletedVisit.tabCompletedCustomerDetails.lblSignHere.text = getI18nString("common.label.noSignature");
            CompletedVisit.tabCompletedCustomerDetails.txtCustomerNameSig.text = completedItems.customernameSigned;
            CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.selectedKey = (!isEmpty(completedItems.noSignatureReason)) ? completedItems.noSignatureReason : PCConstant.COMMON_KEY_SELECT;
            CompletedVisit.tabCompletedCustomerDetails.imgSignature.setVisibility(false);
            CompletedVisit.tabCompletedCustomerDetails.lblSignDate.setVisibility(false);
            CompletedVisit.tabCompletedCustomerDetails.lblSignHere.setVisibility(true);
            CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.setVisibility(true);
        }
    }
    dismissLoading();
}

function failureCallbackCompletedSigData() {
    printMessage(" Inside - error_getCustomerSignature ");
    dismissLoading();
}

function success_getCapturedCompletedPhoto(resultset) {
    printMessage(" Inside - success_getCapturedCompletedPhoto - resultset: " + JSON.stringify(resultset));
    var completedItems = gblMyAppDataObject.selCompletedItem;
    if (!isEmpty(completedItems.visitNote)) {
        CompletedVisit.tabCompletedCustomerDetails.txtAreaVisitNote.text = completedItems.visitNote;
    }
    if (!isEmpty(resultset) && resultset.length > 0) {
        if (!isEmpty(resultset[0].capturedPhoto)) {
            printMessage(" successCallbackCompletedSignData" + resultset[0].capturedPhoto);
            CompletedVisit.tabCompletedVisitNotes.imgCapturedPhoto.base64 = resultset[0].capturedPhoto;
        }
    }
    dismissLoading();
}

function error_getCapturedCompletedPhoto() {
    printMessage(" Inside - error_getCapturedCompletedPhoto ");
    dismissLoading();
}