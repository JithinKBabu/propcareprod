/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer all custom widget creations
 ***********************************************************************/
/** Custom segment row template for Home screen - On swipe on segment row will create this template **/
function createHomeSegTemplateAlt(isGpsEnabled, isTelNumPresent, isMobileNumPresent, visitStatus) {
    var navigateIcon = (isGpsEnabled === true) ? "navigate.png" : "navigatedisabled.png";
    var phoneCallIcon = (isTelNumPresent === true) ? "call.png" : "calldisabled.png";
    var mobileIcon = (isMobileNumPresent === true) ? "mobile.png" : "mobiledisabled.png";
    var navigationAction = (isGpsEnabled === true) ? ActionNavigationClick : ActionVoidClick;
    var phoneCallAction = (isTelNumPresent === true) ? ActionPhoneCallClick : ActionVoidClick;
    var mobileAction = (isMobileNumPresent === true) ? ActionMobileCallClick : ActionVoidClick;
    var startVisitIcon = "startvisit.png";
    var startVisitAction = ActionStartVisitClick;
    flxRowSegHomeContentListOnSwipe = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85dp",
        "id": "flxRowSegHomeContentListOnSwipe",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin"
    }, {}, {});
    flxRowSegHomeContentListOnSwipe.setDefaultUnit(kony.flex.DP);
    var flxNavigateButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "15%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "80dp",
        "id": "flxNavigateButton",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": navigationAction,
        "skin": "slFbox",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    flxNavigateButton.setDefaultUnit(kony.flex.DP);
    var imgNavigateIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40dp",
        "id": "imgNavigateIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": navigateIcon,
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxNavigateButton.add(imgNavigateIcon);
    var flxCallButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "80dp",
        "id": "flxCallButton",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": phoneCallAction,
        "skin": "slFbox",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    flxCallButton.setDefaultUnit(kony.flex.DP);
    var imgCallIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40dp",
        "id": "imgCallIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": phoneCallIcon,
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCallButton.add(imgCallIcon);
    var flxMobileButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "55%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "80dp",
        "id": "flxMobileButton",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        //  "onClick"		: mobileAction,
        "skin": "slFbox",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    flxMobileButton.setDefaultUnit(kony.flex.DP);
    var imgMobileIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40dp",
        "id": "imgMobileIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": mobileIcon,
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMobileButton.add(imgMobileIcon);
    var flxStartVisitButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "75%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "80dp",
        "id": "flxStartVisitButton",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": startVisitAction,
        "skin": "slFbox",
        "width": "60dp",
        "zIndex": 1
    }, {}, {});
    flxStartVisitButton.setDefaultUnit(kony.flex.DP);
    var imgStartVisitIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40dp",
        "id": "imgStartVisitIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": startVisitIcon,
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStartVisitButton.add(imgStartVisitIcon);
    var imgRightArrowIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgRightArrowIcon",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "rightarrow.png",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxRowSegHomeContentListOnSwipe.add(flxNavigateButton, flxCallButton, flxMobileButton, flxStartVisitButton, imgRightArrowIcon);
    var setupTblSwipe = {
        fingers: 1,
        swipedistance: 50,
        swipevelocity: 75
    };
    flxRowSegHomeContentListOnSwipe.setGestureRecognizer(2, setupTblSwipe, handleSwipeGestureOnSegRow);
    return flxRowSegHomeContentListOnSwipe;
}