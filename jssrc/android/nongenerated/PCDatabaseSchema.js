/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer all the offline database and sync methods
 ***********************************************************************/
// kony.db.changeVersion("1.0","1.1",transactioCall,errorCall,sucessCall);
/** To fetch iCabInstances from local DB **/
function getiCabInstancesFromLocalTbl() {
    printMessage(" Inside - getiCabInstancesFromLocalTbl ");
    var orderByMap = [];
    orderByMap[0] = {};
    orderByMap[0].key = "countryCode";
    orderByMap[0].sortType = "asc";
    orderByMap[1] = {};
    orderByMap[1].key = "businessCode";
    orderByMap[1].sortType = "asc";
    com.pc.icabinstances.iCabinstancesPc.getAll(success_getiCabInstances, error_getiCabInstances, orderByMap);
}
/** To fetch employee record from local DB **/
function getEmployeeFromLocalTbl() {
    printMessage(" Inside - getEmployeeFromLocalTbl ");
    var orderByMap = [];
    orderByMap[0] = {};
    orderByMap[0].key = "employeeWorkEmail";
    orderByMap[0].sortType = "asc";
    com.pc.employees.Employees.getAll(success_getEmployee, error_getEmployee, orderByMap, 1, 0);
}
/** To fetch service areas from local DB **/
function getServiceAreasFromLocalTbl() {
    printMessage(" Inside - getServiceAreaFromLocalTbl ");
    /*var sqlStatement		= "SELECT * FROM ServiceAreaPC WHERE assignedArea = 'true'";
    executeCustomQuery(sqlStatement, null, success_getServiceAreas, error_getServiceAreas);*/
    var whereStatmnt = " WHERE assignedArea = 'true' ";
    com.pc.serviceareas.ServiceAreaPc.find(whereStatmnt, success_getServiceAreas, error_getServiceAreas);
}
/** To fetch work orders from local DB **/
function getWorkOrdersFromLocalTbl() {
    printMessage(" Inside - getWorkOrdersFromLocalTbl ");
    var sqlStatement = "SELECT PT.propertyName, PT.propertyCode, PT.propertyAddressLine1, PT.propertyAddressLine2, PT.propertyAddressLine3, PT.propertyAddressLine4, PT.propertyAddressLine5, PT.propertyPostcode, PT.propertyContactMobile, PT.propertyNumber, ";
    sqlStatement += "PT.propertyContactEmail, PT.propertyContactFax, PT.propertyContactTelephone, PT.gpsCoordinateX, PT.gpsCoordinateY, PT.propertyContactName, PT.propertyContactPosition, PC.propertyServiceNote, PC.propertySpecialInstructions, WK.workId, WK.workStatus, WK.businessCode, WK.countryCode, SC.serviceCoverNumber, ";
    sqlStatement += "SC.serviceCoverSequence, SC.serviceLineCode, WO.workOrderPlannerNotes, WO.workorderId, WO.workOrderDateStartPlanned, WO.workOrderStatus, WO.workOrderTypeCode, ";
    sqlStatement += "WO.workStatusLoc, WO.quantityPlanned, WOT.isCallout, WOT.isFollowUp, WOT.isInstallation, WOT.workOrderTypeCode, WOT.updateStateOfService, AC.nationalAccount, ";
    sqlStatement += "CO.contractTypeCode, CO.contractCode FROM Work WK INNER JOIN Property PT ON PT.countryCode = WK.countryCode and PT.businessCode = WK.businessCode and PT.propertyCode = WK.propertyCode ";
    sqlStatement += "INNER JOIN WorkOrder WO ON WO.countryCode = WK.countryCode and WO.businessCode = WK.businessCode and WO.propertyCode = WK.propertyCode AND WO.workId = WK.workId ";
    sqlStatement += "INNER JOIN ServiceCover SC ON SC.countryCode = WO.countryCode AND SC.businessCode = WO.businessCode AND SC.propertyCode = WO.propertyCode AND SC.serviceLineCode = WO.serviceLineCode ";
    sqlStatement += "AND SC.serviceCoverNumber = WO.serviceCoverNumber AND SC.contractCode = WO.contractCode ";
    sqlStatement += "INNER JOIN PropertyContract PC ON PC.countryCode = WO.countryCode AND PC.businessCode = WO.businessCode AND PC.propertyCode = WO.propertyCode AND PC.contractCode = WO.contractCode ";
    sqlStatement += "INNER JOIN Contract CO ON PC.countryCode = CO.countryCode AND PC.businessCode = CO.businessCode AND PC.contractCode = CO.contractCode ";
    sqlStatement += "INNER JOIN Account AC  ON AC.countryCode = CO.countryCode AND AC.businessCode = CO.businessCode AND  AC.accountCode = CO.accountCode ";
    sqlStatement += "INNER JOIN WorkOrderType WOT ON WOT.workOrderTypeCode = WO.workOrderTypeCode AND WK.workId = WO.workId  where WK.workStatus NOT IN ('" + PCConstant.VISIT_CLOSED + "', '" + PCConstant.VISIT_COMPLETED + "') ";
    sqlStatement += "GROUP BY PT.propertyName,PT.propertyCode,PT.propertyAddressLine1,WK.workId,WK.workDateStartPlanned ";
    sqlStatement += "ORDER BY WK.workDateStartPlanned,SC.serviceCoverSequence,PT.propertyName COLLATE NOCASE,PT.propertyAddressLine1 COLLATE NOCASE ";
    printMessage("Inside - getWorkOrdersFromLocalTbl - sqlStatement: " + sqlStatement);
    executeCustomQuery(sqlStatement, null, success_getWorkOrders, error_getWorkOrders);
}
/** To fetch preparations from local DB **/
function getPreparationsFromLocalTbl() {
    printMessage(" Inside - getPreparationsFromLocalTbl  ");
    var orderByMap = [];
    orderByMap[0] = {};
    orderByMap[0].key = "preparationName";
    orderByMap[0].sortType = "asc";
    preparations.getAll(success_getPreparations, error_getPreparations, orderByMap);
}
/** To fetch issue code from local DB (To populate no signature combo box) **/
function getIssueCodesFromLocalTbl(issueTypeCode) {
    printMessage(" Inside - getIssueCodesFromLocalTbl ");
    var whereStatmnt = " WHERE issueTypeCode = '" + issueTypeCode + "' ";
    issue.find(whereStatmnt, success_getIssueCodes, error_getIssueCodes);
}
/** To insert new workDayStart record in local DB **/
function insertWorkDayStartInDB() {
    printMessage(" Inside - insertWorkDayStartInDB ");
    var workDayStartObj = new workDayStart();
    var selectedIcabInstance = gblMyAppDataObject.getIcabInstance();
    if (!isEmpty(selectedIcabInstance)) {
        workDayStartObj.businessCode = selectedIcabInstance.businessCode;
        workDayStartObj.countryCode = selectedIcabInstance.countryCode;
        workDayStartObj.email = selectedIcabInstance.email;
    } else {
        workDayStartObj.businessCode = PCConstant.DEF_BUSINESS_CODE;
        workDayStartObj.countryCode = PCConstant.DEF_COUNTRY_CODE;
        workDayStartObj.email = gblMyAppDataObject.selectedEmail;
    }
    workDayStartObj.languageCode = PCConstant.DEF_LANGUAGE_CODE;
    if (!isEmpty(gblMyAppDataObject.startDayDateStr)) {
        workDayStartObj.workingDayDateStart = gblMyAppDataObject.startDayDateStr;
    }
    if (!isEmpty(gblMyAppDataObject.startDayTimeStr)) {
        workDayStartObj.workingDayTimeStart = gblMyAppDataObject.startDayTimeStr;
    }
    workDayStartObj.markForUpload = true;
    printMessage(" Inside - workDayStartObj " + JSON.stringify(workDayStartObj));
    workDayStartObj.create(success_insertWorkDayStart, error_insertWorkDayStart);
}
/** To insert new workDayEnd record in local DB **/
function insertWorkDayEndInDB() {
    printMessage(" Inside - insertWorkDayEndInDB ");
    var workDayEndObj = new workDayEnd();
    var selectedIcabInstance = gblMyAppDataObject.getIcabInstance();
    if (!isEmpty(selectedIcabInstance)) {
        workDayEndObj.businessCode = selectedIcabInstance.businessCode;
        workDayEndObj.countryCode = selectedIcabInstance.countryCode;
        workDayEndObj.email = selectedIcabInstance.email;
    } else {
        workDayEndObj.businessCode = PCConstant.DEF_BUSINESS_CODE;
        workDayEndObj.countryCode = PCConstant.DEF_COUNTRY_CODE;
        workDayEndObj.email = gblMyAppDataObject.selectedEmail;
    }
    workDayEndObj.languageCode = PCConstant.DEF_LANGUAGE_CODE;
    if (!isEmpty(gblMyAppDataObject.stopDayDateStr)) {
        workDayEndObj.workingDayDateEnd = gblMyAppDataObject.stopDayDateStr;
    }
    if (!isEmpty(gblMyAppDataObject.stopDayTimeStr)) {
        workDayEndObj.workingDayTimeEnd = gblMyAppDataObject.stopDayTimeStr;
    }
    workDayEndObj.markForUpload = true;
    printMessage(" Inside - workDayEndObj " + JSON.stringify(workDayEndObj));
    workDayEndObj.create(success_insertWorkDayEnd, error_insertWorkDayEnd);
}
/** To fetch work history from local DB **/
function getWorkHistoryFromLocalTbl() {
    printMessage("Inside - getWorkHistoryFromLocalTbl - work and service cover");
    var propertyCode = "";
    if (!gblCompletedVisitFlag) {
        propertyCode = gblMyAppDataObject.selScheduledItem.propertyCode;
    } else {
        propertyCode = gblMyAppDataObject.selCompletedItem.propertyCode;
    }
    printMessage("Inside - getWorkHistoryFromLocalTbl - work and service cover - propertyCode: " + propertyCode);
    /*var sqlStatement		= "SELECT * FROM workHistory where propertyCode = '" + propertyCode + "'";
    printMessage("Inside - getWorkHistoryFromLocalTbl - sqlStatement: " + sqlStatement);
    executeCustomQuery(sqlStatement, null, success_getWorkHistory, error_getWorkHistory);*/
    var whereStatmnt = " where propertyCode = '" + propertyCode + "'";
    workHistory.find(whereStatmnt, success_getWorkHistory, error_getWorkHistory);
}
/** To fetch service cover from local DB **/
function getServiceCoverFromLocalTbl() {
    printMessage("Inside - getServiceCoverFromLocalTbl - work and service cover - ");
    var propertyCode = "";
    var contractCode = "";
    if (!gblCompletedVisitFlag) {
        propertyCode = gblMyAppDataObject.selScheduledItem.propertyCode;
        contractCode = gblMyAppDataObject.selScheduledItem.contractCode;
    } else {
        propertyCode = gblMyAppDataObject.selCompletedItem.propertyCode;
        contractCode = gblMyAppDataObject.selCompletedItem.contractCode;
    }
    printMessage("Inside - getServiceCoverFromLocalTbl - work and service cover - propertyCode: " + propertyCode);
    printMessage("Inside - getServiceCoverFromLocalTbl - work and service cover - contractCode: " + contractCode);
    /*var sqlStatement	= "SELECT * FROM servicecover where propertyCode = '" + propertyCode + "' and contractCode = '" + contractCode + "' order by serviceCoverSequence";
    printMessage("Inside - getServiceCoverFromLocalTbl - sqlStatement: " + sqlStatement);
    executeCustomQuery(sqlStatement, null, success_getServiceCover, error_getServiceCover);*/
    var whereStatmnt = " where propertyCode = '" + propertyCode + "' and contractCode = '" + contractCode + "' order by serviceCoverSequence";
    com.pc.servicecovers.servicecover.find(whereStatmnt, success_getServiceCover, error_getServiceCover);
}
/** To update special instructions (Site Risk Assessment) in local DB table PropertyContract **/
function updateSRAInLocalTbl(updatedSRA) {
    printMessage("Inside - updateSRAInLocalTbl ");
    var propertyCode = gblMyAppDataObject.selScheduledItem.propertyCode;
    var contractCode = gblMyAppDataObject.selScheduledItem.contractCode;
    var selectedIcabInstance = gblMyAppDataObject.getIcabInstance();
    //var updatedSRA							= Customer.tabCustomerDetails.txaSiteRiskAssessment.text;
    var valuestable = {};
    valuestable.propertySpecialInstructions = updatedSRA;
    /** Starts - Below values are populated here for pass checkIntegrity sync method **/
    valuestable.countryCode = selectedIcabInstance.countryCode;
    valuestable.contractCode = contractCode;
    valuestable.businessCode = selectedIcabInstance.businessCode;
    /** Ends - Below values are populated here for pass checkIntegrity sync method **/
    var whereStatmnt = " WHERE businessCode = '" + selectedIcabInstance.businessCode + "' AND countryCode = '" + selectedIcabInstance.countryCode + "'";
    whereStatmnt += " AND contractCode = '" + contractCode + "' AND propertyCode = '" + propertyCode + "' ";
    com.pc.portfolios.propertycontract.update(whereStatmnt, valuestable, success_commonCallback, error_commonCallback, true);
}
/** To update work order record in local DB table workorder **/
function updateWorkOrderInLocalTbl() {
    printMessage("Inside - updateWorkOrderInLocalTbl ");
    var visitInProgressObj = gblMyAppDataObject.inProgressVisit;

    function success_updateWorkOrder(result) {
        printMessage("Inside - updateWorkOrderInLocalTbl - Success");
        updateWorkInLocalTbl();
    }

    function error_updateWorkOrder() {
        printMessage("Inside - updateWorkOrderInLocalTbl - Error");
        showMessagePopup("common.message.contactSupport", ActionPopupMessageOk);
        dismissLoading();
    }
    try {
        gblIsWorkSaveInProgress = true;
        var valuestable = {};
        valuestable.workOrderStatus = PCConstant.VISIT_CLOSED;
        valuestable.workOrderVisitNotes = visitInProgressObj.visitNote;
        valuestable.actualEffort = visitInProgressObj.workAttendanceTimeDuration;
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestable.businessCode = visitInProgressObj.businessCode;
        valuestable.countryCode = visitInProgressObj.countryCode;
        valuestable.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        var whereStatmnt = " WHERE workorderId = '" + visitInProgressObj.workorderId + "' ";
        com.pc.workloads.workOrder.update(whereStatmnt, valuestable, success_updateWorkOrder, error_updateWorkOrder, true);
    } catch (err) {
        gblIsWorkSaveInProgress = false;
        printMessage("Inside - updateWorkOrderInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** To update work record in local DB table work **/
function updateWorkInLocalTbl() {
    printMessage("Inside - updateWorkInLocalTbl ");
    var visitInProgressObj = gblMyAppDataObject.inProgressVisit;

    function success_updateWork(result) {
        printMessage("Inside - updateWorkInLocalTbl - Success");
        insertWorkAttendanceInLocalTbl();
    }

    function error_updateWork() {
        printMessage("Inside - updateWorkInLocalTbl - Error");
        showMessagePopup("common.message.contactSupport", ActionPopupMessageOk);
        gblIsWorkSaveInProgress = false;
        dismissLoading();
    }
    try {
        var isCustomerApproved = (isEmpty(visitInProgressObj.noSignatureReason)) ? 'true' : 'false';
        var customerNameSigned = (isCustomerApproved === 'true') ? visitInProgressObj.customerName : '';
        var customerNoSignReason = (!isEmpty(visitInProgressObj.noSignatureReason)) ? visitInProgressObj.noSignatureReason : '';
        var issueTypeCodeNoSignature = (!isEmpty(visitInProgressObj.noSignatureReason)) ? PCConstant.SIGNATURE_ISSUE_TYPE_CODE : "";
        var valuestable = {};
        valuestable.workStatus = PCConstant.VISIT_CLOSED;
        valuestable.customerApproval = isCustomerApproved;
        valuestable.customerApprovalName = customerNameSigned;
        valuestable.issueCodeNoSignature = customerNoSignReason;
        valuestable.issueTypeCodeNoSignature = issueTypeCodeNoSignature;
        valuestable.completedDate = new Date();
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestable.businessCode = visitInProgressObj.businessCode;
        valuestable.countryCode = visitInProgressObj.countryCode;
        valuestable.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        var whereStatmnt = " WHERE workId = '" + visitInProgressObj.workId + "' ";
        com.pc.workloads.work.update(whereStatmnt, valuestable, success_updateWork, error_updateWork, true);
    } catch (err) {
        gblIsWorkSaveInProgress = false;
        printMessage("Inside - updateWorkInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** To insert workAttendace in local DB table **/
function insertWorkAttendanceInLocalTbl() {
    printMessage("Inside - insertWorkAttendanceInLocalTbl");
    var selectedIcabInstance = gblMyAppDataObject.getIcabInstance();
    var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
    printMessage("Inside - insertWorkAttendanceInLocalTbl - visitInProgressObj: " + JSON.stringify(visitInProgressObj));
    var aWorkAttendaceObj = new com.pc.workloads.workAttendance();
    aWorkAttendaceObj.businessCode = selectedIcabInstance.businessCode;
    aWorkAttendaceObj.countryCode = selectedIcabInstance.countryCode;
    aWorkAttendaceObj.employeeCodePrimary = gblEmployeeObject.employeeCode;
    aWorkAttendaceObj.updateDateTime = getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
    aWorkAttendaceObj.workAttendanceDateTimeStart = visitInProgressObj.workAttendanceDateTimeStart;
    aWorkAttendaceObj.workAttendanceDeleted = false;
    aWorkAttendaceObj.workAttendanceStatus = PCConstant.VISIT_CLOSED;
    aWorkAttendaceObj.workAttendanceTimeDuration = visitInProgressObj.workAttendanceTimeDuration;
    aWorkAttendaceObj.workId = visitInProgressObj.workId;
    aWorkAttendaceObj.markForUpload = true;
    aWorkAttendaceObj.workAttendanceId = generateUUID();
    visitInProgressObj.workAttendanceId = aWorkAttendaceObj.workAttendanceId;
    setAppDataObjInStore();

    function success_insertWorkAttendance(result) {
        printMessage("Inside - insertWorkAttendanceInLocalTbl - Success");
        printMessage("Inside - insertWorkAttendanceInLocalTbl - result: " + JSON.stringify(result));
        getDefaultServicePointFromLocalTbl();
    }

    function error_insertWorkAttendance() {
        printMessage("Inside - insertWorkAttendanceInLocalTbl - Error");
        showMessagePopup("common.message.contactSupport", ActionPopupMessageOk);
        gblIsWorkSaveInProgress = false;
        dismissLoading();
    }
    aWorkAttendaceObj.create(success_insertWorkAttendance, error_insertWorkAttendance);
}
/** Get default service point details for workPoint creation **/
function getDefaultServicePointFromLocalTbl() {
    printMessage("Inside - getDefaultServicePointFromLocalTbl ");

    function success_getDefaultServicePoint(resultset) {
        printMessage("Inside - getDefaultServicePointFromLocalTbl - Success");
        printMessage("Inside - getDefaultServicePointFromLocalTbl: " + JSON.stringify(resultset));
        var aRecord = null;
        if (!isEmpty(resultset) && resultset.length > 0) {
            printMessage("resultset.length " + resultset.length);
            var recordsCount = resultset.length;
            for (var i = 0; i < recordsCount; i++) {
                aRecord = resultset[i];
                break;
            }
            insertWorkPointInLocalTbl(aRecord);
        } else {
            var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
            aRecord = {};
            aRecord.propertyCode = visitInProgressObj.propertyCode;
            aRecord.contractCode = visitInProgressObj.contractCode;
            aRecord.serviceLineCode = "";
            aRecord.serviceCoverNumber = "0";
            aRecord.servicePointId = "1";
            insertWorkPointInLocalTbl(aRecord);
        }
    }

    function error_getDefaultServicePoint() {
        printMessage("Inside - getDefaultServicePointFromLocalTbl - Error");
        gblIsWorkSaveInProgress = false;
    }
    var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
    var propertyCode = visitInProgressObj.propertyCode;
    var contractCode = visitInProgressObj.contractCode;
    printMessage("Inside - getDefaultServicePointFromLocalTbl - propertyCode: " + propertyCode);
    printMessage("Inside - getDefaultServicePointFromLocalTbl - contractCode: " + contractCode);
    var whereStatmnt = " WHERE propertyCode = '" + propertyCode + "' AND contractCode = '" + contractCode + "' ";
    com.pc.portfolios.servicePoint.find(whereStatmnt, success_getDefaultServicePoint, error_getDefaultServicePoint);
}
/** To insert workPoint record in local DB table **/
function insertWorkPointInLocalTbl(defServicePoint) {
    printMessage("Inside - insertWorkPointInLocalTbl");

    function success_insertWorkPoint(result) {
        printMessage("Inside - insertWorkPointInLocalTbl - Success");
        printMessage("Inside - insertWorkPointInLocalTbl - result: " + JSON.stringify(result));
        var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
        if (!isEmpty(visitInProgressObj.PreprationsQuantity1) && visitInProgressObj.PreprationsQuantity1 > 0) {
            insertTreatementInLocalTbl(visitInProgressObj.Preprations1, visitInProgressObj.PreprationsQuantity1, visitInProgressObj.workPointId);
        }
        if (!isEmpty(visitInProgressObj.PreprationsQuantity2) && visitInProgressObj.PreprationsQuantity2 > 0) {
            insertTreatementInLocalTbl(visitInProgressObj.Preprations2, visitInProgressObj.PreprationsQuantity2, visitInProgressObj.workPointId);
        }
        if (!isEmpty(visitInProgressObj.PreprationsQuantity3) && visitInProgressObj.PreprationsQuantity3 > 0) {
            insertTreatementInLocalTbl(visitInProgressObj.Preprations3, visitInProgressObj.PreprationsQuantity3, visitInProgressObj.workPointId);
        }
        if (!isEmpty(visitInProgressObj.PreprationsQuantity4) && visitInProgressObj.PreprationsQuantity4 > 0) {
            insertTreatementInLocalTbl(visitInProgressObj.Preprations4, visitInProgressObj.PreprationsQuantity4, visitInProgressObj.workPointId);
        }
        if (!isEmpty(visitInProgressObj.PreprationsQuantity5) && visitInProgressObj.PreprationsQuantity5 > 0) {
            insertTreatementInLocalTbl(visitInProgressObj.Preprations5, visitInProgressObj.PreprationsQuantity5, visitInProgressObj.workPointId);
        }
        gblIsWorkSaveInProgress = false;
        gblMyAppDataObject.moveScheduledJobToCompletedJobs();
        gblMyAppDataObject.selDateStrKey = null;
        gblMyAppDataObject.inProgressVisit = null;
        gblMyAppDataObject.isVisitInProgress = false;
        setAppDataObjInStore();
        dismissConfirmPopup();
        gblShowCompletedJobs = true;
        getWorkOrdersFromLocalTbl();
    }

    function error_insertWorkPoint() {
        printMessage("Inside - insertWorkPointInLocalTbl - Error");
        showMessagePopup("common.message.contactSupport", ActionPopupMessageOk);
        gblIsWorkSaveInProgress = false;
        dismissLoading();
    }
    var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
    var aWorkPointObj = new com.pc.workloads.workPoint();
    aWorkPointObj.workPointId = generateUUID();
    visitInProgressObj.workPointId = aWorkPointObj.workPointId;
    setAppDataObjInStore();
    aWorkPointObj.activationMethod = PCConstant.WORK_POINT_ACTIVATION;
    aWorkPointObj.contractCode = (!isEmpty(defServicePoint) && !isEmpty(defServicePoint.contractCode)) ? defServicePoint.contractCode : "";
    aWorkPointObj.propertyCode = (!isEmpty(defServicePoint) && !isEmpty(defServicePoint.propertyCode)) ? defServicePoint.propertyCode : "";
    if (!isEmpty(defServicePoint) && !isEmpty(defServicePoint.serviceLineCode) && !isEmpty(defServicePoint.serviceCoverNumber)) {
        aWorkPointObj.serviceLineCode = defServicePoint.serviceLineCode;
        aWorkPointObj.serviceCoverNumber = defServicePoint.serviceCoverNumber;
    } else {
        aWorkPointObj.serviceLineCode = visitInProgressObj.serviceLineCode;
        aWorkPointObj.serviceCoverNumber = visitInProgressObj.serviceCoverNumber;
    }
    aWorkPointObj.employeeCodeActual = gblEmployeeObject.employeeCode;
    aWorkPointObj.employeeCodePlanned = gblEmployeeObject.employeeCode;
    aWorkPointObj.issueCode = "";
    aWorkPointObj.issueTypeCode = "";
    aWorkPointObj.servicePointId = (!isEmpty(defServicePoint) && !isEmpty(defServicePoint.servicePointId)) ? defServicePoint.servicePointId : "";
    aWorkPointObj.serviceQuantityActual = "";
    aWorkPointObj.serviceQuantityPlanned = "";
    aWorkPointObj.workAttendanceId = visitInProgressObj.workAttendanceId;
    aWorkPointObj.workOrderId = visitInProgressObj.workorderId;
    aWorkPointObj.workId = visitInProgressObj.workId;
    aWorkPointObj.workPointActivationDateTime = visitInProgressObj.workAttendanceDateTimeStart;
    aWorkPointObj.workPointEffort = visitInProgressObj.workAttendanceTimeDuration;
    aWorkPointObj.workPointDeleted = false;
    aWorkPointObj.updateDateTime = getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
    aWorkPointObj.create(success_insertWorkPoint, error_insertWorkPoint);
}
/** To insert treatment record in local DB table **/
function insertTreatementInLocalTbl(prepCode, prepQuantity, workPointId) {
    printMessage("Inside - insertTreatementInLocalTbl");

    function success_insertTreatement(result) {
        printMessage("Inside - insertTreatementInLocalTbl - Success");
        printMessage("Inside - insertTreatementInLocalTbl - result: " + JSON.stringify(result));
    }

    function error_insertTreatement() {
        printMessage("Inside - insertTreatementInLocalTbl - Error");
        gblIsWorkSaveInProgress = false;
    }
    var aTreatmentObj = new com.pc.workloads.treatment();
    aTreatmentObj.treatmentId = generateUUID();
    aTreatmentObj.workPointId = workPointId;
    aTreatmentObj.treatmentBatch = "";
    aTreatmentObj.treatmentPreparationCode = prepCode;
    aTreatmentObj.treatmentQuantity = prepQuantity;
    aTreatmentObj.treatmentRecordedDateTime = getDateTimeFormat(new Date(), "YYYY-MM-DDTHH:mm:ss.SSS");
    aTreatmentObj.treatmentDeleted = false;
    aTreatmentObj.updateDateTime = getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
    aTreatmentObj.create(success_insertTreatement, error_insertTreatement);
}
/** To delete all closed work records from all related tables **/
function deleteClosedWorkRecords() {
    printMessage("Inside - deleteClosedWorkRecords");
    if (!isEmpty(gblSyncedWorkIds) && gblSyncedWorkIds.length > 0) {
        var workIdsCompleteStr = "";
        for (var i = 0; i < gblSyncedWorkIds.length; i++) {
            workIdsCompleteStr += "'" + gblSyncedWorkIds[i] + "'";
            if (i < (gblSyncedWorkIds.length - 1)) {
                workIdsCompleteStr += ",";
            }
        }
        var whereStatmntWK = " WHERE workId IN (" + workIdsCompleteStr + ")";
        printMessage("Inside - deleteClosedWorkRecords - anOutdatedJobs - whereStatmntWK: " + whereStatmntWK);
        com.pc.workloads.work.removeDeviceInstance(whereStatmntWK, success_commonCallback, error_commonCallback);
        com.pc.workloads.workOrder.removeDeviceInstance(whereStatmntWK, success_commonCallback, error_commonCallback);
        com.pc.workloads.workAttendance.removeDeviceInstance(whereStatmntWK, success_commonCallback, error_commonCallback);
        com.pc.workloads.workPoint.removeDeviceInstance(whereStatmntWK, success_commonCallback, error_commonCallback);
        var workPointIdsCompleteStr = "";
        for (var j = 0; j < gblSyncedWorkPointIds.length; i++) {
            workPointIdsCompleteStr += "'" + gblSyncedWorkPointIds[j] + "'";
            if (j < (gblSyncedWorkPointIds.length - 1)) {
                workPointIdsCompleteStr += ",";
            }
        }
        var whereStatmntWP = " WHERE workPointId IN (" + workPointIdsCompleteStr + ")";
        printMessage("Inside - deleteClosedWorkRecords - anOutdatedJobs - whereStatmntWP: " + whereStatmntWP);
        com.pc.workloads.treatment.removeDeviceInstance(whereStatmntWP, success_commonCallback, error_commonCallback);
    }
    /*
    var workIdsCompleteStr			= "";
    for (var i = 0; i < gblSyncedWorkIds.length; i++) {
      workIdsCompleteStr			+= "'" + gblSyncedWorkIds[i] + "'";
      if (i < (gblSyncedWorkIds.length - 1)) {
        workIdsCompleteStr		+= ",";          	
      }
    }
    
  	// Delete work
  	var stmt 						= "DELETE FROM work WHERE workStatus = '" + PCConstant.VISIT_CLOSED + "' AND workId IN (" + workIdsCompleteStr + ")";
  	printMessage("Inside deleteClosedWorkRecords - delete records from work table: " + stmt);
  	executeCustomQuery(stmt, null, success_commonCallback, error_commonCallback);
  	
  	// Delete workAttendance
  	stmt 							= "DELETE FROM workAttendance WHERE workId IN (" + workIdsCompleteStr + ")";
  	printMessage("Inside deleteClosedWorkRecords - delete records from workAttendance table: " + stmt);
  	executeCustomQuery(stmt, null, success_commonCallback, error_commonCallback);
  
  	// Delete workOrder
  	stmt 							= "DELETE FROM workOrder WHERE workId IN (" + workIdsCompleteStr + ") and workOrderStatus = '" + PCConstant.VISIT_CLOSED + "'";
  	printMessage("Inside deleteClosedWorkRecords - delete records from workOrder table: " + stmt);
  	executeCustomQuery(stmt, null, success_commonCallback, error_commonCallback);
  	
  	// Delete workPoint
  	stmt 							= "DELETE FROM workPoint WHERE workId IN (" + workIdsCompleteStr + ")";
  	printMessage("Inside deleteClosedWorkRecords - delete records from workPoint table: " + stmt);
  	executeCustomQuery(stmt, null, success_commonCallback, error_commonCallback);
  	gblSyncedWorkIds				= [];
  
  	// Delete treatment
  	stmt 							= "DELETE FROM treatment WHERE workPointId IN ("; 
    for (i = 0; i < gblWorkPointIds.length; i++) {
        stmt						+= "'" + gblWorkPointIds[i] + "'";
        if (i < (gblWorkPointIds.length - 1)) {
          	stmt					+= ",";
        }
    }
    stmt							+= ")";
  	printMessage("Inside deleteClosedWorkRecords - delete records from treatment table: " + stmt);
  	executeCustomQuery(stmt, null, success_commonCallback, error_commonCallback);
  	gblWorkPointIds					= [];
    */
}
/** To update work status as 'In Progress' in work table and respective changes in workOrder table **/
function updateWorkStatusAsInProgressInLocalTbl() {
    printMessage("Inside - updateWorkStatusAsInProgressInLocalTbl ");
    var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
    try {
        var workDateStartActual = getCurrentDateTimeInFormat("yyyy-MM-dd");
        var valuestableWO = {};
        valuestableWO.workOrderDateStartActual = workDateStartActual;
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestableWO.businessCode = visitInProgressObj.businessCode;
        valuestableWO.countryCode = visitInProgressObj.countryCode;
        valuestableWO.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        var whereStatmntWO = " WHERE workorderId = '" + visitInProgressObj.workorderId + "' ";
        com.pc.workloads.workOrder.update(whereStatmntWO, valuestableWO, success_commonCallback, error_commonCallback, true);
        var valuestableWK = {};
        valuestableWK.workStatus = PCConstant.VISIT_IN_PROGRESS;
        valuestableWK.workDateStartActual = workDateStartActual;
        valuestableWK.visitStartDateStr = visitInProgressObj.visitStartDateStr;
        valuestableWK.visitStartTimeStr = visitInProgressObj.visitStartTimeStr;
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestableWK.businessCode = visitInProgressObj.businessCode;
        valuestableWK.countryCode = visitInProgressObj.countryCode;
        valuestableWK.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        var whereStatmntWK = " WHERE workId = '" + visitInProgressObj.workId + "' ";
        com.pc.workloads.work.update(whereStatmntWK, valuestableWK, success_commonCallback, error_commonCallback, true);
    } catch (err) {
        printMessage("Inside - updateWorkStatusAsInProgressInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** To update work status as 'Open' in work table and respective changes in workOrder table **/
function updateWorkStatusAsOpenInLocalTbl() {
    printMessage("Inside - updateWorkStatusAsOpenInLocalTbl ");

    function success_updateWorkStatusAsOpen() {
        printMessage("Inside - updateWorkStatusAsOpenInLocalTbl - success ");
        gblMyAppDataObject.updateVisitStatus(gblMyAppDataObject.selDateStrKey, PCConstant.VISIT_OPEN);
        gblMyAppDataObject.selDateStrKey = null;
        gblMyAppDataObject.isVisitInProgress = false;
        gblMyAppDataObject.inProgressVisit = null;
        getWorkOrdersFromLocalTbl();
    }
    var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
    try {
        var workDateStartActual = "";
        var valuestableWO = {};
        valuestableWO.workOrderDateStartActual = workDateStartActual;
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestableWO.businessCode = visitInProgressObj.businessCode;
        valuestableWO.countryCode = visitInProgressObj.countryCode;
        valuestableWO.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        var whereStatmntWO = " WHERE workorderId = '" + visitInProgressObj.workorderId + "' ";
        com.pc.workloads.workOrder.update(whereStatmntWO, valuestableWO, success_commonCallback, error_commonCallback, true);
        var valuestableWK = {};
        valuestableWK.workStatus = PCConstant.VISIT_OPEN;
        valuestableWK.workDateStartActual = workDateStartActual;
        valuestableWK.customerApprovalSignature = "";
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestableWK.businessCode = visitInProgressObj.businessCode;
        valuestableWK.countryCode = visitInProgressObj.countryCode;
        valuestableWK.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        var whereStatmntWK = " WHERE workId = '" + visitInProgressObj.workId + "' ";
        com.pc.workloads.work.update(whereStatmntWK, valuestableWK, success_updateWorkStatusAsOpen, error_commonCallback, true);
    } catch (err) {
        printMessage("Inside - updateWorkStatusAsOpenInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** To update work status as Failed in local DB table work **/
function updateWorkStatusAsFailedInLocalTbl(workIdStr) {
    printMessage("Inside - updateWorkStatusAsFailedInLocalTbl ");
    try {
        var valuestableWK = {};
        valuestableWK.workStatus = PCConstant.VISIT_FAILED;
        var selectedIcabInstance = gblMyAppDataObject.getIcabInstance();
        var businessCodeStr = PCConstant.DEF_BUSINESS_CODE;
        var countryCodeStr = PCConstant.DEF_COUNTRY_CODE;
        if (!isEmpty(selectedIcabInstance)) {
            businessCodeStr = selectedIcabInstance.businessCode;
            countryCodeStr = selectedIcabInstance.countryCode;
        }
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestableWK.businessCode = businessCodeStr;
        valuestableWK.countryCode = countryCodeStr;
        valuestableWK.workId = workIdStr;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        var whereStatmntWK = " WHERE workId = '" + workIdStr + "' ";
        com.pc.workloads.work.update(whereStatmntWK, valuestableWK, success_commonCallback, error_commonCallback, true);
    } catch (err) {
        printMessage("Inside - updateWorkStatusAsFailedInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** To update work status as 'Completed' in work table **/
function updateWorkStatusAsCompletedInLocalTbl(workList, succeededWorkIdList) {
    printMessage("Inside - updateWorkStatusAsCompletedInLocalTbl ");
    try {
        if (!isEmpty(workList) && workList.length > 0) {
            var aWorkIdStr = "";
            var whereStatmntWK = "";
            var valuestableWK = {};
            var selectedIcabInstance = gblMyAppDataObject.getIcabInstance();
            var businessCodeStr = PCConstant.DEF_BUSINESS_CODE;
            var countryCodeStr = PCConstant.DEF_COUNTRY_CODE;
            var isWorkClosedInICabs = false;
            if (!isEmpty(selectedIcabInstance)) {
                businessCodeStr = selectedIcabInstance.businessCode;
                countryCodeStr = selectedIcabInstance.countryCode;
            }
            for (var i = 0; i < workList.length; i++) {
                aWorkIdStr = workList[i];
                isWorkClosedInICabs = getItemIndexFromArray(succeededWorkIdList, aWorkIdStr);
                if (isWorkClosedInICabs !== -1) {
                    /** After successfully synced with iCabs - Make it as Completed **/
                    gblMyAppDataObject.updateVisitStatusForSyncedJob(aWorkIdStr, PCConstant.VISIT_COMPLETED);
                    valuestableWK.workStatus = PCConstant.VISIT_COMPLETED;
                    /** Starts - Below values are populated here for pass checkIntegrity sync method **/
                    valuestableWK.businessCode = businessCodeStr;
                    valuestableWK.countryCode = countryCodeStr;
                    valuestableWK.workId = aWorkIdStr;
                    /** Ends - Below values are populated here for pass checkIntegrity sync method **/
                    whereStatmntWK = " WHERE workId = '" + aWorkIdStr + "' ";
                    com.pc.workloads.work.update(whereStatmntWK, valuestableWK, success_commonCallback, error_commonCallback, true);
                }
            }
        }
    } catch (err) {
        printMessage("Inside - updateWorkStatusAsCompletedInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** Description : To get list of completed works from local db **/
function getClosedWorksFromTable() {
    printMessage("Inside getClosedWorksFromTable ");
    try {
        var stmt = "SELECT WK.workId, WK.propertyCode, WK.customerApproval, WK.customerApprovalName, WK.employeeCodePrimary, WK.issueTypeCode, WK.issueCode, WK.customerApprovalSignature, WK.issueTypeCodeNoSignature, ";
        stmt += " WK.issueCodeNoSignature,WK.workDateStartActual, WK.outcard, WK.outcardCode, WK.outcardPropertyName, WK.outcardPropertyAddressLine1, WK.outcardPropertyAddressLine2, WK.outcardPropertyAddressLine3, ";
        stmt += " WK.outcardPropertyAddressLine4, WK.outcardPropertyAddressLine5, WK.outcardPropertyPostcode, WK.outcardPropertyContactName, WK.outcardPropertyContactTelephone, WK.outcardOfficeRef,WA.workAttendanceId, ";
        stmt += " WA.workAttendanceDateTimeStart, WA.workAttendanceTimeDuration, WO.workorderId, WO.contractCode, WO.serviceLineCode, WO.serviceCoverNumber, WO.quantityPlanned, WO.workOrderDateStartActual, WO.workOrderTypeCode, ";
        stmt += " WO.workOrderVisitNotes, WO.actualEffort, WO.planVisitNumber, WO.csiVisitReference, WO.followUpDate, WO.followUpTime, WO.followUpDuration,WP.workPointId, WP.activationMethod, WP.employeeCodeActual, WP.employeeCodePlanned, ";
        stmt += " WP.servicePointId, WP.serviceQuantityActual, WP.serviceQuantityPlanned, WP.workPointActivationDateTime, WP.workPointEffort  FROM work WK INNER JOIN WorkOrder WO ON WO.countryCode = WK.countryCode and WO.businessCode = WK.businessCode ";
        stmt += " AND WO.propertyCode = WK.propertyCode AND WO.workId = WK.workId  AND WO.workOrderStatus = WK.workStatus INNER JOIN workAttendance WA ON WA.workId = WK.workId AND   WA.countryCode = WK.countryCode AND WA.businessCode = WK.businessCode ";
        stmt += " INNER JOIN workPoint WP ON WP.workAttendanceId = WA.workAttendanceId AND WP.workId = WK.workId AND WP.workOrderId=WO.workorderId AND WP.propertyCode = WK.propertyCode WHERE WK.workStatus = '" + PCConstant.VISIT_CLOSED + "'";
        printMessage("Inside getClosedWorksFromTable - stmt: " + stmt);
        executeDirectQuery(stmt, success_getClosedWorks, error_getClosedWorks);
    } catch (err) {
        printMessage("Error: " + err);
        dismissLoading();
    }
}
/** Description : To get treatments records from local db for completed works **/
function getTreatmentsFromTable() {
    printMessage("Inside getTreatmentsFromTable ");
    try {
        /*if (!isEmpty(gblWorkPointIds) && gblWorkPointIds.length > 0) {
          	var stmt 					= "SELECT treatmentId, workPointId, treatmentBatch, treatmentPreparationCode, treatmentQuantity, treatmentRecordedDateTime FROM treatment WHERE workPointId IN ("; 
            for (var i = 0; i < gblWorkPointIds.length; i++) {
                stmt					+= "'" + gblWorkPointIds[i] + "'";
                if (i < (gblWorkPointIds.length - 1)) {
                    stmt				+= ",";
                }
            }
            stmt						+= ")";
            printMessage("Inside getTreatmentsFromTable - stmt: " + stmt);
            executeDirectQuery(stmt, success_getTreatments, error_getTreatments);
        }*/
        if (!isEmpty(gblWorkPointIds) && gblWorkPointIds.length > 0) {
            var whereStatmnt = " WHERE workPointId IN (";
            for (var i = 0; i < gblWorkPointIds.length; i++) {
                whereStatmnt += "'" + gblWorkPointIds[i] + "'";
                if (i < (gblWorkPointIds.length - 1)) {
                    whereStatmnt += ",";
                }
            }
            whereStatmnt += ")";
            com.pc.workloads.treatment.find(whereStatmnt, success_getTreatments, error_getTreatments);
        }
    } catch (err) {
        printMessage("Error: " + err);
        dismissLoading();
    }
}
/********************************************* Custom DB operations *************************************************/
/** To open DB handler **/
function openLocalDatabase() {
    var dbname = kony.sync.getDBName();
    var dbObjectId = kony.sync.getConnectionOnly(dbname, dbname);
    return dbObjectId;
}
/** To execute custom sql queries from local db **/
function executeCustomQuery(sqlStatement, params, successCallback, errorCallback) {
    var dbname = kony.sync.getDBName();
    kony.sync.single_select_execute(dbname, sqlStatement, params, successCallback, errorCallback);
}
/** To execute direct sql queries in local db **/
function executeDirectQuery(sqlStatement, successCallback, errorCallback) {
    printMessage("Inside - executeCustomQuery " + sqlStatement);
    sync.executeSelectQuery(sqlStatement, successCallback, errorCallback);
}
/********************************************* Transaction Callbacks *************************************************/
/** Transaction common success callback - For sql statement execution **/
function success_transCommonCallback(transactionId, result) {
    printMessage("Inside - success_transCommonCallback");
}
/** Transaction common error callback - For sql statement execution **/
function error_transCommonCallback(error) {
    printMessage("Inside - error_transCommonCallback");
}
/** Common success callback - For sql statement execution **/
function success_commonCallback() {
    printMessage("Inside - success_commonCallback");
}
/** newly added for storing singnature in locad DB **/
/** To update customer signature in local DB table work **/
function updateCustomerSignatureInLocalTbl(customerSignature, signedDateStr) {
    try {
        printMessage("Inside - updateCustomerSignatureInLocalTbl ");
        var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
        var isCustomerApproved = (isEmpty(visitInProgressObj.noSignatureReason)) ? 'true' : 'false';
        var customerNameSigned = (!isEmpty(visitInProgressObj.customerName)) ? visitInProgressObj.customerName : '';
        var customerNoSignReason = (!isEmpty(visitInProgressObj.noSignatureReason)) ? visitInProgressObj.noSignatureReason : '';
        var issueTypeCodeNoSignature = (!isEmpty(visitInProgressObj.noSignatureReason)) ? PCConstant.SIGNATURE_ISSUE_TYPE_CODE : "";
        var customerSignedDate = (!isEmpty(visitInProgressObj.signedDate)) ? visitInProgressObj.signedDate : '';
        var valuestableWK = {};
        valuestableWK.customerApprovalSignature = customerSignature;
        valuestableWK.customerApproval = isCustomerApproved;
        valuestableWK.customerApprovalName = customerNameSigned;
        valuestableWK.issueCodeNoSignature = customerNoSignReason;
        valuestableWK.issueTypeCodeNoSignature = issueTypeCodeNoSignature;
        //valuestableWK.signedDate				= signedDateStr;
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestableWK.businessCode = visitInProgressObj.businessCode;
        valuestableWK.countryCode = visitInProgressObj.countryCode;
        valuestableWK.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        printMessage("Inside - updateCustomerSignatureInLocalTbl: " + visitInProgressObj.workId);
        var whereStatmntWK = " WHERE workId = '" + visitInProgressObj.workId + "' ";
        com.pc.workloads.work.update(whereStatmntWK, valuestableWK, success_commonCallback, error_commonCallback, true);
        /*var sqlStatement		= "update work set customerApproval = ' "+isCustomerApproved+"', customerApprovalName = '"+customerNameSigned+"', issueCodeNoSignature ='"+customerNoSignReason+"', customerApprovalSignature = '"+customerSignature +"',issueTypeCodeNoSignature ='"+issueTypeCodeNoSignature +"' where workId = '" +  visitInProgressObj.workId  + "' ";
  		printMessage("Inside - getCapturedPhotoFromLocalTbl - sqlStatement: " + sqlStatement);
  		executeCustomQuery(sqlStatement, null, success_commonCallback, error_commonCallback);*/
    } catch (err) {
        printMessage("Inside - updateCustomerSignatureInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** To update captured photo in local DB table work **/
function updateCapturedPhotoInLocalTbl(capturedPhotoBase64) {
    printMessage("Inside - updateCapturedPhotoInLocalTbl ");
    try {
        var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
        var valuestableWK = {};
        valuestableWK.capturedPhoto = capturedPhotoBase64;
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestableWK.businessCode = visitInProgressObj.businessCode;
        valuestableWK.countryCode = visitInProgressObj.countryCode;
        valuestableWK.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        var whereStatmntWK = " WHERE workId = '" + visitInProgressObj.workId + "' ";
        com.pc.workloads.work.update(whereStatmntWK, valuestableWK, success_capturedPhotoCallback, error_capturedPhotoCallback, true);
        /*var sqlStatement		= "update work set capturedPhoto = ' "+capturedPhotoBase64+"' where workId = '" +  visitInProgressObj.workId  + "' ";
    	printMessage("Inside - getCapturedPhotoFromLocalTbl - sqlStatement: " + sqlStatement);
  	   	executeCustomQuery(sqlStatement, null, success_capturedPhotoCallback, error_commonCallback);*/
    } catch (err) {
        printMessage("Inside - updateCapturedPhotoInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** Common error callback - For sql statement execution **/
function error_commonCallback(error) {
    printMessage("Inside - error_commonCallback" + JSON.stringify(error));
}
/** To fetch customer signature from local DB **/
function getCustomerSignatureFromLocalTbl(workIdStr) {
    printMessage("Inside - getCustomerSignatureFromLocalTbl - workIdStr: " + workIdStr);
    //var sqlStatement		= "SELECT customerApprovalSignature, signedDate  FROM work where workId = '" + workIdStr + "' ";
    /*var sqlStatement		= "SELECT customerApprovalSignature FROM work where workId = '" + workIdStr + "' ";
    printMessage("Inside - getCustomerSignatureFromLocalTbl - sqlStatement: " + sqlStatement);
    executeCustomQuery(sqlStatement, null, success_getCustomerSignature, error_getCustomerSignature);*/
    var whereStatmnt = " where workId = '" + workIdStr + "' ";
    com.pc.workloads.work.find(whereStatmnt, success_getCustomerSignature, error_getCustomerSignature);
}
/** To fetch captured photo from local DB **/
function getCapturedPhotoFromLocalTbl(workIdStr) {
    printMessage("Inside - getCapturedPhotoFromLocalTbl - workIdStr: " + workIdStr);
    /*var sqlStatement		= "SELECT capturedPhoto FROM work where workId = '" + workIdStr + "' ";
    printMessage("Inside - getCapturedPhotoFromLocalTbl - sqlStatement: " + sqlStatement);
    executeCustomQuery(sqlStatement, null, success_getCapturedPhoto, error_getCapturedPhoto);*/
    var whereStatmnt = " where workId = '" + workIdStr + "' ";
    com.pc.workloads.work.find(whereStatmnt, success_getCapturedPhoto, error_getCapturedPhoto);
}
/** To fetch customer signature from local DB **/
function getCustomerSignatureFromLocalTblForClosedWork(workIdStr) {
    printMessage("Inside - getCustomerSignatureFromLocalTbl - workIdStr: " + workIdStr);
    //var sqlStatement		= "SELECT customerApprovalSignature, signedDate  FROM work where workId = '" + workIdStr + "' ";
    /*var sqlStatement		= "SELECT customerApprovalSignature FROM work where workId = '" + workIdStr + "' ";
    printMessage("Inside - getCustomerSignatureFromLocalTbl - sqlStatement: " + sqlStatement);
    executeCustomQuery(sqlStatement, null, successCallbackValidateData, failureCallbackValidateSigData);*/
    var whereStatmnt = " where workId = '" + workIdStr + "' ";
    com.pc.workloads.work.find(whereStatmnt, successCallbackValidateData, failureCallbackValidateSigData);
}

function getCustomerSignatureFromLocalTblCompleted(workIdStr) {
    printMessage("Inside - getCustomerSignatureFromLocalTbl - workIdStr: " + workIdStr);
    //var sqlStatement		= "SELECT customerApprovalSignature, signedDate  FROM work where workId = '" + workIdStr + "' ";
    /*var sqlStatement		= "SELECT customerApprovalSignature FROM work where workId = '" + workIdStr + "' ";
    printMessage("Inside - getCustomerSignatureFromLocalTbl - sqlStatement: " + sqlStatement);
    executeCustomQuery(sqlStatement, null, successCallbackCompletedSignData, failureCallbackCompletedSigData);*/
    var whereStatmnt = " where workId = '" + workIdStr + "' ";
    com.pc.workloads.work.find(whereStatmnt, successCallbackCompletedSignData, failureCallbackCompletedSigData);
}
/** To fetch captured photo from local DB **/
function getCapturedPhotoFromLocalTblCompleted(workIdStr) {
    printMessage("Inside - getCapturedPhotoFromLocalTbl - workIdStr: " + workIdStr);
    /*var sqlStatement		= "SELECT capturedPhoto FROM work where workId = '" + workIdStr + "' ";
    printMessage("Inside - getCapturedPhotoFromLocalTbl - sqlStatement: " + sqlStatement);
    executeCustomQuery(sqlStatement, null, success_getCapturedCompletedPhoto, error_getCapturedCompletedPhoto);*/
    var whereStatmnt = " where workId = '" + workIdStr + "' ";
    com.pc.workloads.work.find(whereStatmnt, success_getCapturedCompletedPhoto, error_getCapturedCompletedPhoto);
}

function updateCustomerSignatureInLocalTableAsEmpty(customerSignature, signedDateStr) {
    try {
        printMessage("Inside - updateCustomerSignatureInLocalTableAsEmpty ");
        var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
        //var isCustomerApproved				= (isEmpty(visitInProgressObj.noSignatureReason)) ? 'true' : 'false';
        //var customerNameSigned				= (!isEmpty(visitInProgressObj.customerName)) ? visitInProgressObj.customerName : '';
        //var customerNoSignReason				= (!isEmpty(visitInProgressObj.noSignatureReason)) ? visitInProgressObj.noSignatureReason : '';
        //var issueTypeCodeNoSignature			= (!isEmpty(visitInProgressObj.noSignatureReason)) ? PCConstant.SIGNATURE_ISSUE_TYPE_CODE : "";
        //var customerSignedDate				= (!isEmpty(visitInProgressObj.signedDate)) ? visitInProgressObj.signedDate : '';
        var valuestableWK = {};
        valuestableWK.customerApprovalSignature = customerSignature;
        //valuestableWK.signedDate				= signedDateStr;
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestableWK.businessCode = visitInProgressObj.businessCode;
        valuestableWK.countryCode = visitInProgressObj.countryCode;
        valuestableWK.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        printMessage("Inside - updateCustomerSignatureInLocalTbl visitInProgressObj.workId " + visitInProgressObj.workId);
        var whereStatmntWK = " WHERE workId = '" + visitInProgressObj.workId + "' ";
        com.pc.workloads.work.update(whereStatmntWK, valuestableWK, success_SignatureInLocalTblasEmpty, error_commonCallback, true);
        /*var sqlStatement		= "update work set customerApprovalSignature = '"+customerSignature +"' where workId = '" + visitInProgressObj.workId + "' ";
  		printMessage("Inside - getCapturedPhotoFromLocalTbl - sqlStatement: " + sqlStatement);
  		executeCustomQuery(sqlStatement, null, success_SignatureInLocalTblasEmpty, error_commonCallback);*/
    } catch (err) {
        printMessage("Inside - updateCustomerSignatureInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** To update captured photo in local DB table work **/
function updatePhotoInLocalTblasEmpty(capturedPhotoBase64) {
    //printMessage("Inside - updateCapturedPhotoInLocalTbl "+capturedPhotoBase64);
    try {
        var visitInProgressObj = gblMyAppDataObject.inProgressVisit;
        var valuestableWK = {};
        valuestableWK.capturedPhoto = capturedPhotoBase64;
        /** Starts - Below values are populated here for pass checkIntegrity sync method **/
        valuestableWK.businessCode = visitInProgressObj.businessCode;
        valuestableWK.countryCode = visitInProgressObj.countryCode;
        valuestableWK.workId = visitInProgressObj.workId;
        /** Ends - Below values are populated here for pass checkIntegrity sync method **/
        var whereStatmntWK = " WHERE workId = '" + visitInProgressObj.workId + "' ";
        com.pc.workloads.work.update(whereStatmntWK, valuestableWK, success_commonCallback, error_commonCallback, true);
        /*var sqlStatement		= "update work set capturedPhoto = ' "+capturedPhotoBase64+"' where workId = '" +  visitInProgressObj.workId  + "' ";
    	printMessage("Inside - getCapturedPhotoFromLocalTbl - sqlStatement: " + sqlStatement);
  	 	executeCustomQuery(sqlStatement, null, success_commonCallback, error_commonCallback);*/
    } catch (err) {
        printMessage("Inside - updateCapturedPhotoInLocalTbl - Exception: " + err);
        dismissLoading();
    }
}
/** To fetch service areas from local DB  (patch update) production 1.0 to 1.1**/
function getServiceAreasFromLocalTblForUpdate() {
    printMessage(" Inside - getServiceAreaFromLocalTbl ");
    /*var sqlStatement		= "SELECT * FROM ServiceAreaPC WHERE assignedArea = 'true'";
    executeCustomQuery(sqlStatement, null, success_getServiceAreas, error_getServiceAreas);*/
    var whereStatmnt = " WHERE assignedArea = 'true' ";
    com.pc.serviceareas.ServiceAreaPc.find(whereStatmnt, success_getServiceAreas_update, error_getServiceAreas);
}
/** Pilot version patch - This is used for updating work status back to Closed from Completed. To fix a pilot version issue **/
function patchUpdateWorkStatusAsClosedInLocalTbl() {
    printMessage("Inside - patchUpdateWorkStatusAsClosedInLocalTbl ");
    var sqlStatement = "UPDATE work SET workStatus = '" + PCConstant.VISIT_CLOSED + "' WHERE Datetime(workDateStartActual) <= Datetime('2016-12-08') AND workStatus = '" + PCConstant.VISIT_COMPLETED + "'";
    printMessage("Inside - patchUpdateWorkStatusAsClosedInLocalTbl - sqlStatement " + sqlStatement);
    executeCustomQuery(sqlStatement, null, success_commonCallback, error_commonCallback);
}