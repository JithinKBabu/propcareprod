/****************************************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer Email selection, Source selection and Language selection
 *****************************************************************************************/
/** Email selection popup - show **/
function showEmailSelectionPopup() {
    PopupChangeEmail.lblEmailListTitle.text = getI18nString("common.label.chooseAccount");
    PopupChangeEmail.btnCancelEmail.text = getI18nString("common.label.cancel");
    PopupChangeEmail.btnConfirmEmail.text = getI18nString("common.label.confirm");
    PopupChangeEmail.show();
}
/** Settings popup - show **/
function showSettingsPopup() {
    PopupSettings.lblSettingsTitle.text = getI18nString("common.label.settings");
    PopupSettings.lblSource.text = getI18nString("common.label.source");
    PopupSettings.lblLanguage.text = getI18nString("common.label.language");
    PopupSettings.btnCancelSettings.text = getI18nString("common.label.cancel");
    PopupSettings.btnConfirmSettings.text = getI18nString("common.label.confirm");
    PopupSettings.show();
}
/** Validate configured emails in device **/
function getDeviceConfiguredEmails() {
    printMessage("Inside - getDeviceConfiguredEmails");
    var frmObj = kony.application.getCurrentForm();
    var configuredEmails = EmailChecker.getEmail();
    printMessage("Inside - getDeviceConfiguredEmails - configuredEmails" + JSON.stringify(configuredEmails));
    printMessage("Inside - getDeviceConfiguredEmails - typeof(configuredEmails)" + typeof(configuredEmails));
    printMessage("Inside - getDeviceConfiguredEmails - Object.keys(configuredEmails).length" + Object.keys(configuredEmails).length);
    if (typeof(configuredEmails) === "object" && Object.keys(configuredEmails).length === 0) {
        configuredEmails = [];
    }
    if (gblAppMode === "DEVELOPER") {
        configuredEmails.push("service.trak1@rentokil-initial.com");
        configuredEmails.push("service.trak6@rentokil-initial.com");
        configuredEmails.push("lee.peet@rentokil.com");
        configuredEmails.push("damion.isaac@rentokil-initial.com");
    }
    var emailsCount = configuredEmails.length;
    var segMasterData = [];
    var selectedEmailId = (!isEmpty(gblEmployeeObject) && !isEmpty(gblEmployeeObject.email)) ? gblEmployeeObject.email : null;
    if (emailsCount > 0) {
        var aRecord = null;
        var anItem = null;
        if (emailsCount > 1 && emailsCount < 4) {
            for (var i = 0; i < emailsCount; i++) {
                aRecord = configuredEmails[i];
                anItem = {};
                anItem.lblItem = aRecord;
                if (!isEmpty(selectedEmailId)) {
                    if (selectedEmailId == aRecord) {
                        anItem.imgRadioBtnIcon = PCConstant.RADIO_BTN_ON;
                    } else {
                        anItem.imgRadioBtnIcon = PCConstant.RADIO_BTN_OFF;
                    }
                } else {
                    anItem.imgRadioBtnIcon = (i === 0) ? PCConstant.RADIO_BTN_ON : PCConstant.RADIO_BTN_OFF;
                }
                segMasterData.push(anItem);
            }
            gblSegEmailLastSelIndex = 0;
            showOverlayPopupOnLoginScreen(false, true, segMasterData, null);
        } else if (emailsCount > 3) {
            var defSelectedEmail = "";
            for (var j = 0; j < emailsCount; j++) {
                aRecord = configuredEmails[j];
                segMasterData.push([aRecord, aRecord]);
                if (!isEmpty(selectedEmailId) && selectedEmailId == aRecord) {
                    defSelectedEmail = aRecord;
                }
            }
            gblSegEmailLastSelIndex = 0;
            var selEmail = configuredEmails[0];
            if (!isEmpty(defSelectedEmail)) {
                selEmail = defSelectedEmail;
            }
            showOverlayPopupOnLoginScreen(false, false, segMasterData, null, selEmail);
        } else {
            if (frmObj.id === PCConstant.FORM_LOGIN) {
                if (isNetworkAvailable()) {
                    gblMyAppDataObject.selectedEmail = configuredEmails[0];
                    resetSync(onSyncResetSuccessCallback);
                } else {
                    showMessagePopup("common.message.noInternet", ActionOnMessagePopupOkBtn);
                    dismissLoading();
                }
            } else if (gblMyAppDataObject.selectedEmail != configuredEmails[0]) {
                printMessage(" Only one email configured and it is a new one ");
            } else {
                printMessage(" No change in email ");
            }
        }
    } else {
        if (frmObj.id === PCConstant.FORM_LOGIN) {
            showMessagePopup("common.message.noRentokilEmailConfigured", ActionOnMessagePopupOkBtn);
        } else {
            showMessagePopup("common.message.noRentokilEmailConfigured", ActionPopupMessageOk);
        }
        dismissLoading();
    }
}
/** To display settings / Email selections popup on login screen **/
function showOverlayPopupOnLoginScreen(isSettings, isRadioBtnGrp, firstSetData, secondSetData, firstSelKey, secondSelKey) {
    printMessage(" Inside - showOverlayPopupOnLoginScreen ");
    if (isSettings === true) {
        var firstSetDataCount = firstSetData.length;
        var secondSetDataCount = secondSetData.length;
        var aItemObj = null;
        if (isRadioBtnGrp === true) {
            var segDataSet = [];
            var segSectionSet = []; /** Includes Header object and row data sets **/
            var segRowSet = []; /** Includes row data sets **/
            var sectionObj = null;
            var aRowObj = null;
            if (firstSetDataCount > 0) {
                sectionObj = {};
                sectionObj.lblTitle = getI18nString("common.label.source");
                segSectionSet.push(sectionObj);
                for (var i = 0; i < firstSetDataCount; i++) {
                    aItemObj = firstSetData[i];
                    aRowObj = {};
                    if ((isEmpty(firstSelKey) && i === 0) || (!isEmpty(firstSelKey) && firstSelKey == aItemObj.iCABSServer)) {
                        aRowObj.imgRadioBtnIcon = PCConstant.RADIO_BTN_ON;
                    } else {
                        aRowObj.imgRadioBtnIcon = PCConstant.RADIO_BTN_OFF;
                    }
                    aRowObj.lblItem = aItemObj.displayName;
                    aRowObj.itemCode = aItemObj.iCABSServer;
                    segRowSet.push(aRowObj);
                }
                segSectionSet.push(segRowSet);
                segDataSet.push(segSectionSet);
            }
            if (secondSetDataCount > 0) {
                segSectionSet = [];
                segRowSet = [];
                sectionObj = {};
                sectionObj.lblTitle = getI18nString("common.label.language");
                segSectionSet.push(sectionObj);
                for (var j = 0; j < secondSetDataCount; j++) {
                    aItemObj = secondSetData[j];
                    aRowObj = {};
                    if ((isEmpty(secondSelKey) && j === 0) || (!isEmpty(secondSelKey) && secondSelKey == aItemObj.code)) {
                        aRowObj.imgRadioBtnIcon = PCConstant.RADIO_BTN_ON;
                    } else {
                        aRowObj.imgRadioBtnIcon = PCConstant.RADIO_BTN_OFF;
                    }
                    aRowObj.lblItem = aItemObj.name;
                    aRowObj.itemCode = aItemObj.code;
                    segRowSet.push(aRowObj);
                }
                segSectionSet.push(segRowSet);
                segDataSet.push(segSectionSet);
            }
            PopupSettings.segSettingsList.setData(segDataSet);
            PopupSettings.hbxSettingsContentContainer.setVisibility(true);
            PopupSettings.hbxSettingsContentContainerAlt.setVisibility(false);
            PopupSettings.lbxSourceList.masterData = [];
            PopupSettings.lbxLanguageList.masterData = [];
        } else {
            var sourceDataSet = [];
            var languageDataSet = [];
            if (firstSetDataCount > 0) {
                var selKeyFirst = firstSelKey;
                for (var k = 0; k < firstSetDataCount; k++) {
                    aItemObj = firstSetData[k];
                    sourceDataSet.push([aItemObj.iCABSServer, aItemObj.displayName]);
                    if (k === 0 && isEmpty(firstSelKey)) {
                        selKeyFirst = aItemObj.iCABSServer;
                    }
                }
                PopupSettings.lbxSourceList.masterData = sourceDataSet;
                PopupSettings.lbxSourceList.selectedKey = selKeyFirst;
            }
            if (secondSetDataCount > 0) {
                var selKeySecond = secondSelKey;
                for (var l = 0; l < secondSetDataCount; l++) {
                    aItemObj = secondSetData[l];
                    languageDataSet.push([aItemObj.code, aItemObj.name]);
                    if (l === 0 && isEmpty(secondSelKey)) {
                        selKeySecond = aItemObj.code;
                    }
                }
                PopupSettings.lbxLanguageList.masterData = languageDataSet;
                PopupSettings.lbxLanguageList.selectedKey = selKeySecond;
            }
            PopupSettings.hbxSettingsContentContainerAlt.setVisibility(true);
            PopupSettings.hbxSettingsContentContainer.setVisibility(false);
            PopupSettings.segSettingsList.setData([]);
        }
        showSettingsPopup();
    } else {
        if (isRadioBtnGrp === true) {
            PopupChangeEmail.segEmailList.setData(firstSetData);
            PopupChangeEmail.hbxSegEmailListContentContainer.setVisibility(true);
            PopupChangeEmail.hbxLbxEmailListContentContainer.setVisibility(false);
        } else {
            PopupChangeEmail.lbxEmailList.masterData = firstSetData;
            PopupChangeEmail.lbxEmailList.selectedKey = firstSelKey;
            PopupChangeEmail.hbxLbxEmailListContentContainer.setVisibility(true);
            PopupChangeEmail.hbxSegEmailListContentContainer.setVisibility(false);
        }
        showEmailSelectionPopup();
    }
    dismissLoading();
}
/** Selecting a row from email segment **/
function onEmailListRowClick() {
    var selectedRowIndex = PopupChangeEmail.segEmailList.selectedRowIndex[1];
    var selectedSectionIndex = PopupChangeEmail.segEmailList.selectedRowIndex[0];
    if (selectedRowIndex !== gblSegEmailLastSelIndex) {
        var preSelectedRow = PopupChangeEmail.segEmailList.data[gblSegEmailLastSelIndex];
        var curSelectedRow = PopupChangeEmail.segEmailList.data[selectedRowIndex];
        preSelectedRow.imgRadioBtnIcon = PCConstant.RADIO_BTN_OFF;
        curSelectedRow.imgRadioBtnIcon = PCConstant.RADIO_BTN_ON;
        PopupChangeEmail.segEmailList.setDataAt(preSelectedRow, gblSegEmailLastSelIndex, selectedSectionIndex);
        PopupChangeEmail.segEmailList.setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
        gblSegEmailLastSelIndex = selectedRowIndex;
    }
}
/** On click Emails popup Confirm button action **/
function onClickEmailConfirmButton() {
    showLoading("common.message.loading");
    var selectedEmail = null;
    if (PopupChangeEmail.hbxSegEmailListContentContainer.isVisible === true) {
        var selectedItem = PopupChangeEmail.segEmailList.data[gblSegEmailLastSelIndex];
        selectedEmail = selectedItem.lblItem;
    } else {
        selectedEmail = PopupChangeEmail.lbxEmailList.selectedKey;
    }
    printMessage(" selectedEmail is  " + selectedEmail);
    var curEmailAddress = (!isEmpty(gblEmployeeObject)) ? gblEmployeeObject.email : null;
    var isEmpAlreayLoggedIn = kony.store.getItem(PCConstant.IS_EMP_LOGGED_IN);
    PopupChangeEmail.dismiss();
    if (!isEmpty(curEmailAddress) && curEmailAddress == selectedEmail) {
        showMessagePopup("common.message.noChangeInEmail", ActionPopupMessageOk);
        dismissLoading();
    } else if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true) {
        showConfirmPopup("common.message.changeEmailWarning", ActionConfirmChangeEmail, ActionDeclineChangeEmail);
        dismissLoading();
    } else {
        gblMyAppDataObject.resetOnChangeSourceOrEmail();
        gblMyAppDataObject.selectedEmail = selectedEmail;
        resetSync(onSyncResetSuccessCallback);
    }
}
/** User confirmed change email **/
function confirmChangeEmail() {
    if (isNetworkAvailable()) {
        gblMyAppDataObject.resetOnChangeSourceOrEmail();
        var selectedEmail = null;
        if (PopupChangeEmail.hbxSegEmailListContentContainer.isVisible === true) {
            var selectedItem = PopupChangeEmail.segEmailList.data[gblSegEmailLastSelIndex];
            selectedEmail = selectedItem.lblItem;
        } else {
            selectedEmail = PopupChangeEmail.lbxEmailList.selectedKey;
        }
        gblMyAppDataObject.selectedEmail = selectedEmail;
        printMessage(" gblMyAppDataObject.selectedEmail is  " + gblMyAppDataObject.selectedEmail);
        resetSync(onSyncResetSuccessCallback);
        dismissConfirmPopup();
    } else {
        var isEmpAlreayLoggedIn = kony.store.getItem(PCConstant.IS_EMP_LOGGED_IN);
        if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true) {
            printMessage(" Network not available to sync data ");
            dismissLoading();
        } else {
            showMessagePopup("common.message.noNetwork", ActionDoExitApplication);
            dismissLoading();
        }
        dismissConfirmPopup();
    }
}
/** User declined changing email **/
function declineChangeEmail() {
    dismissConfirmPopup();
}
/** On click Emails popup Cancel button action **/
function onClickEmailCancelButton() {
    PopupChangeEmail.dismiss();
    if (isEmpty(gblEmployeeObject)) {
        exitApplication();
    }
}
/** Cancel Email account selection **/
function onCancelEmailSelection() {
    doExitApplication();
}
/** Selecting a row from settings segment **/
function onSettingsListRowClick() {
    var selectedRowIndex = PopupSettings.segSettingsList.selectedRowIndex[1];
    var selectedSectionIndex = PopupSettings.segSettingsList.selectedRowIndex[0];
    var preSelectedRow = null;
    var curSelectedRow = null;
    var sectionData = null;
    var sectionRows = null;
    if (selectedSectionIndex === PCConstant.SOURCE_SECTION) {
        if (selectedRowIndex !== gblSegSourceLastSelIndex) {
            sectionData = PopupSettings.segSettingsList.data[selectedSectionIndex];
            sectionRows = sectionData[1];
            preSelectedRow = sectionRows[gblSegSourceLastSelIndex];
            curSelectedRow = sectionRows[selectedRowIndex];
            preSelectedRow.imgRadioBtnIcon = PCConstant.RADIO_BTN_OFF;
            curSelectedRow.imgRadioBtnIcon = PCConstant.RADIO_BTN_ON;
            PopupSettings.segSettingsList.setDataAt(preSelectedRow, gblSegSourceLastSelIndex, selectedSectionIndex);
            PopupSettings.segSettingsList.setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
            gblSegSourceLastSelIndex = selectedRowIndex;
        }
    } else {
        if (selectedRowIndex !== gblSegLanguageLastSelIndex) {
            sectionData = PopupSettings.segSettingsList.data[selectedSectionIndex];
            sectionRows = sectionData[1];
            preSelectedRow = sectionRows[gblSegLanguageLastSelIndex];
            curSelectedRow = sectionRows[selectedRowIndex];
            preSelectedRow.imgRadioBtnIcon = PCConstant.RADIO_BTN_OFF;
            curSelectedRow.imgRadioBtnIcon = PCConstant.RADIO_BTN_ON;
            PopupSettings.segSettingsList.setDataAt(preSelectedRow, gblSegLanguageLastSelIndex, selectedSectionIndex);
            PopupSettings.segSettingsList.setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
            gblSegLanguageLastSelIndex = selectedRowIndex;
        }
    }
}
/** Confirm Source and Language selection **/
function onConfirmSourceLanguageSelection() {
    printMessage(" Inside - onConfirmSourceLanguageSelection ");
    showLoading("common.message.loading");
    var isEmpAlreayLoggedIn = kony.store.getItem(PCConstant.IS_EMP_LOGGED_IN);
    var selLanCode = "";
    if (PopupSettings.hbxSettingsContentContainer.isVisible === true) {
        var selSourceObjSection = PopupSettings.segSettingsList.data[PCConstant.SOURCE_SECTION];
        var selLanguageObjSection = PopupSettings.segSettingsList.data[PCConstant.LANGUAGE_SECTION];
        var sourceObjRows = selSourceObjSection[1];
        var languageObjRows = selLanguageObjSection[1];
        var selSourceObj = sourceObjRows[gblSegSourceLastSelIndex];
        var selLanguageObj = languageObjRows[gblSegLanguageLastSelIndex];
        if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true) {
            if (gblMyAppDataObject.selICabServerCode == selSourceObj.itemCode && gblMyAppDataObject.selLanguage == selLanguageObj.itemCode) {
                printMessage(" No change in source or language ");
                dismissLoading();
            } else if (gblMyAppDataObject.selLanguage == selLanguageObj.itemCode) {
                gblMyAppDataObject.selICabServerCode = selSourceObj.itemCode;
                gblMyAppDataObject.selLanguage = selLanguageObj.itemCode;
                startSyncEmpServiceAreaAndConfigData();
            } else if (gblMyAppDataObject.selICabServerCode == selSourceObj.itemCode) {
                gblMyAppDataObject.selICabServerCode = selSourceObj.itemCode;
                gblMyAppDataObject.selLanguage = selLanguageObj.itemCode;
                changeDeviceLocale(gblMyAppDataObject.selLanguage, onChangeLanguageSuccessCallback, onChangeLanguageFailureCallback);
            } else {
                gblMyAppDataObject.selICabServerCode = selSourceObj.itemCode;
                gblMyAppDataObject.selLanguage = selLanguageObj.itemCode;
                changeDeviceLocale(gblMyAppDataObject.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
            }
        } else {
            selLanCode = kony.store.getItem(PCConstant.SEL_LAN_CODE);
            gblMyAppDataObject.selICabServerCode = selSourceObj.itemCode;
            gblMyAppDataObject.selLanguage = selLanguageObj.itemCode;
            if (isEmpty(selLanCode) || selLanCode != selLanguageObj.itemCode) {
                changeDeviceLocale(gblMyAppDataObject.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
            } else {
                startSyncEmpServiceAreaAndConfigData();
            }
        }
    } else {
        var selSourceKey = PopupSettings.lbxSourceList.selectedKey;
        var selLanguageKey = PopupSettings.lbxLanguageList.selectedKey;
        if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true) {
            if (gblMyAppDataObject.selICabServerCode == selSourceKey && gblMyAppDataObject.selLanguage == selLanguageKey) {
                printMessage(" No change in source or language ");
                dismissLoading();
            } else if (gblMyAppDataObject.selLanguage == selLanguageKey) {
                gblMyAppDataObject.selICabServerCode = selSourceKey;
                gblMyAppDataObject.selLanguage = selLanguageKey;
                startSyncEmpServiceAreaAndConfigData();
            } else if (gblMyAppDataObject.selICabServerCode == selSourceKey) {
                gblMyAppDataObject.selICabServerCode = selSourceKey;
                gblMyAppDataObject.selLanguage = selLanguageKey;
                changeDeviceLocale(gblMyAppDataObject.selLanguage, onChangeLanguageSuccessCallback, onChangeLanguageFailureCallback);
            } else {
                gblMyAppDataObject.selICabServerCode = selSourceKey;
                gblMyAppDataObject.selLanguage = selLanguageKey;
                changeDeviceLocale(gblMyAppDataObject.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
            }
        } else {
            selLanCode = kony.store.getItem(PCConstant.SEL_LAN_CODE);
            gblMyAppDataObject.selICabServerCode = selSourceKey;
            gblMyAppDataObject.selLanguage = selLanguageKey;
            if (isEmpty(selLanCode) || selLanCode != selLanguageKey) {
                changeDeviceLocale(gblMyAppDataObject.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
            } else {
                startSyncEmpServiceAreaAndConfigData();
            }
        }
    }
    PopupSettings.dismiss();
    printMessage("gblMyAppDataObject " + JSON.stringify(gblMyAppDataObject));
}
/** Cancel Source and Language selection **/
function onCancelSourceLanguageSelection() {
    if (!isEmpty(gblEmployeeObject)) {
        PopupSettings.dismiss();
    } else {
        doExitApplication();
    }
}
/** Success callback - change device locale - Based on settings screen language selection **/
function onChangeLanguageSuccessCallback(oldlocalename, newlocalename) {
    printMessage(" Inside - onChangeLanguageSuccessCallback ");
    kony.store.setItem(PCConstant.SEL_LAN_CODE, newlocalename);
    doRefreshi18n();
    dismissLoading();
}
/** Failure callback - change device locale - Based on settings screen language selection **/
function onChangeLanguageFailureCallback(errCode, errMsg) {
    printMessage(" Inside - onChangeLanguageFailureCallback ");
    dismissLoading();
}
/** Success callback - change device locale will initiate download data  - Based on settings screen language selection **/
function startSyncOnChangeLanguageSuccessCallback(oldlocalename, newlocalename) {
    printMessage(" Inside - startSyncOnChangeLanguageSuccessCallback ");
    kony.store.setItem(PCConstant.SEL_LAN_CODE, newlocalename);
    doRefreshi18n();
    startSyncEmpServiceAreaAndConfigData();
}
/** Failure callback - change device locale will initiate download data - Based on settings screen language selection **/
function startSyncOnChangeLanguageFailureCallback(errCode, errMsg) {
    printMessage(" Inside - startSyncOnChangeLanguageFailureCallback ");
    startSyncEmpServiceAreaAndConfigData();
}