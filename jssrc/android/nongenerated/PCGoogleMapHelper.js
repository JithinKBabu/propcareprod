//Type your code here
/** Class definition - CustomGoogleMapAPI **/
function CustomGoogleMapAPI(postcodeStr, reqType, latitude, longitude) {
    printMessage("Inside CustomGoogleMapAPI :: Object creation - GOOGLE MAP - reqType: " + reqType);
    this.postCode = postcodeStr;
    this.requestType = reqType;
    this.latitude = latitude;
    this.longitude = longitude;
    this.lan = kony.i18n.getCurrentLocale();
    this.geocodeCallback = function() {
        try {
            var strReadyState = this.readyState.toString();
            printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP -  geocodeCallback - state is: " + strReadyState);
            if (kony.string.equalsIgnoreCase(strReadyState, "4")) {
                printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - responseType: " + this.responseType + " status: " + this.status);
                if ((null !== this.response) && (constants.HTTP_RESPONSE_TYPE_JSON == this.responseType) && ("200" == this.status)) {
                    printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - response: " + JSON.stringify(this.response));
                    if (this.response.results.length > 0) {
                        var gpsXcord = this.response.results[0].geometry.location.lat;
                        var gpsYcord = this.response.results[0].geometry.location.lng;
                        printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - Latitude: " + gpsXcord + " Longitude: " + gpsYcord);
                        var addressDetails = this.response.results[0].address_components;
                        if (addressDetails.length > 0) {
                            var anItem = null;
                            for (var i = 0; i < addressDetails.length; i++) {
                                anItem = addressDetails[i];
                                printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - anItem: " + JSON.stringify(anItem));
                                if (anItem.types[0] == "postal_code") {
                                    gblMyAppDataObject.addGeocodeForPostcode(anItem.short_name, gpsXcord, gpsYcord, true);
                                    break;
                                }
                            }
                        }
                    } else {
                        printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - ZERO results returned by Google Map API ");
                        if (result.status == "OVER_QUERY_LIMIT") {
                            printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - OVER_QUERY_LIMIT");
                        }
                    }
                }
            }
        } catch (Error) {
            printMessage("ERROR :", Error);
        }
    };
    this.locationNameCallback = function() {
        try {
            var strReadyState = this.readyState.toString();
            printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - locationNameCallback state is: " + strReadyState);
            if (kony.string.equalsIgnoreCase(strReadyState, "4")) {
                printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - responseType: " + this.responseType + " status: " + this.status);
                if ((null !== this.response) && (constants.HTTP_RESPONSE_TYPE_JSON == this.responseType) && ("200" == this.status)) {
                    printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - response: " + JSON.stringify(this.response));
                    if (this.response.results.length > 0) {
                        printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - this.response.results[0]: " + JSON.stringify(this.response.results[0]));
                        gblMyLocation.desc = this.response.results[0].formatted_address;
                    } else {
                        printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - ZERO results returned by Google Map API ");
                        if (result.status == "OVER_QUERY_LIMIT") {
                            printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - OVER_QUERY_LIMIT");
                        }
                    }
                    dropJobLocationPinsInMap();
                }
            }
        } catch (Error) {
            printMessage("ERROR :", Error);
            dropJobLocationPinsInMap();
        }
    };
    this.locationRouteCallback = function() {
        try {
            var strReadyState = this.readyState.toString();
            printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - locationRouteCallback state is: " + strReadyState);
            if (kony.string.equalsIgnoreCase(strReadyState, "4")) {
                printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - responseType: " + this.responseType + " status: " + this.status);
                if ((null !== this.response) && (constants.HTTP_RESPONSE_TYPE_JSON == this.responseType) && ("200" == this.status)) {
                    printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - response: " + JSON.stringify(this.response));
                    if (this.response.results.length > 0) {
                        var gpsXcord = this.response.results[0].geometry.location.lat;
                        var gpsYcord = this.response.results[0].geometry.location.lng;
                        if (!isEmpty(gpsXcord) && !isEmpty(gpsYcord)) {
                            openGoogleMap(gpsXcord, gpsYcord);
                        } else {
                            printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - No corrdinates found for post code");
                        }
                    } else {
                        printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - ZERO results returned by Google Map API ");
                        if (result.status == "OVER_QUERY_LIMIT") {
                            printMessage("Inside CustomGoogleMapAPI - GOOGLE MAP - OVER_QUERY_LIMIT");
                        }
                    }
                }
            }
        } catch (Error) {
            printMessage("ERROR :", Error);
            dropJobLocationPinsInMap();
        }
    };
    var postCodeURL = "";
    this.httpClientObj = new kony.net.HttpRequest();
    this.httpClientObj.timeout = 5000; // 5 secs 
    if (this.requestType == "LOCATION_NAME") {
        postCodeURL = encodeURI("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + this.latitude + "," + this.longitude + "&sensor=false&language=" + this.lang);
        this.httpClientObj.onReadyStateChange = this.locationNameCallback;
    } else if (this.requestType == "LOCATION_ROUTE") {
        postCodeURL = encodeURI("http://maps.googleapis.com/maps/api/geocode/json?components=postal_code:" + this.postCode + "&sensor=false");
        this.httpClientObj.onReadyStateChange = this.locationRouteCallback;
    } else {
        postCodeURL = encodeURI("http://maps.googleapis.com/maps/api/geocode/json?components=postal_code:" + this.postCode + "&sensor=false");
        this.httpClientObj.onReadyStateChange = this.geocodeCallback;
    }
    printMessage("CustomGoogleMapAPI :: encoded url - GOOGLE MAP -" + postCodeURL);
    this.httpClientObj.open(constants.HTTP_METHOD_GET, postCodeURL, false);
    this.httpClientObj.send();
}
/** Method to find out GeoCoordinates nased on UK Postcode **/
function getGeocoordinateForPostcode() {
    printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP -");
    if (isNetworkAvailable()) {
        printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP - Network available");
        if (!isEmpty(gblMyAppDataObject.geoCoordinates)) {
            printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP - gblMyAppDataObject.geoCoordinates is valid");
            var aGeoCordinateObj = null;
            var aPostcodeKey = "";
            var postcodeKeys = Object.keys(gblMyAppDataObject.geoCoordinates);
            var dummyReference = "";
            printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP - gblMyAppDataObject.geoCoordinates: " + JSON.stringify(gblMyAppDataObject.geoCoordinates));
            printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP - postcodeKeys length: " + postcodeKeys.length);
            if (postcodeKeys.length > 0) {
                for (var i = 0; i < postcodeKeys.length; i++) {
                    aPostcodeKey = postcodeKeys[i];
                    printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP - selected postcode: " + aPostcodeKey);
                    aGeoCordinateObj = gblMyAppDataObject.geoCoordinates[aPostcodeKey];
                    printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP - existing geocoordinates: " + JSON.stringify(aGeoCordinateObj));
                    if (isEmpty(aGeoCordinateObj.latitude) || isEmpty(aGeoCordinateObj.longitude)) {
                        printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP - CustomGoogleMapAPI called for postcode: " + aPostcodeKey);
                        dummyReference = new CustomGoogleMapAPI(aPostcodeKey, "POSTCODE", "", "");
                    }
                }
            }
        } else {
            printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP - gblMyAppDataObject.geoCoordinates is invalid");
        }
    } else {
        printMessage("Inside getGeocoordinateForPostcode ::  GOOGLE MAP - Network not available");
    }
}
/**	Purpose: This function will be called when getCurrentLocation .This method will be used on initial loading of map **/
function getCurrentLocation() {
    printMessage("Inside getCurrentLocation :::: - GOOGLE MAP - ");
    showLoading("common.message.loading");
    gblIsMyLocationCaptured = true;
    Home.mapLocation.clear();
    var positionoptions = {};
    positionoptions.enablehighaccuracy = false;
    positionoptions.timeout = 15000; // 15 secs 
    positionoptions.maximumage = 1000;
    try {
        kony.location.getCurrentPosition(geoLocationSuccessCallback, geoLocationErrorcallback, positionoptions);
    } catch (exception) {
        showMapLocation();
    }
}
/**	Purpose: getCurrentPosition - Success callback **/
function geoLocationSuccessCallback(position) {
    try {
        Home.flxScheduleContainer.setVisibility(true);
        Home.flxMapContentContainer.setVisibility(true);
        gblIsMyLocationCaptured = true;
        printMessage(" Inside geoLocationSuccessCallback: - GOOGLE MAP -  " + JSON.stringify(position));
        gblMyLocation.lat = position.coords.latitude;
        gblMyLocation.lon = position.coords.longitude;
        gblMyLocation.showcallout = false;
        gblMyLocation.image = "pinb.png";
        var dummyReference = new CustomGoogleMapAPI("", "LOCATION_NAME", gblMyLocation.lat, gblMyLocation.lon);
        dismissLoading();
    } catch (error) {
        printMessage(" Inside geoLocationSuccessCallback: Exception - GOOGLE MAP - " + JSON.stringify(error));
        showMapLocation();
        dismissLoading();
    }
}
/**	Purpose: getCurrentPosition - Error callback **/
function geoLocationErrorcallback(positionerror) {
    gblIsMyLocationCaptured = false;
    printMessage(" Inside geoLocationErrorcallback: - GOOGLE MAP - " + JSON.stringify(positionerror));
    Home.flxScheduleContainer.setVisibility(true);
    Home.flxMapContentContainer.setVisibility(true);
    showMessagePopupWithouti18n(positionerror.message, ActionPopupMessageOk);
    showMapLocation();
    dismissLoading();
}
/**	Purpose: This function will be called when getCurrentLocation failed  for showing default location **/
function showMapLocation() {
    printMessage(" Inside showMapLocation: - GOOGLE MAP - ");
    showMessagePopupWithouti18n("Location not available", ActionPopupMessageOk);
    Home.mapLocation.zoomLevel = 5;
    gblMyLocation.lat = "52.4475";
    gblMyLocation.lon = "-2.3552";
    gblMyLocation.showcallout = false;
    gblMyLocation.image = "pinb.png";
    var dummyReference = new CustomGoogleMapAPI("", "LOCATION_NAME", gblMyLocation.lat, gblMyLocation.lon);
}
/**	Purpose :To plot pins in Google map **/
function dropJobLocationPinsInMap() {
    try {
        printMessage(" Inside dropJobLocationPinsInMap: - GOOGLE MAP - ");
        var locationData = [];
        if (!isEmpty(gblMyLocation.lat) && !isEmpty(gblMyLocation.lon)) {
            locationData.push(gblMyLocation);
        }
        printMessage(" Inside dropJobLocationPinsInMap - gblMyAppDataObject.geoCoordinates: - GOOGLE MAP - " + JSON.stringify(gblMyAppDataObject.geoCoordinates));
        var postcodeKeys = Object.keys(gblMyAppDataObject.geoCoordinates);
        printMessage(" Inside dropJobLocationPinsInMap - postcodeKeys.length: - GOOGLE MAP - " + postcodeKeys.length);
        if (postcodeKeys.length > 0) {
            var aCoordinateObj = null;
            var postcodeKey = "";
            var aJobLocationObj = null;
            var callOutDataObj = null;
            for (var i = 0; i < postcodeKeys.length; i++) {
                postcodeKey = postcodeKeys[i];
                aCoordinateObj = gblMyAppDataObject.geoCoordinates[postcodeKey];
                if (!isEmpty(aCoordinateObj.latitude) && !isEmpty(aCoordinateObj.longitude)) {
                    aJobLocationObj = {};
                    aJobLocationObj.lat = aCoordinateObj.latitude;
                    aJobLocationObj.lon = aCoordinateObj.longitude;
                    aJobLocationObj.image = setMapPinImage(aCoordinateObj.jobDate);
                    aJobLocationObj.showcallout = true;
                    callOutDataObj = {};
                    callOutDataObj.lblName = aCoordinateObj.name;
                    callOutDataObj.lblAddress = aCoordinateObj.address;
                    callOutDataObj.lblPostcode = postcodeKey;
                    callOutDataObj.lblPropertyCode = aCoordinateObj.propertyCode;
                    aJobLocationObj.calloutData = callOutDataObj;
                    locationData.push(aJobLocationObj);
                }
            }
        }
        printMessage("Inside dropJobLocationPinsInMap - locationData: length - GOOGLE MAP -  " + locationData.length);
        printMessage("Inside dropJobLocationPinsInMap - locationData: - GOOGLE MAP - " + JSON.stringify(locationData));
        Home.mapLocation.widgetDataMapForCallout = {
            lblPropertyName: "lblName",
            lblAddress: "lblAddress",
            lblPostCode: "lblPostcode",
            lblPropertyCode: "lblPropertyCode"
        };
        if (locationData.length > 0) {
            Home.mapLocation.locationData = locationData;
            Home.mapLocation.zoomLevel = 9;
            Home.mapLocation.navigateToLocation(gblMyLocation, false, true);
        } else {
            printMessage("Inside dropJobLocationPinsInMap - No locations are available: - GOOGLE MAP - ");
        }
    } catch (e) {
        printMessage("Inside dropJobLocationPinsInMap - Exception: - GOOGLE MAP - " + JSON.stringify(e));
    }
}
/**	Purpose: To pick correct map pin image based on date [PAST / PRESENT / FUTURE] **/
function setMapPinImage(requestDate) {
    var pinImageStr = "greenpin.png"; /** Current Date => Green **/
    var dateStatus = getDateStatus(requestDate);
    printMessage("Inside setMapPinImage - GOOGLE MAP - requestDate: " + requestDate + " dateStatus: " + dateStatus);
    if (dateStatus == -1) {
        pinImageStr = "redpin.png"; /** Past Date => Red **/
    } else if (dateStatus == 1) {
        pinImageStr = "bluepin.png"; /** Future Date =>  Blue **/
    }
    printMessage("Inside setMapPinImage - GOOGLE MAP - pinImageStr: " + pinImageStr);
    return pinImageStr;
}
/**	Purpose: To navigate back to user's current location **/
function showNearByLocations() {
    printMessage("Inside showNearByLocations - GOOGLE MAP - gblIsMyLocationCaptured: " + gblIsMyLocationCaptured);
    if (gblIsMyLocationCaptured === false) {
        getCurrentLocation();
    } else {
        printMessage("Inside showNearByLocations - GOOGLE MAP -" + JSON.stringify(gblMyLocation));
        try {
            if (!isEmpty(gblMyLocation.lat) && !isEmpty(gblMyLocation.lon)) {
                Home.mapLocation.zoomLevel = 9;
                Home.mapLocation.navigateToLocation(gblMyLocation, false, true);
            }
        } catch (err) {
            printMessage("Inside showNearByLocations - GOOGLE MAP - Exception: " + JSON.stringify(err));
        }
    }
}
/**	Purpose: Display Previously Selected Location and zoom it **/
function showMeHere() {
    printMessage("Inside showMeHere - GOOGLE MAP - gblPrevSelLocation: " + JSON.stringify(gblPrevSelLocation));
    if (!isEmpty(gblPrevSelLocation.lat) && !isEmpty(gblPrevSelLocation.lon)) {
        Home.mapLocation.zoomLevel = 14;
        Home.mapLocation.navigateToLocation(gblPrevSelLocation, false, true);
    }
}
/**	Purpose: Map pin click handler **/
function onPinClickCallBack(mapid, locationData) {
    printMessage("Inside onPinClickCallBack - GOOGLE MAP - : " + JSON.stringify(locationData));
    if (!isEmpty(locationData.calloutData)) {
        gblPrevSelLocation = locationData;
    }
}
/**	Purpose: Map callout item click handler **/
function navigateToCustomerScreen(location) {
    gblCompletedVisitFlag = false;
    printMessage("Inside navigateToCustomerScreen - GOOGLE MAP - Location : " + JSON.stringify(location));
    printMessage("Inside navigateToCustomerScreen - GOOGLE MAP - calloutData : " + JSON.stringify(location.calloutData));
    if (!isEmpty(location.calloutData) && !isEmpty(location.calloutData.lblPropertyCode) && !isEmpty(location.calloutData.lblPostcode)) {
        var selDateStr = gblMyAppDataObject.getDateStrFromGeoCoordinates(location.calloutData.lblPostcode);
        printMessage("Inside navigateToCustomerScreen - GOOGLE MAP - selDateStr : " + selDateStr);
        var selScheduledSection = gblMyAppDataObject.getScheduledObjectsForDate(selDateStr);
        var selScheduledItem = null;
        var anItemObj = null;
        if (!isEmpty(selScheduledSection) && selScheduledSection.length > 0) {
            for (var i = 0; i < selScheduledSection.length; i++) {
                anItemObj = selScheduledSection[i];
                if (anItemObj.propertyCode == location.calloutData.lblPropertyCode) {
                    selScheduledItem = anItemObj;
                    break;
                }
            }
        }
        if (!isEmpty(selScheduledItem)) {
            gblMyAppDataObject.selScheduledItem = selScheduledItem;
            gblMyAppDataObject.selScheduledDateStr = selDateStr;
            if (isNetworkAvailable()) {
                showLoading("common.message.loading");
                startSyncWorkHistory();
            } else {
                showLoading("common.message.loading");
                //getServiceCoverFromLocalTbl();
                getWorkHistoryFromLocalTbl();
            }
        } else {
            printMessage("Inside navigateToCustomerScreen - GOOGLE MAP - matching property code doesn't exist in scheduled job list");
        }
    } else {
        printMessage("Inside navigateToCustomerScreen - GOOGLE MAP - location.calloutData is empty");
    }
}
/** To open google map in browser **/
function openGoogleMap(gpsXcord, gpsYcord) {
    var googleMapURL = "http://maps.google.com";
    googleMapURL += "?daddr=";
    googleMapURL += gpsXcord;
    googleMapURL += ",";
    googleMapURL += gpsYcord;
    googleMapURL += "&amp;ll=";
    kony.application.openURL(googleMapURL);
}