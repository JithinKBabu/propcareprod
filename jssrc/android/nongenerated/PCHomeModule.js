/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : UI Screen - Home - Methods
 ***********************************************************************/
/** Show home screen **/
function showHomeScreen() {
    printMessage(" Inside showHomeScreen ");
    if (gblRefreshHomeScreenFlag === true) {
        showLoading("common.message.refreshing");
    } else {
        showLoading("common.message.loading");
    }
    Home.lblNoScheduledJobs.setVisibility(false);
    Home.lblNoCompletedJobs.setVisibility(false);
    var flxCompletedListTopVal = Home.segHomeContentList.height;
    Home.flxCompletedList.top = flxCompletedListTopVal;
    Home.imgToggle.src = "scrollup.png";
    Home.flxCompletedJobsHeaderContainer.skin = "flxRedBgBorderedSkin";
    gblIsCompletedJobsVisible = false;
    loadDataOnHomeSegment();
    if (gblRefreshHomeScreenFlag === false) {
        onClickScrollToToday();
    }
    Home.show();
    if (gblShowCompletedJobs === true) {
        gblShowCompletedJobs = false;
        onClickCompletedJobs();
    }
    dismissLoading();
    gblRefreshHomeScreenFlag = false;
}
/** Home screen - PreShow **/
function onPreShowHomeScreen() {
    printMessage(" Inside onPreShowHomeScreen ");
    var isPatch1Executed = kony.store.getItem(PCConstant.IS_PATCH1_EXECUTED);
    var isPatch2Executed = kony.store.getItem(PCConstant.IS_PATCH2_EXECUTED);
    if (isEmpty(isPatch1Executed)) {
        /** patch for 1.0 to 1.1**/
        getServiceAreasFromLocalTblForUpdate();
        kony.store.setItem(PCConstant.IS_PATCH1_EXECUTED, true);
    }
    if (isEmpty(isPatch2Executed)) {
        /** patch for 1.1 to 1.2 **/
        patchUpdateWorkStatusAsClosedInLocalTbl();
        kony.store.setItem(PCConstant.IS_PATCH2_EXECUTED, true);
    }
    //printMessage("prod issue analysis  onPreShowHomeScreen---> "+JSON.stringify(gblMyAppDataObject));
    if (gblMyAppDataObject.isDayStarted === PCConstant.DAY_STARTED_DONT_KNOW || gblMyAppDataObject.isDayStarted === PCConstant.DAY_STARTED_NO) {
        Home.imgStartdayIcon.src = "endday.png";
    } else {
        Home.imgStartdayIcon.src = "startday.png";
    }
    //	printMessage("prod issue analysis  onPreShowHomeScreen---> selServiceArea "+JSON.stringify(gblMyAppDataObject.selServiceArea));
    //  var serviceAreaBool = isEmpty(gblMyAppDataObject.selServiceArea);
    //	printMessage("prod issue analysis  onPreShowHomeScreen---> serviceAreaBool "+serviceAreaBool);
    // 	printMessage("prod issue analysis  onPreShowHomeScreen---> serviceAreaBoolcode "+gblMyAppDataObject.selServiceArea.serviceAreaCode);
    Home.lblServiceAreaCode.text = (!isEmpty(gblMyAppDataObject.selServiceArea)) ? gblMyAppDataObject.selServiceArea.serviceAreaCode : "";
    refreshi18nHome();
}
/** Home screen - PostShow **/
function onPostShowHomeScreen() {
    // printMessage("  prod issue analysis  onPreShowHomeScreen---> "+JSON.stringify(gblMyAppDataObject));
    printMessage(" Inside onPostShowHomeScreen ");
    refreshSettingsUI();
    Home.btnStartday.setEnabled(true);
    Home.btnCalendar.setEnabled(true);
    destroyFormsExceptMe(PCConstant.FORM_HOME);
}
/** Home screen - Refresh all i18n **/
function refreshi18nHome() {
    Home.lblHeaderTitle.text = getI18nString("common.label.workView");
    Home.btnSchedule.text = getI18nString("common.label.schedule");
    Home.btnMap.text = getI18nString("common.label.map");
    Home.lblCompletedJobs.text = getI18nString("common.label.completedJobs");
    Home.lblNoScheduledJobs.text = getI18nString("common.message.noRecords");
    Home.lblNoCompletedJobs.text = getI18nString("common.message.noRecords");
    Home.btnCalendar.text = getCurrentDayStr();
}
/** To ask confirmation for exit application **/
function doConfirmAppExit() {
    showConfirmPopup("common.message.confirmLogout", ActionConfirmExit, ActionDeclineExit);
}
/** User confirmed app exit **/
function confirmExit() {
    exitApplication();
}
/** User declined app exit **/
function declineExit() {
    dismissConfirmPopup();
}
/** To scroll down scheduled list segment to today's date section **/
function onClickScrollToToday() {
    showLoading("common.message.loading");
    var segData = Home.segHomeContentList.data;
    if (!isEmpty(segData)) {
        var segDataCount = segData.length;
        var sectionIndex = -1;
        var rowIndex = -1;
        var aSectionData = [];
        var sectionRows = [];
        var sectionRowsCount = 0;
        var aRowData = {};
        var selDateStr = "";
        var curDateVal = ymdStringToDate(getTodaysDateStr());
        var selDateVal = "";
        var isCurrentDayDataPresent = false;
        if (segDataCount > 0) {
            for (var i = 0; i < segDataCount; i++) {
                aSectionData = segData[i];
                sectionRows = aSectionData[1];
                sectionRowsCount = sectionRows.length;
                for (var j = 0; j < sectionRowsCount; j++) {
                    aRowData = sectionRows[j];
                    selDateStr = aRowData.dateStrKey;
                    selDateVal = ymdStringToDate(selDateStr);
                    if ((curDateVal - selDateVal) === 0) {
                        printMessage("selDateStr & selDateVal: " + selDateStr + " & " + selDateVal);
                        printMessage("curDateVal: " + curDateVal);
                        isCurrentDayDataPresent = true;
                        break;
                    }
                    rowIndex = j;
                }
                if (isCurrentDayDataPresent === true) {
                    sectionIndex = i;
                    break;
                }
            }
        }
        printMessage("isCurrentDayDataPresent,  sectionIndex and rowIndex: " + isCurrentDayDataPresent + " , " + sectionIndex + " , " + rowIndex);
        if (isCurrentDayDataPresent === true && sectionIndex > 0 && rowIndex > -1) {
            Home.segHomeContentList.selectedRowIndex = [(sectionIndex - 1), rowIndex];
        }
    }
    dismissLoading();
}
/** To start a day **/
function onClickStartDay() {
    showStartDayPopup();
}
/** To switch home segment template based on user swipe action **/
function switchHomeSegmentTemplate(selectedSectionIndex, selectedRowIndex) {
    printMessage(" Inside switchHomeSegmentTemplate ");
    var preSelectedSectionData = (gblSegScheduleLastLastSelSecIndex > -1) ? Home.segHomeContentList.data[gblSegScheduleLastLastSelSecIndex][1] : null;
    var curSelectedSectionData = (selectedSectionIndex > -1) ? Home.segHomeContentList.data[selectedSectionIndex][1] : null;
    var preSelectedRow = (gblSegScheduleLastSelRowIndex > -1) ? preSelectedSectionData[gblSegScheduleLastSelRowIndex] : null;
    var curSelectedRow = (selectedRowIndex > -1) ? curSelectedSectionData[selectedRowIndex] : null;
    if (!isEmpty(preSelectedRow)) {
        var setupTblSwipe = {
            fingers: 1,
            swipedistance: 50,
            swipevelocity: 75
        };
        flxRowSegHomeContentList.setGestureRecognizer(2, setupTblSwipe, handleSwipeGestureOnSegRow);
        preSelectedRow.template = flxRowSegHomeContentList;
        Home.segHomeContentList.setDataAt(preSelectedRow, gblSegScheduleLastSelRowIndex, gblSegScheduleLastLastSelSecIndex);
    }
    if (!isEmpty(curSelectedRow)) {
        curSelectedRow.template = createHomeSegTemplateAlt(curSelectedRow.isGPSPresent, curSelectedRow.isPhoneNumberPresent, curSelectedRow.isMobileNumberPresent, curSelectedRow.visitStatus);
        Home.segHomeContentList.setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
    }
    gblSegScheduleLastSelRowIndex = selectedRowIndex;
    gblSegScheduleLastLastSelSecIndex = selectedSectionIndex;
}
/** The below function will get invoked when a swipe gesture is recognized **/
function handleSwipeGestureOnSegRow(widgetRef, gestureInfo) {
    if (gestureInfo.gestureType === 2) {
        var selSectionIndex = 0;
        var selRowIndex = 0;
        if (gestureInfo.swipeDirection === 1 && widgetRef.id === "flxRowSegHomeContentList" && !isEmpty(Home.segHomeContentList.selectedRowIndex)) {
            // Swipe to left
            selSectionIndex = Home.segHomeContentList.selectedRowIndex[0];
            selRowIndex = Home.segHomeContentList.selectedRowIndex[1];
            switchHomeSegmentTemplate(selSectionIndex, selRowIndex);
        } else if (gestureInfo.swipeDirection === 2 && widgetRef.id === "flxRowSegHomeContentListOnSwipe" && !isEmpty(Home.segHomeContentList.selectedRowIndex)) {
            // Swipe to right
            selSectionIndex = Home.segHomeContentList.selectedRowIndex[0];
            selRowIndex = Home.segHomeContentList.selectedRowIndex[1];
            var selectedSectionRows = Home.segHomeContentList.data[selSectionIndex][1];
            var selectedRowData = selectedSectionRows[selRowIndex];
            selectedRowData.template = flxRowSegHomeContentList;
            Home.segHomeContentList.setDataAt(selectedRowData, selRowIndex, selSectionIndex);
        }
    }
}
/** Segment row - Navigation button click **/
/** Segment row - Navigation button click **/
function onClickNavigation() {
    printMessage(" Inside onClickNavigation ");
    var selSectionIndex = gblSegScheduleLastLastSelSecIndex;
    var selRowIndex = gblSegScheduleLastSelRowIndex;
    var selectedSectionRows = Home.segHomeContentList.data[selSectionIndex][1];
    var selectedItem = selectedSectionRows[selRowIndex];
    var selScheduledSection = gblMyAppDataObject.getScheduledObjectsForDate(selectedItem.dateStrKey);
    var selScheduledItem = selScheduledSection[selRowIndex];
    printMessage(" £££££££--> selScheduledItem " + JSON.stringify(selScheduledItem));
    if (!isEmpty(selScheduledItem.propertyPostcode)) {
        ////findMapCordinates(selScheduledItem.propertyPostcode);
        //myPostCodeCordinatesURL(selScheduledItem.propertyPostcode);
        var existingCoordinate = gblMyAppDataObject.geoCoordinates[selScheduledItem.propertyPostcode];
        if (isEmpty(existingCoordinate.latitude) || isEmpty(existingCoordinate.longitude)) {
            CustomGoogleMapAPI(selScheduledItem.propertyPostcode, "LOCATION_ROUTE", "", "");
        } else {
            openGoogleMap(existingCoordinate.latitude, existingCoordinate.longitude);
        }
    } else {
        if (!isEmpty(selScheduledItem.gpsCoordinateX) && !isEmpty(selScheduledItem.gpsCoordinateY)) {
            openGoogleMap(selScheduledItem.gpsCoordinateY, selScheduledItem.gpsCoordinateX);
        } else {
            printMessage(" No GPS coordinates and post code");
        }
    }
}
/** Call open url **/
/** Segment row - Phone button click **/
function onClickPhoneCall() {
    printMessage(" Inside onClickPhoneCall ");
    var selSectionIndex = gblSegScheduleLastLastSelSecIndex;
    var selRowIndex = gblSegScheduleLastSelRowIndex;
    var selectedSectionRows = Home.segHomeContentList.data[selSectionIndex][1];
    var selectedItem = selectedSectionRows[selRowIndex];
    var selScheduledSection = gblMyAppDataObject.getScheduledObjectsForDate(selectedItem.dateStrKey);
    var selScheduledItem = selScheduledSection[selRowIndex];
    var phoneNumber = selScheduledItem.propertyContactTelephone;
    if (!isEmpty(phoneNumber)) {
        kony.phone.dial(phoneNumber);
    } else {
        printMessage(" No Phone number ");
    }
}
/** Segment row - Mobile button click **/
function onClickMobileCall() {
    printMessage(" Inside onClickMobileCall ");
    var selSectionIndex = gblSegScheduleLastLastSelSecIndex;
    var selRowIndex = gblSegScheduleLastSelRowIndex;
    var selectedSectionRows = Home.segHomeContentList.data[selSectionIndex][1];
    var selectedItem = selectedSectionRows[selRowIndex];
    var selScheduledSection = gblMyAppDataObject.getScheduledObjectsForDate(selectedItem.dateStrKey);
    var selScheduledItem = selScheduledSection[selRowIndex];
    var phoneNumber = selScheduledItem.propertyContactMobile;
    if (!isEmpty(phoneNumber)) {
        kony.phone.dial(phoneNumber);
    } else {
        printMessage(" No Mobile number ");
    }
}
/** Navigating to Schedule and Map based on flag value **/
function handleTabSwitch(isSchedulePage) {
    var scheduleBtnSkin = Home.btnSchedule.skin;
    if (isSchedulePage === true && scheduleBtnSkin === "btnTransparentSkin") {
        Home.btnSchedule.skin = "btnSelectedSkin";
        Home.btnMap.skin = "btnTransparentSkin";
        Home.flxScheduleContainer.setVisibility(isSchedulePage);
        Home.flxMapContentContainer.setVisibility(!isSchedulePage);
    } else if (isSchedulePage === false && scheduleBtnSkin === "btnSelectedSkin") {
        Home.btnSchedule.skin = "btnTransparentSkin";
        Home.btnMap.skin = "btnSelectedSkin";
        //Home.flxScheduleContainer.setVisibility(isSchedulePage);
        //Home.flxMapContentContainer.setVisibility(true);
        getCurrentLocation();
    }
    closeCompletedJobsTab();
}
/** On Segment Row Click - Scheduled list */
function onScheduledSegRowClick() {
    gblCompletedVisitFlag = false;
    var selSectionIndex = Home.segHomeContentList.selectedRowIndex[0];
    var selRowIndex = Home.segHomeContentList.selectedRowIndex[1];
    var selectedSectionRows = Home.segHomeContentList.data[selSectionIndex][1];
    var selectedItem = selectedSectionRows[selRowIndex];
    var selectedRowTemplateId = selectedItem.template.id;
    if (selectedRowTemplateId === "flxRowSegHomeContentList") {
        var selScheduledSection = gblMyAppDataObject.getScheduledObjectsForDate(selectedItem.dateStrKey);
        var selScheduledItem = selScheduledSection[selRowIndex];
        gblMyAppDataObject.selScheduledItem = selScheduledItem;
        gblMyAppDataObject.selScheduledDateStr = selectedItem.dateStrKey;
        if (isNetworkAvailable()) {
            showLoading("common.message.loading");
            startSyncWorkHistory();
        } else {
            showLoading("common.message.loading");
            //getServiceCoverFromLocalTbl();
            printMessage("&&&&INSIDE-calltoworkhistory- onCompletedJobsSegRowClick-->matched");
            getWorkHistoryFromLocalTbl();
        }
    }
}
/** On Segment Row Click - Completed list */
function onCompletedJobsSegRowClick(sectionIndex, rowIndex) {
    printMessage("Inside - onCompletedJobsSegRowClick");
    gblCompletedVisitFlag = true;
    var selectedworkorderId = Home.segHomeCompletedList.selectedRowItems[0].workorderId;
    printMessage("Inside - onCompletedJobsSegRowClick  selectedworkorderId" + selectedworkorderId);
    var lengthCompleted = gblMyAppDataObject.completedJobs.length;
    for (i = 0; i < lengthCompleted; i++) {
        printMessage(" onCompletedJobsSegRowClick Inside - loop");
        if (gblMyAppDataObject.completedJobs[i].workorderId === selectedworkorderId) {
            printMessage(" onCompletedJobsSegRowClick-->matched");
            gblMyAppDataObject.selCompletedItem = gblMyAppDataObject.completedJobs[i];
            printMessage(" onCompletedJobsSegRowClick-->completedItems.-->>" + JSON.stringify(gblMyAppDataObject.selCompletedItem));
            if (isNetworkAvailable()) {
                showLoading("common.message.loading");
                startSyncWorkHistory();
            } else {
                showLoading("common.message.loading");
                printMessage("&&&&INSIDE-calltoworkhistory- onCompletedJobsSegRowClick-->matched");
                getWorkHistoryFromLocalTbl();
            }
        }
    }
}
/** Success callback - Get work orders from local db */
function success_getWorkOrders(resultset) {
    printMessage(" Inside success_getWorkOrders " + JSON.stringify(resultset));
    ////gblPortfolioResultSet 					= resultset;
    if (!isEmpty(resultset) && resultset.length > 0) {
        printMessage(" resultset.length " + resultset.length);
        var recordsCount = resultset.length;
        var datewiseSet = {};
        var aRecord = null;
        var dateKey = null;
        var childItems = [];
        var newObject = null;
        var dateStrArray = [];
        var tempPostCode = {};
        for (var i = 0; i < recordsCount; i++) {
            aRecord = resultset[i];
            newObject = {};
            newObject.propertyCode = aRecord.propertyCode;
            newObject.propertyName = aRecord.propertyName;
            newObject.propertyAddress = formatFullAddress(aRecord);
            newObject.propertyShortAddress = formatShortAddress(aRecord);
            newObject.propertyPostcode = aRecord.propertyPostcode;
            newObject.propertyContactMobile = aRecord.propertyContactMobile;
            newObject.propertyContactEmail = aRecord.propertyContactEmail;
            newObject.propertyContactTelephone = aRecord.propertyContactTelephone;
            newObject.gpsCoordinateX = aRecord.gpsCoordinateX;
            newObject.gpsCoordinateY = aRecord.gpsCoordinateY;
            newObject.contractTypeCode = aRecord.contractTypeCode;
            newObject.contractCode = aRecord.contractCode;
            newObject.propertyContactName = aRecord.propertyContactName;
            newObject.propertyContactPosition = aRecord.propertyContactPosition;
            newObject.workorderId = aRecord.workorderId;
            newObject.workId = aRecord.workId;
            newObject.visitStatus = aRecord.workStatus;
            newObject.propertyServiceNote = aRecord.propertyServiceNote;
            newObject.propertySpecialInstructions = aRecord.propertySpecialInstructions;
            newObject.serviceCoverNumber = aRecord.serviceCoverNumber;
            newObject.serviceLineCode = aRecord.serviceLineCode;
            newObject.businessCode = aRecord.businessCode;
            newObject.countryCode = aRecord.countryCode;
            printMessage("newObject.visitStatus " + newObject.visitStatus);
            printMessage(" Inside success_getWorkOrders - GOOGLE MAP - propertyPostcode: " + aRecord.propertyPostcode + " Coordinates lat and long are [" + aRecord.gpsCoordinateY + ", " + aRecord.gpsCoordinateX + "] ");
            // 	gblMyAppDataObject.addGeocodeForPostcode(aRecord.propertyPostcode, aRecord.gpsCoordinateY, aRecord.gpsCoordinateX, false, aRecord.workOrderDateStartPlanned, aRecord.propertyName, newObject.propertyAddress, aRecord.propertyCode);
            // All plotting are based on postcode
            gblMyAppDataObject.addGeocodeForPostcode(aRecord.propertyPostcode, "", "", false, aRecord.workOrderDateStartPlanned, aRecord.propertyName, newObject.propertyAddress, aRecord.propertyCode);
            tempPostCode[aRecord.propertyPostcode] = "";
            if (!isEmpty(aRecord.workOrderDateStartPlanned)) {
                dateKey = aRecord.workOrderDateStartPlanned;
                if (isEmpty(datewiseSet[dateKey])) {
                    childItems = [];
                    dateStrArray.push(dateKey);
                } else {
                    childItems = datewiseSet[dateKey];
                }
                childItems.push(newObject);
                datewiseSet[dateKey] = childItems;
            }
        }
        var postcodeKeys = Object.keys(gblMyAppDataObject.geoCoordinates);
        var aPostcodeKey = "";
        var completedPostcode = [];
        if (postcodeKeys.length > 0) {
            printMessage("postcodeKeys.length" + postcodeKeys.length);
            for (var k = 0; k < postcodeKeys.length; k++) {
                aPostcodeKey = postcodeKeys[k];
                if (tempPostCode.hasOwnProperty(aPostcodeKey)) {
                    // Postcode still exist for scheduled job
                } else {
                    completedPostcode.push(aPostcodeKey);
                }
            }
        }
        if (completedPostcode.length > 0) {
            printMessage("completedPostcode.length" + completedPostcode.length);
            for (var l = 0; l < completedPostcode.length; l++) {
                aPostcodeKey = completedPostcode[l];
                delete gblMyAppDataObject.geoCoordinates[aPostcodeKey];
            }
        }
        getGeocoordinateForPostcode();
        gblMyAppDataObject.scheduledObjects = datewiseSet;
        var sortedDateStrArray = dateStrArray.sort(sortCompare);
        gblMyAppDataObject.scheduledDatesList = sortedDateStrArray;
        showHomeScreen();
    } else {
        printMessage(" success_getWorkOrders - No records found ");
        showHomeScreen();
    }
}
/** Failure callback - Get work orders from local db */
function error_getWorkOrders() {
    printMessage(" Inside error_getWorkOrders ");
    dismissLoading();
}
/** To load data on home segment */
function loadDataOnHomeSegment() {
    printMessage(" Inside loadDataOnHomeSegment ");
    var dateStrArray = [];
    var segData = [];
    gblSegScheduleLastSelRowIndex = -1;
    gblSegScheduleLastLastSelSecIndex = -1;
    if (!isEmpty(gblMyAppDataObject) && !isEmpty(gblMyAppDataObject.scheduledDatesList)) {
        dateStrArray = gblMyAppDataObject.scheduledDatesList;
    }
    var arrayItemsCount = dateStrArray.length;
    if (arrayItemsCount > 0) {
        var segSectionData = [];
        var segRowData = [];
        var aHeaderRecord = null;
        var sectionRowObjects = null;
        var aRowObject = null;
        var aRowRecord = null;
        var dateStrKey = null;
        var todaysDate = ymdStringToDate(getTodaysDateStr());
        var workDate = null;
        var datewiseSet = gblMyAppDataObject.scheduledObjects;
        for (var j = 0; j < arrayItemsCount; j++) {
            dateStrKey = dateStrArray[j];
            segSectionData = [];
            segRowData = [];
            aHeaderRecord = {};
            aHeaderRecord.lblTitle = expandDateStr(dateStrKey);
            workDate = ymdStringToDate(dateStrKey);
            if ((workDate - todaysDate) === 0) {
                aHeaderRecord.template = flxHeaderSegHomeListAlt;
            } else {
                aHeaderRecord.template = flxHeaderSegHomeList;
            }
            sectionRowObjects = datewiseSet[dateStrKey];
            if (sectionRowObjects.length > 0) {
                segSectionData.push(aHeaderRecord);
                for (var k = 0; k < sectionRowObjects.length; k++) {
                    aRowObject = sectionRowObjects[k];
                    aRowRecord = {};
                    aRowRecord.lblItemTop = aRowObject.propertyName;
                    aRowRecord.lblItemMiddle = aRowObject.propertyShortAddress;
                    aRowRecord.lblItemBottom = aRowObject.propertyPostcode;
                    aRowRecord.imgLeftArrowIcon = "leftarrow.png";
                    aRowRecord.sectionIndex = j;
                    aRowRecord.dateStrKey = dateStrKey;
                    aRowRecord.isGPSPresent = false;
                    aRowRecord.isPhoneNumberPresent = false;
                    aRowRecord.isMobileNumberPresent = false;
                    aRowRecord.visitStatus = aRowObject.visitStatus;
                    /** Handle failed visits here. If a visit failed to update in iCabs... Below is the logic **/
                    /** Step 1: Identify it first by comparing each record with completed works. If it exist in completed work, then it is failed to update in iCabs through previous setCloseWork service **/
                    var failedRecord = getMatchingItemFromArray(gblMyAppDataObject.completedJobs, "workId", aRowObject.workId);
                    printMessage("loadDataOnHomeSegment - aRowRecord.workId " + aRowObject.workId);
                    printMessage("loadDataOnHomeSegment - isFailedRecord " + JSON.stringify(failedRecord));
                    printMessage("loadDataOnHomeSegment - completedJobs " + JSON.stringify(gblMyAppDataObject.completedJobs));
                    if (!isEmpty(failedRecord)) {
                        printMessage("loadDataOnHomeSegment - Failed record exist " + failedRecord.workId);
                        /** Step 2: Override current status if it is failed one **/
                        aRowRecord.visitStatus = PCConstant.VISIT_FAILED;
                        aRowObject.visitStatus = PCConstant.VISIT_FAILED;
                        /** Step 3: Update status in database - work table **/
                        updateWorkStatusAsFailedInLocalTbl(failedRecord.workId);
                        /** Step 4: Since it is failed, remove it from Completed section **/
                        removeItemFromArray(gblMyAppDataObject.completedJobs, failedRecord);
                    }
                    /** **/
                    printMessage("loadDataOnHomeSegment - aRowRecord visitStatus: " + aRowRecord.visitStatus);
                    if (aRowObject.visitStatus === PCConstant.VISIT_IN_PROGRESS) {
                        aRowRecord.imgJobTypeIcon = "activeworksmall.png";
                    } else if (aRowObject.visitStatus === PCConstant.VISIT_FAILED) {
                        /** Step 5: Update UI with warning image symbol **/
                        aRowRecord.imgJobTypeIcon = "warning.png";
                    } else {
                        aRowRecord.imgJobTypeIcon = "";
                    }
                    if ((!isEmpty(aRowObject.gpsCoordinateX) && !isEmpty(aRowObject.gpsCoordinateY)) || !isEmpty(aRowObject.propertyPostcode)) {
                        aRowRecord.isGPSPresent = true;
                    }
                    if (!isEmpty(aRowObject.propertyContactTelephone)) {
                        aRowRecord.isPhoneNumberPresent = true;
                    }
                    if (!isEmpty(aRowObject.propertyContactMobile)) {
                        aRowRecord.isMobileNumberPresent = true;
                    }
                    var setupTblSwipe = {
                        fingers: 1,
                        swipedistance: 50,
                        swipevelocity: 75
                    };
                    flxRowSegHomeContentList.setGestureRecognizer(2, setupTblSwipe, handleSwipeGestureOnSegRow);
                    aRowRecord.template = flxRowSegHomeContentList;
                    segRowData.push(aRowRecord);
                }
                segSectionData.push(segRowData);
            }
            segData.push(segSectionData);
        }
        Home.lblNoScheduledJobs.setVisibility(false);
    } else {
        Home.lblNoScheduledJobs.setVisibility(true);
    }
    Home.segHomeContentList.setData(segData);
}
/** To format property address - contains all 5 lines */
function formatFullAddress(recordSet) {
    var propAddress = "";
    var addressArray = [];
    if (!isEmpty(recordSet.propertyAddressLine1)) {
        addressArray.push(recordSet.propertyAddressLine1);
    }
    if (!isEmpty(recordSet.propertyAddressLine2)) {
        addressArray.push(recordSet.propertyAddressLine2);
    }
    if (!isEmpty(recordSet.propertyAddressLine3)) {
        addressArray.push(recordSet.propertyAddressLine3);
    }
    if (!isEmpty(recordSet.propertyAddressLine4)) {
        addressArray.push(recordSet.propertyAddressLine4);
    }
    if (!isEmpty(recordSet.propertyAddressLine5)) {
        addressArray.push(recordSet.propertyAddressLine5);
    }
    if (addressArray.length > 0) {
        propAddress = addressArray.join();
        propAddress = propAddress.replace(/,/g, ", ");
    }
    return propAddress;
}
/** To format property address - Only contains Address 1,2 and 4th lines */
function formatShortAddress(recordSet) {
    var propAddress = "";
    var addressArray = [];
    if (!isEmpty(recordSet.propertyAddressLine1)) {
        addressArray.push(recordSet.propertyAddressLine1);
    }
    if (!isEmpty(recordSet.propertyAddressLine2)) {
        addressArray.push(recordSet.propertyAddressLine2);
    }
    if (!isEmpty(recordSet.propertyAddressLine4)) {
        addressArray.push(recordSet.propertyAddressLine4);
    }
    if (addressArray.length > 0) {
        propAddress = addressArray.join();
        propAddress = propAddress.replace(/,/g, ", ");
    }
    return propAddress;
}
/** To sort dateStr items in the array **/
function sortCompare(val1, val2) {
    var dateVal1 = ymdStringToDate(val1);
    var dateVal2 = ymdStringToDate(val2);
    return dateVal1 - dateVal2;
}
/** To scroll down to completed jobs section **/
/*function scrollToCompletedJobs(){
  	if (gblSegCompletedJobSectionIndex > -1){
       	Home.segHomeContentList.selectedRowIndex = [gblSegCompletedJobSectionIndex, 0];
    }
}*/
function onClickCompletedJobs() {
    Home.lblNoCompletedJobs.setVisibility(false);
    if (gblIsCompletedJobsVisible === false) {
        var completedJobs = gblMyAppDataObject.completedJobs;
        var completedItemsCount = completedJobs.length;
        var segRowsCompletedJobs = [];
        var outDatedCompletedJobs = [];
        if (completedItemsCount > 0) {
            var aCompletedJobObj = null;
            var aCompletedJobRecord = null;
            var currentDate = new Date();
            gblSyncedWorkIds = [];
            gblSyncedWorkPointIds = [];
            for (var c = 0; c < completedItemsCount; c++) {
                aCompletedJobObj = gblMyAppDataObject.completedJobs[c];
                var jobCompletedDate = new Date(aCompletedJobObj.completedDate);
                var durationInDays = daysBtwDates(jobCompletedDate, currentDate);
                printMessage(" onClickCompletedJobs durationInDays: " + durationInDays);
                if (durationInDays > 7) {
                    // The completed jobs will displayed in the app till 7 days from the date of completion
                    outDatedCompletedJobs.push(aCompletedJobObj);
                } else {
                    aCompletedJobRecord = {};
                    aCompletedJobRecord.lblItemTop = aCompletedJobObj.propertyName;
                    aCompletedJobRecord.lblItemMiddle = aCompletedJobObj.propertyShortAddress;
                    aCompletedJobRecord.lblItemBottom = aCompletedJobObj.propertyPostcode;
                    aCompletedJobRecord.imgLeftArrowIcon = "";
                    aCompletedJobRecord.visitStatus = aCompletedJobObj.visitStatus;
                    aCompletedJobRecord.workorderId = aCompletedJobObj.workorderId;
                    aCompletedJobRecord.sectionIndex = 0;
                    segRowsCompletedJobs.push(aCompletedJobRecord);
                }
            }
            printMessage(" anOutdatedJobs length - " + outDatedCompletedJobs.length);
            if (outDatedCompletedJobs.length > 0) {
                var anOutdatedJob = null;
                for (var i = 0; i < outDatedCompletedJobs.length; i++) {
                    anOutdatedJob = outDatedCompletedJobs[i];
                    removeItemFromArray(gblMyAppDataObject.completedJobs, anOutdatedJob);
                    printMessage(" onClickCompletedJobs anOutdatedJob.workId " + anOutdatedJob.workId);
                    printMessage(" onClickCompletedJobs anOutdatedJob.visitStatus " + anOutdatedJob.visitStatus);
                    if (anOutdatedJob.visitStatus === PCConstant.VISIT_COMPLETED || anOutdatedJob.visitStatus === PCConstant.VISIT_CLOSED) {
                        /** Status => Completed means it is closed and then synced with iCab server **/
                        gblSyncedWorkIds.push(anOutdatedJob.workId);
                        gblSyncedWorkPointIds.push(anOutdatedJob.workPointId);
                    }
                }
                printMessage(" gblSyncedWorkIds anOutdatedJobs - " + JSON.stringify(gblSyncedWorkIds));
                printMessage(" gblSyncedWorkPointIds anOutdatedJobs - " + JSON.stringify(gblSyncedWorkPointIds));
                setAppDataObjInStore();
                deleteClosedWorkRecords();
            }
        } else {
            Home.lblNoCompletedJobs.setVisibility(true);
        }
        Home.segHomeCompletedList.setData(segRowsCompletedJobs);
        printMessage(" onClickCompletedJobs " + JSON.stringify(segRowsCompletedJobs));
        openCompletedJobsTab();
    } else {
        closeCompletedJobsTab();
    }
    gblIsCompletedJobsVisible = !gblIsCompletedJobsVisible;
}
/** Open completed jobs **/
function openCompletedJobsTab() {
    printMessage(" Inside openCompletedJobsTab ");

    function openCompletedJobsTab_Callback() {}
    if (Home.flxCompletedList.top != "0%") {
        //Home.flxCompletedList.top					= "0%";
        Home.flxCompletedList.animate(kony.ui.createAnimation({
            "0": {
                "top": "92%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "100": {
                "top": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": openCompletedJobsTab_Callback
        });
        Home.imgToggle.src = "scrolldown.png";
        Home.flxCompletedJobsHeaderContainer.skin = "flxLightRedBgBorderedSkin";
    }
}
/** Close completed jobs **/
function closeCompletedJobsTab() {
    printMessage(" Inside closeCompletedJobsTab ");

    function closeCompletedJobsTab_Callback() {}
    if (Home.flxCompletedList.top != "92%") {
        //Home.flxCompletedList.top					= "92%";
        Home.flxCompletedList.animate(kony.ui.createAnimation({
            "0": {
                "top": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            },
            "100": {
                "top": "92%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": closeCompletedJobsTab_Callback
        });
        Home.imgToggle.src = "scrollup.png";
        Home.flxCompletedJobsHeaderContainer.skin = "flxRedBgBorderedSkin";
        Home.lblNoCompletedJobs.setVisibility(false);
    }
}