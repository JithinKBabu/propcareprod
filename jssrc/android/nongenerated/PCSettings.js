/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : UI Screen - Settings(Burger Menu) - Methods
 ***********************************************************************/
/** Settings Menu (Burger Menu)- UI refreshing like i18n updates, default visibility and all **/
function refreshSettingsUI() {
    printMessage("Inside refreshSettingsUI");
    printMessage("gblMyAppDataObject.isVisitInProgress: " + gblMyAppDataObject.isVisitInProgress);
    printMessage("gblMyAppDataObject.inProgressVisit: " + JSON.stringify(gblMyAppDataObject.inProgressVisit));
    var frmObj = kony.application.getCurrentForm();
    frmObj.flxBurgerMenuContainer.centerX = "-50%";
    frmObj.lblAppVersion.text = getI18nString("common.label.version") + " " + getVersionFromAppConfig();
    frmObj.lblUserEmail.text = (!isEmpty(gblEmployeeObject)) ? gblEmployeeObject.email : "";
    var topSegData = [];
    var bottomSegData = [];
    var aRecord = null;
    if (frmObj.id === PCConstant.FORM_HOME) {
        if (gblMyAppDataObject.isVisitInProgress === true && !isEmpty(gblMyAppDataObject.inProgressVisit)) {
            aRecord = {};
            aRecord.imgBtnIcon = "activework.png";
            aRecord.actionId = "ACTIVE_WORK";
            aRecord.lblItem = getI18nString("common.label.activeWork");
            topSegData.push(aRecord);
            aRecord = {};
            aRecord.imgBtnIcon = "discardvisit.png";
            aRecord.actionId = "DISCARD_VISIT";
            aRecord.lblItem = getI18nString("common.label.discardVisit");
            topSegData.push(aRecord);
        }
    } else if (frmObj.id === PCConstant.FORM_VISIT) {
        aRecord = {};
        aRecord.imgBtnIcon = "workview.png";
        aRecord.actionId = "WORK_VIEW";
        aRecord.lblItem = getI18nString("common.label.workView");
        topSegData.push(aRecord);
        aRecord = {};
        aRecord.imgBtnIcon = "discardvisit.png";
        aRecord.actionId = "DISCARD_VISIT";
        aRecord.lblItem = getI18nString("common.label.discardVisit");
        topSegData.push(aRecord);
    } else if (frmObj.id === PCConstant.FORM_CUSTOMER || frmObj.id === PCConstant.FORM_COMPLETEVISIT) {
        aRecord = {};
        aRecord.imgBtnIcon = "workview.png";
        aRecord.actionId = "WORK_VIEW";
        aRecord.lblItem = getI18nString("common.label.workView");
        topSegData.push(aRecord);
        if (gblMyAppDataObject.isVisitInProgress === true && !isEmpty(gblMyAppDataObject.inProgressVisit)) {
            aRecord = {};
            aRecord.imgBtnIcon = "activework.png";
            aRecord.actionId = "ACTIVE_WORK";
            aRecord.lblItem = getI18nString("common.label.activeWork");
            topSegData.push(aRecord);
            aRecord = {};
            aRecord.imgBtnIcon = "discardvisit.png";
            aRecord.actionId = "DISCARD_VISIT";
            aRecord.lblItem = getI18nString("common.label.discardVisit");
            topSegData.push(aRecord);
        }
    }
    aRecord = {};
    aRecord.imgBtnIcon = "settings.png";
    aRecord.actionId = "SETTINGS";
    aRecord.lblItem = getI18nString("common.label.settings");
    topSegData.push(aRecord);
    aRecord = {};
    aRecord.imgBtnIcon = "support.png";
    aRecord.actionId = "SUPPORT";
    aRecord.lblItem = getI18nString("common.label.support");
    topSegData.push(aRecord);
    frmObj.segMenuListOne.setData(topSegData);
    frmObj.segMenuListTwo.setData(bottomSegData);
    displayLastSyncDate();
}
/** Settings Menu (Burger Menu)- Open menu button action **/
function onClickOpenMenuButton() {
    var frmObj = kony.application.getCurrentForm();
    refreshSettingsUI();
    displayLastSyncDate();

    function openMenu_Callback() {}
    frmObj.flxBurgerMenuContainer.animate(kony.ui.createAnimation({
        "100": {
            "centerX": "50%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": openMenu_Callback
    });
}
/** Settings Menu (Burger Menu)- Close menu button action **/
function closeSettingsMenu() {
    var frmObj = kony.application.getCurrentForm();

    function closeMenu_Callback() {}
    frmObj.flxBurgerMenuContainer.animate(kony.ui.createAnimation({
        "100": {
            "centerX": "-50%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": closeMenu_Callback
    });
}
/** Settings Menu - On click on email address  **/
function onClickEmailId() {
    showLoading("common.message.loading");
    closeSettingsMenu();
    getDeviceConfiguredEmails();
}
/** Settings Menu - Top segment row click  **/
/** Settings Menu - Top segment row click  **/
function onClickTopSegmentRow() {
    var frmObj = kony.application.getCurrentForm();
    var selectedRowIndex = frmObj.segMenuListOne.selectedRowIndex[1];
    var selectedRow = frmObj.segMenuListOne.data[selectedRowIndex];
    if (!isEmpty(selectedRow) && !isEmpty(selectedRow.actionId)) {
        closeSettingsMenu();
        if (selectedRow.actionId === "ACTIVE_WORK") {
            showVisitScreen();
        } else if (selectedRow.actionId === "PAUSE_VISIT") {
            alert("Not implemented");
        } else if (selectedRow.actionId === "DISCARD_VISIT") {
            showConfirmPopup("common.message.discardVisit", ActionDoDiscardVisit, ActionCancelDiscardVisit);
        } else if (selectedRow.actionId === "WORK_VIEW") {
            //showHomeScreen();
            showLoading("common.message.loading");
            getWorkOrdersFromLocalTbl();
        } else if (selectedRow.actionId === "SETTINGS") {
            showLoading("common.message.loading");
            getiCabInstancesFromLocalTbl();
        }
    }
}
/** Settings Menu - Bottom segment row click **/
function onClickBottomSegmentRow() {
    var frmObj = kony.application.getCurrentForm();
    var selectedRowIndex = frmObj.segMenuListTwo.selectedRowIndex[1];
    var selectedRow = frmObj.segMenuListTwo.data[selectedRowIndex];
    if (!isEmpty(selectedRow) && !isEmpty(selectedRow.actionId)) {
        closeSettingsMenu();
        if (selectedRow.actionId === "SETTINGS") {
            showLoading("common.message.loading");
            getiCabInstancesFromLocalTbl();
        }
    }
}
/** Void action **/
function voidClick() {
    printMessage("Void click - Nothing to do");
}
/** Last sync date displya method **/
function displayLastSyncDate() {
    var frmObj = kony.application.getCurrentForm();
    var lastSyncDate = kony.store.getItem(PCConstant.DISPLAY_SYNC_DATE_TIME);
    if (!isEmpty(lastSyncDate)) {
        //frmObj.lblLastSync.text				= getI18nString("common.label.lastSyncDate");
        var lastSyncMsg = getI18nString("common.label.lastSyncDate");
        frmObj.lblLastSyncDate.text = lastSyncMsg + ": " + ((!isEmpty(lastSyncDate)) ? lastSyncDate : "");
    } else {
        frmObj.lblLastSync.text = "";
        frmObj.lblLastSyncDate.text = "";
    }
}
/** To remove all kony store items **/
function resetKonyStore() {
    kony.store.removeItem(PCConstant.DISPLAY_SYNC_DATE_TIME);
    kony.store.removeItem(PCConstant.LAST_SYNC_DATE);
    kony.store.removeItem(PCConstant.LAST_SYNC_DATE_TIME);
    kony.store.removeItem(PCConstant.LAST_WORK_SYNC_DATE_TIME);
    kony.store.removeItem(PCConstant.IS_EMP_LOGGED_IN);
    kony.store.removeItem(PCConstant.EMP_PROFILE);
    kony.store.removeItem(PCConstant.APP_DATA_OBJECT);
    kony.store.removeItem(PCConstant.SEL_LAN_CODE);
}