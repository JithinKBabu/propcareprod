/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Work View - Methods
 ***********************************************************************/
/** Success callback - Create startWorkDay record in local DB **/
function success_insertWorkDayStart() {
    printMessage("Inside - success_insertWorkDayStart");
    if (isNetworkAvailable()) {
        startSyncStartDay(); //This will do parallelly   
    }
    dismissLoading();
}
/** Error callback - Create startWorkDay record in local DB **/
function error_insertWorkDayStart(error) {
    printMessage("Inside - error_insertWorkDayStart" + JSON.stringify(error));
    gblMyAppDataObject.startDayDateStr = null;
    gblMyAppDataObject.startDayTimeStr = null;
    gblMyAppDataObject.isDayStarted = PCConstant.DAY_STARTED_NO;
    setAppDataObjInStore();
    Home.imgStartdayIcon.src = "endday.png";
    showMessagePopup("common.message.unableToStartDay", ActionPopupMessageOk);
    dismissLoading();
}
/** Success callback - Create endWorkDay record in local DB **/
function success_insertWorkDayEnd() {
    printMessage("Inside - success_insertWorkDayStart");
    if (isNetworkAvailable()) {
        startSyncStartDay(); //This will do parallelly   
    }
    dismissLoading();
}
/** Error callback - Create endWorkDay record in local DB **/
function error_insertWorkDayEnd(error) {
    printMessage("Inside - error_insertWorkDayStart" + JSON.stringify(error));
    gblMyAppDataObject.stopDayDateStr = null;
    gblMyAppDataObject.stopDayTimeStr = null;
    gblMyAppDataObject.isDayStarted = PCConstant.DAY_STARTED_YES;
    setAppDataObjInStore();
    Home.imgStartdayIcon.src = "startday.png";
    showMessagePopup("common.message.unableToEndDay", ActionPopupMessageOk);
    dismissLoading();
}
/** Sync timer scheduler every 30 seconds - To fetch delta changes **/
function startSyncScheduler() {
    printMessage("Inside startSyncScheduler ");
    var timeInSecs = PCConstant.SYNC_SCHEDULER_TIME; /** 30 secs time interval **/
    try {
        kony.timer.schedule("SyncScheduler", fetchWorkServiceDeltaChanges, timeInSecs, true);
    } catch (error) {
        printMessage("startSyncScheduler error: " + JSON.stringify(error));
    }
}
/** To cancel sync timer scheduler **/
function cancelSyncScheduler() {
    printMessage("Inside cancelSyncScheduler ");
    try {
        kony.timer.cancel("SyncScheduler");
    } catch (error) {
        printMessage("cancelSyncScheduler error: " + JSON.stringify(error));
    }
}
/** To fetch delta changes from backend for records **/
function fetchWorkServiceDeltaChanges() {
    printMessage("Inside fetchWorkServiceDeltaChanges");
    if (gblIsSyncInProgress === false && isNetworkAvailable()) {
        printMessage("Device is online and no sync is in progress");
        var lastSyncDateTime = kony.store.getItem(PCConstant.LAST_SYNC_DATE_TIME);
        var lastSyncDate = kony.store.getItem(PCConstant.LAST_SYNC_DATE);
        if (!isEmpty(lastSyncDateTime) && !isEmpty(lastSyncDate)) {
            printMessage("lastSyncDate: " + lastSyncDate);
            var currentDateTime = getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
            printMessage("Last sync DateTime: " + lastSyncDateTime + " current DateTime: " + currentDateTime);
            var dateDiffInMins = getDateTimeDiffInMins(currentDateTime, lastSyncDateTime);
            dateDiffInMins = parseInt(dateDiffInMins);
            printMessage(" dateDiffInMins: " + dateDiffInMins);
            var isDailySyncCallRequired = isFutureDate(lastSyncDate);
            printMessage("isDailySyncCallRequired: " + isDailySyncCallRequired);
            if (isDailySyncCallRequired === true) {
                printMessage(" Invoking daily delta changes - daily scheduler call");
                startDailySyncScheduler();
            } else if (dateDiffInMins >= PCConstant.SYNC_WORK_VIEW_SYNC_TIMER && gblIsWorkSaveInProgress === false) {
                printMessage(" Invoking 15 mins delta changes - 15 minutes scheduler call");
                //start15MinutesSyncScheduler();	
                getClosedWorkData();
            }
        } else {
            printMessage("No date values available in local store for LastSyncDate and LastSyncDateTime");
        }
    } else {
        printMessage("Device is either offline or sync is in progress or both");
    }
}
/** To refresh scheduled list on home screen **/
function refreshWorkView() {
    var frmObj = kony.application.getCurrentForm();
    if (frmObj.id === PCConstant.FORM_HOME) {
        gblRefreshHomeScreenFlag = true;
        getWorkOrdersFromLocalTbl();
    }
}