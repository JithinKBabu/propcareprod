//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:02 UTC 2016contract*******************
// **********************************Start contract's helper methods************************
if (typeof(kony) === "undefined") {
    kony = {};
}
if (typeof(kony.sync) === "undefined") {
    kony.sync = {};
}
if (typeof(kony.sync.log) === "undefined") {
    kony.sync.log = {};
}
if (typeof(sync) === "undefined") {
    sync = {};
}
if (typeof(sync.log) === "undefined") {
    sync.log = {};
}
if (typeof(com) === "undefined") {
    com = {};
}
if (typeof(com.pc) === "undefined") {
    com.pc = {};
}
if (typeof(com.pc.portfolios) === "undefined") {
    com.pc.portfolios = {};
}
/************************************************************************************
 * Creates new contract
 *************************************************************************************/
com.pc.portfolios.contract = function() {
    this.businessCode = null;
    this.contractCode = null;
    this.contractDeleted = null;
    this.contractName = null;
    this.contractTypeCode = null;
    this.countryCode = null;
    this.updateDateTime = null;
    this.accountCode = null;
    this.markForUpload = true;
};
com.pc.portfolios.contract.prototype = {
    get businessCode() {
        return this._businessCode;
    },
    set businessCode(val) {
        this._businessCode = val;
    },
    get contractCode() {
        return this._contractCode;
    },
    set contractCode(val) {
        this._contractCode = val;
    },
    get contractDeleted() {
        return kony.sync.getBoolean(this._contractDeleted) + "";
    },
    set contractDeleted(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute contractDeleted in contract.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._contractDeleted = val;
    },
    get contractName() {
        return this._contractName;
    },
    set contractName(val) {
        this._contractName = val;
    },
    get contractTypeCode() {
        return this._contractTypeCode;
    },
    set contractTypeCode(val) {
        this._contractTypeCode = val;
    },
    get countryCode() {
        return this._countryCode;
    },
    set countryCode(val) {
        this._countryCode = val;
    },
    get updateDateTime() {
        return this._updateDateTime;
    },
    set updateDateTime(val) {
        this._updateDateTime = val;
    },
    get accountCode() {
        return this._accountCode;
    },
    set accountCode(val) {
        this._accountCode = val;
    },
};
/************************************************************************************
 * Retrieves all instances of contract SyncObject present in local database with
 * given limit and offset where limit indicates the number of records to be retrieved
 * and offset indicates number of rows to be ignored before returning the records.
 * e.g. var orderByMap = []
 * orderByMap[0] = {};
 * orderByMap[0].key = "businessCode";
 * orderByMap[0].sortType ="desc";
 * orderByMap[1] = {};
 * orderByMap[1].key = "contractCode";
 * orderByMap[1].sortType ="asc";
 * var limit = 20;
 * var offset = 5;
 * com.pc.portfolios.contract.getAll(successcallback,errorcallback, orderByMap, limit, offset)
 *************************************************************************************/
com.pc.portfolios.contract.getAll = function(successcallback, errorcallback, orderByMap, limit, offset) {
    sync.log.trace("Entering com.pc.portfolios.contract.getAll->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    orderByMap = kony.sync.formOrderByClause("contract", orderByMap);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_orderBy(query, orderByMap);
    kony.sync.qb_limitOffset(query, limit, offset);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.getAll->successcallback");
        successcallback(com.pc.portfolios.contract.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Returns number of contract present in local database.
 *************************************************************************************/
com.pc.portfolios.contract.getAllCount = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.getAllCount function");
    com.pc.portfolios.contract.getCount("", successcallback, errorcallback);
};
/************************************************************************************
 * Returns number of contract using where clause in the local Database
 *************************************************************************************/
com.pc.portfolios.contract.getCount = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.getCount->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.getCount", "getCount", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select count(*) from \"" + tbname + "\" " + wcs;
    kony.sync.single_execute_sql(dbname, sql, null, mySuccCallback, errorcallback);

    function mySuccCallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.getCount->successcallback");
        if (null !== res) {
            var count = null;
            count = res["count(*)"];
            kony.sync.verifyAndCallClosure(successcallback, {
                count: count
            });
        } else {
            sync.log.error("Some error occured while getting the count");
        }
    }
};
/************************************************************************************
 * Creates a new instance of contract in the local Database. The new record will 
 * be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.contract.prototype.create = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.portfolios.contract.prototype.create function");
    var valuestable = this.getValuesTable(true);
    com.pc.portfolios.contract.create(valuestable, successcallback, errorcallback, this.markForUpload);
};
com.pc.portfolios.contract.create = function(valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  com.pc.portfolios.contract.create->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.create", "create", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    if (kony.sync.attributeValidation(valuestable, "contract", errorcallback, true) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  com.pc.portfolios.contract.create->success callback");
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
    var pks = [];
    var errMsg = "";

    function createSuccesscallback(res) {
        if (res == null || res.length == 0) {
            var relationshipMap = {};
            relationshipMap = com.pc.portfolios.contract.getRelationshipMap(relationshipMap, valuestable);
            kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
        } else {
            errMsg = "[" + errMsg + "]";
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey, kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
        }
    }
    if (kony.sync.enableORMValidations) {
        errMsg = "businessCode=" + valuestable.businessCode;
        pks["businessCode"] = {
            key: "businessCode",
            value: valuestable.businessCode
        };
        errMsg = errMsg + ", contractCode=" + valuestable.contractCode;
        pks["contractCode"] = {
            key: "contractCode",
            value: valuestable.contractCode
        };
        errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
        pks["countryCode"] = {
            key: "countryCode",
            value: valuestable.countryCode
        };
        com.pc.portfolios.contract.getAllDetailsByPK(pks, createSuccesscallback, errorcallback)
    } else {
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
};
/************************************************************************************
 * Creates number of new instances of contract in the local Database. These new 
 * records will be merged with the enterprise datasource in the next Sync. Based upon 
 * kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var valuesArray = [];
 *		valuesArray[0] = {};
 *		valuesArray[0].businessCode = "businessCode_0";
 *		valuesArray[0].contractCode = "contractCode_0";
 *		valuesArray[0].contractName = "contractName_0";
 *		valuesArray[0].contractTypeCode = "contractTypeCode_0";
 *		valuesArray[0].countryCode = "countryCode_0";
 *		valuesArray[0].accountCode = "accountCode_0";
 *		valuesArray[1] = {};
 *		valuesArray[1].businessCode = "businessCode_1";
 *		valuesArray[1].contractCode = "contractCode_1";
 *		valuesArray[1].contractName = "contractName_1";
 *		valuesArray[1].contractTypeCode = "contractTypeCode_1";
 *		valuesArray[1].countryCode = "countryCode_1";
 *		valuesArray[1].accountCode = "accountCode_1";
 *		valuesArray[2] = {};
 *		valuesArray[2].businessCode = "businessCode_2";
 *		valuesArray[2].contractCode = "contractCode_2";
 *		valuesArray[2].contractName = "contractName_2";
 *		valuesArray[2].contractTypeCode = "contractTypeCode_2";
 *		valuesArray[2].countryCode = "countryCode_2";
 *		valuesArray[2].accountCode = "accountCode_2";
 *		com.pc.portfolios.contract.createAll(valuesArray, successcallback, errorcallback, true);
 *************************************************************************************/
com.pc.portfolios.contract.createAll = function(valuesArray, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.portfolios.contract.createAll function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.createAll", "createAll", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var isProperData = true;
    var arrayLen = 0;
    var errorInfo = [];
    var arrayLength = valuesArray.length;
    var errObject = null;
    var isReferentialIntegrityFailure = false;
    var errMsg = null;
    if (kony.sync.enableORMValidations) {
        var newValuesArray = [];
        //column level validations
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var valuestable = valuesArray[i];
            if (kony.sync.attributeValidation(valuestable, "contract", errorcallback, true) === false) {
                return;
            }
            newValuesArray[i] = valuestable;
        }
        valuesArray = newValuesArray;
        var isDuplicateKey = false;
        //checking for duplicate records
        var connection = kony.sync.getConnectionOnly(dbname, dbname);
        kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
        var isError = false;

        function checkDuplicatePkCallback(tx) {
            arrayLength = valuesArray.length;
            for (var i = 0; valuesArray != null && i < arrayLength; i++) {
                var valuestable = valuesArray[i];
                var pks = [];
                errMsg = "businessCode=" + valuestable.businessCode;
                pks["businessCode"] = {
                    key: "businessCode",
                    value: valuestable.businessCode
                };
                errMsg = errMsg + ", contractCode=" + valuestable.contractCode;
                pks["contractCode"] = {
                    key: "contractCode",
                    value: valuestable.contractCode
                };
                errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
                pks["countryCode"] = {
                    key: "countryCode",
                    value: valuestable.countryCode
                };
                var wcs = [];
                if (com.pc.portfolios.contract.pkCheck(pks, wcs, errorcallback, "searching") === false) {
                    isError = true;
                    return;
                }
                var query = kony.sync.qb_createQuery();
                kony.sync.qb_select(query, null);
                kony.sync.qb_from(query, tbname);
                kony.sync.qb_where(query, wcs);
                var query_compile = kony.sync.qb_compile(query);
                var sql = query_compile[0];
                var params = query_compile[1];
                var resultset = kony.sync.executeSql(tx, sql, params);
                if (resultset === false) {
                    isError = true;
                    return;
                }
                if (resultset.rows.length != 0) {
                    isError = true;
                    errMsg = "[" + errMsg + "]";
                    isDuplicateKey = true;
                    return;
                }
            }
            if (!isError) {
                checkIntegrity(tx);
            }
        }
    } else {
        //copying by value
        var newValuesArray = [];
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
        }
        valuesArray = newValuesArray;
        kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
    }

    function transactionErrorCallback() {
        if (isError == true) {
            //Statement error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
        } else {
            //Transaction error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
        }
    }

    function transactionSuccessCallback() {
        sync.log.trace("Entering  com.pc.portfolios.contract.createAll->transactionSuccessCallback");
        if (!isError) {
            kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
        } else {
            if (isReferentialIntegrityFailure) {
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            }
            if (isDuplicateKey) {
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey, kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
            }
        }
    }
    //foreign key constraints validations
    function checkIntegrity(tx) {
        sync.log.trace("Entering  com.pc.portfolios.contract.createAll->checkIntegrity");
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var relationshipMap = {};
            relationshipMap = com.pc.portfolios.contract.getRelationshipMap(relationshipMap, valuesArray[i]);
            errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
            if (errObject === false) {
                isError = true;
                return;
            }
            if (errObject !== true) {
                isError = true;
                isReferentialIntegrityFailure = true;
                return;
            }
        }
    }
};
/************************************************************************************
 * Updates contract using primary key in the local Database. The update will be
 * merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.contract.prototype.updateByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.portfolios.contract.prototype.updateByPK function");
    var pks = this.getPKTable();
    var valuestable = this.getValuesTable(false);
    com.pc.portfolios.contract.updateByPK(pks, valuestable, successcallback, errorcallback, this.markForUpload);
};
com.pc.portfolios.contract.updateByPK = function(pks, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  com.pc.portfolios.contract.updateByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.updateByPK", "updateByPk", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    if (com.pc.portfolios.contract.pkCheck(pks, wcs, errorcallback, "updating") === false) {
        return;
    }
    if (kony.sync.attributeValidation(valuestable, "contract", errorcallback, false) === false) {
        return;
    }
    var relationshipMap = {};
    relationshipMap = com.pc.portfolios.contract.getRelationshipMap(relationshipMap, valuestable);
    kony.sync.updateByPK(tbname, dbname, relationshipMap, pks, valuestable, successcallback, errorcallback, markForUpload, wcs);
};
/************************************************************************************
 * Updates contract(s) using where clause in the local Database. The update(s)
 * will be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.contract.update = function(wcs, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.portfolios.contract.update function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.update", "update", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    if (kony.sync.attributeValidation(valuestable, "contract", errorcallback, false) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  com.pc.portfolios.contract.update-> success callback of Integrity Check");
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, com.pc.portfolios.contract.getPKTable());
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = com.pc.portfolios.contract.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, com.pc.portfolios.contract.getPKTable());
    }
};
/************************************************************************************
 * Updates contract(s) satisfying one or more where clauses in the local Database. 
 * The update(s) will be merged with the enterprise datasource in the next Sync.
 * Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var inputArray = [];
 *		inputArray[0] = {};
 *		inputArray[0].changeSet = {};
 *		inputArray[0].changeSet.contractName = "contractName_updated0";
 *		inputArray[0].changeSet.contractTypeCode = "contractTypeCode_updated0";
 *		inputArray[0].changeSet.accountCode = "accountCode_updated0";
 *		inputArray[0].whereClause = "where businessCode = '0'";
 *		inputArray[0].whereClause = "where contractCode = '0'";
 *		inputArray[0].whereClause = "where countryCode = '0'";
 *		inputArray[1] = {};
 *		inputArray[1].changeSet = {};
 *		inputArray[1].changeSet.contractName = "contractName_updated1";
 *		inputArray[1].changeSet.contractTypeCode = "contractTypeCode_updated1";
 *		inputArray[1].changeSet.accountCode = "accountCode_updated1";
 *		inputArray[1].whereClause = "where businessCode = '1'";
 *		inputArray[1].whereClause = "where contractCode = '1'";
 *		inputArray[1].whereClause = "where countryCode = '1'";
 *		inputArray[2] = {};
 *		inputArray[2].changeSet = {};
 *		inputArray[2].changeSet.contractName = "contractName_updated2";
 *		inputArray[2].changeSet.contractTypeCode = "contractTypeCode_updated2";
 *		inputArray[2].changeSet.accountCode = "accountCode_updated2";
 *		inputArray[2].whereClause = "where businessCode = '2'";
 *		inputArray[2].whereClause = "where contractCode = '2'";
 *		inputArray[2].whereClause = "where countryCode = '2'";
 *		com.pc.portfolios.contract.updateAll(inputArray,successcallback,errorcallback);
 *************************************************************************************/
com.pc.portfolios.contract.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
        sync.log.trace("Entering com.pc.portfolios.contract.updateAll function");
        if (!kony.sync.isSyncInitialized(errorcallback)) {
            return;
        }
        if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.updateAll", "updateAll", errorcallback)) {
            return;
        }
        var dbname = "100004898512bd8be";
        var tbname = "contract";
        var isError = false;
        var errObject = null;
        if (markForUpload == false || markForUpload == "false") {
            markForUpload = "false"
        } else {
            markForUpload = "true"
        }
        if ((kony.sync.enableORMValidations)) {
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                var valuestable = v.changeSet;
                var isEmpty = true;
                for (var key in valuestable) {
                    isEmpty = false;
                    break;
                }
                if (isEmpty) {
                    errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue, kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
                    return;
                }
                var wcs = v.whereClause;
                var twcs = wcs;
                if (kony.sync.attributeValidation(valuestable, "contract", errorcallback, false) === false) {
                    return;
                }
                newInputArray[i] = [];
                newInputArray[i].changeSet = valuestable;
                newInputArray[i].whereClause = wcs;
            }
            inputArray = newInputArray;
            var connection = kony.sync.getConnectionOnly(dbname, dbname);
            kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        } else {
            //copying by value
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                newInputArray[i] = kony.sync.CreateCopy(v);
            }
            inputArray = newInputArray;
            kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, errorcallback, markForUpload, com.pc.portfolios.contract.getPKTable());
        }

        function transactionSuccessCallback() {
            sync.log.trace("Entering  com.pc.portfolios.contract.updateAll->transactionSuccessCallback");
            if (!isError) {
                kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, transactionErrorCallback, markForUpload, com.pc.portfolios.contract.getPKTable());
            }
        }

        function transactionErrorCallback() {
            if (errObject === false) {
                //Sql statement error has occcurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
                kony.sync.errorObject = null;
            } else if (errObject !== null) {
                // Referential integrity error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            } else {
                //Transaction error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
            }
        }
        //foreign key constraints validations
        function checkIntegrity(tx) {
            sync.log.trace("Entering  com.pc.portfolios.contract.updateAll->checkIntegrity");
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var relationshipMap = {};
                relationshipMap = com.pc.portfolios.contract.getRelationshipMap(relationshipMap, inputArray[i].changeSet);
                sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
                errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
                if (errObject === false) {
                    isError = true;
                    return;
                }
                if (errObject !== true) {
                    isError = true;
                    kony.sync.rollbackTransaction(tx);
                    return;
                }
            }
        }
    }
    /************************************************************************************
     * Deletes contract using primary key from the local Database. The record will be
     * deleted from the enterprise datasource in the next Sync.
     *************************************************************************************/
com.pc.portfolios.contract.prototype.deleteByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.prototype.deleteByPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.contract.deleteByPK(pks, successcallback, errorcallback, this.markForUpload);
};
com.pc.portfolios.contract.deleteByPK = function(pks, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.portfolios.contract.deleteByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.deleteByPK", "deleteByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var twcs = [];
    var deletedRows;
    var record = "";
    if (com.pc.portfolios.contract.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);

    function contractTransactionCallback(tx) {
        sync.log.trace("Entering com.pc.portfolios.contract.deleteByPK->contract_PKPresent successcallback");
        record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (record === false) {
            isError = true;
            return;
        }
        if (null !== record) {} else {
            pkNotFound = true;
        }
        var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
        if (deletedRows === false) {
            isError = true;
        }
    }

    function contractErrorCallback() {
        sync.log.error("Entering com.pc.portfolios.contract.deleteByPK->relationship failure callback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function contractSuccessCallback() {
        sync.log.trace("Entering com.pc.portfolios.contract.deleteByPK->relationship success callback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering com.pc.portfolios.contract.deleteByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, contractTransactionCallback, contractSuccessCallback, contractErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes contract(s) using where clause from the local Database. The record(s)
 * will be deleted from the enterprise datasource in the next Sync.
 * e.g. com.pc.portfolios.contract.remove("where businessCode like 'A%'", successcallback,errorcallback, true);
 *************************************************************************************/
com.pc.portfolios.contract.remove = function(wcs, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.portfolios.contract.remove->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.remove", "remove", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function contract_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function contract_removeSuccess() {
        sync.log.trace("Entering com.pc.portfolios.contract.remove->contract_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering com.pc.portfolios.contract.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering com.pc.portfolios.contract.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, contract_removeTransactioncallback, contract_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Deletes contract using primary key from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
com.pc.portfolios.contract.prototype.removeDeviceInstanceByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.prototype.removeDeviceInstanceByPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.contract.removeDeviceInstanceByPK(pks, successcallback, errorcallback);
};
com.pc.portfolios.contract.removeDeviceInstanceByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.removeDeviceInstanceByPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.removeDeviceInstanceByPK", "removeDeviceInstanceByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var deletedRows;
    if (com.pc.portfolios.contract.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }

    function contractTransactionCallback(tx) {
        sync.log.trace("Entering com.pc.portfolios.contract.removeDeviceInstanceByPK -> contractTransactionCallback");
        var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (null !== record && false != record) {
            deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
            if (deletedRows === false) {
                isError = true;
            }
        } else {
            pkNotFound = true;
        }
    }

    function contractErrorCallback() {
        sync.log.error("Entering com.pc.portfolios.contract.removeDeviceInstanceByPK -> contractErrorCallback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function contractSuccessCallback() {
        sync.log.trace("Entering com.pc.portfolios.contract.removeDeviceInstanceByPK -> contractSuccessCallback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering com.pc.portfolios.contract.removeDeviceInstanceByPK -> PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, contractTransactionCallback, contractSuccessCallback, contractErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes contract(s) using where clause from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
com.pc.portfolios.contract.removeDeviceInstance = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.removeDeviceInstance->main function");
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function contract_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function contract_removeSuccess() {
        sync.log.trace("Entering com.pc.portfolios.contract.remove->contract_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering com.pc.portfolios.contract.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering com.pc.portfolios.contract.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, contract_removeTransactioncallback, contract_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Retrieves contract using primary key from the local Database. 
 *************************************************************************************/
com.pc.portfolios.contract.prototype.getAllDetailsByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.prototype.getAllDetailsByPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.contract.getAllDetailsByPK(pks, successcallback, errorcallback);
};
com.pc.portfolios.contract.getAllDetailsByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.getAllDetailsByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.getAllDetailsByPK", "getAllDetailsByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var wcs = [];
    if (com.pc.portfolios.contract.pkCheck(pks, wcs, errorcallback, "searching") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, wcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.getAllDetailsByPK-> success callback function");
        successcallback(com.pc.portfolios.contract.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Retrieves contract(s) using where clause from the local Database. 
 * e.g. com.pc.portfolios.contract.find("where businessCode like 'A%'", successcallback,errorcallback);
 *************************************************************************************/
com.pc.portfolios.contract.find = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.find function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.find", "find", errorcallback)) {
        return;
    }
    //wcs will be a string formed by the user.
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select * from \"" + tbname + "\" " + wcs;

    function mySuccCallback(res) {
        kony.sync.verifyAndCallClosure(successcallback, com.pc.portfolios.contract.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Marks instance of contract with given primary key for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.contract.prototype.markForUploadbyPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.prototype.markForUploadbyPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.contract.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.pc.portfolios.contract.markForUploadbyPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.markForUploadbyPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.markForUploadbyPK", "markForUploadbyPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var isError = false;
    var recordsFound = false;
    var recordsMarkedForUpload = 0;
    var wcs = [];
    if (com.pc.portfolios.contract.pkCheck(pks, wcs, errorcallback, "marking for upload by PK") === false) {
        return;
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = [];
        twcs = wcs;
        kony.table.insert(twcs, {
            key: kony.sync.historyTableChangeTypeColumn,
            value: record[kony.sync.historyTableChangeTypeColumn],
            optype: "EQ",
            comptype: "AND"
        });
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        kony.sync.qb_where(query, twcs);
        kony.table.remove(twcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function single_transaction_callback(tx) {
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query, tbname);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        var resultSet = kony.sync.executeSql(tx, sql, params);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        if (num_records > 0) {
            recordsFound = true;
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
            var changeType = record[kony.sync.mainTableChangeTypeColumn];
            if (!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith("" + changeType, "9")) {
                recordsMarkedForUpload = 1;
                if (markRecordForUpload(tx, record) === false) {
                    isError = true;
                    return;
                }
            }
        }
        var query1 = kony.sync.qb_createQuery();
        kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
        kony.sync.qb_where(query1, wcs);
        var query1_compile = kony.sync.qb_compile(query1);
        var sql1 = query1_compile[0];
        var params1 = query1_compile[1];
        var resultSet1 = kony.sync.executeSql(tx, sql1, params1);
        if (resultSet1 !== false) {
            var num_records = resultSet1.rows.length;
            for (var i = 0; i <= num_records - 1; i++) {
                var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
                if (markRecordForUploadHistory(tx, record) === false) {
                    isError = true;
                    return;
                }
                recordsFound = true;
            }
        } else {
            isError = true;
        }
    }

    function single_transaction_success_callback() {
        if (recordsFound === true) {
            kony.sync.verifyAndCallClosure(successcallback, {
                count: recordsMarkedForUpload
            });
        } else {
            kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
        }
    }

    function single_transaction_error_callback(res) {
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Marks instance(s) of contract matching given where clause for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.contract.markForUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.markForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.markForUpload", "markForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var isError = false;
    var num_records_main = 0;
    wcs = kony.sync.validateWhereClause(wcs);
    if (!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
        wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    } else {
        wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + wcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = "";
        twcs = wcs;
        twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + twcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function single_transaction_callback(tx) {
        sync.log.trace("Entering com.pc.portfolios.contract.markForUpload->single_transaction_callback");
        //updating main table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        num_records_main = resultSet.rows.length;
        for (var i = 0; i < num_records_main; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUpload(tx, record) === false) {
                isError = true;
                return;
            }
        }
        //updating history table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        for (var i = 0; i <= num_records - 1; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUploadHistory(tx, record) === false) {
                isError = true;
                return;
            }
        }
    }

    function single_transaction_success_callback() {
        sync.log.trace("Entering com.pc.portfolios.contract.markForUpload->single_transaction_success_callback");
        kony.sync.verifyAndCallClosure(successcallback, {
            count: num_records_main
        });
    }

    function single_transaction_error_callback() {
        sync.log.error("Entering com.pc.portfolios.contract.markForUpload->single_transaction_error_callback");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Retrieves instance(s) of contract pending for upload. Records are marked for
 * pending upload if they have been updated or created locally and the changes have
 * not been merged with enterprise datasource.
 *************************************************************************************/
com.pc.portfolios.contract.getPendingUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.getPendingUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.getPendingUpload->successcallback function");
        kony.sync.verifyAndCallClosure(successcallback, com.pc.portfolios.contract.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of contract pending for acknowledgement. This is relevant
 * when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
 * In persistent Sync the  records in the local database are put into a pending 
 * acknowledgement state after an upload.
 *************************************************************************************/
com.pc.portfolios.contract.getPendingAcknowledgement = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.getPendingAcknowledgement->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var mysql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " <> " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.getPendingAcknowledgement success callback function");
        kony.sync.verifyAndCallClosure(successcallback, com.pc.portfolios.contract.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of contract deferred for upload.
 *************************************************************************************/
com.pc.portfolios.contract.getDeferredUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.getDeferredUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.getDeferredUpload->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, com.pc.portfolios.contract.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Rollbacks all changes to contract in local database to last synced state
 *************************************************************************************/
com.pc.portfolios.contract.rollbackPendingLocalChanges = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.rollbackPendingLocalChanges->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.rollbackPendingLocalChanges->main function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }
};
/************************************************************************************
 * Rollbacks changes to contract's record with given primary key in local 
 * database to last synced state
 *************************************************************************************/
com.pc.portfolios.contract.prototype.rollbackPendingLocalChangesByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.prototype.rollbackPendingLocalChangesByPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.contract.rollbackPendingLocalChangesByPK(pks, successcallback, errorcallback);
};
com.pc.portfolios.contract.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.rollbackPendingLocalChangesByPK->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.rollbackPendingLocalChangesByPK", "rollbackPendingLocalChangesByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var wcs = [];
    if (com.pc.portfolios.contract.pkCheck(pks, wcs, errorcallback, "rollbacking") === false) {
        return;
    }
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.rollbackPendingLocalChangesByPK->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering com.pc.portfolios.contract.rollbackPendingLocalChangesByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
};
/************************************************************************************
 * isRecordDeferredForUpload returns true or false depending on whether contract's record  
 * with given primary key got deferred in last sync
 *************************************************************************************/
com.pc.portfolios.contract.prototype.isRecordDeferredForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.portfolios.contract.prototype.isRecordDeferredForUpload function");
    var pks = this.getPKTable();
    com.pc.portfolios.contract.isRecordDeferredForUpload(pks, successcallback, errorcallback);
};
com.pc.portfolios.contract.isRecordDeferredForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.isRecordDeferredForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.isRecordDeferredForUpload", "isRecordDeferredForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var wcs = [];
    var flag;
    if (com.pc.portfolios.contract.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.isRecordDeferredForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            deferred: flag
        });
    }
};
/************************************************************************************
 * isRecordPendingForUpload returns true or false depending on whether contract's record  
 * with given primary key is pending for upload
 *************************************************************************************/
com.pc.portfolios.contract.prototype.isRecordPendingForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.portfolios.contract.prototype.isRecordPendingForUpload function");
    var pks = this.getPKTable();
    com.pc.portfolios.contract.isRecordPendingForUpload(pks, successcallback, errorcallback);
};
com.pc.portfolios.contract.isRecordPendingForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.isRecordPendingForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.contract.isRecordPendingForUpload", "isRecordPendingForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.contract.getTableName();
    var wcs = [];
    var flag;
    if (com.pc.portfolios.contract.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "NOT LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.contract.isRecordPendingForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            pending: flag
        });
    }
};
/************************************************************************************
 * Start of helper functions used internally, not to be used as ORMs
 *************************************************************************************/
//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.pc.portfolios.contract.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal) {
    sync.log.trace("Entering com.pc.portfolios.contract.removeCascade function");
    var tbname = com.pc.portfolios.contract.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);

    function removeCascadeChildren() {}
    if (isCascade) {
        if (removeCascadeChildren() === false) {
            return false;
        }
        if (kony.sync.deleteBatch(tx, tbname, wcs, isLocal, markForUpload, null) === false) {
            return false;
        }
        return true;
    } else {
        var sql = "select * from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            return false;
        }
        var num_records = resultSet.rows.length;
        if (num_records === 0) {
            return true;
        } else {
            sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable));
            errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity, kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable)));
            return false;
        }
    }
};
com.pc.portfolios.contract.convertTableToObject = function(res) {
    sync.log.trace("Entering com.pc.portfolios.contract.convertTableToObject function");
    objMap = [];
    if (res !== null) {
        for (var i in res) {
            var obj = new com.pc.portfolios.contract();
            obj.businessCode = res[i].businessCode;
            obj.contractCode = res[i].contractCode;
            obj.contractDeleted = res[i].contractDeleted;
            obj.contractName = res[i].contractName;
            obj.contractTypeCode = res[i].contractTypeCode;
            obj.countryCode = res[i].countryCode;
            obj.updateDateTime = res[i].updateDateTime;
            obj.accountCode = res[i].accountCode;
            obj.markForUpload = (Math.floor(res[i].konysyncchangetype / 10) == 9) ? false : true;
            objMap[i] = obj;
        }
    }
    return objMap;
};
com.pc.portfolios.contract.filterAttributes = function(valuestable, insert) {
    sync.log.trace("Entering com.pc.portfolios.contract.filterAttributes function");
    var attributeTable = {};
    attributeTable.businessCode = "businessCode";
    attributeTable.contractCode = "contractCode";
    attributeTable.contractName = "contractName";
    attributeTable.contractTypeCode = "contractTypeCode";
    attributeTable.countryCode = "countryCode";
    attributeTable.accountCode = "accountCode";
    var PKTable = {};
    PKTable.businessCode = {}
    PKTable.businessCode.name = "businessCode";
    PKTable.businessCode.isAutoGen = false;
    PKTable.contractCode = {}
    PKTable.contractCode.name = "contractCode";
    PKTable.contractCode.isAutoGen = false;
    PKTable.countryCode = {}
    PKTable.countryCode.name = "countryCode";
    PKTable.countryCode.isAutoGen = false;
    var newvaluestable = {};
    for (var k in valuestable) {
        var v = valuestable[k];
        if (kony.sync.isNull(attributeTable[k])) {
            sync.log.warn("Ignoring the attribute " + k + " for the SyncObject contract. " + k + " not defined as an attribute in SyncConfiguration.");
        } else if (!kony.sync.isNull(PKTable[k])) {
            if (insert === false) {
                sync.log.warn("Ignoring the primary key " + k + " for the SyncObject contract. Primary Key should not be the part of the attributes to be updated in the local device database.");
            } else if (PKTable[k].isAutoGen) {
                sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject contract. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
            } else {
                newvaluestable[k] = v;
            }
        } else {
            newvaluestable[k] = v;
        }
    }
    return newvaluestable;
};
com.pc.portfolios.contract.formOrderByClause = function(orderByMap) {
    sync.log.trace("Entering com.pc.portfolios.contract.formOrderByClause function");
    if (!kony.sync.isNull(orderByMap)) {
        var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
        //var filteredValuestable = com.pc.portfolios.contract.filterAttributes(valuestable, true);
        return kony.sync.convertToValuesTableOrderByMap(orderByMap, valuestable);
    }
    return null;
};
com.pc.portfolios.contract.prototype.getValuesTable = function(isInsert) {
    sync.log.trace("Entering com.pc.portfolios.contract.prototype.getValuesTable function");
    var valuesTable = {};
    if (isInsert === true) {
        valuesTable.businessCode = this.businessCode;
    }
    if (isInsert === true) {
        valuesTable.contractCode = this.contractCode;
    }
    valuesTable.contractName = this.contractName;
    valuesTable.contractTypeCode = this.contractTypeCode;
    if (isInsert === true) {
        valuesTable.countryCode = this.countryCode;
    }
    valuesTable.accountCode = this.accountCode;
    return valuesTable;
};
com.pc.portfolios.contract.prototype.getPKTable = function() {
    sync.log.trace("Entering com.pc.portfolios.contract.prototype.getPKTable function");
    var pkTable = {};
    pkTable.businessCode = {
        key: "businessCode",
        value: this.businessCode
    };
    pkTable.contractCode = {
        key: "contractCode",
        value: this.contractCode
    };
    pkTable.countryCode = {
        key: "countryCode",
        value: this.countryCode
    };
    return pkTable;
};
com.pc.portfolios.contract.getPKTable = function() {
    sync.log.trace("Entering com.pc.portfolios.contract.getPKTable function");
    var pkTable = [];
    pkTable.push("businessCode");
    pkTable.push("contractCode");
    pkTable.push("countryCode");
    return pkTable;
};
com.pc.portfolios.contract.pkCheck = function(pks, wcs, errorcallback, opName) {
    sync.log.trace("Entering com.pc.portfolios.contract.pkCheck function");
    var wc = [];
    if (!kony.sync.isNull(pks.businessCode)) {
        if (!kony.sync.isNull(pks.businessCode.value)) {
            wc.key = "businessCode";
            wc.value = pks.businessCode.value;
        } else {
            wc.key = "businessCode";
            wc.value = pks.businessCode;
        }
    } else {
        sync.log.error("Primary Key businessCode not specified in " + opName + " an item in contract");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("businessCode", opName, "contract")));
        return;
    }
    kony.table.insert(wcs, wc);
    var wc = [];
    if (!kony.sync.isNull(pks.contractCode)) {
        if (!kony.sync.isNull(pks.contractCode.value)) {
            wc.key = "contractCode";
            wc.value = pks.contractCode.value;
        } else {
            wc.key = "contractCode";
            wc.value = pks.contractCode;
        }
    } else {
        sync.log.error("Primary Key contractCode not specified in " + opName + " an item in contract");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("contractCode", opName, "contract")));
        return;
    }
    kony.table.insert(wcs, wc);
    var wc = [];
    if (!kony.sync.isNull(pks.countryCode)) {
        if (!kony.sync.isNull(pks.countryCode.value)) {
            wc.key = "countryCode";
            wc.value = pks.countryCode.value;
        } else {
            wc.key = "countryCode";
            wc.value = pks.countryCode;
        }
    } else {
        sync.log.error("Primary Key countryCode not specified in " + opName + " an item in contract");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("countryCode", opName, "contract")));
        return;
    }
    kony.table.insert(wcs, wc);
    return true;
};
com.pc.portfolios.contract.validateNull = function(valuestable, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.validateNull function");
    return true;
};
com.pc.portfolios.contract.validateNullInsert = function(valuestable, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.contract.validateNullInsert function");
    if (kony.sync.isNull(valuestable.businessCode) || kony.sync.isEmptyString(valuestable.businessCode)) {
        sync.log.error("Mandatory attribute businessCode is missing for the SyncObject contract.");
        errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeMandatoryAttribute, kony.sync.getErrorMessage(kony.sync.errorCodeMandatoryAttribute, "contract", "businessCode")));
        return false;
    }
    if (kony.sync.isNull(valuestable.contractCode) || kony.sync.isEmptyString(valuestable.contractCode)) {
        sync.log.error("Mandatory attribute contractCode is missing for the SyncObject contract.");
        errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeMandatoryAttribute, kony.sync.getErrorMessage(kony.sync.errorCodeMandatoryAttribute, "contract", "contractCode")));
        return false;
    }
    if (kony.sync.isNull(valuestable.countryCode) || kony.sync.isEmptyString(valuestable.countryCode)) {
        sync.log.error("Mandatory attribute countryCode is missing for the SyncObject contract.");
        errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeMandatoryAttribute, kony.sync.getErrorMessage(kony.sync.errorCodeMandatoryAttribute, "contract", "countryCode")));
        return false;
    }
    return true;
};
com.pc.portfolios.contract.getRelationshipMap = function(relationshipMap, valuestable) {
    sync.log.trace("Entering com.pc.portfolios.contract.getRelationshipMap function");
    var r1 = {};
    r1 = {};
    r1.sourceAttribute = [];
    r1.foreignKeyAttribute = [];
    r1.targetAttributeValue = [];
    if (!kony.sync.isNullOrUndefined(valuestable.accountCode)) {
        r1.sourceAttribute.push("accountCode");
        r1.foreignKeyAttribute.push("accountCode");
        r1.targetAttributeValue.push("'" + valuestable.accountCode + "'");
    }
    if (!kony.sync.isNullOrUndefined(valuestable.businessCode)) {
        r1.sourceAttribute.push("businessCode");
        r1.foreignKeyAttribute.push("businessCode");
        r1.targetAttributeValue.push("'" + valuestable.businessCode + "'");
    }
    if (!kony.sync.isNullOrUndefined(valuestable.countryCode)) {
        r1.sourceAttribute.push("countryCode");
        r1.foreignKeyAttribute.push("countryCode");
        r1.targetAttributeValue.push("'" + valuestable.countryCode + "'");
    }
    if (r1.targetAttributeValue.length > 0) {
        if (relationshipMap.account === undefined) {
            relationshipMap.account = [];
        }
        relationshipMap.account.push(r1);
    }
    return relationshipMap;
};
com.pc.portfolios.contract.checkPKValueTables = function(valuetables) {
    var checkPksNotNullFlag = true;
    for (var i = 0; i < valuetables.length; i++) {
        if (kony.sync.isNull(valuetables[i])) {
            checkPksNotNullFlag = false;
            break;
        }
    }
    return checkPksNotNullFlag;
};
com.pc.portfolios.contract.getTableName = function() {
    return "contract";
};
// **********************************End contract's helper methods************************