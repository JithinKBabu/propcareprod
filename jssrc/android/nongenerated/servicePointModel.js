//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:02 UTC 2016servicePoint*******************
// **********************************Start servicePoint's helper methods************************
if (typeof(kony) === "undefined") {
    kony = {};
}
if (typeof(kony.sync) === "undefined") {
    kony.sync = {};
}
if (typeof(kony.sync.log) === "undefined") {
    kony.sync.log = {};
}
if (typeof(sync) === "undefined") {
    sync = {};
}
if (typeof(sync.log) === "undefined") {
    sync.log = {};
}
if (typeof(com) === "undefined") {
    com = {};
}
if (typeof(com.pc) === "undefined") {
    com.pc = {};
}
if (typeof(com.pc.portfolios) === "undefined") {
    com.pc.portfolios = {};
}
/************************************************************************************
 * Creates new servicePoint
 *************************************************************************************/
com.pc.portfolios.servicePoint = function() {
    this.countryCode = null;
    this.businessCode = null;
    this.contractCode = null;
    this.propertyCode = null;
    this.serviceLineCode = null;
    this.serviceCoverNumber = null;
    this.servicePointId = null;
    this.servicePointDescription = null;
    this.servicePointOrder = null;
    this.classedAsInternal = null;
    this.detectorId = null;
    this.physicalId = null;
    this.physicalIdType = null;
    this.quantityCurrent = null;
    this.quantityOutstanding = null;
    this.siteDivisionLevel = null;
    this.siteDivisionId = null;
    this.servicePointDeleted = null;
    this.updateDateTime = null;
    this.markForUpload = true;
};
com.pc.portfolios.servicePoint.prototype = {
    get countryCode() {
        return this._countryCode;
    },
    set countryCode(val) {
        this._countryCode = val;
    },
    get businessCode() {
        return this._businessCode;
    },
    set businessCode(val) {
        this._businessCode = val;
    },
    get contractCode() {
        return this._contractCode;
    },
    set contractCode(val) {
        this._contractCode = val;
    },
    get propertyCode() {
        return this._propertyCode;
    },
    set propertyCode(val) {
        this._propertyCode = val;
    },
    get serviceLineCode() {
        return this._serviceLineCode;
    },
    set serviceLineCode(val) {
        this._serviceLineCode = val;
    },
    get serviceCoverNumber() {
        return this._serviceCoverNumber;
    },
    set serviceCoverNumber(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute serviceCoverNumber in servicePoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._serviceCoverNumber = val;
    },
    get servicePointId() {
        return this._servicePointId;
    },
    set servicePointId(val) {
        this._servicePointId = val;
    },
    get servicePointDescription() {
        return this._servicePointDescription;
    },
    set servicePointDescription(val) {
        this._servicePointDescription = val;
    },
    get servicePointOrder() {
        return this._servicePointOrder;
    },
    set servicePointOrder(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute servicePointOrder in servicePoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._servicePointOrder = val;
    },
    get classedAsInternal() {
        return kony.sync.getBoolean(this._classedAsInternal) + "";
    },
    set classedAsInternal(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute classedAsInternal in servicePoint.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._classedAsInternal = val;
    },
    get detectorId() {
        return this._detectorId;
    },
    set detectorId(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute detectorId in servicePoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._detectorId = val;
    },
    get physicalId() {
        return this._physicalId;
    },
    set physicalId(val) {
        this._physicalId = val;
    },
    get physicalIdType() {
        return this._physicalIdType;
    },
    set physicalIdType(val) {
        this._physicalIdType = val;
    },
    get quantityCurrent() {
        return this._quantityCurrent;
    },
    set quantityCurrent(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute quantityCurrent in servicePoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._quantityCurrent = val;
    },
    get quantityOutstanding() {
        return this._quantityOutstanding;
    },
    set quantityOutstanding(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute quantityOutstanding in servicePoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._quantityOutstanding = val;
    },
    get siteDivisionLevel() {
        return this._siteDivisionLevel;
    },
    set siteDivisionLevel(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute siteDivisionLevel in servicePoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._siteDivisionLevel = val;
    },
    get siteDivisionId() {
        return this._siteDivisionId;
    },
    set siteDivisionId(val) {
        this._siteDivisionId = val;
    },
    get servicePointDeleted() {
        return kony.sync.getBoolean(this._servicePointDeleted) + "";
    },
    set servicePointDeleted(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute servicePointDeleted in servicePoint.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._servicePointDeleted = val;
    },
    get updateDateTime() {
        return this._updateDateTime;
    },
    set updateDateTime(val) {
        this._updateDateTime = val;
    },
};
/************************************************************************************
 * Retrieves all instances of servicePoint SyncObject present in local database with
 * given limit and offset where limit indicates the number of records to be retrieved
 * and offset indicates number of rows to be ignored before returning the records.
 * e.g. var orderByMap = []
 * orderByMap[0] = {};
 * orderByMap[0].key = "countryCode";
 * orderByMap[0].sortType ="desc";
 * orderByMap[1] = {};
 * orderByMap[1].key = "businessCode";
 * orderByMap[1].sortType ="asc";
 * var limit = 20;
 * var offset = 5;
 * com.pc.portfolios.servicePoint.getAll(successcallback,errorcallback, orderByMap, limit, offset)
 *************************************************************************************/
com.pc.portfolios.servicePoint.getAll = function(successcallback, errorcallback, orderByMap, limit, offset) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.getAll->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    orderByMap = kony.sync.formOrderByClause("servicePoint", orderByMap);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_orderBy(query, orderByMap);
    kony.sync.qb_limitOffset(query, limit, offset);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.getAll->successcallback");
        successcallback(com.pc.portfolios.servicePoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Returns number of servicePoint present in local database.
 *************************************************************************************/
com.pc.portfolios.servicePoint.getAllCount = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.getAllCount function");
    com.pc.portfolios.servicePoint.getCount("", successcallback, errorcallback);
};
/************************************************************************************
 * Returns number of servicePoint using where clause in the local Database
 *************************************************************************************/
com.pc.portfolios.servicePoint.getCount = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.getCount->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.getCount", "getCount", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select count(*) from \"" + tbname + "\" " + wcs;
    kony.sync.single_execute_sql(dbname, sql, null, mySuccCallback, errorcallback);

    function mySuccCallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.getCount->successcallback");
        if (null !== res) {
            var count = null;
            count = res["count(*)"];
            kony.sync.verifyAndCallClosure(successcallback, {
                count: count
            });
        } else {
            sync.log.error("Some error occured while getting the count");
        }
    }
};
/************************************************************************************
 * Creates a new instance of servicePoint in the local Database. The new record will 
 * be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.servicePoint.prototype.create = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.portfolios.servicePoint.prototype.create function");
    var valuestable = this.getValuesTable(true);
    com.pc.portfolios.servicePoint.create(valuestable, successcallback, errorcallback, this.markForUpload);
};
com.pc.portfolios.servicePoint.create = function(valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  com.pc.portfolios.servicePoint.create->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.create", "create", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    if (kony.sync.attributeValidation(valuestable, "servicePoint", errorcallback, true) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  com.pc.portfolios.servicePoint.create->success callback");
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
    var pks = [];
    var errMsg = "";

    function createSuccesscallback(res) {
        if (res == null || res.length == 0) {
            var relationshipMap = {};
            relationshipMap = com.pc.portfolios.servicePoint.getRelationshipMap(relationshipMap, valuestable);
            kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
        } else {
            errMsg = "[" + errMsg + "]";
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey, kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
        }
    }
    if (kony.sync.enableORMValidations) {
        errMsg = "countryCode=" + valuestable.countryCode;
        pks["countryCode"] = {
            key: "countryCode",
            value: valuestable.countryCode
        };
        errMsg = errMsg + ", businessCode=" + valuestable.businessCode;
        pks["businessCode"] = {
            key: "businessCode",
            value: valuestable.businessCode
        };
        errMsg = errMsg + ", contractCode=" + valuestable.contractCode;
        pks["contractCode"] = {
            key: "contractCode",
            value: valuestable.contractCode
        };
        errMsg = errMsg + ", propertyCode=" + valuestable.propertyCode;
        pks["propertyCode"] = {
            key: "propertyCode",
            value: valuestable.propertyCode
        };
        errMsg = errMsg + ", servicePointId=" + valuestable.servicePointId;
        pks["servicePointId"] = {
            key: "servicePointId",
            value: valuestable.servicePointId
        };
        com.pc.portfolios.servicePoint.getAllDetailsByPK(pks, createSuccesscallback, errorcallback)
    } else {
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
};
/************************************************************************************
 * Creates number of new instances of servicePoint in the local Database. These new 
 * records will be merged with the enterprise datasource in the next Sync. Based upon 
 * kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var valuesArray = [];
 *		valuesArray[0] = {};
 *		valuesArray[0].countryCode = "countryCode_0";
 *		valuesArray[0].businessCode = "businessCode_0";
 *		valuesArray[0].contractCode = "contractCode_0";
 *		valuesArray[0].propertyCode = "propertyCode_0";
 *		valuesArray[0].serviceLineCode = "serviceLineCode_0";
 *		valuesArray[0].serviceCoverNumber = 0;
 *		valuesArray[0].servicePointId = "servicePointId_0";
 *		valuesArray[0].servicePointDescription = "servicePointDescription_0";
 *		valuesArray[0].servicePointOrder = 0;
 *		valuesArray[0].classedAsInternal = true;
 *		valuesArray[0].detectorId = 0;
 *		valuesArray[0].physicalId = "physicalId_0";
 *		valuesArray[0].physicalIdType = "physicalIdType_0";
 *		valuesArray[0].quantityCurrent = 0;
 *		valuesArray[0].quantityOutstanding = 0;
 *		valuesArray[0].siteDivisionLevel = 0;
 *		valuesArray[0].siteDivisionId = "siteDivisionId_0";
 *		valuesArray[1] = {};
 *		valuesArray[1].countryCode = "countryCode_1";
 *		valuesArray[1].businessCode = "businessCode_1";
 *		valuesArray[1].contractCode = "contractCode_1";
 *		valuesArray[1].propertyCode = "propertyCode_1";
 *		valuesArray[1].serviceLineCode = "serviceLineCode_1";
 *		valuesArray[1].serviceCoverNumber = 1;
 *		valuesArray[1].servicePointId = "servicePointId_1";
 *		valuesArray[1].servicePointDescription = "servicePointDescription_1";
 *		valuesArray[1].servicePointOrder = 1;
 *		valuesArray[1].classedAsInternal = true;
 *		valuesArray[1].detectorId = 1;
 *		valuesArray[1].physicalId = "physicalId_1";
 *		valuesArray[1].physicalIdType = "physicalIdType_1";
 *		valuesArray[1].quantityCurrent = 1;
 *		valuesArray[1].quantityOutstanding = 1;
 *		valuesArray[1].siteDivisionLevel = 1;
 *		valuesArray[1].siteDivisionId = "siteDivisionId_1";
 *		valuesArray[2] = {};
 *		valuesArray[2].countryCode = "countryCode_2";
 *		valuesArray[2].businessCode = "businessCode_2";
 *		valuesArray[2].contractCode = "contractCode_2";
 *		valuesArray[2].propertyCode = "propertyCode_2";
 *		valuesArray[2].serviceLineCode = "serviceLineCode_2";
 *		valuesArray[2].serviceCoverNumber = 2;
 *		valuesArray[2].servicePointId = "servicePointId_2";
 *		valuesArray[2].servicePointDescription = "servicePointDescription_2";
 *		valuesArray[2].servicePointOrder = 2;
 *		valuesArray[2].classedAsInternal = true;
 *		valuesArray[2].detectorId = 2;
 *		valuesArray[2].physicalId = "physicalId_2";
 *		valuesArray[2].physicalIdType = "physicalIdType_2";
 *		valuesArray[2].quantityCurrent = 2;
 *		valuesArray[2].quantityOutstanding = 2;
 *		valuesArray[2].siteDivisionLevel = 2;
 *		valuesArray[2].siteDivisionId = "siteDivisionId_2";
 *		com.pc.portfolios.servicePoint.createAll(valuesArray, successcallback, errorcallback, true);
 *************************************************************************************/
com.pc.portfolios.servicePoint.createAll = function(valuesArray, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.createAll function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.createAll", "createAll", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var isProperData = true;
    var arrayLen = 0;
    var errorInfo = [];
    var arrayLength = valuesArray.length;
    var errObject = null;
    var isReferentialIntegrityFailure = false;
    var errMsg = null;
    if (kony.sync.enableORMValidations) {
        var newValuesArray = [];
        //column level validations
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var valuestable = valuesArray[i];
            if (kony.sync.attributeValidation(valuestable, "servicePoint", errorcallback, true) === false) {
                return;
            }
            newValuesArray[i] = valuestable;
        }
        valuesArray = newValuesArray;
        var isDuplicateKey = false;
        //checking for duplicate records
        var connection = kony.sync.getConnectionOnly(dbname, dbname);
        kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
        var isError = false;

        function checkDuplicatePkCallback(tx) {
            arrayLength = valuesArray.length;
            for (var i = 0; valuesArray != null && i < arrayLength; i++) {
                var valuestable = valuesArray[i];
                var pks = [];
                errMsg = "countryCode=" + valuestable.countryCode;
                pks["countryCode"] = {
                    key: "countryCode",
                    value: valuestable.countryCode
                };
                errMsg = errMsg + ", businessCode=" + valuestable.businessCode;
                pks["businessCode"] = {
                    key: "businessCode",
                    value: valuestable.businessCode
                };
                errMsg = errMsg + ", contractCode=" + valuestable.contractCode;
                pks["contractCode"] = {
                    key: "contractCode",
                    value: valuestable.contractCode
                };
                errMsg = errMsg + ", propertyCode=" + valuestable.propertyCode;
                pks["propertyCode"] = {
                    key: "propertyCode",
                    value: valuestable.propertyCode
                };
                errMsg = errMsg + ", servicePointId=" + valuestable.servicePointId;
                pks["servicePointId"] = {
                    key: "servicePointId",
                    value: valuestable.servicePointId
                };
                var wcs = [];
                if (com.pc.portfolios.servicePoint.pkCheck(pks, wcs, errorcallback, "searching") === false) {
                    isError = true;
                    return;
                }
                var query = kony.sync.qb_createQuery();
                kony.sync.qb_select(query, null);
                kony.sync.qb_from(query, tbname);
                kony.sync.qb_where(query, wcs);
                var query_compile = kony.sync.qb_compile(query);
                var sql = query_compile[0];
                var params = query_compile[1];
                var resultset = kony.sync.executeSql(tx, sql, params);
                if (resultset === false) {
                    isError = true;
                    return;
                }
                if (resultset.rows.length != 0) {
                    isError = true;
                    errMsg = "[" + errMsg + "]";
                    isDuplicateKey = true;
                    return;
                }
            }
            if (!isError) {
                checkIntegrity(tx);
            }
        }
    } else {
        //copying by value
        var newValuesArray = [];
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
        }
        valuesArray = newValuesArray;
        kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
    }

    function transactionErrorCallback() {
        if (isError == true) {
            //Statement error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
        } else {
            //Transaction error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
        }
    }

    function transactionSuccessCallback() {
        sync.log.trace("Entering  com.pc.portfolios.servicePoint.createAll->transactionSuccessCallback");
        if (!isError) {
            kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
        } else {
            if (isReferentialIntegrityFailure) {
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            }
            if (isDuplicateKey) {
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey, kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
            }
        }
    }
    //foreign key constraints validations
    function checkIntegrity(tx) {
        sync.log.trace("Entering  com.pc.portfolios.servicePoint.createAll->checkIntegrity");
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var relationshipMap = {};
            relationshipMap = com.pc.portfolios.servicePoint.getRelationshipMap(relationshipMap, valuesArray[i]);
            errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
            if (errObject === false) {
                isError = true;
                return;
            }
            if (errObject !== true) {
                isError = true;
                isReferentialIntegrityFailure = true;
                return;
            }
        }
    }
};
/************************************************************************************
 * Updates servicePoint using primary key in the local Database. The update will be
 * merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.servicePoint.prototype.updateByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.portfolios.servicePoint.prototype.updateByPK function");
    var pks = this.getPKTable();
    var valuestable = this.getValuesTable(false);
    com.pc.portfolios.servicePoint.updateByPK(pks, valuestable, successcallback, errorcallback, this.markForUpload);
};
com.pc.portfolios.servicePoint.updateByPK = function(pks, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  com.pc.portfolios.servicePoint.updateByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.updateByPK", "updateByPk", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    if (com.pc.portfolios.servicePoint.pkCheck(pks, wcs, errorcallback, "updating") === false) {
        return;
    }
    if (kony.sync.attributeValidation(valuestable, "servicePoint", errorcallback, false) === false) {
        return;
    }
    var relationshipMap = {};
    relationshipMap = com.pc.portfolios.servicePoint.getRelationshipMap(relationshipMap, valuestable);
    kony.sync.updateByPK(tbname, dbname, relationshipMap, pks, valuestable, successcallback, errorcallback, markForUpload, wcs);
};
/************************************************************************************
 * Updates servicePoint(s) using where clause in the local Database. The update(s)
 * will be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.servicePoint.update = function(wcs, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.update function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.update", "update", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    if (kony.sync.attributeValidation(valuestable, "servicePoint", errorcallback, false) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  com.pc.portfolios.servicePoint.update-> success callback of Integrity Check");
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, com.pc.portfolios.servicePoint.getPKTable());
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = com.pc.portfolios.servicePoint.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, com.pc.portfolios.servicePoint.getPKTable());
    }
};
/************************************************************************************
 * Updates servicePoint(s) satisfying one or more where clauses in the local Database. 
 * The update(s) will be merged with the enterprise datasource in the next Sync.
 * Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var inputArray = [];
 *		inputArray[0] = {};
 *		inputArray[0].changeSet = {};
 *		inputArray[0].changeSet.serviceLineCode = "serviceLineCode_updated0";
 *		inputArray[0].changeSet.serviceCoverNumber = 0;
 *		inputArray[0].changeSet.servicePointDescription = "servicePointDescription_updated0";
 *		inputArray[0].changeSet.servicePointOrder = 0;
 *		inputArray[0].whereClause = "where countryCode = '0'";
 *		inputArray[0].whereClause = "where businessCode = '0'";
 *		inputArray[0].whereClause = "where contractCode = '0'";
 *		inputArray[0].whereClause = "where propertyCode = '0'";
 *		inputArray[0].whereClause = "where servicePointId = '0'";
 *		inputArray[1] = {};
 *		inputArray[1].changeSet = {};
 *		inputArray[1].changeSet.serviceLineCode = "serviceLineCode_updated1";
 *		inputArray[1].changeSet.serviceCoverNumber = 1;
 *		inputArray[1].changeSet.servicePointDescription = "servicePointDescription_updated1";
 *		inputArray[1].changeSet.servicePointOrder = 1;
 *		inputArray[1].whereClause = "where countryCode = '1'";
 *		inputArray[1].whereClause = "where businessCode = '1'";
 *		inputArray[1].whereClause = "where contractCode = '1'";
 *		inputArray[1].whereClause = "where propertyCode = '1'";
 *		inputArray[1].whereClause = "where servicePointId = '1'";
 *		inputArray[2] = {};
 *		inputArray[2].changeSet = {};
 *		inputArray[2].changeSet.serviceLineCode = "serviceLineCode_updated2";
 *		inputArray[2].changeSet.serviceCoverNumber = 2;
 *		inputArray[2].changeSet.servicePointDescription = "servicePointDescription_updated2";
 *		inputArray[2].changeSet.servicePointOrder = 2;
 *		inputArray[2].whereClause = "where countryCode = '2'";
 *		inputArray[2].whereClause = "where businessCode = '2'";
 *		inputArray[2].whereClause = "where contractCode = '2'";
 *		inputArray[2].whereClause = "where propertyCode = '2'";
 *		inputArray[2].whereClause = "where servicePointId = '2'";
 *		com.pc.portfolios.servicePoint.updateAll(inputArray,successcallback,errorcallback);
 *************************************************************************************/
com.pc.portfolios.servicePoint.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.updateAll function");
        if (!kony.sync.isSyncInitialized(errorcallback)) {
            return;
        }
        if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.updateAll", "updateAll", errorcallback)) {
            return;
        }
        var dbname = "100004898512bd8be";
        var tbname = "servicePoint";
        var isError = false;
        var errObject = null;
        if (markForUpload == false || markForUpload == "false") {
            markForUpload = "false"
        } else {
            markForUpload = "true"
        }
        if ((kony.sync.enableORMValidations)) {
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                var valuestable = v.changeSet;
                var isEmpty = true;
                for (var key in valuestable) {
                    isEmpty = false;
                    break;
                }
                if (isEmpty) {
                    errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue, kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
                    return;
                }
                var wcs = v.whereClause;
                var twcs = wcs;
                if (kony.sync.attributeValidation(valuestable, "servicePoint", errorcallback, false) === false) {
                    return;
                }
                newInputArray[i] = [];
                newInputArray[i].changeSet = valuestable;
                newInputArray[i].whereClause = wcs;
            }
            inputArray = newInputArray;
            var connection = kony.sync.getConnectionOnly(dbname, dbname);
            kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        } else {
            //copying by value
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                newInputArray[i] = kony.sync.CreateCopy(v);
            }
            inputArray = newInputArray;
            kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, errorcallback, markForUpload, com.pc.portfolios.servicePoint.getPKTable());
        }

        function transactionSuccessCallback() {
            sync.log.trace("Entering  com.pc.portfolios.servicePoint.updateAll->transactionSuccessCallback");
            if (!isError) {
                kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, transactionErrorCallback, markForUpload, com.pc.portfolios.servicePoint.getPKTable());
            }
        }

        function transactionErrorCallback() {
            if (errObject === false) {
                //Sql statement error has occcurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
                kony.sync.errorObject = null;
            } else if (errObject !== null) {
                // Referential integrity error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            } else {
                //Transaction error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
            }
        }
        //foreign key constraints validations
        function checkIntegrity(tx) {
            sync.log.trace("Entering  com.pc.portfolios.servicePoint.updateAll->checkIntegrity");
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var relationshipMap = {};
                relationshipMap = com.pc.portfolios.servicePoint.getRelationshipMap(relationshipMap, inputArray[i].changeSet);
                sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
                errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
                if (errObject === false) {
                    isError = true;
                    return;
                }
                if (errObject !== true) {
                    isError = true;
                    kony.sync.rollbackTransaction(tx);
                    return;
                }
            }
        }
    }
    /************************************************************************************
     * Deletes servicePoint using primary key from the local Database. The record will be
     * deleted from the enterprise datasource in the next Sync.
     *************************************************************************************/
com.pc.portfolios.servicePoint.prototype.deleteByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.prototype.deleteByPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.servicePoint.deleteByPK(pks, successcallback, errorcallback, this.markForUpload);
};
com.pc.portfolios.servicePoint.deleteByPK = function(pks, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.deleteByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.deleteByPK", "deleteByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var twcs = [];
    var deletedRows;
    var record = "";
    if (com.pc.portfolios.servicePoint.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);

    function servicePointTransactionCallback(tx) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.deleteByPK->servicePoint_PKPresent successcallback");
        record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (record === false) {
            isError = true;
            return;
        }
        if (null !== record) {} else {
            pkNotFound = true;
        }
        var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
        if (deletedRows === false) {
            isError = true;
        }
    }

    function servicePointErrorCallback() {
        sync.log.error("Entering com.pc.portfolios.servicePoint.deleteByPK->relationship failure callback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function servicePointSuccessCallback() {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.deleteByPK->relationship success callback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering com.pc.portfolios.servicePoint.deleteByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, servicePointTransactionCallback, servicePointSuccessCallback, servicePointErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes servicePoint(s) using where clause from the local Database. The record(s)
 * will be deleted from the enterprise datasource in the next Sync.
 * e.g. com.pc.portfolios.servicePoint.remove("where countryCode like 'A%'", successcallback,errorcallback, true);
 *************************************************************************************/
com.pc.portfolios.servicePoint.remove = function(wcs, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.remove->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.remove", "remove", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function servicePoint_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function servicePoint_removeSuccess() {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.remove->servicePoint_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, servicePoint_removeTransactioncallback, servicePoint_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Deletes servicePoint using primary key from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
com.pc.portfolios.servicePoint.prototype.removeDeviceInstanceByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.prototype.removeDeviceInstanceByPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.servicePoint.removeDeviceInstanceByPK(pks, successcallback, errorcallback);
};
com.pc.portfolios.servicePoint.removeDeviceInstanceByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.removeDeviceInstanceByPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.removeDeviceInstanceByPK", "removeDeviceInstanceByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var deletedRows;
    if (com.pc.portfolios.servicePoint.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }

    function servicePointTransactionCallback(tx) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.removeDeviceInstanceByPK -> servicePointTransactionCallback");
        var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (null !== record && false != record) {
            deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
            if (deletedRows === false) {
                isError = true;
            }
        } else {
            pkNotFound = true;
        }
    }

    function servicePointErrorCallback() {
        sync.log.error("Entering com.pc.portfolios.servicePoint.removeDeviceInstanceByPK -> servicePointErrorCallback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function servicePointSuccessCallback() {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.removeDeviceInstanceByPK -> servicePointSuccessCallback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering com.pc.portfolios.servicePoint.removeDeviceInstanceByPK -> PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, servicePointTransactionCallback, servicePointSuccessCallback, servicePointErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes servicePoint(s) using where clause from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
com.pc.portfolios.servicePoint.removeDeviceInstance = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.removeDeviceInstance->main function");
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function servicePoint_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function servicePoint_removeSuccess() {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.remove->servicePoint_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, servicePoint_removeTransactioncallback, servicePoint_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Retrieves servicePoint using primary key from the local Database. 
 *************************************************************************************/
com.pc.portfolios.servicePoint.prototype.getAllDetailsByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.prototype.getAllDetailsByPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.servicePoint.getAllDetailsByPK(pks, successcallback, errorcallback);
};
com.pc.portfolios.servicePoint.getAllDetailsByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.getAllDetailsByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.getAllDetailsByPK", "getAllDetailsByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var wcs = [];
    if (com.pc.portfolios.servicePoint.pkCheck(pks, wcs, errorcallback, "searching") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, wcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.getAllDetailsByPK-> success callback function");
        successcallback(com.pc.portfolios.servicePoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Retrieves servicePoint(s) using where clause from the local Database. 
 * e.g. com.pc.portfolios.servicePoint.find("where countryCode like 'A%'", successcallback,errorcallback);
 *************************************************************************************/
com.pc.portfolios.servicePoint.find = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.find function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.find", "find", errorcallback)) {
        return;
    }
    //wcs will be a string formed by the user.
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select * from \"" + tbname + "\" " + wcs;

    function mySuccCallback(res) {
        kony.sync.verifyAndCallClosure(successcallback, com.pc.portfolios.servicePoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Marks instance of servicePoint with given primary key for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.servicePoint.prototype.markForUploadbyPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.prototype.markForUploadbyPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.servicePoint.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.pc.portfolios.servicePoint.markForUploadbyPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.markForUploadbyPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.markForUploadbyPK", "markForUploadbyPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var isError = false;
    var recordsFound = false;
    var recordsMarkedForUpload = 0;
    var wcs = [];
    if (com.pc.portfolios.servicePoint.pkCheck(pks, wcs, errorcallback, "marking for upload by PK") === false) {
        return;
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = [];
        twcs = wcs;
        kony.table.insert(twcs, {
            key: kony.sync.historyTableChangeTypeColumn,
            value: record[kony.sync.historyTableChangeTypeColumn],
            optype: "EQ",
            comptype: "AND"
        });
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        kony.sync.qb_where(query, twcs);
        kony.table.remove(twcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function single_transaction_callback(tx) {
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query, tbname);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        var resultSet = kony.sync.executeSql(tx, sql, params);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        if (num_records > 0) {
            recordsFound = true;
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
            var changeType = record[kony.sync.mainTableChangeTypeColumn];
            if (!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith("" + changeType, "9")) {
                recordsMarkedForUpload = 1;
                if (markRecordForUpload(tx, record) === false) {
                    isError = true;
                    return;
                }
            }
        }
        var query1 = kony.sync.qb_createQuery();
        kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
        kony.sync.qb_where(query1, wcs);
        var query1_compile = kony.sync.qb_compile(query1);
        var sql1 = query1_compile[0];
        var params1 = query1_compile[1];
        var resultSet1 = kony.sync.executeSql(tx, sql1, params1);
        if (resultSet1 !== false) {
            var num_records = resultSet1.rows.length;
            for (var i = 0; i <= num_records - 1; i++) {
                var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
                if (markRecordForUploadHistory(tx, record) === false) {
                    isError = true;
                    return;
                }
                recordsFound = true;
            }
        } else {
            isError = true;
        }
    }

    function single_transaction_success_callback() {
        if (recordsFound === true) {
            kony.sync.verifyAndCallClosure(successcallback, {
                count: recordsMarkedForUpload
            });
        } else {
            kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
        }
    }

    function single_transaction_error_callback(res) {
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Marks instance(s) of servicePoint matching given where clause for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.portfolios.servicePoint.markForUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.markForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.markForUpload", "markForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var isError = false;
    var num_records_main = 0;
    wcs = kony.sync.validateWhereClause(wcs);
    if (!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
        wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    } else {
        wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + wcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = "";
        twcs = wcs;
        twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + twcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function single_transaction_callback(tx) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.markForUpload->single_transaction_callback");
        //updating main table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        num_records_main = resultSet.rows.length;
        for (var i = 0; i < num_records_main; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUpload(tx, record) === false) {
                isError = true;
                return;
            }
        }
        //updating history table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        for (var i = 0; i <= num_records - 1; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUploadHistory(tx, record) === false) {
                isError = true;
                return;
            }
        }
    }

    function single_transaction_success_callback() {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.markForUpload->single_transaction_success_callback");
        kony.sync.verifyAndCallClosure(successcallback, {
            count: num_records_main
        });
    }

    function single_transaction_error_callback() {
        sync.log.error("Entering com.pc.portfolios.servicePoint.markForUpload->single_transaction_error_callback");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Retrieves instance(s) of servicePoint pending for upload. Records are marked for
 * pending upload if they have been updated or created locally and the changes have
 * not been merged with enterprise datasource.
 *************************************************************************************/
com.pc.portfolios.servicePoint.getPendingUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.getPendingUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.getPendingUpload->successcallback function");
        kony.sync.verifyAndCallClosure(successcallback, com.pc.portfolios.servicePoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of servicePoint pending for acknowledgement. This is relevant
 * when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
 * In persistent Sync the  records in the local database are put into a pending 
 * acknowledgement state after an upload.
 *************************************************************************************/
com.pc.portfolios.servicePoint.getPendingAcknowledgement = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.getPendingAcknowledgement->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var mysql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " <> " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.getPendingAcknowledgement success callback function");
        kony.sync.verifyAndCallClosure(successcallback, com.pc.portfolios.servicePoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of servicePoint deferred for upload.
 *************************************************************************************/
com.pc.portfolios.servicePoint.getDeferredUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.getDeferredUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.getDeferredUpload->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, com.pc.portfolios.servicePoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Rollbacks all changes to servicePoint in local database to last synced state
 *************************************************************************************/
com.pc.portfolios.servicePoint.rollbackPendingLocalChanges = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.rollbackPendingLocalChanges->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.rollbackPendingLocalChanges->main function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }
};
/************************************************************************************
 * Rollbacks changes to servicePoint's record with given primary key in local 
 * database to last synced state
 *************************************************************************************/
com.pc.portfolios.servicePoint.prototype.rollbackPendingLocalChangesByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.prototype.rollbackPendingLocalChangesByPK function");
    var pks = this.getPKTable();
    com.pc.portfolios.servicePoint.rollbackPendingLocalChangesByPK(pks, successcallback, errorcallback);
};
com.pc.portfolios.servicePoint.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.rollbackPendingLocalChangesByPK->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.rollbackPendingLocalChangesByPK", "rollbackPendingLocalChangesByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var wcs = [];
    if (com.pc.portfolios.servicePoint.pkCheck(pks, wcs, errorcallback, "rollbacking") === false) {
        return;
    }
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.rollbackPendingLocalChangesByPK->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering com.pc.portfolios.servicePoint.rollbackPendingLocalChangesByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
};
/************************************************************************************
 * isRecordDeferredForUpload returns true or false depending on whether servicePoint's record  
 * with given primary key got deferred in last sync
 *************************************************************************************/
com.pc.portfolios.servicePoint.prototype.isRecordDeferredForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.portfolios.servicePoint.prototype.isRecordDeferredForUpload function");
    var pks = this.getPKTable();
    com.pc.portfolios.servicePoint.isRecordDeferredForUpload(pks, successcallback, errorcallback);
};
com.pc.portfolios.servicePoint.isRecordDeferredForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.isRecordDeferredForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.isRecordDeferredForUpload", "isRecordDeferredForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var wcs = [];
    var flag;
    if (com.pc.portfolios.servicePoint.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.isRecordDeferredForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            deferred: flag
        });
    }
};
/************************************************************************************
 * isRecordPendingForUpload returns true or false depending on whether servicePoint's record  
 * with given primary key is pending for upload
 *************************************************************************************/
com.pc.portfolios.servicePoint.prototype.isRecordPendingForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.portfolios.servicePoint.prototype.isRecordPendingForUpload function");
    var pks = this.getPKTable();
    com.pc.portfolios.servicePoint.isRecordPendingForUpload(pks, successcallback, errorcallback);
};
com.pc.portfolios.servicePoint.isRecordPendingForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.isRecordPendingForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.portfolios.servicePoint.isRecordPendingForUpload", "isRecordPendingForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    var wcs = [];
    var flag;
    if (com.pc.portfolios.servicePoint.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "NOT LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.portfolios.servicePoint.isRecordPendingForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            pending: flag
        });
    }
};
/************************************************************************************
 * Start of helper functions used internally, not to be used as ORMs
 *************************************************************************************/
//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.pc.portfolios.servicePoint.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.removeCascade function");
    var tbname = com.pc.portfolios.servicePoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);

    function removeCascadeChildren() {}
    if (isCascade) {
        if (removeCascadeChildren() === false) {
            return false;
        }
        if (kony.sync.deleteBatch(tx, tbname, wcs, isLocal, markForUpload, null) === false) {
            return false;
        }
        return true;
    } else {
        var sql = "select * from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            return false;
        }
        var num_records = resultSet.rows.length;
        if (num_records === 0) {
            return true;
        } else {
            sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable));
            errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity, kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable)));
            return false;
        }
    }
};
com.pc.portfolios.servicePoint.convertTableToObject = function(res) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.convertTableToObject function");
    objMap = [];
    if (res !== null) {
        for (var i in res) {
            var obj = new com.pc.portfolios.servicePoint();
            obj.countryCode = res[i].countryCode;
            obj.businessCode = res[i].businessCode;
            obj.contractCode = res[i].contractCode;
            obj.propertyCode = res[i].propertyCode;
            obj.serviceLineCode = res[i].serviceLineCode;
            obj.serviceCoverNumber = res[i].serviceCoverNumber;
            obj.servicePointId = res[i].servicePointId;
            obj.servicePointDescription = res[i].servicePointDescription;
            obj.servicePointOrder = res[i].servicePointOrder;
            obj.classedAsInternal = res[i].classedAsInternal;
            obj.detectorId = res[i].detectorId;
            obj.physicalId = res[i].physicalId;
            obj.physicalIdType = res[i].physicalIdType;
            obj.quantityCurrent = res[i].quantityCurrent;
            obj.quantityOutstanding = res[i].quantityOutstanding;
            obj.siteDivisionLevel = res[i].siteDivisionLevel;
            obj.siteDivisionId = res[i].siteDivisionId;
            obj.servicePointDeleted = res[i].servicePointDeleted;
            obj.updateDateTime = res[i].updateDateTime;
            obj.markForUpload = (Math.floor(res[i].konysyncchangetype / 10) == 9) ? false : true;
            objMap[i] = obj;
        }
    }
    return objMap;
};
com.pc.portfolios.servicePoint.filterAttributes = function(valuestable, insert) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.filterAttributes function");
    var attributeTable = {};
    attributeTable.countryCode = "countryCode";
    attributeTable.businessCode = "businessCode";
    attributeTable.contractCode = "contractCode";
    attributeTable.propertyCode = "propertyCode";
    attributeTable.serviceLineCode = "serviceLineCode";
    attributeTable.serviceCoverNumber = "serviceCoverNumber";
    attributeTable.servicePointId = "servicePointId";
    attributeTable.servicePointDescription = "servicePointDescription";
    attributeTable.servicePointOrder = "servicePointOrder";
    attributeTable.classedAsInternal = "classedAsInternal";
    attributeTable.detectorId = "detectorId";
    attributeTable.physicalId = "physicalId";
    attributeTable.physicalIdType = "physicalIdType";
    attributeTable.quantityCurrent = "quantityCurrent";
    attributeTable.quantityOutstanding = "quantityOutstanding";
    attributeTable.siteDivisionLevel = "siteDivisionLevel";
    attributeTable.siteDivisionId = "siteDivisionId";
    var PKTable = {};
    PKTable.countryCode = {}
    PKTable.countryCode.name = "countryCode";
    PKTable.countryCode.isAutoGen = false;
    PKTable.businessCode = {}
    PKTable.businessCode.name = "businessCode";
    PKTable.businessCode.isAutoGen = false;
    PKTable.contractCode = {}
    PKTable.contractCode.name = "contractCode";
    PKTable.contractCode.isAutoGen = false;
    PKTable.propertyCode = {}
    PKTable.propertyCode.name = "propertyCode";
    PKTable.propertyCode.isAutoGen = false;
    PKTable.servicePointId = {}
    PKTable.servicePointId.name = "servicePointId";
    PKTable.servicePointId.isAutoGen = false;
    var newvaluestable = {};
    for (var k in valuestable) {
        var v = valuestable[k];
        if (kony.sync.isNull(attributeTable[k])) {
            sync.log.warn("Ignoring the attribute " + k + " for the SyncObject servicePoint. " + k + " not defined as an attribute in SyncConfiguration.");
        } else if (!kony.sync.isNull(PKTable[k])) {
            if (insert === false) {
                sync.log.warn("Ignoring the primary key " + k + " for the SyncObject servicePoint. Primary Key should not be the part of the attributes to be updated in the local device database.");
            } else if (PKTable[k].isAutoGen) {
                sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject servicePoint. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
            } else {
                newvaluestable[k] = v;
            }
        } else {
            newvaluestable[k] = v;
        }
    }
    return newvaluestable;
};
com.pc.portfolios.servicePoint.formOrderByClause = function(orderByMap) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.formOrderByClause function");
    if (!kony.sync.isNull(orderByMap)) {
        var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
        //var filteredValuestable = com.pc.portfolios.servicePoint.filterAttributes(valuestable, true);
        return kony.sync.convertToValuesTableOrderByMap(orderByMap, valuestable);
    }
    return null;
};
com.pc.portfolios.servicePoint.prototype.getValuesTable = function(isInsert) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.prototype.getValuesTable function");
    var valuesTable = {};
    if (isInsert === true) {
        valuesTable.countryCode = this.countryCode;
    }
    if (isInsert === true) {
        valuesTable.businessCode = this.businessCode;
    }
    if (isInsert === true) {
        valuesTable.contractCode = this.contractCode;
    }
    if (isInsert === true) {
        valuesTable.propertyCode = this.propertyCode;
    }
    valuesTable.serviceLineCode = this.serviceLineCode;
    valuesTable.serviceCoverNumber = this.serviceCoverNumber;
    if (isInsert === true) {
        valuesTable.servicePointId = this.servicePointId;
    }
    valuesTable.servicePointDescription = this.servicePointDescription;
    valuesTable.servicePointOrder = this.servicePointOrder;
    valuesTable.classedAsInternal = this.classedAsInternal;
    valuesTable.detectorId = this.detectorId;
    valuesTable.physicalId = this.physicalId;
    valuesTable.physicalIdType = this.physicalIdType;
    valuesTable.quantityCurrent = this.quantityCurrent;
    valuesTable.quantityOutstanding = this.quantityOutstanding;
    valuesTable.siteDivisionLevel = this.siteDivisionLevel;
    valuesTable.siteDivisionId = this.siteDivisionId;
    return valuesTable;
};
com.pc.portfolios.servicePoint.prototype.getPKTable = function() {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.prototype.getPKTable function");
    var pkTable = {};
    pkTable.countryCode = {
        key: "countryCode",
        value: this.countryCode
    };
    pkTable.businessCode = {
        key: "businessCode",
        value: this.businessCode
    };
    pkTable.contractCode = {
        key: "contractCode",
        value: this.contractCode
    };
    pkTable.propertyCode = {
        key: "propertyCode",
        value: this.propertyCode
    };
    pkTable.servicePointId = {
        key: "servicePointId",
        value: this.servicePointId
    };
    return pkTable;
};
com.pc.portfolios.servicePoint.getPKTable = function() {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.getPKTable function");
    var pkTable = [];
    pkTable.push("countryCode");
    pkTable.push("businessCode");
    pkTable.push("contractCode");
    pkTable.push("propertyCode");
    pkTable.push("servicePointId");
    return pkTable;
};
com.pc.portfolios.servicePoint.pkCheck = function(pks, wcs, errorcallback, opName) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.pkCheck function");
    var wc = [];
    if (!kony.sync.isNull(pks.countryCode)) {
        if (!kony.sync.isNull(pks.countryCode.value)) {
            wc.key = "countryCode";
            wc.value = pks.countryCode.value;
        } else {
            wc.key = "countryCode";
            wc.value = pks.countryCode;
        }
    } else {
        sync.log.error("Primary Key countryCode not specified in " + opName + " an item in servicePoint");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("countryCode", opName, "servicePoint")));
        return;
    }
    kony.table.insert(wcs, wc);
    var wc = [];
    if (!kony.sync.isNull(pks.businessCode)) {
        if (!kony.sync.isNull(pks.businessCode.value)) {
            wc.key = "businessCode";
            wc.value = pks.businessCode.value;
        } else {
            wc.key = "businessCode";
            wc.value = pks.businessCode;
        }
    } else {
        sync.log.error("Primary Key businessCode not specified in " + opName + " an item in servicePoint");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("businessCode", opName, "servicePoint")));
        return;
    }
    kony.table.insert(wcs, wc);
    var wc = [];
    if (!kony.sync.isNull(pks.contractCode)) {
        if (!kony.sync.isNull(pks.contractCode.value)) {
            wc.key = "contractCode";
            wc.value = pks.contractCode.value;
        } else {
            wc.key = "contractCode";
            wc.value = pks.contractCode;
        }
    } else {
        sync.log.error("Primary Key contractCode not specified in " + opName + " an item in servicePoint");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("contractCode", opName, "servicePoint")));
        return;
    }
    kony.table.insert(wcs, wc);
    var wc = [];
    if (!kony.sync.isNull(pks.propertyCode)) {
        if (!kony.sync.isNull(pks.propertyCode.value)) {
            wc.key = "propertyCode";
            wc.value = pks.propertyCode.value;
        } else {
            wc.key = "propertyCode";
            wc.value = pks.propertyCode;
        }
    } else {
        sync.log.error("Primary Key propertyCode not specified in " + opName + " an item in servicePoint");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("propertyCode", opName, "servicePoint")));
        return;
    }
    kony.table.insert(wcs, wc);
    var wc = [];
    if (!kony.sync.isNull(pks.servicePointId)) {
        if (!kony.sync.isNull(pks.servicePointId.value)) {
            wc.key = "servicePointId";
            wc.value = pks.servicePointId.value;
        } else {
            wc.key = "servicePointId";
            wc.value = pks.servicePointId;
        }
    } else {
        sync.log.error("Primary Key servicePointId not specified in " + opName + " an item in servicePoint");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("servicePointId", opName, "servicePoint")));
        return;
    }
    kony.table.insert(wcs, wc);
    return true;
};
com.pc.portfolios.servicePoint.validateNull = function(valuestable, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.validateNull function");
    return true;
};
com.pc.portfolios.servicePoint.validateNullInsert = function(valuestable, errorcallback) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.validateNullInsert function");
    return true;
};
com.pc.portfolios.servicePoint.getRelationshipMap = function(relationshipMap, valuestable) {
    sync.log.trace("Entering com.pc.portfolios.servicePoint.getRelationshipMap function");
    var r1 = {};
    return relationshipMap;
};
com.pc.portfolios.servicePoint.checkPKValueTables = function(valuetables) {
    var checkPksNotNullFlag = true;
    for (var i = 0; i < valuetables.length; i++) {
        if (kony.sync.isNull(valuetables[i])) {
            checkPksNotNullFlag = false;
            break;
        }
    }
    return checkPksNotNullFlag;
};
com.pc.portfolios.servicePoint.getTableName = function() {
    return "servicePoint";
};
// **********************************End servicePoint's helper methods************************