//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:04 UTC 2016workHistory*******************
// **********************************Start workHistory's helper methods************************
if (typeof(kony) === "undefined") {
    kony = {};
}
if (typeof(kony.sync) === "undefined") {
    kony.sync = {};
}
if (typeof(kony.sync.log) === "undefined") {
    kony.sync.log = {};
}
if (typeof(sync) === "undefined") {
    sync = {};
}
if (typeof(sync.log) === "undefined") {
    sync.log = {};
}
/************************************************************************************
 * Creates new workHistory
 *************************************************************************************/
workHistory = function() {
    this.countryCode = null;
    this.businessCode = null;
    this.propertyCode = null;
    this.workNumber = null;
    this.employeeCodePrimary = null;
    this.employeeName = null;
    this.issueCodeNoSignature = null;
    this.workHistoryDeleted = null;
    this.customerApproval = null;
    this.customerApprovalName = null;
    this.workDateStartActual = null;
    this.issueTypeCode = null;
    this.issueTypeCodeNoSignature = null;
    this.updateDateTime = null;
    this.issueCode = null;
    this.customerApprovalSignature = null;
    this.markForUpload = true;
};
workHistory.prototype = {
    get countryCode() {
        return this._countryCode;
    },
    set countryCode(val) {
        this._countryCode = val;
    },
    get businessCode() {
        return this._businessCode;
    },
    set businessCode(val) {
        this._businessCode = val;
    },
    get propertyCode() {
        return this._propertyCode;
    },
    set propertyCode(val) {
        this._propertyCode = val;
    },
    get workNumber() {
        return this._workNumber;
    },
    set workNumber(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute workNumber in workHistory.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._workNumber = val;
    },
    get employeeCodePrimary() {
        return this._employeeCodePrimary;
    },
    set employeeCodePrimary(val) {
        this._employeeCodePrimary = val;
    },
    get employeeName() {
        return this._employeeName;
    },
    set employeeName(val) {
        this._employeeName = val;
    },
    get issueCodeNoSignature() {
        return this._issueCodeNoSignature;
    },
    set issueCodeNoSignature(val) {
        this._issueCodeNoSignature = val;
    },
    get workHistoryDeleted() {
        return kony.sync.getBoolean(this._workHistoryDeleted) + "";
    },
    set workHistoryDeleted(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute workHistoryDeleted in workHistory.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._workHistoryDeleted = val;
    },
    get customerApproval() {
        return kony.sync.getBoolean(this._customerApproval) + "";
    },
    set customerApproval(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute customerApproval in workHistory.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._customerApproval = val;
    },
    get customerApprovalName() {
        return this._customerApprovalName;
    },
    set customerApprovalName(val) {
        this._customerApprovalName = val;
    },
    get workDateStartActual() {
        return this._workDateStartActual;
    },
    set workDateStartActual(val) {
        this._workDateStartActual = val;
    },
    get issueTypeCode() {
        return this._issueTypeCode;
    },
    set issueTypeCode(val) {
        this._issueTypeCode = val;
    },
    get issueTypeCodeNoSignature() {
        return this._issueTypeCodeNoSignature;
    },
    set issueTypeCodeNoSignature(val) {
        this._issueTypeCodeNoSignature = val;
    },
    get updateDateTime() {
        return this._updateDateTime;
    },
    set updateDateTime(val) {
        this._updateDateTime = val;
    },
    get issueCode() {
        return this._issueCode;
    },
    set issueCode(val) {
        this._issueCode = val;
    },
    get customerApprovalSignature() {
        return this._customerApprovalSignature;
    },
    set customerApprovalSignature(val) {
        this._customerApprovalSignature = val;
    },
};
/************************************************************************************
 * Retrieves all instances of workHistory SyncObject present in local database with
 * given limit and offset where limit indicates the number of records to be retrieved
 * and offset indicates number of rows to be ignored before returning the records.
 * e.g. var orderByMap = []
 * orderByMap[0] = {};
 * orderByMap[0].key = "countryCode";
 * orderByMap[0].sortType ="desc";
 * orderByMap[1] = {};
 * orderByMap[1].key = "businessCode";
 * orderByMap[1].sortType ="asc";
 * var limit = 20;
 * var offset = 5;
 * workHistory.getAll(successcallback,errorcallback, orderByMap, limit, offset)
 *************************************************************************************/
workHistory.getAll = function(successcallback, errorcallback, orderByMap, limit, offset) {
    sync.log.trace("Entering workHistory.getAll->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    orderByMap = kony.sync.formOrderByClause("workHistory", orderByMap);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_orderBy(query, orderByMap);
    kony.sync.qb_limitOffset(query, limit, offset);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering workHistory.getAll->successcallback");
        successcallback(workHistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Returns number of workHistory present in local database.
 *************************************************************************************/
workHistory.getAllCount = function(successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.getAllCount function");
    workHistory.getCount("", successcallback, errorcallback);
};
/************************************************************************************
 * Returns number of workHistory using where clause in the local Database
 *************************************************************************************/
workHistory.getCount = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.getCount->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.getCount", "getCount", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select count(*) from \"" + tbname + "\" " + wcs;
    kony.sync.single_execute_sql(dbname, sql, null, mySuccCallback, errorcallback);

    function mySuccCallback(res) {
        sync.log.trace("Entering workHistory.getCount->successcallback");
        if (null !== res) {
            var count = null;
            count = res["count(*)"];
            kony.sync.verifyAndCallClosure(successcallback, {
                count: count
            });
        } else {
            sync.log.error("Some error occured while getting the count");
        }
    }
};
/************************************************************************************
 * Creates a new instance of workHistory in the local Database. The new record will 
 * be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
workHistory.prototype.create = function(successcallback, errorcallback) {
    sync.log.trace("Entering  workHistory.prototype.create function");
    var valuestable = this.getValuesTable(true);
    workHistory.create(valuestable, successcallback, errorcallback, this.markForUpload);
};
workHistory.create = function(valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  workHistory.create->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.create", "create", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    if (kony.sync.attributeValidation(valuestable, "workHistory", errorcallback, true) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  workHistory.create->success callback");
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
    var pks = [];
    var errMsg = "";

    function createSuccesscallback(res) {
        if (res == null || res.length == 0) {
            var relationshipMap = {};
            relationshipMap = workHistory.getRelationshipMap(relationshipMap, valuestable);
            kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
        } else {
            errMsg = "[" + errMsg + "]";
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey, kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
        }
    }
    if (kony.sync.enableORMValidations) {
        errMsg = "countryCode=" + valuestable.countryCode;
        pks["countryCode"] = {
            key: "countryCode",
            value: valuestable.countryCode
        };
        errMsg = errMsg + ", businessCode=" + valuestable.businessCode;
        pks["businessCode"] = {
            key: "businessCode",
            value: valuestable.businessCode
        };
        errMsg = errMsg + ", propertyCode=" + valuestable.propertyCode;
        pks["propertyCode"] = {
            key: "propertyCode",
            value: valuestable.propertyCode
        };
        errMsg = errMsg + ", workNumber=" + valuestable.workNumber;
        pks["workNumber"] = {
            key: "workNumber",
            value: valuestable.workNumber
        };
        workHistory.getAllDetailsByPK(pks, createSuccesscallback, errorcallback)
    } else {
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
};
/************************************************************************************
 * Creates number of new instances of workHistory in the local Database. These new 
 * records will be merged with the enterprise datasource in the next Sync. Based upon 
 * kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var valuesArray = [];
 *		valuesArray[0] = {};
 *		valuesArray[0].countryCode = "countryCode_0";
 *		valuesArray[0].businessCode = "businessCode_0";
 *		valuesArray[0].propertyCode = "propertyCode_0";
 *		valuesArray[0].workNumber = 0;
 *		valuesArray[0].employeeCodePrimary = "employeeCodePrimary_0";
 *		valuesArray[0].employeeName = "employeeName_0";
 *		valuesArray[0].issueCodeNoSignature = "issueCodeNoSignature_0";
 *		valuesArray[0].customerApproval = true;
 *		valuesArray[0].customerApprovalName = "customerApprovalName_0";
 *		valuesArray[0].workDateStartActual = "workDateStartActual_0";
 *		valuesArray[0].issueTypeCode = "issueTypeCode_0";
 *		valuesArray[0].issueTypeCodeNoSignature = "issueTypeCodeNoSignature_0";
 *		valuesArray[0].issueCode = "issueCode_0";
 *		valuesArray[0].customerApprovalSignature = "customerApprovalSignature_0";
 *		valuesArray[1] = {};
 *		valuesArray[1].countryCode = "countryCode_1";
 *		valuesArray[1].businessCode = "businessCode_1";
 *		valuesArray[1].propertyCode = "propertyCode_1";
 *		valuesArray[1].workNumber = 1;
 *		valuesArray[1].employeeCodePrimary = "employeeCodePrimary_1";
 *		valuesArray[1].employeeName = "employeeName_1";
 *		valuesArray[1].issueCodeNoSignature = "issueCodeNoSignature_1";
 *		valuesArray[1].customerApproval = true;
 *		valuesArray[1].customerApprovalName = "customerApprovalName_1";
 *		valuesArray[1].workDateStartActual = "workDateStartActual_1";
 *		valuesArray[1].issueTypeCode = "issueTypeCode_1";
 *		valuesArray[1].issueTypeCodeNoSignature = "issueTypeCodeNoSignature_1";
 *		valuesArray[1].issueCode = "issueCode_1";
 *		valuesArray[1].customerApprovalSignature = "customerApprovalSignature_1";
 *		valuesArray[2] = {};
 *		valuesArray[2].countryCode = "countryCode_2";
 *		valuesArray[2].businessCode = "businessCode_2";
 *		valuesArray[2].propertyCode = "propertyCode_2";
 *		valuesArray[2].workNumber = 2;
 *		valuesArray[2].employeeCodePrimary = "employeeCodePrimary_2";
 *		valuesArray[2].employeeName = "employeeName_2";
 *		valuesArray[2].issueCodeNoSignature = "issueCodeNoSignature_2";
 *		valuesArray[2].customerApproval = true;
 *		valuesArray[2].customerApprovalName = "customerApprovalName_2";
 *		valuesArray[2].workDateStartActual = "workDateStartActual_2";
 *		valuesArray[2].issueTypeCode = "issueTypeCode_2";
 *		valuesArray[2].issueTypeCodeNoSignature = "issueTypeCodeNoSignature_2";
 *		valuesArray[2].issueCode = "issueCode_2";
 *		valuesArray[2].customerApprovalSignature = "customerApprovalSignature_2";
 *		workHistory.createAll(valuesArray, successcallback, errorcallback, true);
 *************************************************************************************/
workHistory.createAll = function(valuesArray, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering workHistory.createAll function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.createAll", "createAll", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var isProperData = true;
    var arrayLen = 0;
    var errorInfo = [];
    var arrayLength = valuesArray.length;
    var errObject = null;
    var isReferentialIntegrityFailure = false;
    var errMsg = null;
    if (kony.sync.enableORMValidations) {
        var newValuesArray = [];
        //column level validations
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var valuestable = valuesArray[i];
            if (kony.sync.attributeValidation(valuestable, "workHistory", errorcallback, true) === false) {
                return;
            }
            newValuesArray[i] = valuestable;
        }
        valuesArray = newValuesArray;
        var isDuplicateKey = false;
        //checking for duplicate records
        var connection = kony.sync.getConnectionOnly(dbname, dbname);
        kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
        var isError = false;

        function checkDuplicatePkCallback(tx) {
            arrayLength = valuesArray.length;
            for (var i = 0; valuesArray != null && i < arrayLength; i++) {
                var valuestable = valuesArray[i];
                var pks = [];
                errMsg = "countryCode=" + valuestable.countryCode;
                pks["countryCode"] = {
                    key: "countryCode",
                    value: valuestable.countryCode
                };
                errMsg = errMsg + ", businessCode=" + valuestable.businessCode;
                pks["businessCode"] = {
                    key: "businessCode",
                    value: valuestable.businessCode
                };
                errMsg = errMsg + ", propertyCode=" + valuestable.propertyCode;
                pks["propertyCode"] = {
                    key: "propertyCode",
                    value: valuestable.propertyCode
                };
                errMsg = errMsg + ", workNumber=" + valuestable.workNumber;
                pks["workNumber"] = {
                    key: "workNumber",
                    value: valuestable.workNumber
                };
                var wcs = [];
                if (workHistory.pkCheck(pks, wcs, errorcallback, "searching") === false) {
                    isError = true;
                    return;
                }
                var query = kony.sync.qb_createQuery();
                kony.sync.qb_select(query, null);
                kony.sync.qb_from(query, tbname);
                kony.sync.qb_where(query, wcs);
                var query_compile = kony.sync.qb_compile(query);
                var sql = query_compile[0];
                var params = query_compile[1];
                var resultset = kony.sync.executeSql(tx, sql, params);
                if (resultset === false) {
                    isError = true;
                    return;
                }
                if (resultset.rows.length != 0) {
                    isError = true;
                    errMsg = "[" + errMsg + "]";
                    isDuplicateKey = true;
                    return;
                }
            }
            if (!isError) {
                checkIntegrity(tx);
            }
        }
    } else {
        //copying by value
        var newValuesArray = [];
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
        }
        valuesArray = newValuesArray;
        kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
    }

    function transactionErrorCallback() {
        if (isError == true) {
            //Statement error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
        } else {
            //Transaction error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
        }
    }

    function transactionSuccessCallback() {
        sync.log.trace("Entering  workHistory.createAll->transactionSuccessCallback");
        if (!isError) {
            kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
        } else {
            if (isReferentialIntegrityFailure) {
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            }
            if (isDuplicateKey) {
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey, kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
            }
        }
    }
    //foreign key constraints validations
    function checkIntegrity(tx) {
        sync.log.trace("Entering  workHistory.createAll->checkIntegrity");
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var relationshipMap = {};
            relationshipMap = workHistory.getRelationshipMap(relationshipMap, valuesArray[i]);
            errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
            if (errObject === false) {
                isError = true;
                return;
            }
            if (errObject !== true) {
                isError = true;
                isReferentialIntegrityFailure = true;
                return;
            }
        }
    }
};
/************************************************************************************
 * Updates workHistory using primary key in the local Database. The update will be
 * merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
workHistory.prototype.updateByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering  workHistory.prototype.updateByPK function");
    var pks = this.getPKTable();
    var valuestable = this.getValuesTable(false);
    workHistory.updateByPK(pks, valuestable, successcallback, errorcallback, this.markForUpload);
};
workHistory.updateByPK = function(pks, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  workHistory.updateByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.updateByPK", "updateByPk", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    if (workHistory.pkCheck(pks, wcs, errorcallback, "updating") === false) {
        return;
    }
    if (kony.sync.attributeValidation(valuestable, "workHistory", errorcallback, false) === false) {
        return;
    }
    var relationshipMap = {};
    relationshipMap = workHistory.getRelationshipMap(relationshipMap, valuestable);
    kony.sync.updateByPK(tbname, dbname, relationshipMap, pks, valuestable, successcallback, errorcallback, markForUpload, wcs);
};
/************************************************************************************
 * Updates workHistory(s) using where clause in the local Database. The update(s)
 * will be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
workHistory.update = function(wcs, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering workHistory.update function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.update", "update", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    if (kony.sync.attributeValidation(valuestable, "workHistory", errorcallback, false) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  workHistory.update-> success callback of Integrity Check");
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, workHistory.getPKTable());
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = workHistory.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, workHistory.getPKTable());
    }
};
/************************************************************************************
 * Updates workHistory(s) satisfying one or more where clauses in the local Database. 
 * The update(s) will be merged with the enterprise datasource in the next Sync.
 * Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var inputArray = [];
 *		inputArray[0] = {};
 *		inputArray[0].changeSet = {};
 *		inputArray[0].changeSet.employeeCodePrimary = "employeeCodePrimary_updated0";
 *		inputArray[0].changeSet.employeeName = "employeeName_updated0";
 *		inputArray[0].changeSet.issueCodeNoSignature = "issueCodeNoSignature_updated0";
 *		inputArray[0].changeSet.customerApproval = true;
 *		inputArray[0].whereClause = "where countryCode = '0'";
 *		inputArray[0].whereClause = "where businessCode = '0'";
 *		inputArray[0].whereClause = "where propertyCode = '0'";
 *		inputArray[0].whereClause = "where workNumber = 0";
 *		inputArray[1] = {};
 *		inputArray[1].changeSet = {};
 *		inputArray[1].changeSet.employeeCodePrimary = "employeeCodePrimary_updated1";
 *		inputArray[1].changeSet.employeeName = "employeeName_updated1";
 *		inputArray[1].changeSet.issueCodeNoSignature = "issueCodeNoSignature_updated1";
 *		inputArray[1].changeSet.customerApproval = true;
 *		inputArray[1].whereClause = "where countryCode = '1'";
 *		inputArray[1].whereClause = "where businessCode = '1'";
 *		inputArray[1].whereClause = "where propertyCode = '1'";
 *		inputArray[1].whereClause = "where workNumber = 1";
 *		inputArray[2] = {};
 *		inputArray[2].changeSet = {};
 *		inputArray[2].changeSet.employeeCodePrimary = "employeeCodePrimary_updated2";
 *		inputArray[2].changeSet.employeeName = "employeeName_updated2";
 *		inputArray[2].changeSet.issueCodeNoSignature = "issueCodeNoSignature_updated2";
 *		inputArray[2].changeSet.customerApproval = true;
 *		inputArray[2].whereClause = "where countryCode = '2'";
 *		inputArray[2].whereClause = "where businessCode = '2'";
 *		inputArray[2].whereClause = "where propertyCode = '2'";
 *		inputArray[2].whereClause = "where workNumber = 2";
 *		workHistory.updateAll(inputArray,successcallback,errorcallback);
 *************************************************************************************/
workHistory.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
        sync.log.trace("Entering workHistory.updateAll function");
        if (!kony.sync.isSyncInitialized(errorcallback)) {
            return;
        }
        if (!kony.sync.validateInput(arguments, "workHistory.updateAll", "updateAll", errorcallback)) {
            return;
        }
        var dbname = "100004898512bd8be";
        var tbname = "workHistory";
        var isError = false;
        var errObject = null;
        if (markForUpload == false || markForUpload == "false") {
            markForUpload = "false"
        } else {
            markForUpload = "true"
        }
        if ((kony.sync.enableORMValidations)) {
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                var valuestable = v.changeSet;
                var isEmpty = true;
                for (var key in valuestable) {
                    isEmpty = false;
                    break;
                }
                if (isEmpty) {
                    errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue, kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
                    return;
                }
                var wcs = v.whereClause;
                var twcs = wcs;
                if (kony.sync.attributeValidation(valuestable, "workHistory", errorcallback, false) === false) {
                    return;
                }
                newInputArray[i] = [];
                newInputArray[i].changeSet = valuestable;
                newInputArray[i].whereClause = wcs;
            }
            inputArray = newInputArray;
            var connection = kony.sync.getConnectionOnly(dbname, dbname);
            kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        } else {
            //copying by value
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                newInputArray[i] = kony.sync.CreateCopy(v);
            }
            inputArray = newInputArray;
            kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, errorcallback, markForUpload, workHistory.getPKTable());
        }

        function transactionSuccessCallback() {
            sync.log.trace("Entering  workHistory.updateAll->transactionSuccessCallback");
            if (!isError) {
                kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, transactionErrorCallback, markForUpload, workHistory.getPKTable());
            }
        }

        function transactionErrorCallback() {
            if (errObject === false) {
                //Sql statement error has occcurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
                kony.sync.errorObject = null;
            } else if (errObject !== null) {
                // Referential integrity error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            } else {
                //Transaction error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
            }
        }
        //foreign key constraints validations
        function checkIntegrity(tx) {
            sync.log.trace("Entering  workHistory.updateAll->checkIntegrity");
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var relationshipMap = {};
                relationshipMap = workHistory.getRelationshipMap(relationshipMap, inputArray[i].changeSet);
                sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
                errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
                if (errObject === false) {
                    isError = true;
                    return;
                }
                if (errObject !== true) {
                    isError = true;
                    kony.sync.rollbackTransaction(tx);
                    return;
                }
            }
        }
    }
    /************************************************************************************
     * Deletes workHistory using primary key from the local Database. The record will be
     * deleted from the enterprise datasource in the next Sync.
     *************************************************************************************/
workHistory.prototype.deleteByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.prototype.deleteByPK function");
    var pks = this.getPKTable();
    workHistory.deleteByPK(pks, successcallback, errorcallback, this.markForUpload);
};
workHistory.deleteByPK = function(pks, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering workHistory.deleteByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.deleteByPK", "deleteByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var twcs = [];
    var deletedRows;
    var record = "";
    if (workHistory.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);

    function workHistoryTransactionCallback(tx) {
        sync.log.trace("Entering workHistory.deleteByPK->workHistory_PKPresent successcallback");
        record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (record === false) {
            isError = true;
            return;
        }
        if (null !== record) {} else {
            pkNotFound = true;
        }
        var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
        if (deletedRows === false) {
            isError = true;
        }
    }

    function workHistoryErrorCallback() {
        sync.log.error("Entering workHistory.deleteByPK->relationship failure callback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function workHistorySuccessCallback() {
        sync.log.trace("Entering workHistory.deleteByPK->relationship success callback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering workHistory.deleteByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, workHistoryTransactionCallback, workHistorySuccessCallback, workHistoryErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes workHistory(s) using where clause from the local Database. The record(s)
 * will be deleted from the enterprise datasource in the next Sync.
 * e.g. workHistory.remove("where countryCode like 'A%'", successcallback,errorcallback, true);
 *************************************************************************************/
workHistory.remove = function(wcs, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering workHistory.remove->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.remove", "remove", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function workHistory_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function workHistory_removeSuccess() {
        sync.log.trace("Entering workHistory.remove->workHistory_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering workHistory.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering workHistory.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, workHistory_removeTransactioncallback, workHistory_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Deletes workHistory using primary key from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
workHistory.prototype.removeDeviceInstanceByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.prototype.removeDeviceInstanceByPK function");
    var pks = this.getPKTable();
    workHistory.removeDeviceInstanceByPK(pks, successcallback, errorcallback);
};
workHistory.removeDeviceInstanceByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.removeDeviceInstanceByPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.removeDeviceInstanceByPK", "removeDeviceInstanceByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var deletedRows;
    if (workHistory.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }

    function workHistoryTransactionCallback(tx) {
        sync.log.trace("Entering workHistory.removeDeviceInstanceByPK -> workHistoryTransactionCallback");
        var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (null !== record && false != record) {
            deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
            if (deletedRows === false) {
                isError = true;
            }
        } else {
            pkNotFound = true;
        }
    }

    function workHistoryErrorCallback() {
        sync.log.error("Entering workHistory.removeDeviceInstanceByPK -> workHistoryErrorCallback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function workHistorySuccessCallback() {
        sync.log.trace("Entering workHistory.removeDeviceInstanceByPK -> workHistorySuccessCallback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering workHistory.removeDeviceInstanceByPK -> PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, workHistoryTransactionCallback, workHistorySuccessCallback, workHistoryErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes workHistory(s) using where clause from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
workHistory.removeDeviceInstance = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.removeDeviceInstance->main function");
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function workHistory_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function workHistory_removeSuccess() {
        sync.log.trace("Entering workHistory.remove->workHistory_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering workHistory.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering workHistory.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, workHistory_removeTransactioncallback, workHistory_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Retrieves workHistory using primary key from the local Database. 
 *************************************************************************************/
workHistory.prototype.getAllDetailsByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.prototype.getAllDetailsByPK function");
    var pks = this.getPKTable();
    workHistory.getAllDetailsByPK(pks, successcallback, errorcallback);
};
workHistory.getAllDetailsByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.getAllDetailsByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.getAllDetailsByPK", "getAllDetailsByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var wcs = [];
    if (workHistory.pkCheck(pks, wcs, errorcallback, "searching") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, wcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering workHistory.getAllDetailsByPK-> success callback function");
        successcallback(workHistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Retrieves workHistory(s) using where clause from the local Database. 
 * e.g. workHistory.find("where countryCode like 'A%'", successcallback,errorcallback);
 *************************************************************************************/
workHistory.find = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.find function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.find", "find", errorcallback)) {
        return;
    }
    //wcs will be a string formed by the user.
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select * from \"" + tbname + "\" " + wcs;

    function mySuccCallback(res) {
        kony.sync.verifyAndCallClosure(successcallback, workHistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Marks instance of workHistory with given primary key for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
workHistory.prototype.markForUploadbyPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.prototype.markForUploadbyPK function");
    var pks = this.getPKTable();
    workHistory.markForUploadbyPK(pks, successcallback, errorcallback);
};
workHistory.markForUploadbyPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.markForUploadbyPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.markForUploadbyPK", "markForUploadbyPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var isError = false;
    var recordsFound = false;
    var recordsMarkedForUpload = 0;
    var wcs = [];
    if (workHistory.pkCheck(pks, wcs, errorcallback, "marking for upload by PK") === false) {
        return;
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = [];
        twcs = wcs;
        kony.table.insert(twcs, {
            key: kony.sync.historyTableChangeTypeColumn,
            value: record[kony.sync.historyTableChangeTypeColumn],
            optype: "EQ",
            comptype: "AND"
        });
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        kony.sync.qb_where(query, twcs);
        kony.table.remove(twcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function single_transaction_callback(tx) {
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query, tbname);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        var resultSet = kony.sync.executeSql(tx, sql, params);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        if (num_records > 0) {
            recordsFound = true;
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
            var changeType = record[kony.sync.mainTableChangeTypeColumn];
            if (!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith("" + changeType, "9")) {
                recordsMarkedForUpload = 1;
                if (markRecordForUpload(tx, record) === false) {
                    isError = true;
                    return;
                }
            }
        }
        var query1 = kony.sync.qb_createQuery();
        kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
        kony.sync.qb_where(query1, wcs);
        var query1_compile = kony.sync.qb_compile(query1);
        var sql1 = query1_compile[0];
        var params1 = query1_compile[1];
        var resultSet1 = kony.sync.executeSql(tx, sql1, params1);
        if (resultSet1 !== false) {
            var num_records = resultSet1.rows.length;
            for (var i = 0; i <= num_records - 1; i++) {
                var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
                if (markRecordForUploadHistory(tx, record) === false) {
                    isError = true;
                    return;
                }
                recordsFound = true;
            }
        } else {
            isError = true;
        }
    }

    function single_transaction_success_callback() {
        if (recordsFound === true) {
            kony.sync.verifyAndCallClosure(successcallback, {
                count: recordsMarkedForUpload
            });
        } else {
            kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
        }
    }

    function single_transaction_error_callback(res) {
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Marks instance(s) of workHistory matching given where clause for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
workHistory.markForUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.markForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.markForUpload", "markForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var isError = false;
    var num_records_main = 0;
    wcs = kony.sync.validateWhereClause(wcs);
    if (!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
        wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    } else {
        wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + wcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = "";
        twcs = wcs;
        twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + twcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function single_transaction_callback(tx) {
        sync.log.trace("Entering workHistory.markForUpload->single_transaction_callback");
        //updating main table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        num_records_main = resultSet.rows.length;
        for (var i = 0; i < num_records_main; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUpload(tx, record) === false) {
                isError = true;
                return;
            }
        }
        //updating history table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        for (var i = 0; i <= num_records - 1; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUploadHistory(tx, record) === false) {
                isError = true;
                return;
            }
        }
    }

    function single_transaction_success_callback() {
        sync.log.trace("Entering workHistory.markForUpload->single_transaction_success_callback");
        kony.sync.verifyAndCallClosure(successcallback, {
            count: num_records_main
        });
    }

    function single_transaction_error_callback() {
        sync.log.error("Entering workHistory.markForUpload->single_transaction_error_callback");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Retrieves instance(s) of workHistory pending for upload. Records are marked for
 * pending upload if they have been updated or created locally and the changes have
 * not been merged with enterprise datasource.
 *************************************************************************************/
workHistory.getPendingUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.getPendingUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering workHistory.getPendingUpload->successcallback function");
        kony.sync.verifyAndCallClosure(successcallback, workHistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of workHistory pending for acknowledgement. This is relevant
 * when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
 * In persistent Sync the  records in the local database are put into a pending 
 * acknowledgement state after an upload.
 *************************************************************************************/
workHistory.getPendingAcknowledgement = function(successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.getPendingAcknowledgement->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var mysql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " <> " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering workHistory.getPendingAcknowledgement success callback function");
        kony.sync.verifyAndCallClosure(successcallback, workHistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of workHistory deferred for upload.
 *************************************************************************************/
workHistory.getDeferredUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.getDeferredUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering workHistory.getDeferredUpload->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, workHistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Rollbacks all changes to workHistory in local database to last synced state
 *************************************************************************************/
workHistory.rollbackPendingLocalChanges = function(successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.rollbackPendingLocalChanges->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering workHistory.rollbackPendingLocalChanges->main function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }
};
/************************************************************************************
 * Rollbacks changes to workHistory's record with given primary key in local 
 * database to last synced state
 *************************************************************************************/
workHistory.prototype.rollbackPendingLocalChangesByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.prototype.rollbackPendingLocalChangesByPK function");
    var pks = this.getPKTable();
    workHistory.rollbackPendingLocalChangesByPK(pks, successcallback, errorcallback);
};
workHistory.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.rollbackPendingLocalChangesByPK->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.rollbackPendingLocalChangesByPK", "rollbackPendingLocalChangesByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var wcs = [];
    if (workHistory.pkCheck(pks, wcs, errorcallback, "rollbacking") === false) {
        return;
    }
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering workHistory.rollbackPendingLocalChangesByPK->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering workHistory.rollbackPendingLocalChangesByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
};
/************************************************************************************
 * isRecordDeferredForUpload returns true or false depending on whether workHistory's record  
 * with given primary key got deferred in last sync
 *************************************************************************************/
workHistory.prototype.isRecordDeferredForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  workHistory.prototype.isRecordDeferredForUpload function");
    var pks = this.getPKTable();
    workHistory.isRecordDeferredForUpload(pks, successcallback, errorcallback);
};
workHistory.isRecordDeferredForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.isRecordDeferredForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.isRecordDeferredForUpload", "isRecordDeferredForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var wcs = [];
    var flag;
    if (workHistory.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering workHistory.isRecordDeferredForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            deferred: flag
        });
    }
};
/************************************************************************************
 * isRecordPendingForUpload returns true or false depending on whether workHistory's record  
 * with given primary key is pending for upload
 *************************************************************************************/
workHistory.prototype.isRecordPendingForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  workHistory.prototype.isRecordPendingForUpload function");
    var pks = this.getPKTable();
    workHistory.isRecordPendingForUpload(pks, successcallback, errorcallback);
};
workHistory.isRecordPendingForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering workHistory.isRecordPendingForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "workHistory.isRecordPendingForUpload", "isRecordPendingForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = workHistory.getTableName();
    var wcs = [];
    var flag;
    if (workHistory.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "NOT LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering workHistory.isRecordPendingForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            pending: flag
        });
    }
};
/************************************************************************************
 * Start of helper functions used internally, not to be used as ORMs
 *************************************************************************************/
//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
workHistory.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal) {
    sync.log.trace("Entering workHistory.removeCascade function");
    var tbname = workHistory.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);

    function removeCascadeChildren() {}
    if (isCascade) {
        if (removeCascadeChildren() === false) {
            return false;
        }
        if (kony.sync.deleteBatch(tx, tbname, wcs, isLocal, markForUpload, null) === false) {
            return false;
        }
        return true;
    } else {
        var sql = "select * from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            return false;
        }
        var num_records = resultSet.rows.length;
        if (num_records === 0) {
            return true;
        } else {
            sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable));
            errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity, kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable)));
            return false;
        }
    }
};
workHistory.convertTableToObject = function(res) {
    sync.log.trace("Entering workHistory.convertTableToObject function");
    objMap = [];
    if (res !== null) {
        for (var i in res) {
            var obj = new workHistory();
            obj.countryCode = res[i].countryCode;
            obj.businessCode = res[i].businessCode;
            obj.propertyCode = res[i].propertyCode;
            obj.workNumber = res[i].workNumber;
            obj.employeeCodePrimary = res[i].employeeCodePrimary;
            obj.employeeName = res[i].employeeName;
            obj.issueCodeNoSignature = res[i].issueCodeNoSignature;
            obj.workHistoryDeleted = res[i].workHistoryDeleted;
            obj.customerApproval = res[i].customerApproval;
            obj.customerApprovalName = res[i].customerApprovalName;
            obj.workDateStartActual = res[i].workDateStartActual;
            obj.issueTypeCode = res[i].issueTypeCode;
            obj.issueTypeCodeNoSignature = res[i].issueTypeCodeNoSignature;
            obj.updateDateTime = res[i].updateDateTime;
            obj.issueCode = res[i].issueCode;
            obj.customerApprovalSignature = res[i].customerApprovalSignature;
            obj.markForUpload = (Math.floor(res[i].konysyncchangetype / 10) == 9) ? false : true;
            objMap[i] = obj;
        }
    }
    return objMap;
};
workHistory.filterAttributes = function(valuestable, insert) {
    sync.log.trace("Entering workHistory.filterAttributes function");
    var attributeTable = {};
    attributeTable.countryCode = "countryCode";
    attributeTable.businessCode = "businessCode";
    attributeTable.propertyCode = "propertyCode";
    attributeTable.workNumber = "workNumber";
    attributeTable.employeeCodePrimary = "employeeCodePrimary";
    attributeTable.employeeName = "employeeName";
    attributeTable.issueCodeNoSignature = "issueCodeNoSignature";
    attributeTable.customerApproval = "customerApproval";
    attributeTable.customerApprovalName = "customerApprovalName";
    attributeTable.workDateStartActual = "workDateStartActual";
    attributeTable.issueTypeCode = "issueTypeCode";
    attributeTable.issueTypeCodeNoSignature = "issueTypeCodeNoSignature";
    attributeTable.issueCode = "issueCode";
    attributeTable.customerApprovalSignature = "customerApprovalSignature";
    var PKTable = {};
    PKTable.countryCode = {}
    PKTable.countryCode.name = "countryCode";
    PKTable.countryCode.isAutoGen = false;
    PKTable.businessCode = {}
    PKTable.businessCode.name = "businessCode";
    PKTable.businessCode.isAutoGen = false;
    PKTable.propertyCode = {}
    PKTable.propertyCode.name = "propertyCode";
    PKTable.propertyCode.isAutoGen = false;
    PKTable.workNumber = {}
    PKTable.workNumber.name = "workNumber";
    PKTable.workNumber.isAutoGen = false;
    var newvaluestable = {};
    for (var k in valuestable) {
        var v = valuestable[k];
        if (kony.sync.isNull(attributeTable[k])) {
            sync.log.warn("Ignoring the attribute " + k + " for the SyncObject workHistory. " + k + " not defined as an attribute in SyncConfiguration.");
        } else if (!kony.sync.isNull(PKTable[k])) {
            if (insert === false) {
                sync.log.warn("Ignoring the primary key " + k + " for the SyncObject workHistory. Primary Key should not be the part of the attributes to be updated in the local device database.");
            } else if (PKTable[k].isAutoGen) {
                sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject workHistory. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
            } else {
                newvaluestable[k] = v;
            }
        } else {
            newvaluestable[k] = v;
        }
    }
    return newvaluestable;
};
workHistory.formOrderByClause = function(orderByMap) {
    sync.log.trace("Entering workHistory.formOrderByClause function");
    if (!kony.sync.isNull(orderByMap)) {
        var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
        //var filteredValuestable = workHistory.filterAttributes(valuestable, true);
        return kony.sync.convertToValuesTableOrderByMap(orderByMap, valuestable);
    }
    return null;
};
workHistory.prototype.getValuesTable = function(isInsert) {
    sync.log.trace("Entering workHistory.prototype.getValuesTable function");
    var valuesTable = {};
    if (isInsert === true) {
        valuesTable.countryCode = this.countryCode;
    }
    if (isInsert === true) {
        valuesTable.businessCode = this.businessCode;
    }
    if (isInsert === true) {
        valuesTable.propertyCode = this.propertyCode;
    }
    if (isInsert === true) {
        valuesTable.workNumber = this.workNumber;
    }
    valuesTable.employeeCodePrimary = this.employeeCodePrimary;
    valuesTable.employeeName = this.employeeName;
    valuesTable.issueCodeNoSignature = this.issueCodeNoSignature;
    valuesTable.customerApproval = this.customerApproval;
    valuesTable.customerApprovalName = this.customerApprovalName;
    valuesTable.workDateStartActual = this.workDateStartActual;
    valuesTable.issueTypeCode = this.issueTypeCode;
    valuesTable.issueTypeCodeNoSignature = this.issueTypeCodeNoSignature;
    valuesTable.issueCode = this.issueCode;
    valuesTable.customerApprovalSignature = this.customerApprovalSignature;
    return valuesTable;
};
workHistory.prototype.getPKTable = function() {
    sync.log.trace("Entering workHistory.prototype.getPKTable function");
    var pkTable = {};
    pkTable.countryCode = {
        key: "countryCode",
        value: this.countryCode
    };
    pkTable.businessCode = {
        key: "businessCode",
        value: this.businessCode
    };
    pkTable.propertyCode = {
        key: "propertyCode",
        value: this.propertyCode
    };
    pkTable.workNumber = {
        key: "workNumber",
        value: this.workNumber
    };
    return pkTable;
};
workHistory.getPKTable = function() {
    sync.log.trace("Entering workHistory.getPKTable function");
    var pkTable = [];
    pkTable.push("countryCode");
    pkTable.push("businessCode");
    pkTable.push("propertyCode");
    pkTable.push("workNumber");
    return pkTable;
};
workHistory.pkCheck = function(pks, wcs, errorcallback, opName) {
    sync.log.trace("Entering workHistory.pkCheck function");
    var wc = [];
    if (!kony.sync.isNull(pks.countryCode)) {
        if (!kony.sync.isNull(pks.countryCode.value)) {
            wc.key = "countryCode";
            wc.value = pks.countryCode.value;
        } else {
            wc.key = "countryCode";
            wc.value = pks.countryCode;
        }
    } else {
        sync.log.error("Primary Key countryCode not specified in " + opName + " an item in workHistory");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("countryCode", opName, "workHistory")));
        return;
    }
    kony.table.insert(wcs, wc);
    var wc = [];
    if (!kony.sync.isNull(pks.businessCode)) {
        if (!kony.sync.isNull(pks.businessCode.value)) {
            wc.key = "businessCode";
            wc.value = pks.businessCode.value;
        } else {
            wc.key = "businessCode";
            wc.value = pks.businessCode;
        }
    } else {
        sync.log.error("Primary Key businessCode not specified in " + opName + " an item in workHistory");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("businessCode", opName, "workHistory")));
        return;
    }
    kony.table.insert(wcs, wc);
    var wc = [];
    if (!kony.sync.isNull(pks.propertyCode)) {
        if (!kony.sync.isNull(pks.propertyCode.value)) {
            wc.key = "propertyCode";
            wc.value = pks.propertyCode.value;
        } else {
            wc.key = "propertyCode";
            wc.value = pks.propertyCode;
        }
    } else {
        sync.log.error("Primary Key propertyCode not specified in " + opName + " an item in workHistory");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("propertyCode", opName, "workHistory")));
        return;
    }
    kony.table.insert(wcs, wc);
    var wc = [];
    if (!kony.sync.isNull(pks.workNumber)) {
        if (!kony.sync.isNull(pks.workNumber.value)) {
            wc.key = "workNumber";
            wc.value = pks.workNumber.value;
        } else {
            wc.key = "workNumber";
            wc.value = pks.workNumber;
        }
    } else {
        sync.log.error("Primary Key workNumber not specified in " + opName + " an item in workHistory");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("workNumber", opName, "workHistory")));
        return;
    }
    kony.table.insert(wcs, wc);
    return true;
};
workHistory.validateNull = function(valuestable, errorcallback) {
    sync.log.trace("Entering workHistory.validateNull function");
    return true;
};
workHistory.validateNullInsert = function(valuestable, errorcallback) {
    sync.log.trace("Entering workHistory.validateNullInsert function");
    return true;
};
workHistory.getRelationshipMap = function(relationshipMap, valuestable) {
    sync.log.trace("Entering workHistory.getRelationshipMap function");
    var r1 = {};
    return relationshipMap;
};
workHistory.checkPKValueTables = function(valuetables) {
    var checkPksNotNullFlag = true;
    for (var i = 0; i < valuetables.length; i++) {
        if (kony.sync.isNull(valuetables[i])) {
            checkPksNotNullFlag = false;
            break;
        }
    }
    return checkPksNotNullFlag;
};
workHistory.getTableName = function() {
    return "workHistory";
};
// **********************************End workHistory's helper methods************************