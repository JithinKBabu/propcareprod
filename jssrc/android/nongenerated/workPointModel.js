//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:04 UTC 2016workPoint*******************
// **********************************Start workPoint's helper methods************************
if (typeof(kony) === "undefined") {
    kony = {};
}
if (typeof(kony.sync) === "undefined") {
    kony.sync = {};
}
if (typeof(kony.sync.log) === "undefined") {
    kony.sync.log = {};
}
if (typeof(sync) === "undefined") {
    sync = {};
}
if (typeof(sync.log) === "undefined") {
    sync.log = {};
}
if (typeof(com) === "undefined") {
    com = {};
}
if (typeof(com.pc) === "undefined") {
    com.pc = {};
}
if (typeof(com.pc.workloads) === "undefined") {
    com.pc.workloads = {};
}
/************************************************************************************
 * Creates new workPoint
 *************************************************************************************/
com.pc.workloads.workPoint = function() {
    this.workPointId = null;
    this.activationMethod = null;
    this.contractCode = null;
    this.propertyCode = null;
    this.serviceLineCode = null;
    this.serviceCoverNumber = null;
    this.employeeCodeActual = null;
    this.employeeCodePlanned = null;
    this.issueCode = null;
    this.issueTypeCode = null;
    this.servicePointId = null;
    this.serviceQuantityActual = null;
    this.serviceQuantityPlanned = null;
    this.workAttendanceId = null;
    this.workOrderId = null;
    this.workId = null;
    this.workPointActivationDateTime = null;
    this.workPointEffort = null;
    this.workPointDeleted = null;
    this.updateDateTime = null;
    this.markForUpload = true;
};
com.pc.workloads.workPoint.prototype = {
    get workPointId() {
        return this._workPointId;
    },
    set workPointId(val) {
        this._workPointId = val;
    },
    get activationMethod() {
        return this._activationMethod;
    },
    set activationMethod(val) {
        this._activationMethod = val;
    },
    get contractCode() {
        return this._contractCode;
    },
    set contractCode(val) {
        this._contractCode = val;
    },
    get propertyCode() {
        return this._propertyCode;
    },
    set propertyCode(val) {
        this._propertyCode = val;
    },
    get serviceLineCode() {
        return this._serviceLineCode;
    },
    set serviceLineCode(val) {
        this._serviceLineCode = val;
    },
    get serviceCoverNumber() {
        return this._serviceCoverNumber;
    },
    set serviceCoverNumber(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute serviceCoverNumber in workPoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._serviceCoverNumber = val;
    },
    get employeeCodeActual() {
        return this._employeeCodeActual;
    },
    set employeeCodeActual(val) {
        this._employeeCodeActual = val;
    },
    get employeeCodePlanned() {
        return this._employeeCodePlanned;
    },
    set employeeCodePlanned(val) {
        this._employeeCodePlanned = val;
    },
    get issueCode() {
        return this._issueCode;
    },
    set issueCode(val) {
        this._issueCode = val;
    },
    get issueTypeCode() {
        return this._issueTypeCode;
    },
    set issueTypeCode(val) {
        this._issueTypeCode = val;
    },
    get servicePointId() {
        return this._servicePointId;
    },
    set servicePointId(val) {
        this._servicePointId = val;
    },
    get serviceQuantityActual() {
        return this._serviceQuantityActual;
    },
    set serviceQuantityActual(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute serviceQuantityActual in workPoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._serviceQuantityActual = val;
    },
    get serviceQuantityPlanned() {
        return this._serviceQuantityPlanned;
    },
    set serviceQuantityPlanned(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute serviceQuantityPlanned in workPoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._serviceQuantityPlanned = val;
    },
    get workAttendanceId() {
        return this._workAttendanceId;
    },
    set workAttendanceId(val) {
        this._workAttendanceId = val;
    },
    get workOrderId() {
        return this._workOrderId;
    },
    set workOrderId(val) {
        this._workOrderId = val;
    },
    get workId() {
        return this._workId;
    },
    set workId(val) {
        this._workId = val;
    },
    get workPointActivationDateTime() {
        return this._workPointActivationDateTime;
    },
    set workPointActivationDateTime(val) {
        this._workPointActivationDateTime = val;
    },
    get workPointEffort() {
        return this._workPointEffort;
    },
    set workPointEffort(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute workPointEffort in workPoint.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._workPointEffort = val;
    },
    get workPointDeleted() {
        return kony.sync.getBoolean(this._workPointDeleted) + "";
    },
    set workPointDeleted(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute workPointDeleted in workPoint.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._workPointDeleted = val;
    },
    get updateDateTime() {
        return this._updateDateTime;
    },
    set updateDateTime(val) {
        this._updateDateTime = val;
    },
};
/************************************************************************************
 * Retrieves all instances of workPoint SyncObject present in local database with
 * given limit and offset where limit indicates the number of records to be retrieved
 * and offset indicates number of rows to be ignored before returning the records.
 * e.g. var orderByMap = []
 * orderByMap[0] = {};
 * orderByMap[0].key = "workPointId";
 * orderByMap[0].sortType ="desc";
 * orderByMap[1] = {};
 * orderByMap[1].key = "activationMethod";
 * orderByMap[1].sortType ="asc";
 * var limit = 20;
 * var offset = 5;
 * com.pc.workloads.workPoint.getAll(successcallback,errorcallback, orderByMap, limit, offset)
 *************************************************************************************/
com.pc.workloads.workPoint.getAll = function(successcallback, errorcallback, orderByMap, limit, offset) {
    sync.log.trace("Entering com.pc.workloads.workPoint.getAll->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    orderByMap = kony.sync.formOrderByClause("workPoint", orderByMap);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_orderBy(query, orderByMap);
    kony.sync.qb_limitOffset(query, limit, offset);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.getAll->successcallback");
        successcallback(com.pc.workloads.workPoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Returns number of workPoint present in local database.
 *************************************************************************************/
com.pc.workloads.workPoint.getAllCount = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.getAllCount function");
    com.pc.workloads.workPoint.getCount("", successcallback, errorcallback);
};
/************************************************************************************
 * Returns number of workPoint using where clause in the local Database
 *************************************************************************************/
com.pc.workloads.workPoint.getCount = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.getCount->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.getCount", "getCount", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select count(*) from \"" + tbname + "\" " + wcs;
    kony.sync.single_execute_sql(dbname, sql, null, mySuccCallback, errorcallback);

    function mySuccCallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.getCount->successcallback");
        if (null !== res) {
            var count = null;
            count = res["count(*)"];
            kony.sync.verifyAndCallClosure(successcallback, {
                count: count
            });
        } else {
            sync.log.error("Some error occured while getting the count");
        }
    }
};
/************************************************************************************
 * Creates a new instance of workPoint in the local Database. The new record will 
 * be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.workloads.workPoint.prototype.create = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.workloads.workPoint.prototype.create function");
    var valuestable = this.getValuesTable(true);
    com.pc.workloads.workPoint.create(valuestable, successcallback, errorcallback, this.markForUpload);
};
com.pc.workloads.workPoint.create = function(valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  com.pc.workloads.workPoint.create->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.create", "create", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    if (kony.sync.attributeValidation(valuestable, "workPoint", errorcallback, true) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  com.pc.workloads.workPoint.create->success callback");
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
    var pks = [];
    var errMsg = "";

    function createSuccesscallback(res) {
        if (res == null || res.length == 0) {
            var relationshipMap = {};
            relationshipMap = com.pc.workloads.workPoint.getRelationshipMap(relationshipMap, valuestable);
            kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
        } else {
            errMsg = "[" + errMsg + "]";
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey, kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
        }
    }
    if (kony.sync.enableORMValidations) {
        errMsg = "workPointId=" + valuestable.workPointId;
        pks["workPointId"] = {
            key: "workPointId",
            value: valuestable.workPointId
        };
        com.pc.workloads.workPoint.getAllDetailsByPK(pks, createSuccesscallback, errorcallback)
    } else {
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
};
/************************************************************************************
 * Creates number of new instances of workPoint in the local Database. These new 
 * records will be merged with the enterprise datasource in the next Sync. Based upon 
 * kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var valuesArray = [];
 *		valuesArray[0] = {};
 *		valuesArray[0].workPointId = "workPointId_0";
 *		valuesArray[0].activationMethod = "activationMethod_0";
 *		valuesArray[0].contractCode = "contractCode_0";
 *		valuesArray[0].propertyCode = "propertyCode_0";
 *		valuesArray[0].serviceLineCode = "serviceLineCode_0";
 *		valuesArray[0].serviceCoverNumber = 0;
 *		valuesArray[0].employeeCodeActual = "employeeCodeActual_0";
 *		valuesArray[0].employeeCodePlanned = "employeeCodePlanned_0";
 *		valuesArray[0].issueCode = "issueCode_0";
 *		valuesArray[0].issueTypeCode = "issueTypeCode_0";
 *		valuesArray[0].servicePointId = "servicePointId_0";
 *		valuesArray[0].serviceQuantityActual = 0;
 *		valuesArray[0].serviceQuantityPlanned = 0;
 *		valuesArray[0].workAttendanceId = "workAttendanceId_0";
 *		valuesArray[0].workOrderId = "workOrderId_0";
 *		valuesArray[0].workId = "workId_0";
 *		valuesArray[0].workPointActivationDateTime = "workPointActivationDateTime_0";
 *		valuesArray[0].workPointEffort = 0;
 *		valuesArray[1] = {};
 *		valuesArray[1].workPointId = "workPointId_1";
 *		valuesArray[1].activationMethod = "activationMethod_1";
 *		valuesArray[1].contractCode = "contractCode_1";
 *		valuesArray[1].propertyCode = "propertyCode_1";
 *		valuesArray[1].serviceLineCode = "serviceLineCode_1";
 *		valuesArray[1].serviceCoverNumber = 1;
 *		valuesArray[1].employeeCodeActual = "employeeCodeActual_1";
 *		valuesArray[1].employeeCodePlanned = "employeeCodePlanned_1";
 *		valuesArray[1].issueCode = "issueCode_1";
 *		valuesArray[1].issueTypeCode = "issueTypeCode_1";
 *		valuesArray[1].servicePointId = "servicePointId_1";
 *		valuesArray[1].serviceQuantityActual = 1;
 *		valuesArray[1].serviceQuantityPlanned = 1;
 *		valuesArray[1].workAttendanceId = "workAttendanceId_1";
 *		valuesArray[1].workOrderId = "workOrderId_1";
 *		valuesArray[1].workId = "workId_1";
 *		valuesArray[1].workPointActivationDateTime = "workPointActivationDateTime_1";
 *		valuesArray[1].workPointEffort = 1;
 *		valuesArray[2] = {};
 *		valuesArray[2].workPointId = "workPointId_2";
 *		valuesArray[2].activationMethod = "activationMethod_2";
 *		valuesArray[2].contractCode = "contractCode_2";
 *		valuesArray[2].propertyCode = "propertyCode_2";
 *		valuesArray[2].serviceLineCode = "serviceLineCode_2";
 *		valuesArray[2].serviceCoverNumber = 2;
 *		valuesArray[2].employeeCodeActual = "employeeCodeActual_2";
 *		valuesArray[2].employeeCodePlanned = "employeeCodePlanned_2";
 *		valuesArray[2].issueCode = "issueCode_2";
 *		valuesArray[2].issueTypeCode = "issueTypeCode_2";
 *		valuesArray[2].servicePointId = "servicePointId_2";
 *		valuesArray[2].serviceQuantityActual = 2;
 *		valuesArray[2].serviceQuantityPlanned = 2;
 *		valuesArray[2].workAttendanceId = "workAttendanceId_2";
 *		valuesArray[2].workOrderId = "workOrderId_2";
 *		valuesArray[2].workId = "workId_2";
 *		valuesArray[2].workPointActivationDateTime = "workPointActivationDateTime_2";
 *		valuesArray[2].workPointEffort = 2;
 *		com.pc.workloads.workPoint.createAll(valuesArray, successcallback, errorcallback, true);
 *************************************************************************************/
com.pc.workloads.workPoint.createAll = function(valuesArray, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.workloads.workPoint.createAll function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.createAll", "createAll", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var isProperData = true;
    var arrayLen = 0;
    var errorInfo = [];
    var arrayLength = valuesArray.length;
    var errObject = null;
    var isReferentialIntegrityFailure = false;
    var errMsg = null;
    if (kony.sync.enableORMValidations) {
        var newValuesArray = [];
        //column level validations
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var valuestable = valuesArray[i];
            if (kony.sync.attributeValidation(valuestable, "workPoint", errorcallback, true) === false) {
                return;
            }
            newValuesArray[i] = valuestable;
        }
        valuesArray = newValuesArray;
        var isDuplicateKey = false;
        //checking for duplicate records
        var connection = kony.sync.getConnectionOnly(dbname, dbname);
        kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
        var isError = false;

        function checkDuplicatePkCallback(tx) {
            arrayLength = valuesArray.length;
            for (var i = 0; valuesArray != null && i < arrayLength; i++) {
                var valuestable = valuesArray[i];
                var pks = [];
                errMsg = "workPointId=" + valuestable.workPointId;
                pks["workPointId"] = {
                    key: "workPointId",
                    value: valuestable.workPointId
                };
                var wcs = [];
                if (com.pc.workloads.workPoint.pkCheck(pks, wcs, errorcallback, "searching") === false) {
                    isError = true;
                    return;
                }
                var query = kony.sync.qb_createQuery();
                kony.sync.qb_select(query, null);
                kony.sync.qb_from(query, tbname);
                kony.sync.qb_where(query, wcs);
                var query_compile = kony.sync.qb_compile(query);
                var sql = query_compile[0];
                var params = query_compile[1];
                var resultset = kony.sync.executeSql(tx, sql, params);
                if (resultset === false) {
                    isError = true;
                    return;
                }
                if (resultset.rows.length != 0) {
                    isError = true;
                    errMsg = "[" + errMsg + "]";
                    isDuplicateKey = true;
                    return;
                }
            }
            if (!isError) {
                checkIntegrity(tx);
            }
        }
    } else {
        //copying by value
        var newValuesArray = [];
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
        }
        valuesArray = newValuesArray;
        kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
    }

    function transactionErrorCallback() {
        if (isError == true) {
            //Statement error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
        } else {
            //Transaction error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
        }
    }

    function transactionSuccessCallback() {
        sync.log.trace("Entering  com.pc.workloads.workPoint.createAll->transactionSuccessCallback");
        if (!isError) {
            kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
        } else {
            if (isReferentialIntegrityFailure) {
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            }
            if (isDuplicateKey) {
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey, kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
            }
        }
    }
    //foreign key constraints validations
    function checkIntegrity(tx) {
        sync.log.trace("Entering  com.pc.workloads.workPoint.createAll->checkIntegrity");
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var relationshipMap = {};
            relationshipMap = com.pc.workloads.workPoint.getRelationshipMap(relationshipMap, valuesArray[i]);
            errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
            if (errObject === false) {
                isError = true;
                return;
            }
            if (errObject !== true) {
                isError = true;
                isReferentialIntegrityFailure = true;
                return;
            }
        }
    }
};
/************************************************************************************
 * Updates workPoint using primary key in the local Database. The update will be
 * merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.workloads.workPoint.prototype.updateByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.workloads.workPoint.prototype.updateByPK function");
    var pks = this.getPKTable();
    var valuestable = this.getValuesTable(false);
    com.pc.workloads.workPoint.updateByPK(pks, valuestable, successcallback, errorcallback, this.markForUpload);
};
com.pc.workloads.workPoint.updateByPK = function(pks, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  com.pc.workloads.workPoint.updateByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.updateByPK", "updateByPk", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    if (com.pc.workloads.workPoint.pkCheck(pks, wcs, errorcallback, "updating") === false) {
        return;
    }
    if (kony.sync.attributeValidation(valuestable, "workPoint", errorcallback, false) === false) {
        return;
    }
    var relationshipMap = {};
    relationshipMap = com.pc.workloads.workPoint.getRelationshipMap(relationshipMap, valuestable);
    kony.sync.updateByPK(tbname, dbname, relationshipMap, pks, valuestable, successcallback, errorcallback, markForUpload, wcs);
};
/************************************************************************************
 * Updates workPoint(s) using where clause in the local Database. The update(s)
 * will be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.workloads.workPoint.update = function(wcs, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.workloads.workPoint.update function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.update", "update", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    if (kony.sync.attributeValidation(valuestable, "workPoint", errorcallback, false) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  com.pc.workloads.workPoint.update-> success callback of Integrity Check");
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, com.pc.workloads.workPoint.getPKTable());
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = com.pc.workloads.workPoint.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, com.pc.workloads.workPoint.getPKTable());
    }
};
/************************************************************************************
 * Updates workPoint(s) satisfying one or more where clauses in the local Database. 
 * The update(s) will be merged with the enterprise datasource in the next Sync.
 * Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var inputArray = [];
 *		inputArray[0] = {};
 *		inputArray[0].changeSet = {};
 *		inputArray[0].changeSet.activationMethod = "activationMethod_updated0";
 *		inputArray[0].changeSet.contractCode = "contractCode_updated0";
 *		inputArray[0].changeSet.propertyCode = "propertyCode_updated0";
 *		inputArray[0].changeSet.serviceLineCode = "serviceLineCode_updated0";
 *		inputArray[0].whereClause = "where workPointId = '0'";
 *		inputArray[1] = {};
 *		inputArray[1].changeSet = {};
 *		inputArray[1].changeSet.activationMethod = "activationMethod_updated1";
 *		inputArray[1].changeSet.contractCode = "contractCode_updated1";
 *		inputArray[1].changeSet.propertyCode = "propertyCode_updated1";
 *		inputArray[1].changeSet.serviceLineCode = "serviceLineCode_updated1";
 *		inputArray[1].whereClause = "where workPointId = '1'";
 *		inputArray[2] = {};
 *		inputArray[2].changeSet = {};
 *		inputArray[2].changeSet.activationMethod = "activationMethod_updated2";
 *		inputArray[2].changeSet.contractCode = "contractCode_updated2";
 *		inputArray[2].changeSet.propertyCode = "propertyCode_updated2";
 *		inputArray[2].changeSet.serviceLineCode = "serviceLineCode_updated2";
 *		inputArray[2].whereClause = "where workPointId = '2'";
 *		com.pc.workloads.workPoint.updateAll(inputArray,successcallback,errorcallback);
 *************************************************************************************/
com.pc.workloads.workPoint.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
        sync.log.trace("Entering com.pc.workloads.workPoint.updateAll function");
        if (!kony.sync.isSyncInitialized(errorcallback)) {
            return;
        }
        if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.updateAll", "updateAll", errorcallback)) {
            return;
        }
        var dbname = "100004898512bd8be";
        var tbname = "workPoint";
        var isError = false;
        var errObject = null;
        if (markForUpload == false || markForUpload == "false") {
            markForUpload = "false"
        } else {
            markForUpload = "true"
        }
        if ((kony.sync.enableORMValidations)) {
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                var valuestable = v.changeSet;
                var isEmpty = true;
                for (var key in valuestable) {
                    isEmpty = false;
                    break;
                }
                if (isEmpty) {
                    errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue, kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
                    return;
                }
                var wcs = v.whereClause;
                var twcs = wcs;
                if (kony.sync.attributeValidation(valuestable, "workPoint", errorcallback, false) === false) {
                    return;
                }
                newInputArray[i] = [];
                newInputArray[i].changeSet = valuestable;
                newInputArray[i].whereClause = wcs;
            }
            inputArray = newInputArray;
            var connection = kony.sync.getConnectionOnly(dbname, dbname);
            kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        } else {
            //copying by value
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                newInputArray[i] = kony.sync.CreateCopy(v);
            }
            inputArray = newInputArray;
            kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, errorcallback, markForUpload, com.pc.workloads.workPoint.getPKTable());
        }

        function transactionSuccessCallback() {
            sync.log.trace("Entering  com.pc.workloads.workPoint.updateAll->transactionSuccessCallback");
            if (!isError) {
                kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, transactionErrorCallback, markForUpload, com.pc.workloads.workPoint.getPKTable());
            }
        }

        function transactionErrorCallback() {
            if (errObject === false) {
                //Sql statement error has occcurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
                kony.sync.errorObject = null;
            } else if (errObject !== null) {
                // Referential integrity error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            } else {
                //Transaction error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
            }
        }
        //foreign key constraints validations
        function checkIntegrity(tx) {
            sync.log.trace("Entering  com.pc.workloads.workPoint.updateAll->checkIntegrity");
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var relationshipMap = {};
                relationshipMap = com.pc.workloads.workPoint.getRelationshipMap(relationshipMap, inputArray[i].changeSet);
                sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
                errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
                if (errObject === false) {
                    isError = true;
                    return;
                }
                if (errObject !== true) {
                    isError = true;
                    kony.sync.rollbackTransaction(tx);
                    return;
                }
            }
        }
    }
    /************************************************************************************
     * Deletes workPoint using primary key from the local Database. The record will be
     * deleted from the enterprise datasource in the next Sync.
     *************************************************************************************/
com.pc.workloads.workPoint.prototype.deleteByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.prototype.deleteByPK function");
    var pks = this.getPKTable();
    com.pc.workloads.workPoint.deleteByPK(pks, successcallback, errorcallback, this.markForUpload);
};
com.pc.workloads.workPoint.deleteByPK = function(pks, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.workloads.workPoint.deleteByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.deleteByPK", "deleteByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var twcs = [];
    var deletedRows;
    var record = "";
    if (com.pc.workloads.workPoint.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);

    function workPointTransactionCallback(tx) {
        sync.log.trace("Entering com.pc.workloads.workPoint.deleteByPK->workPoint_PKPresent successcallback");
        record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (record === false) {
            isError = true;
            return;
        }
        if (null !== record) {} else {
            pkNotFound = true;
        }
        var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
        if (deletedRows === false) {
            isError = true;
        }
    }

    function workPointErrorCallback() {
        sync.log.error("Entering com.pc.workloads.workPoint.deleteByPK->relationship failure callback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function workPointSuccessCallback() {
        sync.log.trace("Entering com.pc.workloads.workPoint.deleteByPK->relationship success callback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering com.pc.workloads.workPoint.deleteByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, workPointTransactionCallback, workPointSuccessCallback, workPointErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes workPoint(s) using where clause from the local Database. The record(s)
 * will be deleted from the enterprise datasource in the next Sync.
 * e.g. com.pc.workloads.workPoint.remove("where workPointId like 'A%'", successcallback,errorcallback, true);
 *************************************************************************************/
com.pc.workloads.workPoint.remove = function(wcs, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering com.pc.workloads.workPoint.remove->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.remove", "remove", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function workPoint_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function workPoint_removeSuccess() {
        sync.log.trace("Entering com.pc.workloads.workPoint.remove->workPoint_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering com.pc.workloads.workPoint.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering com.pc.workloads.workPoint.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, workPoint_removeTransactioncallback, workPoint_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Deletes workPoint using primary key from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
com.pc.workloads.workPoint.prototype.removeDeviceInstanceByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.prototype.removeDeviceInstanceByPK function");
    var pks = this.getPKTable();
    com.pc.workloads.workPoint.removeDeviceInstanceByPK(pks, successcallback, errorcallback);
};
com.pc.workloads.workPoint.removeDeviceInstanceByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.removeDeviceInstanceByPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.removeDeviceInstanceByPK", "removeDeviceInstanceByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var deletedRows;
    if (com.pc.workloads.workPoint.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }

    function workPointTransactionCallback(tx) {
        sync.log.trace("Entering com.pc.workloads.workPoint.removeDeviceInstanceByPK -> workPointTransactionCallback");
        var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (null !== record && false != record) {
            deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
            if (deletedRows === false) {
                isError = true;
            }
        } else {
            pkNotFound = true;
        }
    }

    function workPointErrorCallback() {
        sync.log.error("Entering com.pc.workloads.workPoint.removeDeviceInstanceByPK -> workPointErrorCallback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function workPointSuccessCallback() {
        sync.log.trace("Entering com.pc.workloads.workPoint.removeDeviceInstanceByPK -> workPointSuccessCallback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering com.pc.workloads.workPoint.removeDeviceInstanceByPK -> PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, workPointTransactionCallback, workPointSuccessCallback, workPointErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes workPoint(s) using where clause from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
com.pc.workloads.workPoint.removeDeviceInstance = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.removeDeviceInstance->main function");
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function workPoint_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function workPoint_removeSuccess() {
        sync.log.trace("Entering com.pc.workloads.workPoint.remove->workPoint_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering com.pc.workloads.workPoint.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering com.pc.workloads.workPoint.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, workPoint_removeTransactioncallback, workPoint_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Retrieves workPoint using primary key from the local Database. 
 *************************************************************************************/
com.pc.workloads.workPoint.prototype.getAllDetailsByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.prototype.getAllDetailsByPK function");
    var pks = this.getPKTable();
    com.pc.workloads.workPoint.getAllDetailsByPK(pks, successcallback, errorcallback);
};
com.pc.workloads.workPoint.getAllDetailsByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.getAllDetailsByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.getAllDetailsByPK", "getAllDetailsByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var wcs = [];
    if (com.pc.workloads.workPoint.pkCheck(pks, wcs, errorcallback, "searching") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, wcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.getAllDetailsByPK-> success callback function");
        successcallback(com.pc.workloads.workPoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Retrieves workPoint(s) using where clause from the local Database. 
 * e.g. com.pc.workloads.workPoint.find("where workPointId like 'A%'", successcallback,errorcallback);
 *************************************************************************************/
com.pc.workloads.workPoint.find = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.find function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.find", "find", errorcallback)) {
        return;
    }
    //wcs will be a string formed by the user.
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select * from \"" + tbname + "\" " + wcs;

    function mySuccCallback(res) {
        kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.workPoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Marks instance of workPoint with given primary key for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.workloads.workPoint.prototype.markForUploadbyPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.prototype.markForUploadbyPK function");
    var pks = this.getPKTable();
    com.pc.workloads.workPoint.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.pc.workloads.workPoint.markForUploadbyPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.markForUploadbyPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.markForUploadbyPK", "markForUploadbyPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var isError = false;
    var recordsFound = false;
    var recordsMarkedForUpload = 0;
    var wcs = [];
    if (com.pc.workloads.workPoint.pkCheck(pks, wcs, errorcallback, "marking for upload by PK") === false) {
        return;
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = [];
        twcs = wcs;
        kony.table.insert(twcs, {
            key: kony.sync.historyTableChangeTypeColumn,
            value: record[kony.sync.historyTableChangeTypeColumn],
            optype: "EQ",
            comptype: "AND"
        });
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        kony.sync.qb_where(query, twcs);
        kony.table.remove(twcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function single_transaction_callback(tx) {
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query, tbname);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        var resultSet = kony.sync.executeSql(tx, sql, params);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        if (num_records > 0) {
            recordsFound = true;
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
            var changeType = record[kony.sync.mainTableChangeTypeColumn];
            if (!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith("" + changeType, "9")) {
                recordsMarkedForUpload = 1;
                if (markRecordForUpload(tx, record) === false) {
                    isError = true;
                    return;
                }
            }
        }
        var query1 = kony.sync.qb_createQuery();
        kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
        kony.sync.qb_where(query1, wcs);
        var query1_compile = kony.sync.qb_compile(query1);
        var sql1 = query1_compile[0];
        var params1 = query1_compile[1];
        var resultSet1 = kony.sync.executeSql(tx, sql1, params1);
        if (resultSet1 !== false) {
            var num_records = resultSet1.rows.length;
            for (var i = 0; i <= num_records - 1; i++) {
                var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
                if (markRecordForUploadHistory(tx, record) === false) {
                    isError = true;
                    return;
                }
                recordsFound = true;
            }
        } else {
            isError = true;
        }
    }

    function single_transaction_success_callback() {
        if (recordsFound === true) {
            kony.sync.verifyAndCallClosure(successcallback, {
                count: recordsMarkedForUpload
            });
        } else {
            kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
        }
    }

    function single_transaction_error_callback(res) {
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Marks instance(s) of workPoint matching given where clause for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
com.pc.workloads.workPoint.markForUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.markForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.markForUpload", "markForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var isError = false;
    var num_records_main = 0;
    wcs = kony.sync.validateWhereClause(wcs);
    if (!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
        wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    } else {
        wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + wcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = "";
        twcs = wcs;
        twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + twcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function single_transaction_callback(tx) {
        sync.log.trace("Entering com.pc.workloads.workPoint.markForUpload->single_transaction_callback");
        //updating main table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        num_records_main = resultSet.rows.length;
        for (var i = 0; i < num_records_main; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUpload(tx, record) === false) {
                isError = true;
                return;
            }
        }
        //updating history table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        for (var i = 0; i <= num_records - 1; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUploadHistory(tx, record) === false) {
                isError = true;
                return;
            }
        }
    }

    function single_transaction_success_callback() {
        sync.log.trace("Entering com.pc.workloads.workPoint.markForUpload->single_transaction_success_callback");
        kony.sync.verifyAndCallClosure(successcallback, {
            count: num_records_main
        });
    }

    function single_transaction_error_callback() {
        sync.log.error("Entering com.pc.workloads.workPoint.markForUpload->single_transaction_error_callback");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Retrieves instance(s) of workPoint pending for upload. Records are marked for
 * pending upload if they have been updated or created locally and the changes have
 * not been merged with enterprise datasource.
 *************************************************************************************/
com.pc.workloads.workPoint.getPendingUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.getPendingUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.getPendingUpload->successcallback function");
        kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.workPoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of workPoint pending for acknowledgement. This is relevant
 * when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
 * In persistent Sync the  records in the local database are put into a pending 
 * acknowledgement state after an upload.
 *************************************************************************************/
com.pc.workloads.workPoint.getPendingAcknowledgement = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.getPendingAcknowledgement->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var mysql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " <> " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.getPendingAcknowledgement success callback function");
        kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.workPoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of workPoint deferred for upload.
 *************************************************************************************/
com.pc.workloads.workPoint.getDeferredUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.getDeferredUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.getDeferredUpload->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.workPoint.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Rollbacks all changes to workPoint in local database to last synced state
 *************************************************************************************/
com.pc.workloads.workPoint.rollbackPendingLocalChanges = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.rollbackPendingLocalChanges->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.rollbackPendingLocalChanges->main function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }
};
/************************************************************************************
 * Rollbacks changes to workPoint's record with given primary key in local 
 * database to last synced state
 *************************************************************************************/
com.pc.workloads.workPoint.prototype.rollbackPendingLocalChangesByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.prototype.rollbackPendingLocalChangesByPK function");
    var pks = this.getPKTable();
    com.pc.workloads.workPoint.rollbackPendingLocalChangesByPK(pks, successcallback, errorcallback);
};
com.pc.workloads.workPoint.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.rollbackPendingLocalChangesByPK->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.rollbackPendingLocalChangesByPK", "rollbackPendingLocalChangesByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var wcs = [];
    if (com.pc.workloads.workPoint.pkCheck(pks, wcs, errorcallback, "rollbacking") === false) {
        return;
    }
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.rollbackPendingLocalChangesByPK->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering com.pc.workloads.workPoint.rollbackPendingLocalChangesByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
};
/************************************************************************************
 * isRecordDeferredForUpload returns true or false depending on whether workPoint's record  
 * with given primary key got deferred in last sync
 *************************************************************************************/
com.pc.workloads.workPoint.prototype.isRecordDeferredForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.workloads.workPoint.prototype.isRecordDeferredForUpload function");
    var pks = this.getPKTable();
    com.pc.workloads.workPoint.isRecordDeferredForUpload(pks, successcallback, errorcallback);
};
com.pc.workloads.workPoint.isRecordDeferredForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.isRecordDeferredForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.isRecordDeferredForUpload", "isRecordDeferredForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var wcs = [];
    var flag;
    if (com.pc.workloads.workPoint.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.isRecordDeferredForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            deferred: flag
        });
    }
};
/************************************************************************************
 * isRecordPendingForUpload returns true or false depending on whether workPoint's record  
 * with given primary key is pending for upload
 *************************************************************************************/
com.pc.workloads.workPoint.prototype.isRecordPendingForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  com.pc.workloads.workPoint.prototype.isRecordPendingForUpload function");
    var pks = this.getPKTable();
    com.pc.workloads.workPoint.isRecordPendingForUpload(pks, successcallback, errorcallback);
};
com.pc.workloads.workPoint.isRecordPendingForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.isRecordPendingForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "com.pc.workloads.workPoint.isRecordPendingForUpload", "isRecordPendingForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = com.pc.workloads.workPoint.getTableName();
    var wcs = [];
    var flag;
    if (com.pc.workloads.workPoint.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "NOT LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering com.pc.workloads.workPoint.isRecordPendingForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            pending: flag
        });
    }
};
/************************************************************************************
 * Start of helper functions used internally, not to be used as ORMs
 *************************************************************************************/
//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.pc.workloads.workPoint.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal) {
    sync.log.trace("Entering com.pc.workloads.workPoint.removeCascade function");
    var tbname = com.pc.workloads.workPoint.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);

    function removeCascadeChildren() {}
    if (isCascade) {
        if (removeCascadeChildren() === false) {
            return false;
        }
        if (kony.sync.deleteBatch(tx, tbname, wcs, isLocal, markForUpload, null) === false) {
            return false;
        }
        return true;
    } else {
        var sql = "select * from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            return false;
        }
        var num_records = resultSet.rows.length;
        if (num_records === 0) {
            return true;
        } else {
            sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable));
            errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity, kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable)));
            return false;
        }
    }
};
com.pc.workloads.workPoint.convertTableToObject = function(res) {
    sync.log.trace("Entering com.pc.workloads.workPoint.convertTableToObject function");
    objMap = [];
    if (res !== null) {
        for (var i in res) {
            var obj = new com.pc.workloads.workPoint();
            obj.workPointId = res[i].workPointId;
            obj.activationMethod = res[i].activationMethod;
            obj.contractCode = res[i].contractCode;
            obj.propertyCode = res[i].propertyCode;
            obj.serviceLineCode = res[i].serviceLineCode;
            obj.serviceCoverNumber = res[i].serviceCoverNumber;
            obj.employeeCodeActual = res[i].employeeCodeActual;
            obj.employeeCodePlanned = res[i].employeeCodePlanned;
            obj.issueCode = res[i].issueCode;
            obj.issueTypeCode = res[i].issueTypeCode;
            obj.servicePointId = res[i].servicePointId;
            obj.serviceQuantityActual = res[i].serviceQuantityActual;
            obj.serviceQuantityPlanned = res[i].serviceQuantityPlanned;
            obj.workAttendanceId = res[i].workAttendanceId;
            obj.workOrderId = res[i].workOrderId;
            obj.workId = res[i].workId;
            obj.workPointActivationDateTime = res[i].workPointActivationDateTime;
            obj.workPointEffort = res[i].workPointEffort;
            obj.workPointDeleted = res[i].workPointDeleted;
            obj.updateDateTime = res[i].updateDateTime;
            obj.markForUpload = (Math.floor(res[i].konysyncchangetype / 10) == 9) ? false : true;
            objMap[i] = obj;
        }
    }
    return objMap;
};
com.pc.workloads.workPoint.filterAttributes = function(valuestable, insert) {
    sync.log.trace("Entering com.pc.workloads.workPoint.filterAttributes function");
    var attributeTable = {};
    attributeTable.workPointId = "workPointId";
    attributeTable.activationMethod = "activationMethod";
    attributeTable.contractCode = "contractCode";
    attributeTable.propertyCode = "propertyCode";
    attributeTable.serviceLineCode = "serviceLineCode";
    attributeTable.serviceCoverNumber = "serviceCoverNumber";
    attributeTable.employeeCodeActual = "employeeCodeActual";
    attributeTable.employeeCodePlanned = "employeeCodePlanned";
    attributeTable.issueCode = "issueCode";
    attributeTable.issueTypeCode = "issueTypeCode";
    attributeTable.servicePointId = "servicePointId";
    attributeTable.serviceQuantityActual = "serviceQuantityActual";
    attributeTable.serviceQuantityPlanned = "serviceQuantityPlanned";
    attributeTable.workAttendanceId = "workAttendanceId";
    attributeTable.workOrderId = "workOrderId";
    attributeTable.workId = "workId";
    attributeTable.workPointActivationDateTime = "workPointActivationDateTime";
    attributeTable.workPointEffort = "workPointEffort";
    var PKTable = {};
    PKTable.workPointId = {}
    PKTable.workPointId.name = "workPointId";
    PKTable.workPointId.isAutoGen = false;
    var newvaluestable = {};
    for (var k in valuestable) {
        var v = valuestable[k];
        if (kony.sync.isNull(attributeTable[k])) {
            sync.log.warn("Ignoring the attribute " + k + " for the SyncObject workPoint. " + k + " not defined as an attribute in SyncConfiguration.");
        } else if (!kony.sync.isNull(PKTable[k])) {
            if (insert === false) {
                sync.log.warn("Ignoring the primary key " + k + " for the SyncObject workPoint. Primary Key should not be the part of the attributes to be updated in the local device database.");
            } else if (PKTable[k].isAutoGen) {
                sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject workPoint. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
            } else {
                newvaluestable[k] = v;
            }
        } else {
            newvaluestable[k] = v;
        }
    }
    return newvaluestable;
};
com.pc.workloads.workPoint.formOrderByClause = function(orderByMap) {
    sync.log.trace("Entering com.pc.workloads.workPoint.formOrderByClause function");
    if (!kony.sync.isNull(orderByMap)) {
        var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
        //var filteredValuestable = com.pc.workloads.workPoint.filterAttributes(valuestable, true);
        return kony.sync.convertToValuesTableOrderByMap(orderByMap, valuestable);
    }
    return null;
};
com.pc.workloads.workPoint.prototype.getValuesTable = function(isInsert) {
    sync.log.trace("Entering com.pc.workloads.workPoint.prototype.getValuesTable function");
    var valuesTable = {};
    if (isInsert === true) {
        valuesTable.workPointId = this.workPointId;
    }
    valuesTable.activationMethod = this.activationMethod;
    valuesTable.contractCode = this.contractCode;
    valuesTable.propertyCode = this.propertyCode;
    valuesTable.serviceLineCode = this.serviceLineCode;
    valuesTable.serviceCoverNumber = this.serviceCoverNumber;
    valuesTable.employeeCodeActual = this.employeeCodeActual;
    valuesTable.employeeCodePlanned = this.employeeCodePlanned;
    valuesTable.issueCode = this.issueCode;
    valuesTable.issueTypeCode = this.issueTypeCode;
    valuesTable.servicePointId = this.servicePointId;
    valuesTable.serviceQuantityActual = this.serviceQuantityActual;
    valuesTable.serviceQuantityPlanned = this.serviceQuantityPlanned;
    valuesTable.workAttendanceId = this.workAttendanceId;
    valuesTable.workOrderId = this.workOrderId;
    valuesTable.workId = this.workId;
    valuesTable.workPointActivationDateTime = this.workPointActivationDateTime;
    valuesTable.workPointEffort = this.workPointEffort;
    return valuesTable;
};
com.pc.workloads.workPoint.prototype.getPKTable = function() {
    sync.log.trace("Entering com.pc.workloads.workPoint.prototype.getPKTable function");
    var pkTable = {};
    pkTable.workPointId = {
        key: "workPointId",
        value: this.workPointId
    };
    return pkTable;
};
com.pc.workloads.workPoint.getPKTable = function() {
    sync.log.trace("Entering com.pc.workloads.workPoint.getPKTable function");
    var pkTable = [];
    pkTable.push("workPointId");
    return pkTable;
};
com.pc.workloads.workPoint.pkCheck = function(pks, wcs, errorcallback, opName) {
    sync.log.trace("Entering com.pc.workloads.workPoint.pkCheck function");
    var wc = [];
    if (kony.sync.isNull(pks)) {
        sync.log.error("Primary Key workPointId not specified in  " + opName + "  an item in workPoint");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("workPointId", opName, "workPoint")));
        return false;
    } else if (kony.sync.isValidJSTable(pks)) {
        if (!kony.sync.isNull(pks.workPointId)) {
            if (!kony.sync.isNull(pks.workPointId.value)) {
                wc.key = "workPointId";
                wc.value = pks.workPointId.value;
            } else {
                wc.key = "workPointId";
                wc.value = pks.workPointId;
            }
        } else {
            sync.log.error("Primary Key workPointId not specified in  " + opName + "  an item in workPoint");
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("workPointId", opName, "workPoint")));
            return false;
        }
    } else {
        wc.key = "workPointId";
        wc.value = pks;
    }
    kony.table.insert(wcs, wc);
    return true;
};
com.pc.workloads.workPoint.validateNull = function(valuestable, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.validateNull function");
    return true;
};
com.pc.workloads.workPoint.validateNullInsert = function(valuestable, errorcallback) {
    sync.log.trace("Entering com.pc.workloads.workPoint.validateNullInsert function");
    return true;
};
com.pc.workloads.workPoint.getRelationshipMap = function(relationshipMap, valuestable) {
    sync.log.trace("Entering com.pc.workloads.workPoint.getRelationshipMap function");
    var r1 = {};
    return relationshipMap;
};
com.pc.workloads.workPoint.checkPKValueTables = function(valuetables) {
    var checkPksNotNullFlag = true;
    for (var i = 0; i < valuetables.length; i++) {
        if (kony.sync.isNull(valuetables[i])) {
            checkPksNotNullFlag = false;
            break;
        }
    }
    return checkPksNotNullFlag;
};
com.pc.workloads.workPoint.getTableName = function() {
    return "workPoint";
};
// **********************************End workPoint's helper methods************************