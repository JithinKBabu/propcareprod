//startup.js file
var globalhttpheaders = {};
var appConfig = {
    appId: "PropertyServiceT",
    appName: "PropertyServiceT",
    appVersion: "1.0.2",
    platformVersion: null,
    serverIp: "10.44.215.109",
    serverPort: "80",
    secureServerPort: "443",
    isDebug: false,
    middlewareContext: "PropertyServiceT",
    isturlbase: "https://rentokil-initial.konycloud.com/services",
    isMFApp: true,
    appKey: "58c7de7ba04d12f63cafdcf37e17ca43",
    appSecret: "7ed094b69a4185ada9a413111ebb1c54",
    serviceUrl: "https://100004898.auth.konycloud.com/appconfig",
    svcDoc: {
        "appId": "a86ee008-1a7f-4998-b489-27dfcd40953b",
        "baseId": "d21ed215-f9b1-4a94-9570-ebf6e788fb15",
        "name": "PropertyServiceTPROD",
        "selflink": "https://100004898.auth.konycloud.com/appconfig",
        "integsvc": {
            "WorkdayPc": "https://rentokil-initial.konycloud.com/services/WorkdayPc",
            "EmployeesPc": "https://rentokil-initial.konycloud.com/services/EmployeesPc",
            "WorkloadsPc": "https://rentokil-initial.konycloud.com/services/WorkloadsPc",
            "ConfigDataPc": "https://rentokil-initial.konycloud.com/services/ConfigDataPc",
            "PortfoliosPc": "https://rentokil-initial.konycloud.com/services/PortfoliosPc",
            "WorkHistoryPc": "https://rentokil-initial.konycloud.com/services/WorkHistoryPc",
            "ServiceAreasPc": "https://rentokil-initial.konycloud.com/services/ServiceAreasPc",
            "iCabInstancePc": "https://rentokil-initial.konycloud.com/services/iCabInstancePc",
            "ServiceCoversPc": "https://rentokil-initial.konycloud.com/services/ServiceCoversPc"
        },
        "sync": {
            "appId": "100004898512bd8be",
            "url": "https://rentokil-initial.sync.konycloud.com/syncservice/api/v1/100004898512bd8be"
        },
        "reportingsvc": {
            "custom": "https://rentokil-initial.konycloud.com/services/CMS",
            "session": "https://rentokil-initial.konycloud.com/services/IST"
        },
        "services_meta": {
            "WorkdayPc": {
                "version": "3.0",
                "url": "https://rentokil-initial.konycloud.com/services/WorkdayPc",
                "type": "integsvc"
            },
            "EmployeesPc": {
                "version": "3.0",
                "url": "https://rentokil-initial.konycloud.com/services/EmployeesPc",
                "type": "integsvc"
            },
            "WorkloadsPc": {
                "version": "3.0",
                "url": "https://rentokil-initial.konycloud.com/services/WorkloadsPc",
                "type": "integsvc"
            },
            "ConfigDataPc": {
                "version": "3.0",
                "url": "https://rentokil-initial.konycloud.com/services/ConfigDataPc",
                "type": "integsvc"
            },
            "PortfoliosPc": {
                "version": "3.0",
                "url": "https://rentokil-initial.konycloud.com/services/PortfoliosPc",
                "type": "integsvc"
            },
            "WorkHistoryPc": {
                "version": "3.0",
                "url": "https://rentokil-initial.konycloud.com/services/WorkHistoryPc",
                "type": "integsvc"
            },
            "ServiceAreasPc": {
                "version": "3.0",
                "url": "https://rentokil-initial.konycloud.com/services/ServiceAreasPc",
                "type": "integsvc"
            },
            "iCabInstancePc": {
                "version": "3.0",
                "url": "https://rentokil-initial.konycloud.com/services/iCabInstancePc",
                "type": "integsvc"
            },
            "ServiceCoversPc": {
                "version": "3.0",
                "url": "https://rentokil-initial.konycloud.com/services/ServiceCoversPc",
                "type": "integsvc"
            }
        }
    },
    svcDocRefresh: false,
    svcDocRefreshTimeSecs: -1,
    eventTypes: ["FormEntry", "FormExit", "Touch", "Gesture", "ServiceRequest", "ServiceResponse", "Error", "Crash"],
    url: "https://rentokil-initial.konycloud.com/PropertyServiceT/MWServlet",
    secureurl: "https://rentokil-initial.konycloud.com/PropertyServiceT/MWServlet"
};
sessionID = "";

function appInit(params) {
    skinsInit();
    initializetplSegCommonHeader();
    initializetplSegCommonList();
    initializetplSegCommonSingleRow();
    initializetplSegHeaderSelectionList();
    initializetplSegHomeContentList();
    initializetplSegHomeHeader();
    initializetplSegHomeHeaderAlt();
    initializetplSegMenuList();
    initializetplSegMenuListTop();
    initializetplSegSelectionList();
    initializePCMapCallout();
    CompletedVisitGlobals();
    CustomerGlobals();
    HomeGlobals();
    LoginGlobals();
    PopupChangeEmailGlobals();
    PopupConfirmationGlobals();
    PopupMessageGlobals();
    PopupProgressGlobals();
    PopupSettingsGlobals();
    PopupShowImageGlobals();
    PopupStartDayGlobals();
    VisitGlobals();
    setAppBehaviors();
};

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        APILevel: 7000
    })
};

function themeCallBack() {
    callAppMenu();
    initializeGlobalVariables();
    kony.application.setApplicationInitializationEvents({
        preappinit: AS_AppEvents_1c41cd45c4d04d9aa73d1236fd864901,
        init: appInit,
        postappinit: AS_AppEvents_1255ca9b508441779247d6b3604da112,
        showstartupform: function() {
            Login.show();
        }
    });
};

function loadResources() {
    globalhttpheaders = {};
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_EmailChecker"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_signatureFFI"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_compressImage"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_ConvertImageJPEG"
    });
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    //This is to define sync global variable if application has sync
    var mbaasSdkObj = kony.sdk && kony.sdk.getCurrentInstance();
    if (mbaasSdkObj.getSyncService()) {
        sync = mbaasSdkObj.getSyncService();
    }
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//If default locale is specified. This is set even before any other app life cycle event is called.
kony.i18n.setDefaultLocaleAsync("en_GB", onSuccess, onFailure, null);