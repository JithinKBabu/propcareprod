/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all class definitions
***********************************************************************/

/** Class definition - User/Employee **/
function Employee(){
  	this.empId						= 0;
  	this.employeeCode				= "0";
	this.firstName					= "";
	this.lastName					= "";
	this.email						= "";
  	this.countryCode				= "UK";
  	this.businessCode				= "R";
  	this.branchNumber				= "";  	
    
  	this.setEmployeeCode			= function(idVal){
      	this.employeeCode			= idVal;
    };
  
  	this.setEmployeeId				= function(idVal){
      	this.empId					= idVal;
    }; 	
  
  	this.getEmployeeName			= function(){
      	/*var empName					= "";
      	empName						+= (!isEmpty(this.firstName)) ? this.firstName : "";
      	empName						+= " ";
      	empName						+= (!isEmpty(this.lastName)) ? this.lastName : "";
      	return empName;*/
      	return this.email;
    };
}

/** Class definition - MyAppDataObject **/
function MyAppDataObject() {
  	this.isDayStarted				= PCConstant.DAY_STARTED_DONT_KNOW; /** NA or YES or NO**/
  	this.startDayDateStr			= "";
    this.startDayTimeStr			= "";
  	this.stopDayDateStr				= "";  	  	
	this.stopDayTimeStr				= "";
  	
  	this.iCabInstances				= [];
  	this.languages					= [];
  	this.scheduledDatesList			= [];
  	this.scheduledObjects			= null;
  	this.completedJobs				= [];
  	
  	this.selDateStrKey				= "";
  	this.isVisitInProgress			= false;
  	this.inProgressVisit			= null;
  	
  	this.selectedEmail				= null;
  	this.selICabServerCode			= null;
  	this.selLanguage				= null;
  	this.selServiceArea				= {};
  
  	this.selScheduledItem			= null;  // No need to save in local store
  	this.selScheduledDateStr		= "";	 // No need to save in local store	
  
  	this.historyObjects				= null;
    this.historyDatesList 			= [];
  
    this.serviceCoverObjects       	= null;
    this.serviceCoverCodeList	   	= [];
  
    this.selCompletedItem          	= null;
  	
  	this.geoCoordinates				= {}; /** JSON object having key as postcode and value as geocoordinates **/
  
  	var addDataObj					= getAppDataObjFromStore();
  
   printMessage("prod issue analysis --initializeGlobals  outside addDataObj" );
  
	if (!isEmpty(addDataObj)) { 
       printMessage("prod issue analysis --initializeGlobals  insided addDataObj" );
      	this.iCabInstances			= addDataObj.iCabInstances;
      	this.languages				= addDataObj.languages;
      	this.scheduledDatesList		= addDataObj.scheduledDatesList; // Array having date strings in sorted order
      	this.scheduledObjects		= addDataObj.scheduledObjects;   // json
      	this.completedJobs			= addDataObj.completedJobs;
      
      	this.selectedEmail			= addDataObj.selectedEmail;
      	this.selICabServerCode		= addDataObj.selICabServerCode;
      	this.selLanguage			= addDataObj.selLanguage;
      	this.selServiceArea			= addDataObj.selServiceArea;
      
      	this.isDayStarted			= addDataObj.isDayStarted;
      	this.startDayDateStr		= addDataObj.startDayDateStr;
    	this.startDayTimeStr		= addDataObj.startDayTimeStr;
      	this.stopDayDateStr			= addDataObj.stopDayDateStr;	  	
		this.stopDayTimeStr			= addDataObj.stopDayTimeStr;
      	
      	this.selDateStrKey			= addDataObj.selDateStrKey;
      	this.isVisitInProgress		= addDataObj.isVisitInProgress;
      	this.inProgressVisit		= addDataObj.inProgressVisit; 
      	this.geoCoordinates			= addDataObj.geoCoordinates;
    }
  
  	this.addICabInstance			= function(aRecord){
      	var isAlreadyAdded			= isRecordExist(aRecord, this.iCabInstances, "iCABSServer");
      	if (isAlreadyAdded === false){
          	this.iCabInstances.push(aRecord);
        }
    };
  
  	this.addLanguage				= function(aRecord){
      	var isAlreadyAdded			= isRecordExist(aRecord, this.languages, "code");
      	if (isAlreadyAdded === false){
          	this.languages.push(aRecord);
        }
    };
  
  	this.getScheduledObjectsForDate	= function(dateStrKey){
      	var jobs					= this.scheduledObjects[dateStrKey];
      	return jobs;
    };
  
  	this.getWorkUsingWorkIdAndDateStr= function(workId, dateStrKey){
      	var jobs					= this.scheduledObjects[dateStrKey];
      	var aJobObj					= null;
      	if (jobs.length > 0) {
          	
          	for (var i = 0; i < jobs.length; i++) {
              	aJobObj					= jobs[i];
              	if (aJobObj.workId == workId) {
                  	break;
                }
            }
        }
      
      	return aJobObj;
    };
  
  	this.updateVisitStatus			= function(dateStrKey, visitStatus){
      	var jobs					= this.scheduledObjects[dateStrKey];
      	if (jobs.length > 0) {
          	var aJobObj					= null;
          	for (var i = 0; i < jobs.length; i++) {
              	aJobObj					= jobs[i];
              	if (this.isInProgressVisitMatchingVisit(aJobObj) === true) {
                  	removeItemFromArray(jobs, aJobObj);
                  	aJobObj.visitStatus	= visitStatus;			
                  	addItemToArray(jobs, aJobObj, i);
                  	this.scheduledObjects[dateStrKey]	= jobs;
                  	setAppDataObjInStore();
                  	break;
                }
            }
        }
    };
  
  	this.getIcabInstance			= function(){
      	var selIcabInstance			= null;
      	if (this.iCabInstances.length > 0 && !isEmpty(this.selICabServerCode)) {
          	for (var i = 0; i < this.iCabInstances.length; i++) {
              	selIcabInstance		= this.iCabInstances[i];
              	if (selIcabInstance.iCABSServer == this.selICabServerCode) {
                      break;
                }
            }
        }
      	return selIcabInstance;
    };
  
  	this.isInProgressVisitMatchingVisit	= function(aVisitObj){
      	var returnFlag					= false;
      	if (this.inProgressVisit.workorderId == aVisitObj.workorderId) {
          	returnFlag					= true;
        }
      	return returnFlag;
    };
  

	this.moveScheduledJobToCompletedJobs= function(){
      	var scheduledJobs				= this.getScheduledObjectsForDate(this.selDateStrKey);
      	var updatedJobs					= [];
      	if (scheduledJobs.length > 0) {
          	var aJob					= null;
          	for (var i = 0; i < scheduledJobs.length; i++) {
              	aJob					= scheduledJobs[i];
              	if (this.isInProgressVisitMatchingVisit(aJob) === true) {
                  	aJob.visitStatus	= PCConstant.VISIT_CLOSED;
                  	aJob.completedDate	= new Date();
                  	this.completedJobs.push(aJob);
                } else {
                  	updatedJobs.push(aJob);
                }
            }
          	this.scheduledObjects[this.selDateStrKey]	= updatedJobs;
          	this.updateScheduledDatesList();
          	setAppDataObjInStore();
        }
    };
  
  	this.updateVisitStatusForSyncedJob	= function(workIdStr, visitStatus){      	
      	var jobs						= this.completedJobs;
      	if (jobs.length > 0) {
          	var aJobObj					= null;
          	for (var i = 0; i < jobs.length; i++) {
              	aJobObj					= jobs[i];
              	if (workIdStr === aJobObj.workId) {
                  	aJobObj.visitStatus	= visitStatus;
                  	setAppDataObjInStore();
                  	break;
                }
            }
        }
    };
  
  	this.updateScheduledDatesList		= function () {
      	if (this.scheduledDatesList.length > 0) {
          	var scheduledJobs			= [];
          	var selDateStrKey			= "";
          	var newScheduledDatesList	= [];
          	for (var i = 0; i < this.scheduledDatesList.length; i++) {
              	selDateStrKey			= this.scheduledDatesList[i];
              	scheduledJobs			= this.getScheduledObjectsForDate(selDateStrKey);
              	if (isEmpty(scheduledJobs) || scheduledJobs.length === 0) {
                  	// Nothing to do
                } else {
                  	newScheduledDatesList.push(selDateStrKey);
                }
            }
          	this.scheduledDatesList		= newScheduledDatesList;
        }          
    };
  
  	this.resetOnChangeSourceOrEmail		= function() {
      	this.isDayStarted				= PCConstant.DAY_STARTED_DONT_KNOW; /** NA or YES or NO**/
  		this.startDayDateStr			= "";
    	this.startDayTimeStr			= "";
  		this.stopDayDateStr				= "";  	  	
		this.stopDayTimeStr				= "";
  	
  		this.iCabInstances				= [];
  		this.languages					= [];
  		this.scheduledDatesList			= [];
  		this.scheduledObjects			= null;
  		this.completedJobs				= [];
  	
  		this.selDateStrKey				= "";
  		this.isVisitInProgress			= false;
  		this.inProgressVisit			= null;
  	
  		this.selectedEmail				= null;
  		this.selICabServerCode			= null;
  		this.selLanguage				= null;
  		this.selServiceArea				= {};
  
  		this.selScheduledItem			= null;  // No need to save in local store
  		this.selScheduledDateStr		= "";	 // No need to save in local store	
  
  		this.historyObjects				= null;
    	this.historyDatesList 			= [];
  
    	this.serviceCoverObjects       	= null;
    	this.serviceCoverCodeList	   	= [];
      	this.geoCoordinates				= {};
      	setAppDataObjInStore();
    };
  
  	this.addGeocodeForPostcode					= function(postCode, latitude, longitude, forceUpdate, dateStr, nameStr, addressStr, propertyCodeStr){
      	if (isEmpty(this.geoCoordinates[postCode])) {
          	this.geoCoordinates[postCode]		= new GeoCoordinate(latitude, longitude, dateStr, nameStr, addressStr, propertyCodeStr);          
        } else {
          	var existingCoordinate				= this.geoCoordinates[postCode];
          	if (isEmpty(existingCoordinate.latitude) || isEmpty(existingCoordinate.longitude)) {
              	existingCoordinate.latitude		= latitude;
              	existingCoordinate.longitude	= longitude;
              	this.geoCoordinates[postCode]	= existingCoordinate; 
            } else if (forceUpdate === true) {
              	existingCoordinate.latitude		= latitude;
              	existingCoordinate.longitude	= longitude;
              	this.geoCoordinates[postCode]	= existingCoordinate;
            }
        }      	     
    };
  
  	this.getDateStrFromGeoCoordinates			= function (postCodeStr) {
      	var dateStr								= "";
      	var aGeoCoordinateObj					= null;
      	if (!isEmpty(this.geoCoordinates[postCodeStr])) {
          	aGeoCoordinateObj					= this.geoCoordinates[postCodeStr];
          	dateStr								= aGeoCoordinateObj.jobDate;
        }
      	return dateStr;
    };
}

/** Class definition - CloseWorkData **/
function closeWorkData(){
  	this.businessCode 					= PCConstant.DEF_BUSINESS_CODE;
    this.countryCode 					= PCConstant.DEF_COUNTRY_CODE;
    this.email 							= "";
  	this.languageCode					= PCConstant.DEF_LANGUAGE_CODE;
  
  	this.works							= [];
  	this.workAttendances				= [];
  	this.workOrders						= [];
  	this.workPoints						= [];
  	this.treatments						= {};
  
  	this.processedWorkIds				= [];
  	this.succeededWorkIds				= [];
  	this.failedWorkIds					= [];
  
  	this.getTreatmentCol				= function(workPointIdRef) {
      	var treatmentObjs				= this.treatments[workPointIdRef];
      	return treatmentObjs;	
    };
}

/** Class definition - GeoCoordinate **/
function GeoCoordinate(latitude, longitude, dateStr, nameStr, addressStr, propertyCodeStr) {
  	this.latitude						= latitude;
  	this.longitude						= longitude;
  	
    this.jobDate						= (!isEmpty(dateStr)) ? dateStr : "";
    this.name							= (!isEmpty(nameStr)) ? nameStr : "";
    this.address						= (!isEmpty(addressStr)) ? addressStr : "";
    this.propertyCode					= (!isEmpty(propertyCodeStr)) ? propertyCodeStr : "";
}