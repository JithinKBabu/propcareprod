
/** Show customer screen **/
function showCustomerScreen(){	
  	Customer.show();
}

/** Customer screen - PreShow **/
function onPreShowCustomerScreen(){
  	printMessage(" Inside - onPreShowCustomerScreen ");
  	onCancelSRASelection();
  	Customer.tabCustomerDetails.activeTabs					= [0];
  	Customer.tabCustomerDetails.txaSiteRiskAssessment.setEnabled(false); 
  	Customer.tabCustomerDetails.txaServiceNotes.setEnabled(false); 
  	Customer.tabCustomerDetails.flxSelectSRA.skin 			= "flxGrayBgWithBorderSkin";
    Customer.tabCustomerDetails.flxSelectSRA.setEnabled(false);
    Customer.tabCustomerDetails.imgEditAndDone.src			= "edit.png";
  	Customer.tabCustomerDetails.txaSiteRiskAssessment.text	= "";
  	refreshi18nCustomer();
}

/** Customer screen - PostShow **/
function onPostShowCustomerScreen(){
  	printMessage(" Inside - onPostShowCustomerScreen ");
  	showLoading("common.message.loading");
  	refreshSettingsUI();
  	
  	if (gblMyAppDataObject.isDayStarted === PCConstant.DAY_STARTED_DONT_KNOW || gblMyAppDataObject.isDayStarted === PCConstant.DAY_STARTED_NO) {
      	Customer.imgStartdayIcon.src					= "endday.png";
    } else {
      	Customer.imgStartdayIcon.src					= "startday.png";
    }
  
  	var selScheduledItem								= gblMyAppDataObject.selScheduledItem;
  	Customer.lblPropertyName.text						= selScheduledItem.propertyName;
    Customer.tabCustomerDetails.lblCustomerNumber.text 	= selScheduledItem.contractTypeCode + "/" + selScheduledItem.contractCode;
    Customer.tabCustomerDetails.lblAddress.text 		= selScheduledItem.propertyAddress;
    Customer.tabCustomerDetails.lblPostcode.text		= selScheduledItem.propertyPostcode;
  
  	if (!isEmpty(selScheduledItem.propertyContactName)) {
       	Customer.tabCustomerDetails.lblName.text		= selScheduledItem.propertyContactName;
      	Customer.tabCustomerDetails.lblName.skin		= lblBlk100;
    } else {
      	Customer.tabCustomerDetails.lblName.text		= getI18nString("common.label.notAvailable");
      	Customer.tabCustomerDetails.lblName.skin		= lblNotAvailableSkin;
    }
  
  	if (!isEmpty(selScheduledItem.propertyContactPosition)) {
       	Customer.tabCustomerDetails.lblDesingnation.text= selScheduledItem.propertyContactPosition;
      	Customer.tabCustomerDetails.lblDesingnation.skin= lblBlk100;
    } else {
      	Customer.tabCustomerDetails.lblDesingnation.text= getI18nString("common.label.notAvailable");
      	Customer.tabCustomerDetails.lblDesingnation.skin= lblNotAvailableSkin;
    }

  //	if (!isEmpty(selScheduledItem.gpsCoordinateX) && !isEmpty(selScheduledItem.gpsCoordinateY)) {
    if ((!isEmpty(selScheduledItem.propertyPostcode)) || (!isEmpty(selScheduledItem.gpsCoordinateX) && !isEmpty(selScheduledItem.gpsCoordinateY))) {
        Customer.tabCustomerDetails.imgLocIcon.src		= "navigate.png";
        Customer.tabCustomerDetails.flxAddressLeftContainer.setEnabled(true);
    } else {
      	Customer.tabCustomerDetails.imgLocIcon.src		= "navigatedisabled.png";
        Customer.tabCustomerDetails.flxAddressLeftContainer.setEnabled(false);
    }
    
    if (!isEmpty(selScheduledItem.propertyContactTelephone)) {
        Customer.tabCustomerDetails.lblPhone.text		= selScheduledItem.propertyContactTelephone;
      	Customer.tabCustomerDetails.lblPhone.skin		= phoneSkin;
      	Customer.tabCustomerDetails.imgPhoneIcon.src	= "call.png";
        Customer.tabCustomerDetails.flxPhoneContainer.setEnabled(true);
    } else {
        Customer.tabCustomerDetails.lblPhone.text		= getI18nString("common.label.notAvailable");
      	Customer.tabCustomerDetails.lblPhone.skin		= lblNotAvailableSkin;
      	Customer.tabCustomerDetails.imgPhoneIcon.src	= "calldisabled.png";
        Customer.tabCustomerDetails.flxPhoneContainer.setEnabled(false);
    }

    if (!isEmpty(selScheduledItem.propertyContactMobile)) {
        Customer.tabCustomerDetails.lblMobile.text		= selScheduledItem.propertyContactMobile;
      	Customer.tabCustomerDetails.lblMobile.skin		= phoneSkin;
      	Customer.tabCustomerDetails.imgMobileIcon.src	= "mobile.png";
      	Customer.tabCustomerDetails.flxMobileContainer.setEnabled(true);
    } else {
        Customer.tabCustomerDetails.lblMobile.text		= getI18nString("common.label.notAvailable");
      	Customer.tabCustomerDetails.lblMobile.skin		= lblNotAvailableSkin;
      	Customer.tabCustomerDetails.imgMobileIcon.src	= "mobiledisabled.png";
        Customer.tabCustomerDetails.flxMobileContainer.setEnabled(false);
    }

    if (!isEmpty(selScheduledItem.propertyContactEmail)) {
        Customer.tabCustomerDetails.lblEmail.text		= selScheduledItem.propertyContactEmail;
      	Customer.tabCustomerDetails.lblEmail.skin		= lblEmailSkin;
      	Customer.tabCustomerDetails.imgEmailIcon.src	= "email.png";
        Customer.tabCustomerDetails.flxEmailContainer.setEnabled(true);
    } else {
        Customer.tabCustomerDetails.lblEmail.text		= getI18nString("common.label.notAvailable");
      	Customer.tabCustomerDetails.lblEmail.skin		= lblNotAvailableSkin;
      	Customer.tabCustomerDetails.imgEmailIcon.src	= "emaildisabled.png";
        Customer.tabCustomerDetails.flxEmailContainer.setEnabled(false);
    }
  
  	if (!isEmpty(selScheduledItem.propertySpecialInstructions)) {
      	Customer.tabCustomerDetails.txaSiteRiskAssessment.text	= selScheduledItem.propertySpecialInstructions; 
    } 
  
  	if (!isEmpty(selScheduledItem.propertyServiceNote)) {
      	Customer.tabCustomerDetails.txaServiceNotes.text		= selScheduledItem.propertyServiceNote; 
    } 
  
  	handleUserInteractionOnCustomerTextFields(Customer.tabCustomerDetails.txaSiteRiskAssessment);
    handleUserInteractionOnCustomerTextFields(Customer.tabCustomerDetails.txaServiceNotes);
  printMessage("gblMyAppDataObject.historyDatesList: " + JSON.stringify(gblMyAppDataObject.historyDatesList));
  	printMessage(" gblMyAppDataObject.historyObjects " + JSON.stringify( gblMyAppDataObject.historyObjects));
  	loadDataOnHistorySegment(gblMyAppDataObject.historyDatesList, gblMyAppDataObject.historyObjects);
  	printMessage("gblMyAppDataObject.serviceCoverCodeList: " + JSON.stringify(gblMyAppDataObject.serviceCoverCodeList));
  	printMessage("gblMyAppDataObject.serviceCoverObjects: " + JSON.stringify(gblMyAppDataObject.serviceCoverObjects));
 	loadDataOnServiceCoverSegment(gblMyAppDataObject.serviceCoverCodeList, gblMyAppDataObject.serviceCoverObjects);
  	dismissLoading();
  	destroyFormsExceptMe(PCConstant.FORM_CUSTOMER); /** Issue fix - Swipe operation on Home segment doesn't work if it is not destroyed here **/
}

/** Customer screen - Refresh all i18n **/
function refreshi18nCustomer(){
  	Customer.lblHeaderTitle.text						= getI18nString("common.label.customer");  	
  	Customer.tabCustomerDetails.tabCustomerInfo			= prefixWhiteSpace(getI18nString("common.tab.customerinformation"), 2);
  	Customer.tabCustomerDetails.tabRiskAsst				= prefixWhiteSpace(getI18nString("common.tab.siteriskassessment"), 2);
  	Customer.tabCustomerDetails.tabSplCovers			= prefixWhiteSpace(getI18nString("common.tab.serviceNotes"), 2);
  	Customer.tabCustomerDetails.tabSummary				= prefixWhiteSpace(getI18nString("common.tab.serviceCovers"), 2);
  	Customer.tabCustomerDetails.tabHistory				= prefixWhiteSpace(getI18nString("common.tab.history"), 2);
  
  	var sracmbData										= [];
  	if (gblSRAList.length > 0) {
      	for (i = 0; i < gblSRAList.length; i++){
          	anItem										= gblSRAList[i];
          	sracmbData.push([anItem.key, getI18nString(anItem.val)]);
        }
    }
  	Customer.checkBxSRA.masterData						= sracmbData;
  	Customer.tabCustomerDetails.lblSelectSRA.text		= getI18nString("common.label.select");
  	Customer.tabCustomerDetails.lblNoServiceCovers.text	= getI18nString("common.message.noRecords");
  	Customer.tabCustomerDetails.lblNoHistory.text		= getI18nString("common.message.noRecords");
}

/** Customer screen - Location click action **/
function onClickCustomerLoc(){
  	printMessage(" Inside onClickCustomerLoc ");
  	var selScheduledItem							= gblMyAppDataObject.selScheduledItem;
    if(!isEmpty(selScheduledItem.propertyPostcode)){
      	////findMapCordinates(selScheduledItem.propertyPostcode);
     	//myPostCodeCordinatesURL(selScheduledItem.propertyPostcode);
      	var existingCoordinate							= gblMyAppDataObject.geoCoordinates[selScheduledItem.propertyPostcode];
        if (isEmpty(existingCoordinate.latitude) || isEmpty(existingCoordinate.longitude)) {
      		CustomGoogleMapAPI(selScheduledItem.propertyPostcode, "LOCATION_ROUTE", "", "");
        } else {
          	openGoogleMap(existingCoordinate.latitude, existingCoordinate.longitude);
        }
    } else {   
        if(!isEmpty(selScheduledItem.gpsCoordinateX) && !isEmpty(selScheduledItem.gpsCoordinateY)){
          	openGoogleMap(selScheduledItem.gpsCoordinateY, selScheduledItem.gpsCoordinateX);
        } else {
            printMessage(" No GPS coordinates and post code");
        } 
    }
}

/** Customer screen - Phone click action **/
function onClickCustomerPhone(){
  	printMessage(" Inside onClickCustomerPhone ");
  	var selScheduledItem							= gblMyAppDataObject.selScheduledItem;
  	if (!isEmpty(selScheduledItem) && !isEmpty(selScheduledItem.propertyContactTelephone)) {
      	showLoading("common.message.loading");
  		var phoneNumber								= selScheduledItem.propertyContactTelephone;		
        if(!isEmpty(phoneNumber)){
            kony.phone.dial(phoneNumber);
        } else {
            printMessage(" No Phone number ");
        }
      	dismissLoading();
    } 
}

/** Customer screen - Mobile click action **/
function onClickCustomerMobile(){
  	printMessage(" Inside onClickCustomerMobile ");
  	var selScheduledItem							= gblMyAppDataObject.selScheduledItem;
  	if (!isEmpty(selScheduledItem) && !isEmpty(selScheduledItem.propertyContactMobile)) {
      	showLoading("common.message.loading");
  		var phoneNumber								= selScheduledItem.propertyContactMobile;		
        if(!isEmpty(phoneNumber)){
            kony.phone.dial(phoneNumber);
        } else {
            printMessage(" No Mobile number ");
        }
      	dismissLoading();
    } 
}

/** Customer screen - Email click action **/
function onClickCustomerEmail(){
  	printMessage(" Inside onClickCustomerEmail ");
  	try {
      	var selScheduledItem						= gblMyAppDataObject.selScheduledItem;
  		if (!isEmpty(selScheduledItem) && !isEmpty(selScheduledItem.propertyContactEmail)) {
          	showLoading("common.message.loading");
          	sendMail(selScheduledItem.propertyContactEmail);
          	dismissLoading();
		}
	} catch(e){
		displayError(e);
	}
}

/** Customer screen - On click SRA select button **/
function onSRASelectBtnAction(){  
  	printMessage("onSRASelectBtnAction ");
  	Customer.checkBxSRA.selectedKeys 				= null;
  	Customer.flxSRAContainer.setVisibility(true);
}

/** Customer screen - Edit button action **/
function doHandleSRAEditButton(){
  	printMessage("doHandleSRAEditButton - edit btn img: " + imgSrc);
  	var imgSrc											= Customer.tabCustomerDetails.imgEditAndDone.src;
  	if (imgSrc == "edit.png") {
    	Customer.tabCustomerDetails.flxSelectSRA.skin 	= "flxWhiteBgWithBorderSkin";
    	Customer.tabCustomerDetails.flxSelectSRA.setEnabled(true);
  		Customer.tabCustomerDetails.txaSiteRiskAssessment.setEnabled(true);
      	Customer.tabCustomerDetails.imgEditAndDone.src	= "done.png";
  	} else {
      	// Need to save in local db
      	var selScheduledItem							= gblMyAppDataObject.selScheduledItem;
      	var updatedSRA									= Customer.tabCustomerDetails.txaSiteRiskAssessment.text.trim();
      	updateSRAInLocalTbl(updatedSRA);
      	gblMyAppDataObject.selScheduledItem.propertySpecialInstructions	= updatedSRA;
      	printMessage("gblMyAppDataObject.selScheduledItem" + JSON.stringify(gblMyAppDataObject.selScheduledItem));
      	printMessage("gblMyAppDataObject.scheduledObjects" + JSON.stringify(gblMyAppDataObject.scheduledObjects));      
      	Customer.tabCustomerDetails.flxSelectSRA.skin 	= "flxGrayBgWithBorderSkin";
    	Customer.tabCustomerDetails.flxSelectSRA.setEnabled(false);
  		Customer.tabCustomerDetails.txaSiteRiskAssessment.setEnabled(false);
      	Customer.tabCustomerDetails.imgEditAndDone.src	= "edit.png";
    }
}

/** Customer screen - Hide SRA selections **/
function onCancelSRASelection(){    	
  	printMessage("onCancelSRASelection ");
  	Customer.flxSRAContainer.setVisibility(false);
  	Customer.checkBxSRA.selectedKeys 					= null;  	
}

/** Customer screen - On confirm SRA checkbox selections **/
function onConfirmSRASelection(){
  	printMessage("onConfirmSRASelection ");
  	showLoading("common.message.loading");
	var selectedValues 											= Customer.checkBxSRA.selectedKeyValues;
  	var currentTextAreaVal										= Customer.tabCustomerDetails.txaSiteRiskAssessment.text;
  	var additionalTextAreaVal									= "";
  	var aSelectedItem											= null;
	if(!isEmpty(selectedValues)){
		for(var i = 0; i < selectedValues.length; i++){
			aSelectedItem										= selectedValues[i][1];
          	if (!isEmpty(currentTextAreaVal)) {
              	if (currentTextAreaVal.indexOf(aSelectedItem) == -1){
                  	additionalTextAreaVal						+= aSelectedItem + "\n";
                }
            } else {
              	additionalTextAreaVal							+= aSelectedItem + "\n";
            }
		}
	}
  
  	if (!isEmpty(additionalTextAreaVal) && !isEmpty(currentTextAreaVal)){
      	currentTextAreaVal										= currentTextAreaVal + "\n" + additionalTextAreaVal;
      	toggleCustomerFormPlaceHolders(false, "SITE_RISK_ASSESSMENT");
    } else if (!isEmpty(additionalTextAreaVal) && isEmpty(currentTextAreaVal)){
      	currentTextAreaVal										= additionalTextAreaVal + "\n" ;
      	toggleCustomerFormPlaceHolders(false, "SITE_RISK_ASSESSMENT");
    }
  
  	Customer.tabCustomerDetails.txaSiteRiskAssessment.text		= currentTextAreaVal;
  	onCancelSRASelection();
  	dismissLoading();
}

/** Textbox / Textarea begin or end editing callback handlers **/
function handleUserInteractionOnCustomerTextFields(widgetRef){
  	printMessage(" Inside - handleUserInteractionOnCustomerTextFields ");
  	var curInputVal				= "";
  	if (widgetRef.id == "txaSiteRiskAssessment") {
      	curInputVal				= Customer.tabCustomerDetails.txaSiteRiskAssessment.text;
      	if (isEmpty(curInputVal)) {
          	toggleCustomerFormPlaceHolders(true, "SITE_RISK_ASSESSMENT");
        } else {
          	toggleCustomerFormPlaceHolders(false, "SITE_RISK_ASSESSMENT");
        }
    } else if (widgetRef.id == "txaServiceNotes") {
      	curInputVal				= Customer.tabCustomerDetails.txaServiceNotes.text;
      	if (isEmpty(curInputVal)) {
          	toggleCustomerFormPlaceHolders(true, "SERVICE_NOTE");
        } else {
          	toggleCustomerFormPlaceHolders(false, "SERVICE_NOTE");
        }
    }
}

/** To toggle placeholder labels on Customer screen **/
function toggleCustomerFormPlaceHolders(addPlaceholder, fieldName){
  	printMessage(" Inside toggleCustomerFormPlaceHolders - addPlaceholder & fieldName " + addPlaceholder + " and " + fieldName);
  	if (addPlaceholder === true) {
      	if (fieldName === "SERVICE_NOTE") {
          	Customer.tabCustomerDetails.lblServiceNotesPlaceholder.text	= getI18nString("common.label.notAvailable");
        } else if (fieldName === "SITE_RISK_ASSESSMENT") {
          	Customer.tabCustomerDetails.lblSRAPlaceholder.text			= getI18nString("common.label.notAvailable");
        }
    	
    } else {
      	if (fieldName === "SERVICE_NOTE") {
          	Customer.tabCustomerDetails.lblServiceNotesPlaceholder.text	= "";
        } else if (fieldName === "SITE_RISK_ASSESSMENT") {
          	Customer.tabCustomerDetails.lblSRAPlaceholder.text			= "";
        }
      
    }  	
}

/** Success callback - Get work history from local db */ 
function success_getWorkHistory(resultset){
	printMessage("Inside success_gethistory - work and service cover - " + JSON.stringify(resultset));
  	
  	if (!isEmpty(resultset) && resultset.length > 0) {
      	var recordsCount					= resultset.length;
      	var datewiseSet						= {};
      	var aRecord							= null;
      	var dateKey							= null;
      	var childItems						= [];
      	var newObject						= null;
      	var dateStrArray					= [];
      	for (var i = 0; i < recordsCount; i++) {
          	aRecord							= resultset[i];
          	newObject						= {};
           	newObject.workDate       	 	= aRecord.workDateStartActual;
            newObject.employeeCode   		= aRecord.employeeCodePrimary;
            newObject.employeename   		= aRecord.employeeName;
     
          	if (!isEmpty(aRecord.workDateStartActual)) {
            	dateKey						= aRecord.workDateStartActual;
                if(isEmpty(datewiseSet[dateKey])){
					childItems				= [];
                  	dateStrArray.push(dateKey);
                } else {
                  	childItems				= datewiseSet[dateKey];
                }
              	childItems.push(newObject);
              	datewiseSet[dateKey]		= childItems;
            }
        }

      	var sortedDateStrArray				= dateStrArray.sort(sortCompare);
      	gblMyAppDataObject.historyObjects	= datewiseSet;
    	gblMyAppDataObject.historyDatesList = sortedDateStrArray;
      	getServiceCoverFromLocalTbl();
    } else {
      	printMessage("&&&& success_getWorkHistory - No records found");
       //fix for repeated history for the previous contract value
        gblMyAppDataObject.historyObjects={};
        gblMyAppDataObject.historyDatesList=[];
      	getServiceCoverFromLocalTbl();
    }
}

/** Failure callback - Get work history from local db */
function error_getWorkHistory(){
  	printMessage("Inside error_getWorkHistory");
  	getServiceCoverFromLocalTbl();
}

/** To load data on History segment **/
function loadDataOnHistorySegment(dateStrArray, dataSetArray){
  	printMessage("Inside loadDataOnHistorySegment");
  	var segData								= [];    
  	if (!isEmpty(dateStrArray) && dateStrArray.length > 0) {
      	var arrayItemsCount					= dateStrArray.length; 
      	var datewiseSet						= dataSetArray;
        var segSectionData					= [];
        var segRowData						= [];
        var aHeaderRecord					= null;    
        var sectionRowObjects				= null;
        var aRowObject						= null;
        var aRowRecord						= null;
        var dateStrKey						= null;

        for (var j = 0; j < arrayItemsCount; j++) {
          	dateStrKey						= dateStrArray[j];
          	segSectionData					= [];
          	segRowData						= [];
          	aHeaderRecord					= {};         
          	aHeaderRecord.lblTitle 			= expandDateStr(dateStrKey);
          	sectionRowObjects				= datewiseSet[dateStrKey];	
          	if (sectionRowObjects.length > 0) {
            	segSectionData.push(aHeaderRecord);
                for (var k = 0; k < sectionRowObjects.length; k++) {
                    aRowObject				= sectionRowObjects[k];
                    aRowRecord				= {};
                    aRowRecord.lblTop		= aRowObject.employeename;
                    segRowData.push(aRowRecord);
                }
            	segSectionData.push(segRowData); 
          	}
          	segData.push(segSectionData); 
        }
      	if(!gblCompletedVisitFlag){
      	Customer.tabCustomerDetails.lblNoHistory.setVisibility(false);
        }else{
         CompletedVisit.tabCompletedCustomerDetails.lblNoHistory.setVisibility(false)   ;
        }
    } else {
       if(!gblCompletedVisitFlag){
      	Customer.tabCustomerDetails.lblNoHistory.setVisibility(true);
       }else{
        CompletedVisit.tabCompletedCustomerDetails.lblNoHistory.setVisibility(true)   ; 
       }
    }
    if(!gblCompletedVisitFlag){
  	Customer.tabCustomerDetails.segCustomerHistory.setData(segData);
    }else{
     CompletedVisit.tabCompletedCustomerDetails.segCustomerHistory.setData(segData)  ;   
    }
}
 
/** Success callback - Get service history from local db */ 
function success_getServiceCover(resultset){
	printMessage("Inside success_getServiceCover - work and service cover - " + JSON.stringify(resultset));
  	if (!isEmpty(resultset) && resultset.length > 0) {
      	printMessage("resultset.length " + resultset.length);
      	var recordsCount							= resultset.length;
      	var serviceCoverCodeObjects					= {};
      	var aRecord									= null;
      	var slCodeKey								= null;
      	var childItems								= [];
      	var newObject								= null;
      	var slCodeArray								= [];
      	for (var i = 0; i < recordsCount; i++) {
          	aRecord									= resultset[i];
          	newObject								= {};
            newObject.serviceLineDescription		= aRecord.serviceLineDescription;
            newObject.serviceSpecialInstructions	= aRecord.serviceSpecialInstructions;
     
          	if (!isEmpty(aRecord.serviceLineCode)) {
            	slCodeKey							= aRecord.serviceLineCode;
                if(isEmpty(serviceCoverCodeObjects[slCodeKey])){
					childItems						= [];
                  	slCodeArray.push(slCodeKey);
                } else {
                  	childItems						= serviceCoverCodeObjects[slCodeKey];
                }
              	childItems.push(newObject);
              	serviceCoverCodeObjects[slCodeKey]	= childItems;
            }
        }
		  
    	gblMyAppDataObject.serviceCoverObjects      = serviceCoverCodeObjects;
    	gblMyAppDataObject.serviceCoverCodeList	   	= slCodeArray;
      
       	//Setting global variable for identifying the screen
      	if(!gblCompletedVisitFlag){
         	showCustomerScreen();
       	}else{
            showCompleteVisitScreen();
        }
    } else {
      	printMessage("success_getServiceCover - No records found");
       if(!gblCompletedVisitFlag){
         	showCustomerScreen();
         }else{
            showCompleteVisitScreen();
         }
    }
}

/** Failure callback - Get service Cover from local db */
function error_getServiceCover(){
  	printMessage("Inside error_getServiceCover");
  	showCustomerScreen();
}

/** To load data on home segment */
function loadDataOnServiceCoverSegment(serviceCoverCodeList, dataSetArray){
  	printMessage("Inside loadDataOnServiceCoverSegment");
  	var segData								= [];
  	if (!isEmpty(serviceCoverCodeList) && serviceCoverCodeList.length > 0) {
      	var dateStrArray                    = serviceCoverCodeList;
        var arrayItemsCount					= dateStrArray.length; 
        var segSectionData					= [];
        var segRowData						= [];
        var aHeaderRecord					= null;    
        var sectionRowObjects				= null;
        var aRowObject						= null;
        var aRowRecord						= null;
        var dateStrKey						= null;
        for (var j = 0; j < arrayItemsCount; j++) {
            dateStrKey						= serviceCoverCodeList[j];
            segSectionData					= [];
            segRowData						= [];
            aHeaderRecord					= {};         
            aHeaderRecord.lblTitle 			= dateStrKey;
            sectionRowObjects				= dataSetArray[dateStrKey];	
            if (sectionRowObjects.length > 0) {
              	segSectionData.push(aHeaderRecord);
              	for (var k = 0; k < sectionRowObjects.length; k++) {
                	aRowObject				= sectionRowObjects[k];
                	aRowRecord				= {};
                	aRowRecord.lblTop		= aRowObject.serviceLineDescription;
                	aRowRecord.lblBottom	= aRowObject.serviceSpecialInstructions;
                	segRowData.push(aRowRecord);
              	}
              	segSectionData.push(segRowData); 
            }
          	segData.push(segSectionData);
      	}
      //condition to check the screen navigation
       if(!gblCompletedVisitFlag){
      	Customer.tabCustomerDetails.lblNoServiceCovers.setVisibility(false);
         }else{
           CompletedVisit.tabCompletedCustomerDetails.lblNoServiceCovers.setVisibility(false);
         }
    } else {
         if(!gblCompletedVisitFlag){
      	Customer.tabCustomerDetails.lblNoServiceCovers.setVisibility(true);
             }else{
         CompletedVisit.tabCompletedCustomerDetails.lblNoServiceCovers.setVisibility(true);  
         }
    } 
     if(!gblCompletedVisitFlag){
  	Customer.tabCustomerDetails.segService.setData(segData);
           }else{
    CompletedVisit.tabCompletedCustomerDetails.segService.setData(segData);
         }
}