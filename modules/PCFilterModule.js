
function onFilterPagePostShow(){

  //onC
}

function onClickFlxKeyAccnt(){
  kony.print("Inside onSelectChkBxKeyAccnt ::::");
  if(Filter.tabpaneFilter.imgAllAccFil.src == "uncheckbox.png"){
    Filter.tabpaneFilter.imgAllAccFil.src = "checkbox.png";
    Filter.tabpaneFilter.checkBxKeyAct.selectedKeys = ["0","1"];
  }else{
    Filter.tabpaneFilter.imgAllAccFil.src = "uncheckbox.png";
    Filter.tabpaneFilter.checkBxKeyAct.selectedKeys = null;
  }
}

function onSelectChkBxKeyAccnt(){
  kony.print("Inside onSelectChkBxKeyAccnt ::::");
  var selAccFil = Filter.tabpaneFilter.checkBxKeyAct.selectedKeyValues;
  if(!isEmpty(selAccFil)){
    if(selAccFil.length == 2){
      Filter.tabpaneFilter.imgAllAccFil.src = "checkbox.png";
    }else{
      Filter.tabpaneFilter.imgAllAccFil.src = "uncheckbox.png";
    }
  }
}

//==============ContactType==============

function onClickFlxContactType(){
  kony.print("Inside onSelectChkBxKeyAccnt ::::");

  if(Filter.tabpaneFilter.imgAllConType.src == "uncheckbox.png"){
    Filter.tabpaneFilter.imgAllConType.src = "checkbox.png";
    Filter.tabpaneFilter.checkBxContentType.selectedKeys = ["0","1","2"];
  }else{
    Filter.tabpaneFilter.imgAllConType.src = "uncheckbox.png";
    Filter.tabpaneFilter.checkBxContentType.selectedKeys = null;
  }
}

function onSelectChkBxContractType(){
  kony.print("Inside onSelectChkBxContractType ::::");
  var selConTypeFil = Filter.tabpaneFilter.checkBxContentType.selectedKeyValues;
  if(!isEmpty(selConTypeFil)){
    if(selConTypeFil.length == 3){
      Filter.tabpaneFilter.imgAllConType.src = "checkbox.png";
    }else{
      Filter.tabpaneFilter.imgAllConType.src = "uncheckbox.png";
    }
  }
}
//==============ServiceCover()==============
function onClickFlxServiceCover(){
  kony.print("Inside onClickHbxServiceCover ::::");

  if(Filter.tabpaneFilter.imgAllServiceCover.src == "uncheckbox.png"){
    Filter.tabpaneFilter.imgAllServiceCover.src = "checkbox.png";
    Filter.tabpaneFilter.checkBxServiceCvr.selectedKeys = ["0","1","2","3","4","5","6","7","8"];
  }else{
    Filter.tabpaneFilter.imgAllServiceCover.src = "uncheckbox.png";
    Filter.tabpaneFilter.checkBxServiceCvr.selectedKeys = null;
  }
}

function onSelectChkBxServiceCover(){
  kony.print("Inside onSelectChkBxServiceCover ::::");
  var selServiceCoverStatusFil = Filter.tabpaneFilter.checkBxServiceCvr.selectedKeyValues;
  if(!isEmpty(selServiceCoverStatusFil)){
    if(selServiceCoverStatusFil.length == 9){
      Filter.tabpaneFilter.imgAllServiceCover.src = "checkbox.png";
    }else{
      Filter.tabpaneFilter.imgAllServiceCover.src = "uncheckbox.png";
    }
  }
}

//==============Visit Type==============
function onClickFlxVisitType(){
  kony.print("Inside onClickFlxVisitType ::::");

  if(Filter.tabpaneFilter.imgAllVisitType.src == "uncheckbox.png"){
    Filter.tabpaneFilter.imgAllVisitType.src = "checkbox.png";
    Filter.tabpaneFilter.checkBxVisitype.selectedKeys = ["0","1","2"];
  }else{
    Filter.tabpaneFilter.imgAllVisitType.src = "uncheckbox.png";
    Filter.tabpaneFilter.checkBxVisitype.selectedKeys = null;
  }
}

function onSelectChkBxVisitType(){
  kony.print("Inside onSelectChkBxContractType ::::");
  var selConTypeFil = Filter.tabpaneFilter.checkBxVisitype.selectedKeyValues;
  if(!isEmpty(selConTypeFil)){
    if(selConTypeFil.length == 3){
      Filter.tabpaneFilter.imgAllVisitType.src = "checkbox.png";
    }else{
      Filter.tabpaneFilter.imgAllVisitType.src = "uncheckbox.png";
    }
  }
}
//==============Add Filter==============
function onClickFlxAddFilter(){
  kony.print("Inside onClickFlxVisitType ::::");

  if(Filter.tabpaneFilter.imgAllAddFilter.src == "uncheckbox.png"){
    Filter.tabpaneFilter.imgAllAddFilter.src = "checkbox.png";
    Filter.tabpaneFilter.chkbxAddFilter.selectedKeys = ["0","1"];
  }else{
    Filter.tabpaneFilter.imgAllAddFilter.src = "uncheckbox.png";
    Filter.tabpaneFilter.chkbxAddFilter.selectedKeys = null;
  }
}

function onSelectChkBxFlxAddFilter(){
  kony.print("Inside onSelectChkBxContractType ::::");
  var selConTypeFil = Filter.tabpaneFilter.chkbxAddFilter.selectedKeyValues;
  if(!isEmpty(selConTypeFil)){
    if(selConTypeFil.length == 2){
      Filter.tabpaneFilter.imgAllAddFilter.src = "checkbox.png";
    }else{
      Filter.tabpaneFilter.imgAllAddFilter.src = "uncheckbox.png";
    }
  }
}

function onClickClearFilter(){
  try{
    kony.print("Inside onClickClearFilter::::");
    //Key Accounts
    Filter.tabpaneFilter.imgAllAccFil.src = "uncheckbox.png";
    Filter.tabpaneFilter.checkBxKeyAct.selectedKeys = null;
    //ContactType
    Filter.tabpaneFilter.imgAllConType.src = "uncheckbox.png";
    Filter.tabpaneFilter.checkBxContentType.selectedKeys = null;
    //ServiceCover
    Filter.tabpaneFilter.imgAllServiceCover.src = "uncheckbox.png";
    Filter.tabpaneFilter.checkBxServiceCvr.selectedKeys = null;
    //Visit Type
    Filter.tabpaneFilter.imgAllVisitType.src = "uncheckbox.png";
    Filter.tabpaneFilter.checkBxVisitype.selectedKeys = null;
    //Add Filters
    Filter.tabpaneFilter.imgAllAddFilter.src = "uncheckbox.png";
    Filter.tabpaneFilter.chkbxAddFilter.selectedKeys = null;

  } catch (e) {
    //displayError(e);
  }	
}

function onClickSelectAllFilters(){
  try{
    kony.print("Inside onClickSelectAllFilters::::");
    Filter.tabpaneFilter.checkBxKeyAct.selectedKeys = ["0","1"];
    Filter.tabpaneFilter.imgAllAccFil.src = "checkbox.png";

    Filter.tabpaneFilter.checkBxContentType.selectedKeys = ["0","1","2"];
    Filter.tabpaneFilter.imgAllConType.src = "checkbox.png";

    Filter.tabpaneFilter.checkBxServiceCvr.selectedKeys = ["0","1","2","3","4","5","6","7","8"];
    Filter.tabpaneFilter.imgAllServiceCover.src = "checkbox.png";

    Filter.tabpaneFilter.checkBxVisitype.selectedKeys = ["0","1","2"];
    Filter.tabpaneFilter.imgAllVisitType.src = "checkbox.png";

    Filter.tabpaneFilter.chkbxAddFilter.selectedKeys = ["0","1"];
    Filter.tabpaneFilter.imgAllAddFilter.src = "checkbox.png";

  } catch (e) {
    //displayError(e);
  }
}
function onClickGoFiltersSchedule(){
}
