/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer the geo location based methods
***********************************************************************/

/** To capture current location **/
/** timeout = 5 seconds and cached location age = 10 minutes **/
function captureCurrentLocation(successCallbackHandler, errorCallbackHandler){
  	var positionOptions 						= {timeout: 5000, maximumAge: 600000};   	
  	kony.location.getCurrentPosition(successCallbackHandler, errorCallbackHandler, positionOptions);
}

/** Success callback locaion capture **/
function success_geoLocCallback(position) {
  	//position.coords.latitude;
  	//position.coords.longitude;  
  	dismissLoading();
}

/** Failure callback locaion capture  **/
function error_geoLocCallback(positionerror){
  	//positionerror.message;
  	/*if (positionerror.message == PCConstant.LOCATION_UNAVAILABLE || positionerror.message == PCConstant.LOCATION_TIMEOUT || positionerror.message == PCConstant.LOCATION_PERMSN_DENIED) {
      	showMessagePopup("common.message.turnOnLocation", ActionPopupMessageOk);
    }*/
  	dismissLoading();
}

/** Success callback locaion capture - At start visit **/
function success_geoLocCallbackStartVisit(position) {
  	//position.coords.latitude;
  	//position.coords.longitude;  
}

/** Failure callback locaion capture - At start visit **/
function error_geoLocCallbackStartVisit(positionerror){
  	//positionerror.message;  
  	/*if (positionerror.message == PCConstant.LOCATION_UNAVAILABLE || positionerror.message == PCConstant.LOCATION_TIMEOUT || positionerror.message == PCConstant.LOCATION_PERMSN_DENIED) {
      	showMessagePopup("common.message.turnOnLocation", ActionPopupMessageOk);
    }*/
}

/** Success callback locaion capture - At stop visit **/
function success_geoLocCallbackStopVisit(position) {
  	//position.coords.latitude;
  	//position.coords.longitude;  	  
}

/** Failure callback locaion capture - At stop visit **/
function error_geoLocCallbackStopVisit(positionerror){
  	//positionerror.message;  
  	/*if (positionerror.message == PCConstant.LOCATION_UNAVAILABLE || positionerror.message == PCConstant.LOCATION_TIMEOUT || positionerror.message == PCConstant.LOCATION_PERMSN_DENIED) {
      	showMessagePopup("common.message.turnOnLocation", ActionPopupMessageOk);
    }*/
}