/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all global variables and methods
***********************************************************************/

/** Application - Pre app init call **/
function onPreAppInit(){
  	getCurrentDevLocale();	
  	setApplicationBehaviors();
}

/** Application - Post app init call **/
function onPostAppInit(){
  	printMessage("Inside -  onPostAppInit");
  	initializeGlobals();
  	gblDeviceInfo						= kony.os.deviceInfo();  
}

/** Initializing global variables **/
function initializeGlobals(){
  	printMessage("Inside InitializeGlobals"); 
  	/** APP Configurations Starts **/
  	gblServerType						= "ON_CLOUD";  	// "ON_PREMISE" or "ON_CLOUD"
  	gblBuildMode						= "STAGING";  		// "SIT" or "DEV" or "TESTING" or "STAGING"	
  	gblAppMode							= "PRODUCTION"; 	// ["DEVELOPER", "PRODUCTION"]
  	gblAddEncodeURI						= true;			// [true, false]  For OnPremise assign it as false and for Cloud assign it as true
  
	gblSyncConfiguration				= {};											  
	gblSyncConfiguration.ON_PREMISE		= {	"DEV"		: {SYNC_APP_ID: "", SYNC_SERVER_HOST: "", SYNC_SERVER_PORT: "", SYNC_IS_SECURE: false}, 
                                            "SIT"		: {SYNC_APP_ID: "100004898d072a0d5", SYNC_SERVER_HOST: "wklxkonyappdv05.uk.ri.ad", SYNC_SERVER_PORT: "8080", SYNC_IS_SECURE: false},
                                            "TESTING"	: {SYNC_APP_ID: "100004898d072a0d5", SYNC_SERVER_HOST: "wklxkonyappts06.uk.ri.ad", SYNC_SERVER_PORT: "8080", SYNC_IS_SECURE: false}, 
                                            "STAGING"	: {SYNC_APP_ID: "100000002970f36ce", SYNC_SERVER_HOST: "servicetrak-pest-staging-emea.ri-gateway.com", SYNC_SERVER_PORT: "", SYNC_IS_SECURE: true}	
                                          };
	gblSyncConfiguration.ON_CLOUD		= {	"DEV"		: {SYNC_APP_ID: "", SYNC_SERVER_HOST: "", SYNC_SERVER_PORT: "", SYNC_IS_SECURE: true}, 
                                            "SIT"		: {SYNC_APP_ID: "100004898d072a0d5", SYNC_SERVER_HOST: "manage.kony.com", SYNC_SERVER_PORT: "", SYNC_IS_SECURE: true}, 
                                            "TESTING"	: {SYNC_APP_ID: "", SYNC_SERVER_HOST: "", SYNC_SERVER_PORT: "", SYNC_IS_SECURE: true}, 
                                            "STAGING"	: {SYNC_APP_ID: "100000002970f36ce", SYNC_SERVER_HOST: "rentokil-initial-dev2.sync.konycloud.com", SYNC_SERVER_PORT: "", SYNC_IS_SECURE: true},
                                           "PRODUCTION"	: {SYNC_APP_ID: "100004898512bd8be", SYNC_SERVER_HOST: "rentokil-initial.sync.konycloud.com", SYNC_SERVER_PORT: "", SYNC_IS_SECURE: true}	
                                          };											  

	gblAPICallAccessKeys				= {	"DEV"		: {"client_id": "57eceb02d85a41ef9cbc3c18b56a526c", "client_secret": "5d391aae43e64a23A0AC49DFFD359C0D"}, 
                                            "SIT"		: {"client_id": "57eceb02d85a41ef9cbc3c18b56a526c", "client_secret": "5d391aae43e64a23A0AC49DFFD359C0D"},  
                                            "TESTING"	: {"client_id": "57eceb02d85a41ef9cbc3c18b56a526c", "client_secret": "5d391aae43e64a23A0AC49DFFD359C0D"},
                                            "STAGING"	: {"client_id": "c89acbf594ec450480d9dbf518a0f4fe", "client_secret": "0c3d0df4ffc74563BDF5AA7D001F5CAC"},
                                            "PRODUCTION": {"client_id": "e4dfc2b9d3fa474dbacbe1d9048745f2", "client_secret": "0055aa2686434b58AEA5F6691321A0A3"}	
                                          };
  	loadCurrentAppConfiguration();
  	/** APP Configurations Ends **/ 
  
  	gblMyAppDataObject					= null;
	gblEmployeeObject					= null;  	
	gblSegEmailLastSelIndex				= 0;
  	gblSegSourceLastSelIndex			= 0;
  	gblSegLanguageLastSelIndex			= 0;
  	gblSegScheduleLastSelRowIndex		= -1;
  	gblSegScheduleLastLastSelSecIndex	= -1;
  	gblShowCustomProgress				= false;
  	gblIsSyncInProgress					= false;
  	gblSyncCallInProgress				= "";
  	gblIsProgressPopupOpen				= false;
  	gblProgressIndicatorFlag			= false;
  	gblIsCompletedJobsVisible			= false;
  	gblScopeMappings					= {"SynciCabInstancesPc": "common.label.iCabInstances",
                                           "SyncEmployeesPc"	: "common.label.employees",
                                           "SyncServiceAreasPc"	: "common.label.serviceAreas",
                                           "SyncPortfoliosPc"	: "common.label.portfolios",
                                           "SyncWorkLoadsPc"	: "common.label.workLoads",
                                           "SyncConfigDataPc"	: "common.label.configData",
                                           "SyncServiceCoversPc": "common.label.serviceCovers",
                                           "SyncWorkDayStartPc"	: "common.label.workDayStarted",	
										   "SyncWorkDayEndPc"	: "common.label.workDayEnded",
                                           "SyncWorkHistoryPc"	: "common.label.workHistory"
                                          };
  	gblPrepQuantityList					= [{"key":"0", "val":"0"}, {"key":"1", "val":"1"}, {"key":"2", "val":"2"}, {"key":"3", "val":"3"}, {"key":"4", "val":"4"},
                                           {"key":"5", "val":"5"}, {"key":"6", "val":"6"}, {"key":"7", "val":"7"}, {"key":"8", "val":"8"}, {"key":"9", "val":"9"}, {"key":"10", "val":"10"}];
  	gblSRAList							= [{"key":"ENCLOSED", "val":"common.app.enclosed"}, 
                                           {"key":"FIRE", "val":"common.app.fire"}, 
                                           {"key":"OTHER", "val":"common.label.other"}, 
                                           {"key":"POOR_LIGHTING", "val":"common.label.poorligting"}];
  	gblCustomerNoSignReasons			= [	{"key":"No signature reason", "val":"common.label.noSignatureReason"},
                                          	{"key":"Customer not available", "val":"common.label.customerNotAvailable"},
                                          	{"key":"Customer refused to sign", "val":"common.label.customerRefusedToSign"}];
   	gblMyAppDataObject					= new MyAppDataObject();
  
  
     var addDataObj					= getAppDataObjFromStore();
  	
  	if (addDataObj !== null)
   		printMessage("prod issue analysis --initializeGlobals  addDataObj.selServiceArea" + JSON.stringify(addDataObj.selServiceArea));
    // printMessage("prod issue analysis --initializeGlobals  gblMyAppDataObject.selServiceArea" + JSON.stringify(gblMyAppDataObject.selServiceArea));
    
  	gblCompletedVisitFlag               = false;
    gblShowCompletedJobs                = false;
  	gblSyncedWorkIds					= [];
  	gblSyncedWorkPointIds				= [];
  	gblWorkPointIds						= [];
  	gblIsWorkSaveInProgress				= false;
  	gblRefreshHomeScreenFlag			= false;
  
  	gblClosedWorkData					= null;  
    gblloadingchecker					= 1;
  	gblIsMyLocationCaptured				= false;
  	gblMyLocation						= {};
  	gblPrevSelLocation					= {};  
  	gblAppConfiguration					= {};   
  
  // printMessage("prod issue analysis --initializeGlobals " + JSON.stringify(gblMyAppDataObject));
}

/** Get Current MBAAS Configuration **/
function loadCurrentAppConfiguration() {  	
  	var syncServerDetails				= gblSyncConfiguration[gblServerType];
  	var syncConfiguration				= syncServerDetails[gblBuildMode];  	
  	gblAppConfiguration					= {};
 /*  gblAppConfiguration.syncAppId		= syncConfiguration.SYNC_APP_ID;
    gblAppConfiguration.syncServerHost	= syncConfiguration.SYNC_SERVER_HOST;
    gblAppConfiguration.syncServerPort	= syncConfiguration.SYNC_SERVER_PORT;
  	gblAppConfiguration.syncIsSecure	= syncConfiguration.SYNC_IS_SECURE; */
     gblAppConfiguration.syncAppId		= "100004898512bd8be";
    gblAppConfiguration.syncServerHost	= "rentokil-initial.sync.konycloud.com"; 
  
  /* gblAppConfiguration.syncAppId		= "100000002970f36ce";
    gblAppConfiguration.syncServerHost	= "rentokil-initial-dev2.sync.konycloud.com"; */
    gblAppConfiguration.syncServerPort	= "";
  	gblAppConfiguration.syncIsSecure	= true;
    
  
   printMessage("prod issue analysis --gblAppConfiguration -loadCurrentAppConfiguration " +JSON.stringify(gblAppConfiguration));
}

/** Reset global variables **/
function resetGlobals(){
  	gblMyAppDataObject					= null;
  	gblEmployeeObject					= null;
  	gblSegEmailLastSelIndex				= 0;
  	gblSegSourceLastSelIndex			= 0;
  	gblSegLanguageLastSelIndex			= 0;
  	gblSegScheduleLastSelRowIndex		= -1;
  	gblSegScheduleLastLastSelSecIndex	= -1;
  	gblIsSyncInProgress					= false;
  	gblShowCustomProgress				= false;
  	gblSyncCallInProgress				= "";
  	gblIsProgressPopupOpen				= false;
  	gblIsCompletedJobsVisible			= false;
  
    gblCompletedVisitFlag               = false;
    gblShowCompletedJobs                = false;
  	gblSyncedWorkIds					= [];
  	gblSyncedWorkPointIds				= [];
  	gblWorkPointIds						= [];
  	gblIsWorkSaveInProgress				= false;
  	gblRefreshHomeScreenFlag			= false;
  
  	gblClosedWorkData					= null;
    gblloadingchecker					= 1;
  	gblIsMyLocationCaptured				= false;
  	gblMyLocation						= {};
  	gblPrevSelLocation					= {};
}

/** Device back button action **/
function onDeviceBackButtonClick(){
	var frmObj								= kony.application.getCurrentForm();
  
  	if (frmObj.id === PCConstant.FORM_HOME) {
      	showConfirmPopup("common.message.confirmExit", ActionConfirmPopupYesBtn, ActionConfirmPopupNoBtn);
    } else if (frmObj.id === PCConstant.FORM_CUSTOMER || frmObj.id === PCConstant.FORM_VISIT || frmObj.id === PCConstant.FORM_COMPLETEVISIT) {
		showLoading("common.message.loading");
      	closeSettingsMenu(); 
        getWorkOrdersFromLocalTbl();
    } else if (frmObj.id === PCConstant.FORM_LOGIN) {
      	showConfirmPopup("common.message.confirmExit", ActionConfirmPopupYesBtn, ActionConfirmPopupNoBtn);
    }
}

/** To set some of the application behaviors **/
function setApplicationBehaviors() {
  	printMessage("Inside setApplicationBehaviors");
 	var inputParamTable 						= {}; 
    inputParamTable.defaultIndicatorColor		= "#ffe6e6";
  	inputParamTable.hidedefaultloadingindicator	= true;
    kony.application.setApplicationBehaviors(inputParamTable); 
  	var callbackParamTable 						= {}; 
    callbackParamTable.onbackground				= ActionApplicationWentBackground;
    callbackParamTable.onforeground				= ActionApplicationBecomesForeground;
  	kony.application.setApplicationCallbacks(callbackParamTable);
}

/** Callback - App running in foreground **/
function onAppBecomesForeground(){
  	printMessage("Inside -  onAppBecomesForeground");
  
  printMessage("prod issue analysis --onAppBecomesForeground " + JSON.stringify(gblMyAppDataObject));
  	//startSyncScheduler();
}

/** Callback - App running in background **/
function onAppWentBackground(){
  	printMessage("Inside -  onAppWentBackground");
  // printMessage("prod issue analysis --onAppWentBackground " + JSON.stringify(gblMyAppDataObject));
  //	printMessage("prod issue analysis --onAppWentBackground - gblMyAppDataObject: " + JSON.stringify(gblMyAppDataObject));
  //	printMessage("prod issue analysis --onAppWentBackground - gblMyAppDataObject.selServiceArea: " + JSON.stringify(gblMyAppDataObject.selServiceArea));
  	//cancelSyncScheduler();
}

/** To fetch current device locale  **/
function getCurrentDevLocale(){
  	var selLanCode			= kony.store.getItem(PCConstant.SEL_LAN_CODE);
  	if (isEmpty(selLanCode)) {
      	var curDevLocale	= kony.i18n.getCurrentDeviceLocale();
        var language		= curDevLocale.language;
        var country			= curDevLocale.country;
        var devLocale		= (!isEmpty(language) && !isEmpty(country)) ? (language + "_" + toUpperCase(country)) : PCConstant.DEF_LOCALE;
        if (kony.i18n.isLocaleSupportedByDevice(devLocale) && kony.i18n.isResourceBundlePresent(devLocale)){
            changeDeviceLocale(devLocale);
        } else {
            changeDeviceLocale(PCConstant.DEF_LOCALE);
        }
    } else {
      	changeDeviceLocale(selLanCode);
    }
}

/** To change current device locale  **/
function changeDeviceLocale(selLocale, callBackSuccess, callBackFailure){
  	if (kony.i18n.isLocaleSupportedByDevice(selLocale) && kony.i18n.isResourceBundlePresent(selLocale)){
      	if (callBackSuccess !== undefined && callBackFailure !== undefined) {
          	kony.i18n.setCurrentLocaleAsync(selLocale, callBackSuccess, callBackFailure, {});
        } else {
          	kony.i18n.setCurrentLocaleAsync(selLocale, success_setLocaleCallback, failure_setLocaleCallback, {});
        }
    }  
}

/** Success callback - change device locale  **/
function success_setLocaleCallback(oldlocalename, newlocalename){
	printMessage("Inside -  success_setLocaleCallback");
  	kony.store.setItem(PCConstant.SEL_LAN_CODE, newlocalename);
  	doRefreshi18n();
}

/** Failure callback - change device locale  **/
function failure_setLocaleCallback(errCode, errMsg){
	printMessage("Inside -  failure_setLocaleCallback");
  	dismissLoading();
}

/** To set global employee object **/
function setGblUserObject(employeeInfo){
  	gblEmployeeObject					= new Employee();
  	gblEmployeeObject.email				= (isEmpty(employeeInfo.employeeWorkEmail) || employeeInfo.employeeWorkEmail == "null") ? gblMyAppDataObject.selectedEmail : employeeInfo.employeeWorkEmail;
  	gblEmployeeObject.firstName			= (!isEmpty(employeeInfo.employeeName1)) ? employeeInfo.employeeName1 : gblEmployeeObject.firstName;
  	gblEmployeeObject.lastName			= (!isEmpty(employeeInfo.employeeNameLast)) ? employeeInfo.employeeNameLast : gblEmployeeObject.lastName;
  	gblEmployeeObject.countryCode		= (!isEmpty(employeeInfo.countryCode)) ? employeeInfo.countryCode : gblEmployeeObject.countryCode;
  	gblEmployeeObject.businessCode		= (!isEmpty(employeeInfo.businessCode)) ? employeeInfo.businessCode : gblEmployeeObject.businessCode;
  	gblEmployeeObject.branchNumber		= (!isEmpty(employeeInfo.employeeBranchNumber)) ? employeeInfo.employeeBranchNumber : gblEmployeeObject.branchNumber;
  	if (!isEmpty(employeeInfo.employeeCode)){
      	gblEmployeeObject.setEmployeeCode(employeeInfo.employeeCode);
    }
}

/** To refresh form i18ns **/
function doRefreshi18n(){
  	var frmObj								= kony.application.getCurrentForm();  
  	if (frmObj.id === PCConstant.FORM_HOME) {
      	refreshi18nHome();
    } else if (frmObj.id === PCConstant.FORM_CUSTOMER) {
      	refreshi18nCustomer();
    } else if (frmObj.id === PCConstant.FORM_VISIT) {
      	refreshi18nVisit();
    } else if (frmObj.id === PCConstant.FORM_LOGIN) {
      	refreshi18nLogin();
    } else if (frmObj.id === PCConstant.FORM_COMPLETEVISIT) {
      	refreshi18nCompleteVisit();
    }
}

/** To handle form when keyboard become visible **/
function onTextboxBeginEditing(widgetRef){
  	printMessage("Inside -  onTextboxBeginEditing");
  	var frmObj								= kony.application.getCurrentForm();  
  	if (frmObj.id === PCConstant.FORM_CUSTOMER) {
      	frmObj.flxStartVisitBtnContainer.setVisibility(false);
      	frmObj.tabCustomerDetails.bottom			= "0dp";
      	handleUserInteractionOnCustomerTextFields(widgetRef);
    } else if (frmObj.id === PCConstant.FORM_VISIT) {
      	frmObj.flxStopVisitBtnContainer.setVisibility(false);
      	frmObj.flxTabContainer.bottom				= "0dp";
      	handleUserInteractionOnVisitTextFields(widgetRef);
    } 
}

/** To handle form when keyboard become invisible **/
function onTextboxEndEditing(widgetRef){
  	printMessage("Inside -  onTextboxEndEditing");
  	var frmObj										= kony.application.getCurrentForm();  
  	if (frmObj.id === PCConstant.FORM_CUSTOMER) {      	
      	frmObj.tabCustomerDetails.bottom			= "75dp";
      	frmObj.flxStartVisitBtnContainer.setVisibility(true);
      	handleUserInteractionOnCustomerTextFields(widgetRef);
    } else if (frmObj.id === PCConstant.FORM_VISIT) {
      	frmObj.flxTabContainer.bottom				= "75dp";
      	frmObj.flxStopVisitBtnContainer.setVisibility(true);
      	handleUserInteractionOnVisitTextFields(widgetRef);
    }
}

/** To handle form when textfield changes **/
function onTextChangeOnInputField(widgetRef){
  	printMessage("Inside -  onTextboxEndEditing");
  	var frmObj								= kony.application.getCurrentForm();  
  	if (frmObj.id === PCConstant.FORM_CUSTOMER) {
      	handleUserInteractionOnCustomerTextFields(widgetRef);
    } else if (frmObj.id === PCConstant.FORM_VISIT) {
      	handleUserInteractionOnVisitTextFields(widgetRef);
    }
}

/** Start visit logic **/
function onClickStartVisit(){
  	var frmObj									= kony.application.getCurrentForm();
  	var selScheduledItem						= null;
  	var selDateStrKey							= "";
  	if (frmObj.id === PCConstant.FORM_HOME) {
      	var selSectionIndex 					= gblSegScheduleLastLastSelSecIndex;
        var selRowIndex 						= gblSegScheduleLastSelRowIndex;
        var selectedSectionRows					= Home.segHomeContentList.data[selSectionIndex][1];
        var selectedItem						= selectedSectionRows[selRowIndex];
      	selDateStrKey							= selectedItem.dateStrKey;
        var selScheduledSection					= gblMyAppDataObject.getScheduledObjectsForDate(selectedItem.dateStrKey);
        selScheduledItem						= selScheduledSection[selRowIndex];
    } else if (frmObj.id === PCConstant.FORM_CUSTOMER) {
      	selScheduledItem						= gblMyAppDataObject.selScheduledItem;
      	selDateStrKey							= gblMyAppDataObject.selScheduledDateStr;
    }
  	
  	if (gblMyAppDataObject.isDayStarted === PCConstant.DAY_STARTED_YES) {
  		var visitInProgressObj					= gblMyAppDataObject.inProgressVisit;
      	if (isEmpty(visitInProgressObj)) {
          	selScheduledItem.visitStartDateStr	= getTodaysDateStr();
  			selScheduledItem.visitStartTimeStr	= getCurrentTimeStr();
          	selScheduledItem.workAttendanceDateTimeStart	= getDateTimeFormat(new Date(),"YYYY-MM-DDTHH:mm:ss.SSS");
      		gblMyAppDataObject.inProgressVisit	= selScheduledItem;
          	gblMyAppDataObject.selDateStrKey	= selDateStrKey;
          	gblMyAppDataObject.updateVisitStatus(selDateStrKey, PCConstant.VISIT_IN_PROGRESS);          	
          	gblMyAppDataObject.isVisitInProgress= true;
          	setAppDataObjInStore();
          	updateWorkStatusAsInProgressInLocalTbl();          	
          	if (frmObj.id !== PCConstant.FORM_VISIT) {
          		showVisitScreen();
        	}
        } else if (gblMyAppDataObject.isInProgressVisitMatchingVisit(selScheduledItem) === true) {
          	if (frmObj.id !== PCConstant.FORM_VISIT) {
          		showVisitScreen();
        	}
        } else {
          	showMessagePopup("common.message.cannotStartNewVisit", ActionPopupMessageOk);
        }
    } else {
      	if (frmObj.id === PCConstant.FORM_CUSTOMER) {
          	showMessagePopup("common.message.dayNotStartedSoNavigateToHome", ActionPopupMessageOk);
        } else {
          	showMessagePopup("common.message.dayHasNotBeenStarted", ActionPopupMessageOk);
        }
    }
}

/** To destroy all forms other than given **/
function destroyFormsExceptMe(formId){
  	if (formId === PCConstant.FORM_HOME) {
      	Visit.destroy();
      	Customer.destroy();
      	CompletedVisit.destroy();
    } else if (formId === PCConstant.FORM_VISIT) {
      	Home.destroy();
      	Customer.destroy();
      	CompletedVisit.destroy();
    } else if (formId === PCConstant.FORM_CUSTOMER) {
      	Home.destroy();
      	Visit.destroy();
      	CompletedVisit.destroy();
    } else if (formId === PCConstant.FORM_COMPLETEVISIT) {
      	Home.destroy();
      	Visit.destroy();
      	Customer.destroy();
    } else if (formId === PCConstant.FORM_LOGIN) {
      	// Nothing to do here
    }
}

/** To get app version from config **/
function getVersionFromAppConfig() {
  	printMessage("appConfig: " + JSON.stringify(appConfig));
  	return appConfig.appVersion;  
}