/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : UI Screen - Login - Methods
***********************************************************************/

/** Login screen - PreShow **/
function onPreShowLoginScreen(){
  	printMessage("Inside - onPreShowLoginScreen");
  	gblEmployeeObject					= null;
  	refreshi18nLogin();
}

/** Login screen - PostShow **/
function onPostShowLoginScreen(){	
  	printMessage("Inside - onPostShowLoginScreen");
  	showLoading("common.message.loading");  
  	initSyncModule();
}

/** Login screen - Refresh all i18n **/
function refreshi18nLogin(){
  	printMessage("Inside - refreshi18nLogin");
  	Login.lblAppName.text				= getI18nString("common.app.title");
  	Login.lblWelcome.text				= getI18nString("common.label.welcome");
  	Login.lblUsername.text				= (!isEmpty(gblEmployeeObject)) ? gblEmployeeObject.getEmployeeName() : "";
  	Login.lblAppVersion.text			= getI18nString("common.label.version") +" " + getVersionFromAppConfig();
}

/** Sync initialization - Success callback  **/
function success_initSyncModule(response){
	printMessage("Inside - success_initSyncModule");
  	var isEmpAlreayLoggedIn				= kony.store.getItem(PCConstant.IS_EMP_LOGGED_IN);
  	if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true) {
      	cancelSyncScheduler();
      	var loggedEmpProfile			= getEmpProfielFromStore();
      	setGblUserObject(loggedEmpProfile);   
    	updateWelcomeLabel();
      	getWorkOrdersFromLocalTbl();   
  		startSyncScheduler();
    } else {
      	getDeviceConfiguredEmails();
    }
}

/** Sync initialization - Failure callback  **/
function failure_initSyncModule(response){
	printMessage("Inside  - failure_initSyncModule");
  	dismissLoading();
}

/** Success callback - SQL query execution - get iCabInstances  **/
function success_getiCabInstances(result){
  	printMessage("Inside - success_getiCabInstances");
	if(result !== null && result.length > 0) {
      	var selectediCabServerCode				= "";
      	if (isEmpty(gblMyAppDataObject)) {
          	gblMyAppDataObject					= new MyAppDataObject();
        } else {
          	gblMyAppDataObject.iCabInstances	= [];
        }
      
      	var rowItem 							= null;
      	var anIcabInstance						= null;
      	for (var i = 0; i < result.length; i++) {
          	rowItem								= result[i];
          	anIcabInstance						= {};
          	anIcabInstance.businessCode			= rowItem.businessCode;
          	anIcabInstance.countryCode			= rowItem.countryCode;
          	anIcabInstance.email				= rowItem.email;
          	anIcabInstance.iCABSServer			= rowItem.iCABSServer;
          	anIcabInstance.displayName			= rowItem.countryCode + " - " + rowItem.businessCode;
          	gblMyAppDataObject.addICabInstance(anIcabInstance);
          
          	if (isEmpty(gblMyAppDataObject.selICabServerCode) && i === 0){
        		selectediCabServerCode			= rowItem.iCABSServer;
        	} else if (!isEmpty(gblMyAppDataObject.selICabServerCode) && gblMyAppDataObject.selICabServerCode == rowItem.iCABSServer){
              	selectediCabServerCode			= rowItem.iCABSServer;
            }
        }
      
      	gblMyAppDataObject.languages			= [];
      	gblMyAppDataObject.addLanguage({"code":"en_GB", "name":"English"});
      	gblMyAppDataObject.addLanguage({"code":"es_ES", "name":"Spanish"});
      
      	if (isEmpty(gblMyAppDataObject.selLanguage)){
          	gblMyAppDataObject.selLanguage		= "en_GB";
        }
      	var totalSettingsItemCount				= gblMyAppDataObject.iCabInstances.length + gblMyAppDataObject.languages.length;
      	if (totalSettingsItemCount > 3) {
          	showOverlayPopupOnLoginScreen(true, false, gblMyAppDataObject.iCabInstances, gblMyAppDataObject.languages, selectediCabServerCode, gblMyAppDataObject.selLanguage);
        } else {
          	showOverlayPopupOnLoginScreen(true, true, gblMyAppDataObject.iCabInstances, gblMyAppDataObject.languages, selectediCabServerCode, gblMyAppDataObject.selLanguage);
        }
    } else {
      	printMessage("Error - getIcabInstances returned empty records");
      	showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
      	dismissLoading();
    }
}

/** Error callback - SQL query execution - get iCabInstances **/
function error_getiCabInstances(error) {
  	printMessage("Inside - error_getiCabInstances");
  	showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
    dismissLoading();
}

/** Success callback - SQL query execution - get employee record **/
function success_getEmployee(result){
  	printMessage("Inside - success_getEmployee");
	if(result !== null && result.length > 0) {
      	var rowItem 					= result[0];
      	setGblUserObject(rowItem);   
    	updateWelcomeLabel();
      	getServiceAreasFromLocalTbl();
    } else {
      	printMessage("Error - getEmployee returned empty records");
      	showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
      	dismissProgressMessage();
      	dismissLoading();
    }
}

/** Error callback - SQL query execution - get employee record **/
function error_getEmployee(error) {
  	printMessage("Inside - error_getEmployee");
    showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
    dismissProgressMessage();
    dismissLoading();
}

/** Success callback - SQL query execution - get service areas **/
function success_getServiceAreas(result){
  	printMessage("Inside - success_getServiceAreas");
	if(result !== null && result.length > 0) {
      	var serviceAreaCode 		= "";
      	var formatedOutput			= null;
      	for (var i = 0; i < result.length; i++) {
          	serviceAreaCode 		= result[i].serviceAreaCode;
         	if (!isEmpty(serviceAreaCode)) {
              	formatedOutput						= {};
              	formatedOutput.branchNumber			= result[i].branchNumber;
              	formatedOutput.businessCode			= result[i].businessCode;
              	formatedOutput.countryCode			= result[i].countryCode;
              	formatedOutput.employeeforename1	= result[i].employeeforename1;
              	formatedOutput.employeeSurname		= result[i].employeeSurname;
              	formatedOutput.serviceAreaCode		= result[i].serviceAreaCode;
              	formatedOutput.serviceAreaDescription= result[i].serviceAreaDescription;
              	formatedOutput.assignedArea			= result[i].assignedArea;
              	gblMyAppDataObject.selServiceArea	= formatedOutput;
              	setAppDataObjInStore();
      			break;
            }
        }
      	startSyncWorkLoads();
    } else {
      	printMessage("Error - getServiceAreas returned empty records");
      	showMessagePopup("common.message.noServiceArea", ActionOnMessagePopupOkBtn);
      	dismissProgressMessage();
      	dismissLoading();
    }
}



/** Error callback - SQL query execution - get service areas **/
function error_getServiceAreas(error) {
  	printMessage("Inside - error_getServiceAreas");
    showMessagePopup("common.message.noServiceArea", ActionOnMessagePopupOkBtn);
    dismissProgressMessage();
    dismissLoading();
}

/** The method called on success of initial sync  **/
function onInitialSyncDone(){
  	kony.store.setItem(PCConstant.IS_EMP_LOGGED_IN, true);
    setEmpProfielInStore(gblEmployeeObject); 
    getWorkOrdersFromLocalTbl();
}

/** To get employee profile from store **/
function getEmpProfielFromStore(){
  	var empProfileStr						= kony.store.getItem(PCConstant.EMP_PROFILE);
  	var empProfileObj						= JSON.parse(empProfileStr);
  	return empProfileObj;
}

/** To store employee profile in store **/
function setEmpProfielInStore(empProfileObj){
  	var empProfileStr						= JSON.stringify(empProfileObj);
  	kony.store.setItem(PCConstant.EMP_PROFILE, empProfileStr);
}

/** To get appDataObject from store **/
function getAppDataObjFromStore(){
  	var appDataObjStr						= kony.store.getItem(PCConstant.APP_DATA_OBJECT);
  //	printMessage("prod issue analysis - getAppDataObjFromStore- appDataObjStr: " + JSON.stringify(appDataObjStr));
  	var appDataObj							= null;
  	if (!isEmpty(appDataObjStr)) {
      	appDataObj							= JSON.parse(appDataObjStr);
    }
  	return appDataObj;
}

/** To store appDataObject in store **/
function setAppDataObjInStore(){
  	var appDataObjStr						= JSON.stringify(gblMyAppDataObject);
  	kony.store.setItem(PCConstant.APP_DATA_OBJECT, appDataObjStr);
}

/** To update welcome label text **/
function updateWelcomeLabel(){
  	Login.lblUsername.text				= gblEmployeeObject.getEmployeeName();
}

/** On click Emails popup Cancel button action **/
function doExitApplication(){
    exitApplication();
}




function success_getServiceAreas_update(result){
  	printMessage("Inside - success_getServiceAreas");
	if(result !== null && result.length > 0) {
      	var serviceAreaCode 		= "";
      	var formatedOutput			= null;
      	for (var i = 0; i < result.length; i++) {
          	serviceAreaCode 		= result[i].serviceAreaCode;
         	if (!isEmpty(serviceAreaCode)) {
              	formatedOutput						= {};
              	formatedOutput.branchNumber			= result[i].branchNumber;
              	formatedOutput.businessCode			= result[i].businessCode;
              	formatedOutput.countryCode			= result[i].countryCode;
              	formatedOutput.employeeforename1	= result[i].employeeforename1;
              	formatedOutput.employeeSurname		= result[i].employeeSurname;
              	formatedOutput.serviceAreaCode		= result[i].serviceAreaCode;
              	formatedOutput.serviceAreaDescription= result[i].serviceAreaDescription;
              	formatedOutput.assignedArea			= result[i].assignedArea;
              	gblMyAppDataObject.selServiceArea	= formatedOutput;
              	setAppDataObjInStore();
                Home.lblServiceAreaCode.text	= (!isEmpty(gblMyAppDataObject.selServiceArea)) ? gblMyAppDataObject.selServiceArea.serviceAreaCode : "";
      			break;
            }
        }
    } 
}