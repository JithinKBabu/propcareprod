/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer Popup related methods
***********************************************************************/

/** Popup for displaying warning and error  messages **/
function showMessagePopup(msgKey, buttonHandlerAction){
  	PopupMessage.lblTitle.text		= getI18nString("common.app.title");
  	PopupMessage.lblMessage.text	= getI18nString(msgKey);
    PopupMessage.btnOk.text			= getI18nString("common.label.ok");
  	if (buttonHandlerAction !== null){
      	PopupMessage.btnOk.onClick	= buttonHandlerAction;
    } else {
      	PopupMessage.btnOk.onClick	= ActionPopupMessageOk;
    }
    PopupMessage.show();  
}

/** Popup for displaying warning and error messages - msgKey considered as message instead of i18n **/
function showMessagePopupWithouti18n(msgKey, buttonHandlerAction){
  	PopupMessage.lblTitle.text		= getI18nString("common.app.title");
  	PopupMessage.lblMessage.text	= msgKey;
    PopupMessage.btnOk.text			= getI18nString("common.label.ok");
  	if (buttonHandlerAction !== null){
      	PopupMessage.btnOk.onClick	= buttonHandlerAction;
    } else {
      	PopupMessage.btnOk.onClick	= ActionPopupMessageOk;
    }
    PopupMessage.show();  
}

/** Message Popup - Dismiss **/
function dismissMessagePopup(){
  	PopupMessage.dismiss(); 
}

/** Confirm popup - show **/
function showConfirmPopup(msgKey, yesButtonHandlerAction, noButtonHandlerAction){
  	PopupConfirmation.lblTitle.text		= getI18nString("common.app.title");
  	PopupConfirmation.lblMessage.text	= getI18nString(msgKey);
    PopupConfirmation.btnNo.text		= getI18nString("common.label.no");
  	PopupConfirmation.btnYes.text		= getI18nString("common.label.yes");
    PopupConfirmation.btnYes.onClick	= yesButtonHandlerAction;
  	if (noButtonHandlerAction !== null){
      	PopupConfirmation.btnNo.onClick	= noButtonHandlerAction;
    } else {
      	PopupConfirmation.btnNo.onClick	= ActionConfirmPopupNo;
    }
    PopupConfirmation.show();  
}

/** Confirm popup - Dismiss **/
function dismissConfirmPopup(){
  	PopupConfirmation.dismiss(); 
}

/** On click message popup Ok button action - This will exit application **/
function onClickMessagePopupOkButton(){
  	showLoading("common.message.loading");
  	dismissMessagePopup();
  	dismissLoading();
  	exitApplication();
}

/** Progress popup - show **/
function showProgressPopup(titleKey, messageStr, progressKey){
    PopupProgress.lblMessage.text	= getI18nString(messageStr);
  	//PopupProgress.lblProgress.text	= getI18nString(progressKey);
  	if (gblIsProgressPopupOpen === false) {
       	PopupProgress.imgLoading.src	= "zero.png" ; 
      	gblIsProgressPopupOpen		= true;
      	PopupProgress.lblTitle.text	= getI18nString(titleKey);
      	PopupProgress.show();
    	//kony.timer.schedule("ToggleProgressImageTimer", toggleProgressImage, 0.5, true);
    } 
}

/** Progress popup - to toggle progress indicator image **/
function toggleProgressImage(){
  	PopupProgress.imgLoading.src	= (gblProgressIndicatorFlag === true) ? "loadingone.png" : "loadingtwo.png" ; 
  	gblProgressIndicatorFlag		= !gblProgressIndicatorFlag;
}

/** Progress popup - update message **/
function updateMessageInProgressPopup(messageStr){
  	//PopupProgress.lblMessage.text	= getI18nString(messageStr);
  	if (messageStr == 'common.label.iCabInstances'){
    	if (gblloadingchecker == 1){
    		PopupProgress.imgLoading.src	= "ten.png" ; 
      		gblloadingchecker				= 2;
    	}
  	}
  	if (messageStr == 'common.label.employees'){
     	if (gblloadingchecker == 2){
    		PopupProgress.imgLoading.src	= "twenty.png" ; 
       		gblloadingchecker				= 3;
     	}
  	}
  	if(messageStr == 'common.label.serviceAreas'){
     	if (gblloadingchecker == 3){
     		PopupProgress.imgLoading.src	= "fourty.png" ; 
        	gblloadingchecker				= 4;
     	}
   	}
  	if (messageStr == 'common.label.configData'){
    	if(gblloadingchecker == 4){
     		PopupProgress.imgLoading.src	= "fifty.png" ;
      		gblloadingchecker 				= 5;
      	} 
      	if(gblloadingchecker == 5){
     		PopupProgress.imgLoading.src	= "sixty.png" ;
      		gblloadingchecker 				= 6;
      	}
   	}
  	if(messageStr == 'common.label.workLoads'){
        if (gblloadingchecker == 6){
            PopupProgress.imgLoading.src	= "eighty.png" ;
            gblloadingchecker				= 7;
        }
   	}
    if(messageStr == 'common.label.portfolio'){
        if(gblloadingchecker == 7){
            PopupProgress.imgLoading.src	= "ninty.png" ;
           	gblloadingchecker				= 8;
         }
    }
    if(messageStr == 'common.label.serviceCovers'){
      	if(gblloadingchecker == 8){
          	PopupProgress.imgLoading.src	= "hundred.png" ; 
        	gblloadingchecker				= 1;
      	}
    }
  	printMessage("Inside - messageStr  "+ messageStr);
}

/** Progress popup - update progress **/
function updateProgressInProgressPopup(progressKey){
  	 PopupProgress.lblProgress.text	= getI18nString(progressKey);
}

/** Progress popup - dismiss **/
function dismissProgressMessage(){
  	gblIsProgressPopupOpen			= false;
  	kony.timer.cancel("ToggleProgressImageTimer"); 
  	PopupProgress.dismiss();
}

/** Start day popup - show **/
function showStartDayPopup(){
  	if (gblMyAppDataObject.isVisitInProgress === true) {
      	showMessagePopup("common.message.completeInProgressVisit", ActionPopupMessageOk);
    } else {
      	PopupStartDay.lblTitle.text		= getI18nString("common.label.startOrEndDay");
        PopupStartDay.btnStartDay.text	= getI18nString("common.label.startDay");
        PopupStartDay.btnEndDay.text	= getI18nString("common.label.EndDay");
        if (gblMyAppDataObject.isDayStarted	=== PCConstant.DAY_STARTED_DONT_KNOW || gblMyAppDataObject.isDayStarted	=== PCConstant.DAY_STARTED_NO) {
            PopupStartDay.btnStartDay.setEnabled(true);
            PopupStartDay.btnEndDay.setEnabled(false);
            PopupStartDay.btnStartDay.skin		= btnNormalSkin;
            PopupStartDay.btnStartDay.focusSkin	= btnNormalSkin;
            PopupStartDay.btnEndDay.skin		= btnDisabledSkin;
            PopupStartDay.btnEndDay.focusSkin	= btnDisabledSkin;
        } else {
            PopupStartDay.btnStartDay.setEnabled(false);
            PopupStartDay.btnEndDay.setEnabled(true);
            PopupStartDay.btnStartDay.skin		= btnDisabledSkin;
            PopupStartDay.btnStartDay.focusSkin	= btnDisabledSkin;
            PopupStartDay.btnEndDay.skin		= btnNormalSkin;
            PopupStartDay.btnEndDay.focusSkin	= btnNormalSkin;
        }
        PopupStartDay.show();
    }
}

/** On click start day button **/
function onClickStartDayBtn(){  	
  	showLoading("common.message.loading");
  	PopupStartDay.dismiss();
  	gblMyAppDataObject.startDayDateStr		= getCurrentDateTimeInFormat("yyyy-MM-dd");
  	gblMyAppDataObject.startDayTimeStr		= getCurrentDateTimeInFormat("HH:mm:ss");
  	gblMyAppDataObject.isDayStarted			= PCConstant.DAY_STARTED_YES;
  	setAppDataObjInStore();
  	Home.imgStartdayIcon.src				= "startday.png";
  	insertWorkDayStartInDB();
}

/** On click end day button **/
function onClickEndDayBtn(){
    showLoading("common.message.loading");
    PopupStartDay.dismiss();
    gblMyAppDataObject.stopDayDateStr	= getCurrentDateTimeInFormat("yyyy-MM-dd");
    gblMyAppDataObject.stopDayTimeStr	= getCurrentDateTimeInFormat("HH:mm:ss");
    gblMyAppDataObject.isDayStarted		= PCConstant.DAY_STARTED_NO;
    setAppDataObjInStore();
    Home.imgStartdayIcon.src			= "endday.png";
    insertWorkDayEndInDB();
}

/** Confirm popup (exit app) - Yes btn action **/
function onClickConfirmPopupYesBtn(){
  	dismissConfirmPopup();
  	exitApplication();
}

/** Confirm popup (exit app) - No btn action **/
function onClickConfirmPopupNoBtn(){
  	dismissConfirmPopup();
}

/** Confirm popup (discard visit) - Yes btn action **/
function doDiscardVisit(){
  	showLoading("common.message.loading");
  	dismissConfirmPopup();
  	updateWorkStatusAsOpenInLocalTbl();  	
}

/** Confirm popup (discard visit) - No btn action **/
function cancelDiscardVisit(){
  	dismissConfirmPopup();
}


/** Confirm popup (end visit) - Yes btn action **/
function doEndVisit(){
  	showLoading("common.message.loading");
	//insertWorkAttendanceInLocalTbl();
  	updateWorkOrderInLocalTbl();
  
  	/*gblMyAppDataObject.moveScheduledJobToCompletedJobs();
    gblMyAppDataObject.selDateStrKey			= null;
    gblMyAppDataObject.inProgressVisit			= null;
    gblMyAppDataObject.isVisitInProgress		= false;
    setAppDataObjInStore();
    dismissConfirmPopup();
    gblShowCompletedJobs                		= true;
    getWorkOrdersFromLocalTbl();*/
}

/** Confirm popup (end visit) - No btn action **/
function cancelEndVisit(){
  	dismissConfirmPopup();
}

/** Confirmed captured photo deletion **/
function confirmedDeletePhoto(){
  	showLoading("common.message.loading");
  	updatePhotoInLocalTblasEmpty("", "");
   	//gblMyAppDataObject.inProgressVisit.capturedPhoto= "";
    setAppDataObjInStore();
    Visit.TabPaneVisits.imgCapturedPhoto.src		= "noimage.png";   
    toggleButtonVisibility(false);
  	dismissConfirmPopup();
    dismissLoading();
}

/** Declined photo deletion **/
function declineDeletePhoto(){
  	dismissConfirmPopup();
}

/** Show image in popup **/
function showImageInPopup(imgBase64Data){
  	showLoading("common.message.loading");
  	PopupShowImage.imgPhoto.base64	= imgBase64Data;
  	PopupShowImage.show();
  	dismissLoading();
}