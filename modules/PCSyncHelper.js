/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer sync based methods
***********************************************************************/

/** Initialize Sync Module  **/
function initSyncModule(){ 	
  	printMessage("Inside - initSyncModule");
  	try {
      	sync.init(success_initSyncModule, failure_initSyncModule);
    } catch(exception) {
      	printMessage("Exception: " + exception.message);
   	}
}

/** Reset sync **/
function resetSync(successCallbackMethod){
  	printMessage("Inside - resetSync");
  	printMessage(" gblMyAppDataObject.selectedEmail is  " + gblMyAppDataObject.selectedEmail);
  	sync.reset(successCallbackMethod, onSyncResetFailureCallback);
}

/** Sync reset success callback **/
function onSyncResetSuccessCallback(response){
  	printMessage("Inside - onSyncResetSuccessCallback");
  	startSyncICabInstances();
}

/** Sync reset failure callback **/
function onSyncResetFailureCallback(response){
  	printMessage("Inside - onSyncResetFailureCallback");
}

/** Data sync - iCabInstances **/
function startSyncICabInstances(){
  	printMessage("Inside - startSyncICabInstances");
  	gblShowCustomProgress				= true;
  	gblSyncCallInProgress				= PCConstant.SYNC_ICAB_INSTANCES;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"SynciCabInstancesPc":{doupload:false, dodownload:true, uploaderrorpolicy:"abortonerror"},
                                           "SyncEmployeesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceAreasPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncConfigDataPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkLoadsPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncPortfoliosPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceCoversPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkDayStartPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}, 
                                           "SyncWorkDayEndPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkHistoryPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}
                                          };  	
  	initDataSync(syncConfigData, "SynciCabInstancesPc");
}

/** Data sync - Initial download on iCab instances, employees and service areas **/
function startSyncEmpServiceAreaAndConfigData(){
  	printMessage("Inside - startSyncEmpServiceAreaAndConfigData");
  	gblShowCustomProgress				= true;
  	gblSyncCallInProgress				= PCConstant.SYNC_EMP_S_AREA_C_DATA;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"SyncEmployeesPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceAreasPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
      									   "SyncConfigDataPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkLoadsPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncPortfoliosPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceCoversPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SynciCabInstancesPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkDayStartPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}, 
                                           "SyncWorkDayEndPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkHistoryPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}
                                          };  	
  	initDataSync(syncConfigData, "SyncEmployeesPc");
}

/** Data sync - Work Loads **/
function startSyncWorkLoads(){
  	printMessage("Inside - startSyncWorkLoads");
  	gblShowCustomProgress				= true;
  	gblSyncCallInProgress				= PCConstant.SYNC_WORK_LOADS;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"SyncWorkLoadsPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
      									   "SyncEmployeesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceAreasPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncConfigDataPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncPortfoliosPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceCoversPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SynciCabInstancesPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkDayStartPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}, 
                                           "SyncWorkDayEndPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkHistoryPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}
                                          };  	
  	initDataSync(syncConfigData, "SyncWorkLoadsPc");
}

/** Data sync - Portfolios **/
function startSyncPortfolios(){
  	printMessage(" Inside - startSyncPortfolios ");
  	gblShowCustomProgress				= true;
  	gblSyncCallInProgress				= PCConstant.SYNC_PORTFOLIOS;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"SyncPortfoliosPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceCoversPc":{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
      									   "SyncWorkLoadsPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncEmployeesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceAreasPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncConfigDataPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SynciCabInstancesPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkDayStartPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}, 
                                           "SyncWorkDayEndPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkHistoryPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}
                                          };  	
  	initDataSync(syncConfigData, "SyncPortfoliosPc");
}

/** Data sync - Start day **/
function startSyncStartDay(){
  	printMessage(" Inside - startSyncStartDay ");
  	gblShowCustomProgress				= false;
  	gblSyncCallInProgress				= PCConstant.SYNC_START_DAY;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"SyncWorkDayStartPc"	:{doupload:true, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkDayEndPc"	:{doupload:true, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncPortfoliosPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceCoversPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncWorkLoadsPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
      									   "SyncEmployeesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceAreasPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncConfigDataPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SynciCabInstancesPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},                                            
                                           "SyncWorkHistoryPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}
                                          };  	
  	initDataSync(syncConfigData, "SyncWorkDayStartPc");
}

/** Data sync - End day **/
function startSyncEndDay(){
  	printMessage(" Inside - startSyncEndDay ");
  	gblShowCustomProgress				= false;
  	gblSyncCallInProgress				= PCConstant.SYNC_END_DAY;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"SyncWorkDayStartPc"	:{doupload:true, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkDayEndPc"	:{doupload:true, dodownload:false, uploaderrorpolicy:"continueonerror"},                                           
                                           "SyncPortfoliosPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceCoversPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncWorkLoadsPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncEmployeesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceAreasPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncConfigDataPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SynciCabInstancesPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},                               
                                           "SyncWorkHistoryPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}
                                          };  	
  	initDataSync(syncConfigData, "SyncWorkDayEndPc");
}

/** Data sync - Work History **/
function startSyncWorkHistory(){
  	printMessage("Inside - startSyncWorkHistory");
  	gblShowCustomProgress				= false;
  	gblSyncCallInProgress				= PCConstant.SYNC_WORK_HISTORY;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"SyncWorkHistoryPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
      									   "SyncPortfoliosPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceCoversPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncWorkLoadsPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncEmployeesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceAreasPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncConfigDataPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SynciCabInstancesPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkDayStartPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}, 
                                           "SyncWorkDayEndPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}
                                          };  	
  	initDataSync(syncConfigData, "SyncWorkHistory");
}

/** Data sync - 15 minutes sync - scheduler sync **/
function start15MinutesSyncScheduler(){
  	printMessage(" Inside - start15MinutesSyncScheduler ");
    printMessage("prod issue analysis --start15MinutesSyncScheduler " + JSON.stringify(gblMyAppDataObject));
  	gblShowCustomProgress				= false;
  	gblSyncCallInProgress				= PCConstant.SYNC_15_MINUTES_SCHEDULER;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"SyncWorkDayStartPc"	:{doupload:true, dodownload:false, uploaderrorpolicy:"continueonerror"}, 
                                           "SyncWorkDayEndPc"	:{doupload:true, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncPortfoliosPc"	:{doupload:true, dodownload:true, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkLoadsPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceCoversPc":{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},                                                                                      
      									   "SyncEmployeesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceAreasPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncConfigDataPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SynciCabInstancesPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkHistoryPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}
                                          };  	
  	initDataSync(syncConfigData, "SyncWorkLoadsPc");
}

/** Data sync - Daily sync - scheduler sync **/
function startDailySyncScheduler(){
  	printMessage(" Inside - startDailySyncScheduler ");
  	gblShowCustomProgress				= false;
  	gblSyncCallInProgress				= PCConstant.SYNC_DAILY_SCHEDULER;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"SyncEmployeesPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkLoadsPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
                                           "SyncPortfoliosPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
                                           "SyncServiceCoversPc":{doupload:false, dodownload:true, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkDayStartPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}, 
                                           "SyncWorkDayEndPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},                                           
                                           "SyncServiceAreasPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
      									   "SyncConfigDataPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SynciCabInstancesPc":{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"},
                                           "SyncWorkHistoryPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:"continueonerror"}
                                          };  	
  	initDataSync(syncConfigData, "SyncEmployeesPc");
}

/** Start sync session **/
function initDataSync(configObj, scopeId){
  	printMessage(" Inside - initDataSync ");
  	gblIsSyncInProgress					= true;  	
  	if (gblShowCustomProgress === true) {
      	var scopeNameStr				= gblScopeMappings[scopeId];	
      	showProgressPopup("common.app.title", scopeNameStr, "common.message.syncStarting");
    } 
  	configObj							= (isEmpty(configObj)) ? {} : configObj;  	
	configObj.appid 					= "100004898512bd8be";
	configObj.serverhost 				= "rentokil-initial.sync.konycloud.com";
    /*configObj.appid 					= "100000002970f36ce";
	configObj.serverhost 				= "rentokil-initial-dev2.sync.konycloud.com"; */
  
  	configObj.issecure 					= true;
    configObj.serverport                ="";

  
  	configObj.userid					= PCConstant.SYNC_USER_ID;
	configObj.password					= PCConstant.SYNC_PASSWORD;
  	configObj.batchsize 				= PCConstant.SYNC_BATCH_SIZE;
	configObj.chunksize 				= PCConstant.SYNC_CHUNK_SIZE;
  	configObj.networktimeout 			= PCConstant.SYNC_TIMEOUT;
	
  	//sync life cycle callbacks
	configObj.onsyncstart 				= onSyncStartCallback;
	configObj.onscopestart 				= onScopeStartCallback;
	configObj.onscopecerror 			= onScopeErrorCallback;
	configObj.onscopesuccess 			= onScopeSuccessCallback;
	configObj.onuploadstart 			= onUploadStartCallback;
	configObj.onuploadsuccess 			= onUploadSuccessCallback;
  
  	configObj.ondownloadstart 			= onDownloadStartCallback;
	configObj.ondownloadsuccess 		= onDownloadSuccessCallback;
  	configObj.onbatchstored 			= onBatchstoredCallback;
	configObj.onbatchprocessingstart 	= onBatchProcessingStartCallback;
	configObj.onbatchprocessingsuccess 	= onBatchProcessingSuccessCallback;
	configObj.onuploadbatchsuccess 		= onUploadBatchSuccessCallback;
  	configObj.onchunkstart 				= onChunkStartCallback;
	configObj.onchunksuccess 			= onChunkSuccessCallback;
	configObj.onchunkerror 				= onChunkErrorCallback;	
  
  	configObj.onperformupgradesuccess  			= onPerformUpgradeSuccessCallback;
	configObj.onupgradescriptsdownloadstart 	= onUpgradeScriptsDownloadStartCallback;
	configObj.onupgradescriptsdownloadsuccess 	= onUpgradeScriptsDownloadSuccessCallback;
	configObj.onupgradescriptsdownloaderror 	= onUpgradeScriptsDownloadErrorCallback;
	configObj.onupgradescriptsexecutionstart 	= onUpgradeScriptsExecutionStartCallback;
	configObj.onupgradescriptsexecutionsuccess 	= onUpgradeScriptsExecutionSuccessCallback;
	configObj.onupgradescriptsexecutionerror 	= onUpgradeScriptsExecutionErrorCallback;
	configObj.onperformupgradeerror 			= onPerformUpgradeErrorCallback;
	configObj.onperformupgradestart 			= onPerformUpgradeStartCallback;
	configObj.onupgraderequired 				= onUpgradeRequiredCallback;
  
  	if (scopeId === "SyncWorkDayStartPc" || scopeId === "SyncWorkDayEndPc") {
      	configObj.removeafterupload 			= {"SyncWorkDayStartPc":["workDayStart"],"SyncWorkDayEndPc":["workDayEnd"]};
    }
  	
  	//configObj.onretry 				= onRetryCallback;	
  	/** Retry Error Codes **/
  	//1000 - Unknown Error while connecting
  	//1011 - Device has no WIFI or mobile connectivity
  	//1014 - Request Timed out
  	//1015 - Cannot find host
  	//1016 - Cannot connect to host
	//configObj.numberofretryattempts 	= PCConstant.SYNC_RETRY_ATTEMPTS;
	//configObj.retrywaittime 			= PCConstant.SYNC_RETRY_WAIT_TIME;
  
	configObj.onsyncsuccess 			= onSyncSuccessCallback;
	configObj.onsyncerror 				= onSyncErrorCallBack;
  	sync.startSession(configObj);
}

/** Sync start callback **/
function onSyncStartCallback(response){
  	printMessage(" Inside - onSyncStartCallback ");
}

/** Sync scope start callback **/
function onScopeStartCallback(response){
  	printMessage(" Inside - onScopeStartCallback ");
  	printMessage(" response " + JSON.stringify(response));
  	if (gblShowCustomProgress === true) {
      	var scopeNameStr				= (!isEmpty(response) && !isEmpty(response.currentScope)) ? gblScopeMappings[response.currentScope] : "";
      	if (!isEmpty(scopeNameStr)){
          	updateMessageInProgressPopup(scopeNameStr);
        }
      	
      	updateProgressInProgressPopup("common.message.syncInProgress");
    } 
}

/** Sync scope error callback **/
function onScopeErrorCallback(response){
	printMessage(" Inside - onScopeErrorCallback ");
}

/** Sync scope success callback **/
function onScopeSuccessCallback(response){
  	printMessage(" Inside - onScopeSuccessCallback ");
  	if (gblShowCustomProgress === true) {
      	updateProgressInProgressPopup("common.message.syncDone");
    } 
}

/** Sync upload start callback **/
function onUploadStartCallback(response){
	printMessage("Inside - onUploadStartCallback ");
  	if (gblShowCustomProgress === true) {
      	updateProgressInProgressPopup("common.message.syncUploading");
    }
  
  	var reqData 									= response.uploadRequest;
    reqData.clientcontext 							= {};      
   	
  	var apiCallKeys									= gblAPICallAccessKeys[gblBuildMode];
 /* reqData.clientcontext.client_id 				= apiCallKeys.client_id;
    reqData.clientcontext.client_secret 			= apiCallKeys.client_secret;*/
  
  /*  STAGING 
  reqData.clientcontext.client_id 				= "c89acbf594ec450480d9dbf518a0f4fe";
    reqData.clientcontext.client_secret 			= "0c3d0df4ffc74563BDF5AA7D001F5CAC";
  
   	/* PRODUCTION */
    reqData.clientcontext.client_id 				= "e4dfc2b9d3fa474dbacbe1d9048745f2";
    reqData.clientcontext.client_secret 			= "0055aa2686434b58AEA5F6691321A0A3"; 
  
	printMessage("Inside - onUploadStartCallback - reqData: " + JSON.stringify(reqData));
  	var selectedIcabInstance						= gblMyAppDataObject.getIcabInstance();
  	if (!isEmpty(selectedIcabInstance)) {
      	reqData.clientcontext.businessCode 			= selectedIcabInstance.businessCode;
      	reqData.clientcontext.countryCode 			= selectedIcabInstance.countryCode;
      	reqData.clientcontext.email 				= selectedIcabInstance.email;
    } else {
      	reqData.clientcontext.businessCode 			= PCConstant.DEF_BUSINESS_CODE;
      	reqData.clientcontext.countryCode 			= PCConstant.DEF_COUNTRY_CODE;
      	reqData.clientcontext.email 				= gblMyAppDataObject.selectedEmail;
    }
  	reqData.clientcontext.languageCode				= PCConstant.DEF_LANGUAGE_CODE;
  
    //modifying for site risk assesment upload
   	if (gblSyncCallInProgress === PCConstant.SYNC_15_MINUTES_SCHEDULER){
       reqData.clientcontext.propertyContactName="";
      reqData.clientcontext.propertyContactPosition="";
      reqData.clientcontext.propertyContactTelephone="";
      reqData.clientcontext.propertyContactMobile="";
      reqData.clientcontext.propertyContactEmail="";
      reqData.clientcontext.propertySpecialInstructions="";
      reqData.clientcontext.parkingGpsCoordinateX="";
      reqData.clientcontext.parkingGpsCoordinateY="";
      reqData.clientcontext.propertyAccessTimeStart1="";
      reqData.clientcontext.propertyAccessTimeEnd1="";
      reqData.clientcontext.propertyAccessTimeStart2="";
      reqData.clientcontext.propertyAccessTimeEnd2="";
      reqData.clientcontext.propertyAccessTimeStart3="";
      reqData.clientcontext.propertyAccessTimeEnd3="";
      reqData.clientcontext.propertyAccessTimeStart4="";
      reqData.clientcontext.propertyAccessTimeEnd4="";
      reqData.clientcontext.propertyAccessTimeStart5="";
      reqData.clientcontext.propertyAccessTimeEnd5="";
      reqData.clientcontext.propertyAccessTimeStart6="";
      reqData.clientcontext.propertyAccessTimeEnd6="";
      reqData.clientcontext.propertyAccessTimeStart7="";
      reqData.clientcontext.propertyAccessTimeEnd7="";
      reqData.clientcontext.propertyAccessTimeStart8="";
      reqData.clientcontext.propertyAccessTimeEnd8="";
      reqData.clientcontext.propertyAccessTimeStart9="";
      reqData.clientcontext.propertyAccessTimeEnd9="";
      reqData.clientcontext.propertyAccessTimeStart10="";
      reqData.clientcontext.propertyAccessTimeEnd10="";
      reqData.clientcontext.propertyAccessTimeStart11="";
      reqData.clientcontext.propertyAccessTimeEnd11="";
      reqData.clientcontext.propertyAccessTimeStart12="";
      reqData.clientcontext.propertyAccessTimeEnd12="";
      reqData.clientcontext.propertyAccessTimeStart13="";
      reqData.clientcontext.propertyAccessTimeEnd13="";
      reqData.clientcontext.propertyAccessTimeStart14="";
      reqData.clientcontext.propertyAccessTimeEnd14="";
      reqData.clientcontext.preferredDay1=false;
      reqData.clientcontext.preferredDay2=false;
      reqData.clientcontext.preferredDay3=false;
      reqData.clientcontext.preferredDay4=false;
      reqData.clientcontext.preferredDay5=false;
      reqData.clientcontext.preferredDay6=false;
      reqData.clientcontext.preferredDay7=false;
      reqData.clientcontext.propertyContractDeleted=false;
      reqData.clientcontext.propertyAccessTimeEnd3=false;
      reqData.clientcontext.updateDateTime= getDateTimeFormat(new Date(),"YYYY-MM-DDTHH:mm:ss.SSS");
    }
  
    //changes end
	printMessage("ReqData " + JSON.stringify(reqData));
  	return reqData;
}

/** Sync upload success callback **/
function onUploadSuccessCallback(response){
  	printMessage("Inside - onUploadSuccessCallback ");
}

/** Sync download start callback **/
function onDownloadStartCallback(response){
  	printMessage("Inside - onDownloadStartCallback ");
  	printMessage("Response " + JSON.stringify(response));
  	if (gblShowCustomProgress === true) {
      	updateProgressInProgressPopup("common.message.syncDownloading");
    }
  	var reqData 									= response.downloadRequest;
  	reqData.clientcontext 							= {};
  
  	var apiCallKeys									= gblAPICallAccessKeys[gblBuildMode];
    reqData.clientcontext.client_id 				= "e4dfc2b9d3fa474dbacbe1d9048745f2";
    reqData.clientcontext.client_secret 			= "0055aa2686434b58AEA5F6691321A0A3"; 
  
/*  STAGING 
    reqData.clientcontext.client_id 				= "c89acbf594ec450480d9dbf518a0f4fe";
    reqData.clientcontext.client_secret 			="0c3d0df4ffc74563BDF5AA7D001F5CAC"; */
  	
  	var selectedIcabInstance						= gblMyAppDataObject.getIcabInstance();
  	if (!isEmpty(selectedIcabInstance)) {
      	reqData.clientcontext.businessCode 			= selectedIcabInstance.businessCode;
      	reqData.clientcontext.countryCode 			= selectedIcabInstance.countryCode;
      	reqData.clientcontext.email 				= selectedIcabInstance.email;
    } else {
      	reqData.clientcontext.businessCode 			= PCConstant.DEF_BUSINESS_CODE;
      	reqData.clientcontext.countryCode 			= PCConstant.DEF_COUNTRY_CODE;
      	reqData.clientcontext.email 				= gblMyAppDataObject.selectedEmail;
    }
  	reqData.clientcontext.languageCode				= PCConstant.DEF_LANGUAGE_CODE;
    printMessage("prod issue analysis --onDownloadStartCallback----->> servicearea ---->>>>>>" + JSON.stringify(gblMyAppDataObject.selServiceArea));
  	if (gblSyncCallInProgress === PCConstant.SYNC_EMP_S_AREA_C_DATA){
      	reqData.clientcontext.actionsStandards 		= false;
		reqData.clientcontext.calendarEventTypes 	= false;
		reqData.clientcontext.contractTypes 		= false;
		reqData.clientcontext.customerContactTypes 	= false;
		reqData.clientcontext.customerTypes 		= false;
		reqData.clientcontext.detectorTypes 		= false;
		reqData.clientcontext.detectors 			= false;
		reqData.clientcontext.infestationLevels 	= false; 
		reqData.clientcontext.infestationLocations 	= false;
		reqData.clientcontext.issueTypes 			= true;
		reqData.clientcontext.issues 				= true;
		reqData.clientcontext.jobTitles 			= false;
		reqData.clientcontext.noteStandards 		= false;
		reqData.clientcontext.noteTypes 			= false;
		reqData.clientcontext.occupations 			= false;
		reqData.clientcontext.pestTypes 			= false;
		reqData.clientcontext.pests					= false;
		reqData.clientcontext.physicalIDTypes 		= false;
		reqData.clientcontext.portfolioStatuses 	= false;
		reqData.clientcontext.preparations 			= true;
		reqData.clientcontext.previousRequestDate 	= getDateTimeFormat(new Date(),"YYYY-MM-DDTHH:mm:ss.SSS");
		reqData.clientcontext.recommendationStandards= false;
		reqData.clientcontext.recommendationStatuses= false;
		reqData.clientcontext.recommendationTypes 	= false;
		reqData.clientcontext.serviceLines 			= false;
		reqData.clientcontext.taskStandards 		= false;
		reqData.clientcontext.taskStatuses 			= false;
		reqData.clientcontext.taskTypes 			= false;
		reqData.clientcontext.workOrderTypes 		= true;
    } else if (gblSyncCallInProgress === PCConstant.SYNC_WORK_LOADS || gblSyncCallInProgress === PCConstant.SYNC_PORTFOLIOS || gblSyncCallInProgress === PCConstant.SYNC_15_MINUTES_SCHEDULER || gblSyncCallInProgress === PCConstant.SYNC_DAILY_SCHEDULER){
      	var serviceAreas							= [];
        printMessage("prod issue analysis --onDownloadStartCallback " + JSON.stringify(gblMyAppDataObject));
      printMessage("prod issue analysis --onDownloadStartCallback----->> servicearea" + JSON.stringify(gblMyAppDataObject.selServiceArea));
      	serviceAreas.push({"branchNumber":gblMyAppDataObject.selServiceArea.branchNumber,"serviceAreaCode":gblMyAppDataObject.selServiceArea.serviceAreaCode});
      	reqData.clientcontext.serviceArea 			= (gblAddEncodeURI === true) ? encodeURI(JSON.stringify(serviceAreas)) : JSON.stringify(serviceAreas);
      	reqData.clientcontext.serviceCover 			= "";
      
      	if (gblSyncCallInProgress === PCConstant.SYNC_WORK_LOADS || gblSyncCallInProgress === PCConstant.SYNC_PORTFOLIOS) {
          	reqData.clientcontext.requestDaysAhead	= PCConstant.REQ_DAYS_AHEAD;
       		reqData.clientcontext.requestDaysBehind	= PCConstant.REQ_DAYS_BEHIND;
             printMessage("requestDaysAhead " + PCConstant.REQ_DAYS_AHEAD);
             printMessage("requestDaysBehind " + PCConstant.REQ_DAYS_BEHIND);
        } else {
          	var lastSyncDateTime					= kony.store.getItem(PCConstant.LAST_SYNC_DATE_TIME); 
            if (!isEmpty(lastSyncDateTime)) {
                var currentDateTime 				= getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
                printMessage("Last sync DateTime: " + lastSyncDateTime + " current DateTime: " + currentDateTime);
                var dateDiffInMins 					= getDateTimeDiffInMins(currentDateTime, lastSyncDateTime);
                dateDiffInMins 						= parseInt(dateDiffInMins);
                printMessage(" dateDiffInMins: " + dateDiffInMins);
                if (dateDiffInMins >= 1440){ /** 1440 minutes = 24 hours **/
                    reqData.clientcontext.requestDaysAhead	= PCConstant.REQ_DAYS_AHEAD;
                    reqData.clientcontext.requestDaysBehind	= PCConstant.REQ_DAYS_BEHIND;
                    
                } else {
                    reqData.clientcontext.requestDaysAhead	= PCConstant.REQ_DAYS_AHEAD;
                    reqData.clientcontext.requestDaysBehind	= PCConstant.REQ_DAYS_BEHIND;
                    printMessage("requestDaysAhead inside else " + PCConstant.REQ_DAYS_AHEAD);
                    printMessage("requestDaysBehind inside else " + PCConstant.REQ_DAYS_BEHIND);
                }
            } else {
                reqData.clientcontext.requestDaysAhead		= PCConstant.REQ_DAYS_BEHIND;
                reqData.clientcontext.requestDaysBehind		= PCConstant.REQ_DAYS_AHEAD;
            }
        }
    } else if (gblSyncCallInProgress === PCConstant.SYNC_START_DAY) {
      
    } else if (gblSyncCallInProgress === PCConstant.SYNC_END_DAY) {
      
    } else if (gblSyncCallInProgress === PCConstant.SYNC_WORK_HISTORY){
      	var propertyCode					= "";
      	var contractCode					= "";
      	if(!gblCompletedVisitFlag){
       		propertyCode					= gblMyAppDataObject.selScheduledItem.propertyCode;
       		contractCode					= gblMyAppDataObject.selScheduledItem.contractCode;
        } else {
         	propertyCode	                = gblMyAppDataObject.selCompletedItem.propertyCode;
         	contractCode					= gblMyAppDataObject.selCompletedItem.contractCode;
        }
      	var propertyContract				= [];
      	propertyContract.push({"contractCode":contractCode,"propertyCode":propertyCode});
      	reqData.clientcontext.propertyContract 		= (gblAddEncodeURI === true) ? encodeURI(JSON.stringify(propertyContract)) : JSON.stringify(propertyContract);
      	reqData.clientcontext.maxNumberOfVisits		= PCConstant.MAX_NO_DAYS;
        reqData.clientcontext.historyDayRange		= PCConstant.HIS_DAY_RANGE;
      	printMessage(" ReqData - work and service cover - " + JSON.stringify(reqData.clientcontext));
    }
  	printMessage("ReqData " + JSON.stringify(reqData));
  
  	return reqData;
}

/** Sync download success callback **/
function onDownloadSuccessCallback(response){
  	printMessage("Inside - onDownloadSuccessCallback");
  	printMessage("Response" + JSON.stringify(response));
}

/** Sync batch stored callback **/
function onBatchstoredCallback(response){
  	printMessage("Inside - onBatchstoredCallback");
}

/** Sync batch processing start callback **/
function onBatchProcessingStartCallback(response){
  	printMessage("Inside - onBatchProcessingStartCallback");
}

/** Sync batch processing success callback **/
function onBatchProcessingSuccessCallback(response){
  	printMessage("Inside - onBatchProcessingSuccessCallback");
}

/** Sync batch upload success callback **/
function onUploadBatchSuccessCallback(response){
  	printMessage("Inside - onUploadBatchSuccessCallback");
}

/** Sync chunk start callback **/
function onChunkStartCallback(response){
  	printMessage("Inside - onChunkStartCallback");
}

/** Sync chunk success callback **/
function onChunkSuccessCallback(response){
  	printMessage("Inside - onChunkSuccessCallback");
}

/** Sync chunk error callback **/
function onChunkErrorCallback(response){
  	printMessage("Inside - onChunkErrorCallback");
}

/** Sync success callback **/
function onSyncSuccessCallback(response){
  	printMessage("Inside - onSyncSuccessCallback");
  	if (gblShowCustomProgress === true) {
      	updateProgressInProgressPopup("common.message.syncFinished");
    }
  
  	var today 						= new Date();
	var dateStr 					= today.toLocaleDateString();
	var timeStr 					= today.toLocaleTimeString();
  	var syncDateStr					= dateStr + " " + timeStr;
  	kony.store.setItem(PCConstant.DISPLAY_SYNC_DATE_TIME, syncDateStr); /** To display in settings screen **/
  	gblIsSyncInProgress				= false;
  
  	var lastSyncDate 				= ""; /** Used to check Daily sync criteria **/
  	var lastSyncDateTime 			= ""; /** Used to check 15 minutes sync criteria **/
  	var lastWorkSyncDateTime 		= ""; /**  **/
  	
  	if (gblShowCustomProgress === true) {
        if (gblSyncCallInProgress === PCConstant.SYNC_ICAB_INSTANCES){
          	showLoading("common.message.loading");
          	dismissProgressMessage();
          	getiCabInstancesFromLocalTbl();
        } else if (gblSyncCallInProgress === PCConstant.SYNC_EMP_S_AREA_C_DATA){
          	lastSyncDate 			= getDateTimeFormat(new Date(), "YYYY-MM-DD");
	    	kony.store.setItem(PCConstant.LAST_SYNC_DATE, lastSyncDate);
	    
	   	 	lastSyncDateTime 		= getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
	    	kony.store.setItem(PCConstant.LAST_SYNC_DATE_TIME, lastSyncDateTime);
          
          	getEmployeeFromLocalTbl();
        } else if (gblSyncCallInProgress === PCConstant.SYNC_WORK_LOADS){
          	lastWorkSyncDateTime 	= getDateTimeFormat(new Date(),"YYYY-MM-DDTHH:mm:ss.SSS");
	    	kony.store.setItem(PCConstant.LAST_WORK_SYNC_DATE_TIME, lastWorkSyncDateTime);
    		startSyncPortfolios();
    	} else if (gblSyncCallInProgress === PCConstant.SYNC_PORTFOLIOS){
    		dismissProgressMessage();
          	showLoading("common.message.loading");
          	startSyncScheduler();
          	onInitialSyncDone();
    	} else if (gblSyncCallInProgress === PCConstant.SYNC_START_DAY) {
      		//nothing to do now
          	printMessage("Inside - onSyncSuccessCallback - PCConstant.SYNC_START_DAY");
        } else if (gblSyncCallInProgress === PCConstant.SYNC_END_DAY) {
			//nothing to do now
          	printMessage("Inside - onSyncSuccessCallback - PCConstant.SYNC_END_DAY");
        }
    } else {
   		if (gblSyncCallInProgress === PCConstant.SYNC_WORK_HISTORY){
          	printMessage("onSyncSuccessCallback workhistory");
     		getWorkHistoryFromLocalTbl();
    	} else if (gblSyncCallInProgress === PCConstant.SYNC_15_MINUTES_SCHEDULER){
    		lastSyncDateTime 		= getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
			kony.store.setItem(PCConstant.LAST_SYNC_DATE_TIME, lastSyncDateTime);
			lastWorkSyncDateTime 	= getDateTimeFormat(new Date(),"YYYY-MM-DDTHH:mm:ss.SSS");
	    	kony.store.setItem(PCConstant.LAST_WORK_SYNC_DATE_TIME, lastWorkSyncDateTime);
          	refreshWorkView();          	
    	} else if (gblSyncCallInProgress === PCConstant.SYNC_DAILY_SCHEDULER){
    		lastSyncDate 			= getDateTimeFormat(new Date(), "YYYY-MM-DD");
	    	kony.store.setItem(PCConstant.LAST_SYNC_DATE, lastSyncDate);
	    	lastWorkSyncDateTime 	= getDateTimeFormat(new Date(),"YYYY-MM-DDTHH:mm:ss.SSS");
	    	kony.store.setItem(PCConstant.LAST_WORK_SYNC_DATE_TIME, lastWorkSyncDateTime); 
    	} else {
          	dismissLoading();
        }
    }
}

/** Sync error callback **/
function onSyncErrorCallBack(response){
  	printMessage("Inside - onSyncErrorCallBack");
  	printMessage("Response " + JSON.stringify(response));
  	gblIsSyncInProgress			= false;
  	if (gblShowCustomProgress === true) {
      	showMessagePopup("common.message.syncError", ActionOnMessagePopupOkBtn);
      	dismissProgressMessage();
      	dismissLoading();
    } 

  	if (gblSyncCallInProgress === PCConstant.SYNC_ICAB_INSTANCES){
    	showMessagePopup("common.message.syncError", ActionOnMessagePopupOkBtn);
      	dismissProgressMessage();
      	dismissLoading();
    }
  
  	if (gblSyncCallInProgress === PCConstant.SYNC_WORK_HISTORY){
    	printMessage("onSyncSuccessCallback workhistory");
		getWorkHistoryFromLocalTbl();
   	} 
}

/** Stop sync session **/
function stopSyncSession(){
  	sync.stopSession(onSyncStopSessionCallback);
}

/** Sync stop session callback **/
function onSyncStopSessionCallback(response){
  	gblIsSyncInProgress		= false;
  	dismissProgressMessage();
  	setFlxOverlayMsgOkButtonStyle(true);
  	dismissLoading();
}

/** Sync upgrade success callback **/
function onPerformUpgradeSuccessCallback(response){
  
}

/** Sync upgrade scripts download callback **/
function onUpgradeScriptsDownloadStartCallback(response){
  
}

/** Sync upgrade scripts download success callback **/
function onUpgradeScriptsDownloadSuccessCallback(response){
  
}

/** Sync upgrade scripts download error callback **/
function onUpgradeScriptsDownloadErrorCallback(response){
  
}

/** Sync upgrade scripts execution start callback **/
function onUpgradeScriptsExecutionStartCallback(response){
  
}

/** Sync upgrade scripts execution success callback **/
function onUpgradeScriptsExecutionSuccessCallback(response){
  
}

/** Sync upgrade scripts execution error callback **/
function onUpgradeScriptsExecutionErrorCallback(response){
  
}

/** Sync perform upgrade error callback **/
function onPerformUpgradeErrorCallback(response){
  
}

/** Sync perform upgrade start callback **/
function onPerformUpgradeStartCallback(response){
  
}

/** Sync upgrade required callback **/
function onUpgradeRequiredCallback(response){
  
}