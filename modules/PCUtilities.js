/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all utility methods
***********************************************************************/

/** To show loading indicator **/
function showLoading(messageKey){
	var lodingText 		= getI18nString(messageKey);
	try {
		kony.application.showLoadingScreen(loadingSkin, lodingText,"center", true,true, null);
	} catch(e) {
		printMessage("Error while showing loading indicator: " + e);
	}	
}

/** To dismiss loading indicator **/
function dismissLoading(){
	kony.application.dismissLoadingScreen();
}

/** To get i18n localized string for given key **/
function getI18nString(key){
	return key ? kony.i18n.getLocalizedString(key) : "";
}

/** To remove whitespaces  **/
function trim(inputString){
	var outputString 	= "";
	if(inputString !== null){
		outputString 	= kony.string.trim(inputString);
	}
	return outputString;
}

/** To check a string or object is valid or not  **/
function isEmpty(val){
	return (val === undefined || val === null || val === '' || val.length <= 0 || (typeof(val) === "object" && Object.keys(val).length === 0)) ? true : false;
}

/** To make given string to uppercase  **/
function toUpperCase(inputString){
	var outputVal = "";
	if(!isEmpty(inputString)){
		outputVal = inputString.toUpperCase();
	}
	return outputVal;
}

/** To make given string to uppercase  **/
function toLowerCase(inputString){
	var outputVal = "";
	if(!isEmpty(inputString)){
		outputVal = inputString.toLowerCase();
	}
	return outputVal;
}

/** To add a leading zero on given string  **/
function appendLeadingZero(data) {
	return (parseInt(data) < 10) ? '0' + data : data;
}

/** To validate string length limit  **/
function isValidCharsCountInString(textStr, charLimit){
	var boolResult			= true;
	if (!isEmpty(textStr)){
		if (textStr.length > charLimit){
			boolResult		= false;
		}
	}
	return boolResult;
}

/** To get Current locale  **/
function getCurrentLocale(){
	var currentLocale = "";
	try {
		currentLocale = kony.i18n.getCurrentLocale();		
	} catch(i18nError) {
		printMessage("Exception While getting currentLocale  : "+i18nError );
	}	
	return currentLocale;
}

/** To check network availability  **/
function isNetworkAvailable(){	
	return kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);
}

/** To exit application  **/
function exitApplication(){
	kony.application.exit();
}

/** To print a message on console  **/
function printMessage(message){
  	var finalMsg				= "*** PROPERTY_CARE *** :: " + message;
	kony.print(finalMsg);
	
}

/** To get a matching record from given array of elements  **/
function getMatchingItemFromArray(recordsArray, matchingKey, matchingValue){
	var matchingItem			= null;
	if (!isEmpty(recordsArray) && recordsArray.length > 0){
		var arrayLength			= recordsArray.length;
		var aRecord				= null;
		for (var i = 0; i < arrayLength; i++){
			aRecord				= recordsArray[i];
			
			if (aRecord[matchingKey] === matchingValue){
				matchingItem	= aRecord;
				break;
			}
		}
	}
	
	return matchingItem;
}

/** To find index of a record from an array  **/
function getItemIndexFromArray(recordsArray, record){
	var index 	= (!isEmpty(recordsArray) && !isEmpty(record)) ? recordsArray.indexOf(record) : -1;
	return index;
}

/** To remove a record from an array  **/
function removeItemFromArray(recordsArray, record){
	var index	= getItemIndexFromArray(recordsArray, record);
	if(index !== -1) {
		recordsArray.splice(index, 1);
	}
}

/** To add a record in an array  **/
function addItemToArray(recordsArray, record, index){
	recordsArray.splice(index, 0, record);
}

/** To search a record in given array  **/
function isRecordExist(record, recordsArray, matchingKey, matchingKey2){
	var result					= false;
	if (!isEmpty(recordsArray) && recordsArray.length > 0){
		var arrayLength			= recordsArray.length;
		var aRecord				= null;
		for (var i = 0; i < arrayLength; i++){
			aRecord				= recordsArray[i];
			
			if (aRecord[matchingKey] === record[matchingKey] || (!isEmpty(matchingKey2) && aRecord[matchingKey2] === record[matchingKey2])){
				result			= true;
				break;
			}
		}
	}
	
	return result;
}

/** To identify matched record from an array  **/
function getMatchedRecordFromArray(recordsArray, matchingKey, matchingValue){
	var resultRecord			= null;
	if (!isEmpty(recordsArray) && recordsArray.length > 0){
		var arrayLength			= recordsArray.length;
		var aRecord				= null;
		for (var i = 0; i < arrayLength; i++){
			aRecord				= recordsArray[i];
			
			if (aRecord[matchingKey] === matchingValue){
				resultRecord	= aRecord;
				break;
			}
		}
	}
	return resultRecord;
}

/** To validate time  **/
function isValidTime(time) {
    var result 	= false, m;
    var re 		= /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;
    if ((m = time.match(re))) {
        result 	= true;
    }
    return result;
}

/** To get todays date string for comparison **/
function getTodaysDateStr(){
  	var today	= new Date();
  	var yy		= today.getFullYear();
  	var mm		= today.getMonth();
  	var dd		= today.getDate();
  	mm			= mm + 1;
  	mm			= (mm <= 9) ? ("0" + mm.toString()) : mm.toString();
  	dd			= (dd <= 9) ? ("0" + dd.toString()) : dd.toString(); 
  	yy			= yy.toString();
  	var dateStr	= yy + "-" + mm + "-" + dd;
  	return dateStr;
}

/** To get current time string (hh:mm) **/
function getCurrentTimeStr(){
  	var today	= new Date();
  	var hh		= today.getHours();
  	var mm		= today.getMinutes();
  	hh			= (hh <= 9) ? ("0" + hh.toString()) : hh.toString(); 
  	mm			= (mm <= 9) ? ("0" + mm.toString()) : mm.toString(); 
  	var timeStr	= hh + ":" + mm;
  	return timeStr;
}

/** To format a date string in more readable style **/
function expandDateStr(dateStr){
  	var dateVal			= ymdStringToDate(dateStr);
	var dateExpandedStr	= dateVal.toLocaleDateString();  
  	return dateExpandedStr;
}

/** To get todays date string for comparison **/
function getCurrentDayStr(){
  	var today	= new Date();
  	var dd		= today.getDate();
  	dd			= (dd <= 9) ? ("0" + dd.toString()) : dd.toString(); 
  	return dd;
}

/** String [in the format YYYY-mm-dd] to Date **/
function ymdStringToDate(dateStr){
	var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
	var date 	= new Date(dateStr.replace(pattern,'$1-$2-$3'));
  	return date;
}

/** Date time string [in the format YYYY-mm-dd and hh:mm] to Date **/
function dateTimeStringToDate(dateStr, timeStr){
  	var dateTimeStr		= dateStr + " " + timeStr + ":00";
	var pattern 		= /(\d{4})\-(\d{2})\-(\d{2}) (\d{2})\:(\d{2})\:(\d{2})/;
	var date 			= new Date(dateTimeStr.replace(pattern,'$1-$2-$3 $4:$5:$6'));
  	return date;
}

/** To add space in the beiging of given string **/
function prefixWhiteSpace(strText, spaceCount){
	spaceCount		= (!isEmpty(spaceCount)) ? spaceCount : 3;
  	var spaceStr 	= "";
  	for (var i = 0; i < spaceCount; i++) {
      	spaceStr	+= "&nbsp;";
    }
  	var newStr		= spaceStr + strText;
  	return newStr;
}

/** To format time in hour minutes and seconds style **/
function getActualTime(timeStampValue){  
  	var timeDiffHours 						= Math.floor(parseInt(timeStampValue) / 1000 / 60 / 60);
  	  	
	var timeDiff1 							= parseInt(timeStampValue) - (timeDiffHours * 1000 * 60 * 60);
  	var timeDiffMinutes 					= Math.floor(timeDiff1 / 1000 / 60);
  	
  	//var timeDiff2 							= timeDiff1 - (timeDiffMinutes * 1000 * 60);
	//var timeDiffSeconds 					= Math.floor(timeDiff2 / 1000);
  	
  	timeDiffHours							= (timeDiffHours < 10) ? ("0" + timeDiffHours) : timeDiffHours;
  	timeDiffMinutes							= (timeDiffMinutes < 10) ? ("0" + timeDiffMinutes) : timeDiffMinutes;
  	//timeDiffSeconds							= (timeDiffSeconds < 10) ? ("0" + timeDiffSeconds) : timeDiffSeconds;
  
  	//var totalTimeDiffInFormat				= timeDiffHours+":"+timeDiffMinutes+":"+timeDiffSeconds;
  	var totalTimeDiffInFormat				= timeDiffHours+":"+timeDiffMinutes;
  
  	return totalTimeDiffInFormat;
}

/**
* Determines Date 
*/
function getDateStatus(pastDate){
	var isFuture 		= false;
	try {
      	printMessage("Test:: Request Date:: " + pastDate);
		var date 		= getDateTimeFormat(new Date(pastDate),"YYYY-MM-DD");
        printMessage("Test:: After Date:: " + date);
         
		var now 		= getDateTimeFormat(new Date(),"YYYY-MM-DD");
        printMessage("Test:: Request now:: " + now);
     
		if (now < date) {
		   	// date is past
          	return 1;
		} else if(now > date){
		   	// date is future
          	return -1;
		} else {
           // date is Current Date
           return 0;
        }
	} catch(error){
		printMessage("Error while getting future date function");
	}
}

//Formats currentDateTime in given format
function getCurrentDateTimeInFormat(fmt){
    var today 		= new Date();
    var strDate1 	= today.getDate();
    strDate1 		= (strDate1 < 10) ? ("0" + strDate1) : strDate1;
    var strMonth1 	= today.getMonth() + 1;
    strMonth1 		= (strMonth1 < 10) ? ("0" + strMonth1) : strMonth1;
    var strYear1 	= today.getFullYear();
    var startDate 	= strYear1+"-"+strMonth1+"-"+strDate1;    
    var H 			= today.getHours();
    var HH 			= (H <= 9)? '0' + H : H;    
    var m 			= today.getMinutes();
    var mm 			= (m <= 9)? '0' + m : m;        
    var s 			= today.getSeconds();
    var ss 			= (s <= 9)? '0' + s : s;      
    var startTime 	= HH+":"+mm+":"+ss;
        
    switch (fmt) {
      	case "yyyy-MM-dd":
        	reqFmt 		= strYear1+"-"+strMonth1+"-"+strDate1;
        	break;
      	case "dd-MM-yyyy":
        	reqFmt 		= strDate1+"-"+strMonth1+"-"+strYear1;
        	break;
      	case "HH:mm:ss":
        	reqFmt 		= HH+":"+mm+":"+ss;
        	break;
     	case "yyyy-MM-dd HH:mm:ss":
        	reqFmt 		= strYear1+"-"+strMonth1+"-"+strDate1+" "+HH+":"+mm+":"+ss;
        	break;
        case "dd-MM-yyyy HH:mm:ss":
        	reqFmt 		= strDate1+"-"+strMonth1+"-"+strYear1+" "+HH+":"+mm+":"+ss;
        	break;
    }
    return reqFmt;
}

/*function getCurrentDateTime(){
  	var date 			= new Date();
    var x 				= date.getDate()+"";
    var dlen 			= x.length;
    if(dlen == 1){
      	x 				= "0"+x;
    }
    var year 			= date.getFullYear();
    var month 			= date.getMonth()+1;
    var twodigitsmonth 	= month+"";
    var mlen 			= twodigitsmonth.length;
    if(mlen == 1){
      	twodigitsmonth 	= "0"+twodigitsmonth;
    }		
    var hours = date.getHours()+"";
    var twodigitshours 	= hours.length;
    if(twodigitshours == 1){
      	hours 			= "0"+hours;
    }
    var minutes 		= date.getMinutes()+"";
    var twodigitsmin 	= minutes.length;
    if(twodigitsmin == 1){
      	minutes 		= "0"+minutes;
    }
    var seconds 		= date.getSeconds()+"";
    var twodigitsseconds= 	seconds.length;
    if(twodigitsseconds == 1){
      	seconds 		= "0"+seconds;
    }	
    var total 			= x+"/"+twodigitsmonth+"/"+year+"  "+hours+":"+minutes+":"+seconds;
  	return total;
}*/

/*****************************************************************
  Purpose : To date format(Ex: 12/23/2013,21 August,2013)
  Inputs  : Need date as object or string (Ex : new Date(yyyy,mm,dd,hh,mm,ss) or "02/04/2013 12:30:30")
             format can give as input      
  Ex:   
  
  MM/DD/YYYY    - 12/23/2013,
  DD/MM/YYYY    - 23/12/2013,
  Do MMMM, YYYY - 21st August, 2013 
  dddd Do, MMMM – Thursday, 3rd March
  Do MMMM, YYYY – 9th April, 2015
  hh:mm a     -   04:30 pm              
******************************************************************/
function getDateTimeFormat(date, format) {      
    var dateFormat 		= "";
    try {
        if(moment(date).isValid()){  
           	dateFormat 	= moment(date).format(format);
        }
        printMessage("&&&&&&&&& dateFormat is:" + dateFormat);
    } catch(error){
       	printMessage("moment date format error :" + error);
    }
    return dateFormat;
}

function isResultNotNull(resultTable){
	if (resultTable !== null && resultTable !== undefined && resultTable !== ""){
		return true;
	}else{
		return false;
	}
}

function sendMail(toMail){
    printMessage("Inside sendMail ::::"+toMail);
    var torecipients 		= [toMail];
    var ccrecipients 		= [];
    var bccrecipients 		= [];
    var subject 			= "Reg :";
    var messagebody 		= "Hi Team,";
    var ismessagebodyhtml 	= false;

    kony.phone.openEmail(torecipients, ccrecipients, bccrecipients, subject, messagebody, ismessagebodyhtml);
}

function isFutureDate(pastDate){
	var isFuture 			= false;
	try {
		var date 			= getDateTimeFormat(new Date(pastDate),"YYYY-MM-DD");
		var now 			= getDateTimeFormat(new Date(),"YYYY-MM-DD");
		if (now < date) {
		    isFuture 		= false;
		} else if(now > date){
		    isFuture 		= true;
		}
	} catch(error) {
		printMessage("Error while getting future date function");
	}
	return isFuture;
}

function getDateTimeDiffInMins(startDate, endDate){
  	printMessage("getDateTimeDiffInMins : startDate: " + startDate);
  	printMessage("getDateTimeDiffInMins : endDate: " + endDate);
	var mins 		= 0;
	try {
		var ms 		= moment(startDate,"DD/MM/YYYY HH:mm:ss").diff(moment(endDate,"DD/MM/YYYY HH:mm:ss"));
		var d 		= moment.duration(ms);
		var s 		= Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss")+"";
		
		var tempArr = s.split(":");
		mins 		= tempArr[1];
	} catch(error) {
		printMessage("Error while getDateTimeDiffInHours :"+JSON.stringify(error));
	}
	return mins;
}


/** To find out days between two given dates **/
function daysBtwDates(fromDate, toDate){
 	var oneDay 		= 24*60*60*1000; // hours*minutes*seconds*milliseconds
	var diffDays 	= Math.round(Math.abs((fromDate.getTime() - toDate.getTime())/(oneDay)));
   	printMessage("daysBtwDates : diffDays: " + diffDays);
   	return diffDays;
}

function generateUUID() {
    //// return uuid of form xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
    var uuid = '';
    var i;
    for (i = 0; i < 32; i += 1) {
      switch (i) {
      case 8:
      case 20:
        uuid += '-';
        uuid += (Math.random() * 16 | 0).toString(16);
        break;
      case 12:
        uuid += '-';
        uuid += '4';
        break;
      case 16:
        uuid += '-';
        uuid += (Math.random() * 4 | 8).toString(16);
        break;
      default:
        uuid += (Math.random() * 16 | 0).toString(16);
      }
    }
    kony.print("printing uuid"+uuid);
    return uuid;
   
  }