

/** Show visit screen **/
function showVisitScreen(){ 
  	gblCompletedVisitFlag                		= false;
  	Visit.show();
}

/** Visit Details screen - PreShow **/
function onPreShowVisitDetailsScreen(){
    printMessage("Inside - onPreShowVisitDetailsScreen");
    refreshi18nVisit();
    Visit.flxStopVisitBtnContainer.setVisibility(true);
    Visit.TabPaneVisits.activeTabs				= [0];
    Visit.lblPropertyName.text					= gblMyAppDataObject.inProgressVisit.propertyName;
    Visit.TabPaneVisits.lblStartTimeValue.text	= gblMyAppDataObject.inProgressVisit.visitStartTimeStr;
    Visit.TabPaneVisits.lblEndTime.text			= getCurrentTimeStr();
    Visit.TabPaneVisits.lblDurationValue.text	= "--:--";
    getPreparationsFromLocalTbl();
    getIssueCodesFromLocalTbl(PCConstant.SIGNATURE_ISSUE_TYPE_CODE);
}

/** Visit Details screen - PostShow **/
function onPostShowVisitDetailsScreen(){  
    printMessage("Inside - onPostShowVisitDetailsScreen");
    showLoading("common.message.loading");
    refreshSettingsUI();
    Visit.TabPaneVisits.txtAreaVisitNote.text	= (!isEmpty(gblMyAppDataObject.inProgressVisit.visitNote)) ? gblMyAppDataObject.inProgressVisit.visitNote : "";
    Visit.TabPaneVisits.txtCustomerNameSig.text	= (!isEmpty(gblMyAppDataObject.inProgressVisit.customerName)) ? gblMyAppDataObject.inProgressVisit.customerName : ""; 	
    handleUserInteractionOnVisitTextFields(Visit.TabPaneVisits.txtAreaVisitNote);
    handleUserInteractionOnVisitTextFields(Visit.TabPaneVisits.txtCustomerNameSig);
    //cheking the signature value and diabling visit note.
   printMessage("--->Sept05 gblMyAppDataObject.inProgressVisit.signedDate"+gblMyAppDataObject.inProgressVisit.signedDate);
   if((!isEmpty(gblMyAppDataObject.inProgressVisit.signedDate))){
     Visit.TabPaneVisits.txtAreaVisitNote.setEnabled(false);
     Visit.TabPaneVisits.txtAreaVisitNote.skin = sknTxtAreaDisabledVNote;
     Visit.TabPaneVisits.camVisitPhoto.setEnabled(false);
     Visit.TabPaneVisits.imgVisitPhoto.setVisibility(false);
      toggleButtonVisibility(false);
   }else{
     
     Visit.TabPaneVisits.txtAreaVisitNote.setEnabled(true);
     Visit.TabPaneVisits.txtAreaVisitNote.skin = sknTxtAreaVNote;
     Visit.TabPaneVisits.camVisitPhoto.setEnabled(true);
     Visit.TabPaneVisits.imgVisitPhoto.setVisibility(true);
     toggleButtonVisibility(true); 
     
   }
     
    if (gblMyAppDataObject.isDayStarted === PCConstant.DAY_STARTED_DONT_KNOW || gblMyAppDataObject.isDayStarted === PCConstant.DAY_STARTED_NO) {
      	Visit.imgStartdayIcon.src				= "endday.png";
    } else {
      	Visit.imgStartdayIcon.src				= "startday.png";
    }

    if (!isEmpty(gblMyAppDataObject.inProgressVisit.base64SignData)) {
        Visit.TabPaneVisits.lblSignDate.text 	= gblMyAppDataObject.inProgressVisit.signedDate;
        Visit.TabPaneVisits.imgSignature.setVisibility(true);
        Visit.TabPaneVisits.lblSignDate.setVisibility(false);
        Visit.TabPaneVisits.lblSignHere.setVisibility(false); 
        toggleNoSignReasonComboAccess(false);
    } else {
        Visit.TabPaneVisits.lblSignDate.text 	= "";
        Visit.TabPaneVisits.imgSignature.base64 = "";
        toggleNoSignReasonComboAccess(true);
        Visit.TabPaneVisits.lblSignHere.setVisibility(true);
        Visit.TabPaneVisits.imgSignature.setVisibility(false);
        Visit.TabPaneVisits.lblSignDate.setVisibility(false);
    } 	

    durationCalculator();
    dismissLoading();
    destroyFormsExceptMe(PCConstant.FORM_VISIT); /** Issue fix - Swipe operation on Home segment doesn't work if it is not destroyed here **/
}

/** Visit Details screen - Refresh all i18n **/
function refreshi18nVisit(){
    printMessage("Inside - refreshi18nVisit");
    Visit.lblHeaderVisit.text						= getI18nString("common.label.visitdetails");  
    Visit.TabPaneVisits.TabVTime.tabName			= prefixWhiteSpace(getI18nString("common.tab.time"), 2);
    Visit.TabPaneVisits.TabPreparations.tabName		= prefixWhiteSpace(getI18nString("common.tab.preparations"), 2);
    Visit.TabPaneVisits.TabVNotes.tabName			= prefixWhiteSpace(getI18nString("common.tab.visitNotes"), 2);
    Visit.TabPaneVisits.TabVSignature.tabName		= prefixWhiteSpace(getI18nString("common.tab.signature"), 2);

    Visit.TabPaneVisits.lblVisitStartTime.text		= getI18nString("common.label.startTime");
    Visit.TabPaneVisits.lblVisitEndTime.text		= getI18nString("common.label.endTime");
    Visit.TabPaneVisits.lblDuration.text			= getI18nString("common.label.duration");
    Visit.TabPaneVisits.lblPreprationHead.text		= getI18nString("common.label.preparations");
    Visit.TabPaneVisits.lblQuatity.text				= getI18nString("common.label.quantity");
    Visit.TabPaneVisits.lblSignHere.text			= getI18nString("common.label.signHere");

    var anItem										= null;
    var i											= 0;
    var sampleMasterDataQuantity					= [];
    if (gblPrepQuantityList.length > 0) {
        for (i = 0; i < gblPrepQuantityList.length; i++){
            anItem									= gblPrepQuantityList[i];
            sampleMasterDataQuantity.push([anItem.key, anItem.val]);
        }
    }

    Visit.TabPaneVisits.quatityCmb1.masterData		= sampleMasterDataQuantity;
    Visit.TabPaneVisits.quatityCmb2.masterData		= sampleMasterDataQuantity;
    Visit.TabPaneVisits.quatityCmb3.masterData		= sampleMasterDataQuantity;
    Visit.TabPaneVisits.quatityCmb4.masterData		= sampleMasterDataQuantity;
    Visit.TabPaneVisits.quatityCmb5.masterData		= sampleMasterDataQuantity;
    Visit.TabPaneVisits.quatityCmb1.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.PreprationsQuantity1)) ? gblMyAppDataObject.inProgressVisit.PreprationsQuantity1 : "0";
    Visit.TabPaneVisits.quatityCmb2.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.PreprationsQuantity2)) ? gblMyAppDataObject.inProgressVisit.PreprationsQuantity2 : "0";
    Visit.TabPaneVisits.quatityCmb3.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.PreprationsQuantity3)) ? gblMyAppDataObject.inProgressVisit.PreprationsQuantity3 : "0";
    Visit.TabPaneVisits.quatityCmb4.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.PreprationsQuantity4)) ? gblMyAppDataObject.inProgressVisit.PreprationsQuantity4 : "0";
    Visit.TabPaneVisits.quatityCmb5.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.PreprationsQuantity5)) ? gblMyAppDataObject.inProgressVisit.PreprationsQuantity5 : "0";

    if (isEmpty(gblMyAppDataObject.inProgressVisit.customerName)){
      	gblMyAppDataObject.inProgressVisit.customerName	= "";
    } 

    setAppDataObjInStore();
}

/** Duration calculating between start time and end time **/
function durationCalculator(){  
    var endTimeStr 									= getCurrentTimeStr();  
    var currentVisit								= gblMyAppDataObject.inProgressVisit;
    var startDate 									= dateTimeStringToDate(currentVisit.visitStartDateStr, currentVisit.visitStartTimeStr);
    var currentDateStr								= getTodaysDateStr();	
    if (isEmpty(endTimeStr)){
      	endTimeStr									= getCurrentTimeStr();
    } 
    var endDate 									= dateTimeStringToDate(currentDateStr, endTimeStr);
    var timeDiff 									= Math.abs(endDate - startDate); // in milli seconds
    var duration 									= getActualTime(timeDiff);
    Visit.TabPaneVisits.lblDurationValue.text 		= duration;
    currentVisit.visitEndDateStr					= currentDateStr;
    currentVisit.visitEndTimeStr					= endTimeStr;  
    currentVisit.duration							= duration;
    currentVisit.workAttendanceTimeDuration			= (timeDiff/1000); // in seconds
    printMessage("Inside - durationCalculator - Duration: " + currentVisit.workAttendanceTimeDuration + " seconds");
    gblMyAppDataObject.inProgressVisit				= currentVisit;
    setAppDataObjInStore();
}

/** Signature button action - FFI call **/
function onClickVisitSign(){
    printMessage("Inside - onClickVisitSign");
  
   // Added for new signature details
  
     var strimg = "";
    var SignHere=kony.i18n.getLocalizedString("common.label.signHereBtn");
    var Clear=kony.i18n.getLocalizedString("common.label.ClearBtn");
    var NewBtn=kony.i18n.getLocalizedString("common.label.SaveBtn");
    try {
        if(kony.string.equalsIgnoreCase("android", gblDeviceInfo.name)) {
         // 	signatureFFI.getSignature(callbackBase);
          signatureFFI.getSignature(signCallBack,strimg,SignHere,Clear,NewBtn);
          
        }
    } catch(e){
    	printMessage("Error Error while signature FFI call: " + e);
    }
}

/** Signature FFI callback **/
//function callbackBase(base64String){
  function signCallBack(base64String,unScaledBase64) {
  	printMessage("Inside - callbackBase");
  	try {
    	showLoading("common.message.loading");
        if (base64String.length > 0){
 			var signedDate										= getCurrentDateTimeInFormat("dd-MM-yyyy HH:mm:ss");
      		var updatedBase64Data								= base64String.replace(/\s+/g, ''); // Removing whitespace
       		Visit.TabPaneVisits.imgSignature.base64 			= updatedBase64Data;
      		Visit.TabPaneVisits.lblSignDate.text 				= signedDate;
      		gblMyAppDataObject.inProgressVisit.signedDate		= signedDate;
      		gblMyAppDataObject.inProgressVisit.noSignatureReason= "";
      		gblMyAppDataObject.inProgressVisit.customerName		= Visit.TabPaneVisits.txtCustomerNameSig.text;
          	setAppDataObjInStore();
            Visit.TabPaneVisits.signatureReasoncmb.selectedKey	= PCConstant.COMMON_KEY_SELECT;
            Visit.TabPaneVisits.imgSignature.setVisibility(true);
            Visit.TabPaneVisits.lblSignDate.setVisibility(false);
            Visit.TabPaneVisits.lblSignHere.setVisibility(false);  
            // disable visit note 
            Visit.TabPaneVisits.txtAreaVisitNote.setEnabled(false);
            Visit.TabPaneVisits.txtAreaVisitNote.skin = sknTxtAreaDisabledVNote;
            Visit.TabPaneVisits.camVisitPhoto.setEnabled(false);
     		Visit.TabPaneVisits.imgVisitPhoto.setVisibility(false);
            toggleButtonVisibility(false);
            toggleNoSignReasonComboAccess(false);
            updateCustomerSignatureInLocalTbl(updatedBase64Data, signedDate);
    	} else {
      		Visit.TabPaneVisits.imgSignature.base64 			= "";
            Visit.TabPaneVisits.lblSignDate.text 				= "";
            Visit.TabPaneVisits.signatureReasoncmb.selectedKey	= PCConstant.COMMON_KEY_SELECT;
            toggleNoSignReasonComboAccess(true);
            Visit.TabPaneVisits.lblSignHere.setVisibility(true);          	
            Visit.TabPaneVisits.imgSignature.setVisibility(false);
            Visit.TabPaneVisits.lblSignDate.setVisibility(false);          	
            gblMyAppDataObject.inProgressVisit.signedDate		= "";
      		gblMyAppDataObject.inProgressVisit.noSignatureReason= "";
          	setAppDataObjInStore();
      		Visit.TabPaneVisits.txtCustomerNameSig.text			= "";          	
      		updateCustomerSignatureInLocalTbl("", "");
    	}
    	dismissLoading();
  	} catch(e) {
    	printMessage("callbackBase : " + e);
  	}
}

/** On click image flex **/
function onClickImageFlx(){  
    if (!isEmpty(Visit.TabPaneVisits.imgCapturedPhoto.base64)){
        showLoading("common.message.loading");
        showImageInPopup(Visit.TabPaneVisits.imgCapturedPhoto.base64);
        dismissLoading();
    }	
}

/** On camera photo capture callback method **/
function onCameraPhotoCaptureCallback(eventObject){
  	printMessage("Inside onCameraPhotoCaptureCallback");	
  	showLoading("common.message.loading");
  	var capturedImg1 							= compressImage.compressImageToBase64(kony.convertToBase64(eventObject.rawBytes));  
    Visit.TabPaneVisits.imgCapturedPhoto.base64	= capturedImg1;
  	updateCapturedPhotoInLocalTbl(capturedImg1);
}

/** To update no signature reason on visit object **/
function updateNoSignatureReasonOnVisitObj(){
  	var selectedKey												= Visit.TabPaneVisits.signatureReasoncmb.selectedKey;
    if (selectedKey !== PCConstant.COMMON_KEY_SELECT) {
      	gblMyAppDataObject.inProgressVisit.noSignatureReason	= selectedKey;
    } else {
      	gblMyAppDataObject.inProgressVisit.noSignatureReason	= "";
    }
  	gblMyAppDataObject.inProgressVisit.signedDate				= "";
  	gblMyAppDataObject.inProgressVisit.customerName				= Visit.TabPaneVisits.txtCustomerNameSig.text;
  	Visit.TabPaneVisits.txtCustomerNameSig.text					= "";
  	setAppDataObjInStore();
  	updateCustomerSignatureInLocalTbl("", "");
}

/** Success callback - SQL query execution - get Preparations  **/
function success_getPreparations(result){
  	printMessage("Inside - getPreparationsFromLocalTbl success ");
  	var prepMasterData									= [];
  	prepMasterData.push([PCConstant.COMMON_KEY_SELECT, getI18nString("common.label.pleaseSelect")]);
  	if(result !== null && result.length > 0) {
    	var rowItem 									= null;
        for (var i = 0; i < result.length; i++) {
          	rowItem										= result[i];
          	prepMasterData.push([rowItem.preparationCode, rowItem.preparationName]);
        }
  	} 

  	printMessage(" Inside - getPreparationsFromLocalTbl - gblCompletedVisitFlag : " + gblCompletedVisitFlag);
  	printMessage(" Inside - getPreparationsFromLocalTbl - prepMasterData : " + JSON.stringify(prepMasterData));
  
  	//flag to check the screen
  	if (!gblCompletedVisitFlag) {
        Visit.TabPaneVisits.PreprationsCmb1.masterData		= prepMasterData;
        Visit.TabPaneVisits.PreprationsCmb2.masterData		= prepMasterData;
        Visit.TabPaneVisits.PreprationsCmb3.masterData		= prepMasterData;
        Visit.TabPaneVisits.PreprationsCmb4.masterData		= prepMasterData;
        Visit.TabPaneVisits.PreprationsCmb5.masterData		= prepMasterData;

        Visit.TabPaneVisits.PreprationsCmb1.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.Preprations1)) ? gblMyAppDataObject.inProgressVisit.Preprations1 : PCConstant.COMMON_KEY_SELECT;
        Visit.TabPaneVisits.PreprationsCmb2.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.Preprations2)) ? gblMyAppDataObject.inProgressVisit.Preprations2 : PCConstant.COMMON_KEY_SELECT;
        Visit.TabPaneVisits.PreprationsCmb3.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.Preprations3)) ? gblMyAppDataObject.inProgressVisit.Preprations3 : PCConstant.COMMON_KEY_SELECT;
        Visit.TabPaneVisits.PreprationsCmb4.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.Preprations4)) ? gblMyAppDataObject.inProgressVisit.Preprations4 : PCConstant.COMMON_KEY_SELECT;
        Visit.TabPaneVisits.PreprationsCmb5.selectedKey		= (!isEmpty(gblMyAppDataObject.inProgressVisit.Preprations5)) ? gblMyAppDataObject.inProgressVisit.Preprations5 : PCConstant.COMMON_KEY_SELECT;
  	} else {
        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb1.masterData = prepMasterData;
        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb2.masterData = prepMasterData;
        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb3.masterData = prepMasterData;
        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb4.masterData = prepMasterData;
        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb5.masterData = prepMasterData;

        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb1.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.Preprations1)) ? gblMyAppDataObject.selCompletedItem.Preprations1 : PCConstant.COMMON_KEY_SELECT;
        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb2.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.Preprations2)) ? gblMyAppDataObject.selCompletedItem.Preprations2 : PCConstant.COMMON_KEY_SELECT;
        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb3.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.Preprations3)) ? gblMyAppDataObject.selCompletedItem.Preprations3 : PCConstant.COMMON_KEY_SELECT;
        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb4.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.Preprations4)) ? gblMyAppDataObject.selCompletedItem.Preprations4 : PCConstant.COMMON_KEY_SELECT;
        CompletedVisit.tabCompletedCustomerDetails.PreprationsCmb5.selectedKey = (!isEmpty(gblMyAppDataObject.selCompletedItem.Preprations5)) ? gblMyAppDataObject.selCompletedItem.Preprations5 : PCConstant.COMMON_KEY_SELECT;
  	}
}

/** Error callback - SQL query execution - get Preparations **/
function error_getPreparations(error) {
  	printMessage(" Inside - getPreparationsFromLocalTbl - error ");
}

/** Success callback - SQL query execution - get Issue codes  **/
function success_getIssueCodes(result){
  	printMessage("Inside - success_getIssueCodes");
  	var signatureReasoncmbData				= [];
  	signatureReasoncmbData.push([PCConstant.COMMON_KEY_SELECT, getI18nString("common.placeholder.noSignatureReason")]);
    if(result !== null && result.length > 0) {
      	var rowItem 						= null;
        for (var i = 0; i < result.length; i++) {
          	rowItem							= result[i];
          	signatureReasoncmbData.push([rowItem.issueCode, rowItem.issueDescription]);
        }
    } 
  	
    if (!gblCompletedVisitFlag) {
      	Visit.TabPaneVisits.signatureReasoncmb.masterData							= signatureReasoncmbData;
        Visit.TabPaneVisits.signatureReasoncmb.selectedKey							= (!isEmpty(gblMyAppDataObject.inProgressVisit.noSignatureReason)) ? gblMyAppDataObject.inProgressVisit.noSignatureReason : PCConstant.COMMON_KEY_SELECT;
    } else {
      	CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.masterData	= signatureReasoncmbData;
      	CompletedVisit.tabCompletedCustomerDetails.signatureReasoncmb.selectedKey	= (!isEmpty(gblMyAppDataObject.selCompletedItem.noSignatureReason)) ? gblMyAppDataObject.selCompletedItem.noSignatureReason : PCConstant.COMMON_KEY_SELECT;
    }
}

/** Error callback - SQL query execution - get Issue codes **/
function error_getIssueCodes(error) {
  	printMessage(" Inside - error_getIssueCodes ");
}

/** To update preparations combo selection **/
function updatePreparationOnVisitObj(widgetRef){
    var comboRef											= widgetRef.id.slice(-1);
    var prepKey												= "Preprations" + comboRef;
    var selectedKey											= Visit.TabPaneVisits[widgetRef.id].selectedKey;
    if (selectedKey !== PCConstant.COMMON_KEY_SELECT) {
      	gblMyAppDataObject.inProgressVisit[prepKey]			= selectedKey;
    } else {
      	gblMyAppDataObject.inProgressVisit[prepKey]			= "";
    }
    setAppDataObjInStore();
}

/** To update preparations quantity selection **/
function updatePreparationQuantityOnVisitObj(widgetRef){
    var comboRef											= widgetRef.id.slice(-1);
    var qntyKey												= "PreprationsQuantity" + comboRef;
    var selectedKey											= Visit.TabPaneVisits[widgetRef.id].selectedKey;
    gblMyAppDataObject.inProgressVisit[qntyKey]				= selectedKey;
    setAppDataObjInStore();
}

/** To delete captured photo **/
function onClickDeletePhoto(){
   	showConfirmPopup("common.message.deletePhoto", ActionConfirmPhotoDeletion, ActionDeclineDeletion);
}

/** End visit action **/
function onClickEndVisit(){
  	printMessage(" Inside - onClickEndVisit ");
   	//checking signature done or not
  	var visitInProgressObj									= gblMyAppDataObject.inProgressVisit;
  	printMessage(" Inside - onClickEndVisit "+visitInProgressObj.workId);
  	getCustomerSignatureFromLocalTblForClosedWork(visitInProgressObj.workId);
}

/** Textbox / Textarea begin or end editing callback handlers **/
function handleUserInteractionOnVisitTextFields(widgetRef){
  	printMessage(" Inside - handleUserInteractionOnVisitTextFields ");
  	var curInputVal				= "";
    if (widgetRef.id == "txtCustomerNameSig") {
        curInputVal				= Visit.TabPaneVisits.txtCustomerNameSig.text;
        gblMyAppDataObject.inProgressVisit.customernameSigned	= curInputVal;
        if (isEmpty(curInputVal)) {
              toggleVisitFormPlaceHolders(true, "CUSTOMER_NAME");
        } else {
              toggleVisitFormPlaceHolders(false, "CUSTOMER_NAME");
        }
    } else if (widgetRef.id == "txtAreaVisitNote") {
    	curInputVal				= Visit.TabPaneVisits.txtAreaVisitNote.text;
    	gblMyAppDataObject.inProgressVisit.visitNote	= curInputVal;
        if (isEmpty(curInputVal)) {
            toggleVisitFormPlaceHolders(true, "NOTE");
        } else {
            toggleVisitFormPlaceHolders(false, "NOTE");
        }
  }
  setAppDataObjInStore();
}

/** To toggle access of no signature reason combobox **/
function toggleNoSignReasonComboAccess(enableFlag){
    if (enableFlag === true) {
        Visit.TabPaneVisits.signatureReasoncmb.setEnabled(true);
        Visit.TabPaneVisits.signatureReasoncmb.skin			= sknListBoxNormal;
        Visit.TabPaneVisits.signatureReasoncmb.focusSkin	= sknListBoxNormal;
    } else {
        Visit.TabPaneVisits.signatureReasoncmb.setEnabled(false);
        Visit.TabPaneVisits.signatureReasoncmb.skin			= sknListBoxDisabled;
        //Visit.TabPaneVisits.signatureReasoncmb.focusSkin	= sknListBoxDisabled;
        Visit.TabPaneVisits.signatureReasoncmb.focusSkin	= sknListBoxNormal;
    }
}

/** To toggle placeholder labels on visit screen **/
function toggleVisitFormPlaceHolders(addPlaceholder, fieldName){
  printMessage(" Inside toggleVisitFormPlaceHolders - addPlaceholder & fieldName " + addPlaceholder + " and " + fieldName);
    if (addPlaceholder === true) {
      	if (fieldName === "NOTE") {
        	Visit.TabPaneVisits.lblVisitNotePlaceholder.text	= getI18nString("common.label.note");
      	} else if (fieldName === "CUSTOMER_NAME") {
        	Visit.TabPaneVisits.lblCustomerNamePlaceholder.text	= getI18nString("common.label.enterCustomerName");
      	}
    } else {
        if (fieldName === "NOTE") {
             Visit.TabPaneVisits.lblVisitNotePlaceholder.text	= "";
        } else if (fieldName === "CUSTOMER_NAME") {
             Visit.TabPaneVisits.lblCustomerNamePlaceholder.text	= "";
        }
    }  	
}

/** To toggle delete button visibility **/
function toggleButtonVisibility(showFlag){
    Visit.TabPaneVisits.btnDeletePhoto.setEnabled(showFlag);
    Visit.TabPaneVisits.btnDeletePhoto.setVisibility(showFlag);
    Visit.TabPaneVisits.imgDeleteIcon.setVisibility(showFlag);
}

/** Tab click functionality **/
function onTabClick(eventObject, curIndex, isExpanded){
  	var visitInProgressObj					= null;
    if (curIndex === 3) { 		/** Duration calculation **/
      	Visit.TabPaneVisits.lblEndTime.text	= getCurrentTimeStr();
      	durationCalculator();
    } else if (curIndex === 1){ /** Captured photo fetching **/
      	showLoading("common.message.loading");
      	visitInProgressObj					= gblMyAppDataObject.inProgressVisit;
      	getCapturedPhotoFromLocalTbl(visitInProgressObj.workId);
    } else if (curIndex === 2){ /** Customer signature fetching **/
        showLoading("common.message.loading");
      	visitInProgressObj					= gblMyAppDataObject.inProgressVisit;
      	getCustomerSignatureFromLocalTbl(visitInProgressObj.workId);
    }
}

/** Success Callback - Get customer signature **/
function success_getCustomerSignature(resultset) {
  	printMessage(" Inside - success_getCustomerSignature");
  	if (!isEmpty(resultset) && resultset.length > 0) {
        if(!isEmpty(resultset[0].customerApprovalSignature)){
            Visit.TabPaneVisits.lblSignDate.text 					= gblMyAppDataObject.inProgressVisit.signedDate;
            Visit.TabPaneVisits.imgSignature.base64	 				= resultset[0].customerApprovalSignature;
            Visit.TabPaneVisits.imgSignature.setVisibility(true);
            Visit.TabPaneVisits.lblSignDate.setVisibility(false);
            Visit.TabPaneVisits.lblSignHere.setVisibility(false); 
            gblMyAppDataObject.inProgressVisit.noSignatureReason	= "";
            Visit.TabPaneVisits.signatureReasoncmb.selectedKey		= PCConstant.COMMON_KEY_SELECT;
            toggleNoSignReasonComboAccess(false);
      	}else{
            Visit.TabPaneVisits.lblSignDate.text 					= "";
            Visit.TabPaneVisits.imgSignature.base64 				= "";
            Visit.TabPaneVisits.imgSignature.setVisibility(false);
            Visit.TabPaneVisits.lblSignDate.setVisibility(false);
            Visit.TabPaneVisits.lblSignHere.setVisibility(true); 
            toggleNoSignReasonComboAccess(true);
      	}
	}
  	dismissLoading();
} 

/** Error Callback - Get customer signature **/
function error_getCustomerSignature(error) {
  	printMessage(" Inside - error_getCustomerSignature ");
  	Visit.TabPaneVisits.imgSignature.base64 			= "";
    Visit.TabPaneVisits.lblSignDate.text 				= "";
    Visit.TabPaneVisits.signatureReasoncmb.selectedKey	= PCConstant.COMMON_KEY_SELECT;
    toggleNoSignReasonComboAccess(true);
    Visit.TabPaneVisits.lblSignHere.setVisibility(true);          	
    Visit.TabPaneVisits.imgSignature.setVisibility(false);
    Visit.TabPaneVisits.lblSignDate.setVisibility(false);          	
  	gblMyAppDataObject.inProgressVisit.signedDate		= "";
    gblMyAppDataObject.inProgressVisit.noSignatureReason= "";
    gblMyAppDataObject.inProgressVisit.customerName		= "";
    Visit.TabPaneVisits.txtCustomerNameSig.text			= "";
  	dismissLoading();
}

/** Success Callback - Get captured photo **/
function success_getCapturedPhoto(resultset) { 
  	printMessage(" Inside - success_getCapturedPhoto ");
  	if (!isEmpty(resultset) && resultset.length > 0) {
      	if ((!isEmpty(resultset[0].capturedPhoto)) &&( 'NULL'!=resultset[0].capturedPhoto)) {
  			Visit.TabPaneVisits.imgCapturedPhoto.base64	= resultset[0].capturedPhoto;
          // cheking sign date is provided or not
             if((!isEmpty(gblMyAppDataObject.inProgressVisit.signedDate))){
                  toggleButtonVisibility(false);
               }
             else{
        		 toggleButtonVisibility(true);
               }
    	} else {
      		Visit.TabPaneVisits.imgCapturedPhoto.src	= "noimage.png";
        	toggleButtonVisibility(false);
   	 	}
    }
  	dismissLoading();
} 

/** Error Callback - Get captured photo **/
function error_getCapturedPhoto(error) {
  	printMessage(" Inside - error_getCapturedPhoto ");
  	Visit.TabPaneVisits.imgCapturedPhoto.src		= "noimage.png";
    toggleButtonVisibility(false);
  	dismissLoading();
}

function successCallbackValidateData(resultset){
  	printMessage(" Inside - successCallbackValidateData ");
  	Visit.TabPaneVisits.lblEndTime.text						= getCurrentTimeStr();
  	durationCalculator();
  	var inProgressVisitObj									= gblMyAppDataObject.inProgressVisit;

  	if (!isEmpty(inProgressVisitObj)) {
    	var isVisitFormValidationPassed						= true;
    	var validationErrorMsgi18nKey						= "";
    	var tabIdToFocus									= -1;
 		if (!isEmpty(resultset) && resultset.length > 0) {    
     		if(isEmpty(resultset[0].customerApprovalSignature) && isEmpty(inProgressVisitObj.noSignatureReason)){
        		printMessage(" Inside - successCallbackValidateData resultset ");
       			isVisitFormValidationPassed					= false;
       			validationErrorMsgi18nKey					= "common.message.customerSignOrNoSignReasonRequired";
       			tabIdToFocus								= 2;
       		} else if(!isEmpty(resultset[0].customerApprovalSignature)){
                printMessage(" Inside - successCallbackValidateData resultset customerApprovalSignature not empty");
                if(isEmpty(Visit.TabPaneVisits.txtCustomerNameSig.text)){ 
                    validationErrorMsgi18nKey				= "";
                    printMessage(" Inside - successCallbackValidateData resultset txtCustomerNameSig  empty");
                    tabIdToFocus							= 2;
                    isVisitFormValidationPassed				= false;
                    validationErrorMsgi18nKey				= "common.message.enterCustomerName";
                }
       		}
   		}

    	if (isVisitFormValidationPassed === true) {
      		showConfirmPopup("common.message.endVisit", ActionConfirmEndVisit, ActionCancelConfirmEndVisit);
    	} else {
      		showMessagePopup(validationErrorMsgi18nKey, ActionPopupMessageOk);
            if (tabIdToFocus > -1) {
              	Visit.TabPaneVisits.activeTabs				= [tabIdToFocus];
            }
    	}
    } else {
      	printMessage(" Inside - onClickEndVisit - InProgressVisit object is null ");
    }
}


function failureCallbackValidateSigData(){
    if (isEmpty(inProgressVisitObj.base64SignData) && isEmpty(inProgressVisitObj.noSignatureReason)) {
      	isVisitFormValidationPassed						= false;
      	validationErrorMsgi18nKey						= "common.message.customerSignOrNoSignReasonRequired";
      	tabIdToFocus									= 2;
    } 
    if (isVisitFormValidationPassed === true) {
      	showConfirmPopup("common.message.endVisit", ActionConfirmEndVisit, ActionCancelConfirmEndVisit);
    } else {
      	showMessagePopup(validationErrorMsgi18nKey, ActionPopupMessageOk);
      	if (tabIdToFocus > -1) {
        	Visit.TabPaneVisits.activeTabs				= [tabIdToFocus];
      	}
    }
}

function deleteSignature(){
   	printMessage(" Inside - deleteSignature - resultset: " );
    showLoading("common.message.loading");
   	updateCustomerSignatureInLocalTableAsEmpty("", "");
}

function success_SignatureInLocalTblasEmpty(){
   	printMessage(" Inside - success_SignatureInLocalTblasEmpty  " );
    Visit.TabPaneVisits.lblSignDate.text 	= "";
    Visit.TabPaneVisits.imgSignature.base64 = "";
    Visit.TabPaneVisits.imgSignature.setVisibility(false);
    Visit.TabPaneVisits.lblSignDate.setVisibility(false);
    Visit.TabPaneVisits.lblSignHere.setVisibility(true); 
    toggleNoSignReasonComboAccess(true);
    dismissLoading();      
}

function success_capturedPhotoCallback() {
  	dismissLoading();
  	printMessage(" Inside - success_capturedPhotoCallback  " );
 	// Visit.TabPaneVisits.imgCapturedPhoto.base64			= Visit.TabPaneVisits.camVisitPhoto.base64;  
  	toggleButtonVisibility(true);  
}
  
function error_capturedPhotoCallback(){
   	printMessage(" Inside - error_capturedPhotoCallback  " );
    dismissLoading();
}