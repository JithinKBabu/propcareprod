//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:02 UTC 2016ServiceAreaPc*******************
// **********************************Start ServiceAreaPc's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(com)=== "undefined"){ com = {}; }
if(typeof(com.pc)=== "undefined"){ com.pc = {}; }
if(typeof(com.pc.serviceareas)=== "undefined"){ com.pc.serviceareas = {}; }

/************************************************************************************
* Creates new ServiceAreaPc
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc = function(){
	this.assignedArea = null;
	this.branchNumber = null;
	this.businessCode = null;
	this.countryCode = null;
	this.email = null;
	this.employeeforename1 = null;
	this.employeeSurname = null;
	this.errorDescription = null;
	this.errorNumber = null;
	this.languageCode = null;
	this.serviceAreaCode = null;
	this.serviceAreaDeleted = null;
	this.updateDateTime = null;
	this.serviceAreaDescription = null;
	this.markForUpload = true;
};

com.pc.serviceareas.ServiceAreaPc.prototype = {
	get assignedArea(){
		return this._assignedArea;
	},
	set assignedArea(val){
		this._assignedArea = val;
	},
	get branchNumber(){
		return this._branchNumber;
	},
	set branchNumber(val){
		this._branchNumber = val;
	},
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get employeeforename1(){
		return this._employeeforename1;
	},
	set employeeforename1(val){
		this._employeeforename1 = val;
	},
	get employeeSurname(){
		return this._employeeSurname;
	},
	set employeeSurname(val){
		this._employeeSurname = val;
	},
	get errorDescription(){
		return this._errorDescription;
	},
	set errorDescription(val){
		this._errorDescription = val;
	},
	get errorNumber(){
		return this._errorNumber;
	},
	set errorNumber(val){
		this._errorNumber = val;
	},
	get languageCode(){
		return this._languageCode;
	},
	set languageCode(val){
		this._languageCode = val;
	},
	get serviceAreaCode(){
		return this._serviceAreaCode;
	},
	set serviceAreaCode(val){
		this._serviceAreaCode = val;
	},
	get serviceAreaDeleted(){
		return kony.sync.getBoolean(this._serviceAreaDeleted)+"";
	},
	set serviceAreaDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute serviceAreaDeleted in ServiceAreaPc.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._serviceAreaDeleted = val;
	},
	get updateDateTime(){
		return this._updateDateTime;
	},
	set updateDateTime(val){
		this._updateDateTime = val;
	},
	get serviceAreaDescription(){
		return this._serviceAreaDescription;
	},
	set serviceAreaDescription(val){
		this._serviceAreaDescription = val;
	},
};

/************************************************************************************
* Retrieves all instances of ServiceAreaPc SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "assignedArea";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "branchNumber";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* com.pc.serviceareas.ServiceAreaPc.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	orderByMap = kony.sync.formOrderByClause("ServiceAreaPc",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getAll->successcallback");
		successcallback(com.pc.serviceareas.ServiceAreaPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of ServiceAreaPc present in local database.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getAllCount function");
	com.pc.serviceareas.ServiceAreaPc.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of ServiceAreaPc using where clause in the local Database
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of ServiceAreaPc in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.prototype.create function");
	var valuestable = this.getValuesTable(true);
	com.pc.serviceareas.ServiceAreaPc.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.serviceareas.ServiceAreaPc.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"ServiceAreaPc",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = com.pc.serviceareas.ServiceAreaPc.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "branchNumber=" + valuestable.branchNumber;
		pks["branchNumber"] = {key:"branchNumber",value:valuestable.branchNumber};
		errMsg = errMsg + ", businessCode=" + valuestable.businessCode;
		pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
		errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
		pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
		errMsg = errMsg + ", serviceAreaCode=" + valuestable.serviceAreaCode;
		pks["serviceAreaCode"] = {key:"serviceAreaCode",value:valuestable.serviceAreaCode};
		com.pc.serviceareas.ServiceAreaPc.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of ServiceAreaPc in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].assignedArea = "assignedArea_0";
*		valuesArray[0].branchNumber = "branchNumber_0";
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].email = "email_0";
*		valuesArray[0].employeeforename1 = "employeeforename1_0";
*		valuesArray[0].employeeSurname = "employeeSurname_0";
*		valuesArray[0].errorDescription = "errorDescription_0";
*		valuesArray[0].errorNumber = "errorNumber_0";
*		valuesArray[0].languageCode = "languageCode_0";
*		valuesArray[0].serviceAreaCode = "serviceAreaCode_0";
*		valuesArray[0].serviceAreaDeleted = true;
*		valuesArray[0].updateDateTime = 0;
*		valuesArray[0].serviceAreaDescription = "serviceAreaDescription_0";
*		valuesArray[1] = {};
*		valuesArray[1].assignedArea = "assignedArea_1";
*		valuesArray[1].branchNumber = "branchNumber_1";
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].email = "email_1";
*		valuesArray[1].employeeforename1 = "employeeforename1_1";
*		valuesArray[1].employeeSurname = "employeeSurname_1";
*		valuesArray[1].errorDescription = "errorDescription_1";
*		valuesArray[1].errorNumber = "errorNumber_1";
*		valuesArray[1].languageCode = "languageCode_1";
*		valuesArray[1].serviceAreaCode = "serviceAreaCode_1";
*		valuesArray[1].serviceAreaDeleted = true;
*		valuesArray[1].updateDateTime = 1;
*		valuesArray[1].serviceAreaDescription = "serviceAreaDescription_1";
*		valuesArray[2] = {};
*		valuesArray[2].assignedArea = "assignedArea_2";
*		valuesArray[2].branchNumber = "branchNumber_2";
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].email = "email_2";
*		valuesArray[2].employeeforename1 = "employeeforename1_2";
*		valuesArray[2].employeeSurname = "employeeSurname_2";
*		valuesArray[2].errorDescription = "errorDescription_2";
*		valuesArray[2].errorNumber = "errorNumber_2";
*		valuesArray[2].languageCode = "languageCode_2";
*		valuesArray[2].serviceAreaCode = "serviceAreaCode_2";
*		valuesArray[2].serviceAreaDeleted = true;
*		valuesArray[2].updateDateTime = 2;
*		valuesArray[2].serviceAreaDescription = "serviceAreaDescription_2";
*		com.pc.serviceareas.ServiceAreaPc.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"ServiceAreaPc",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "branchNumber=" + valuestable.branchNumber;
				pks["branchNumber"] = {key:"branchNumber",value:valuestable.branchNumber};
				errMsg = errMsg + ", businessCode=" + valuestable.businessCode;
				pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
				errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
				pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
				errMsg = errMsg + ", serviceAreaCode=" + valuestable.serviceAreaCode;
				pks["serviceAreaCode"] = {key:"serviceAreaCode",value:valuestable.serviceAreaCode};
				var wcs = [];
				if(com.pc.serviceareas.ServiceAreaPc.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = com.pc.serviceareas.ServiceAreaPc.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates ServiceAreaPc using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	com.pc.serviceareas.ServiceAreaPc.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.serviceareas.ServiceAreaPc.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(com.pc.serviceareas.ServiceAreaPc.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"ServiceAreaPc",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = com.pc.serviceareas.ServiceAreaPc.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates ServiceAreaPc(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"ServiceAreaPc",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.serviceareas.ServiceAreaPc.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = com.pc.serviceareas.ServiceAreaPc.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.serviceareas.ServiceAreaPc.getPKTable());
	}
};

/************************************************************************************
* Updates ServiceAreaPc(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.assignedArea = "assignedArea_updated0";
*		inputArray[0].changeSet.email = "email_updated0";
*		inputArray[0].changeSet.employeeforename1 = "employeeforename1_updated0";
*		inputArray[0].changeSet.employeeSurname = "employeeSurname_updated0";
*		inputArray[0].whereClause = "where branchNumber = '0'";
*		inputArray[0].whereClause = "where businessCode = '0'";
*		inputArray[0].whereClause = "where countryCode = '0'";
*		inputArray[0].whereClause = "where serviceAreaCode = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.assignedArea = "assignedArea_updated1";
*		inputArray[1].changeSet.email = "email_updated1";
*		inputArray[1].changeSet.employeeforename1 = "employeeforename1_updated1";
*		inputArray[1].changeSet.employeeSurname = "employeeSurname_updated1";
*		inputArray[1].whereClause = "where branchNumber = '1'";
*		inputArray[1].whereClause = "where businessCode = '1'";
*		inputArray[1].whereClause = "where countryCode = '1'";
*		inputArray[1].whereClause = "where serviceAreaCode = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.assignedArea = "assignedArea_updated2";
*		inputArray[2].changeSet.email = "email_updated2";
*		inputArray[2].changeSet.employeeforename1 = "employeeforename1_updated2";
*		inputArray[2].changeSet.employeeSurname = "employeeSurname_updated2";
*		inputArray[2].whereClause = "where branchNumber = '2'";
*		inputArray[2].whereClause = "where businessCode = '2'";
*		inputArray[2].whereClause = "where countryCode = '2'";
*		inputArray[2].whereClause = "where serviceAreaCode = '2'";
*		com.pc.serviceareas.ServiceAreaPc.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898512bd8be";
	var tbname = "ServiceAreaPc";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"ServiceAreaPc",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, com.pc.serviceareas.ServiceAreaPc.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, com.pc.serviceareas.ServiceAreaPc.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = com.pc.serviceareas.ServiceAreaPc.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes ServiceAreaPc using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.prototype.deleteByPK function");
	var pks = this.getPKTable();
	com.pc.serviceareas.ServiceAreaPc.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
com.pc.serviceareas.ServiceAreaPc.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(com.pc.serviceareas.ServiceAreaPc.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function ServiceAreaPcTransactionCallback(tx){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.deleteByPK->ServiceAreaPc_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function ServiceAreaPcErrorCallback(){
		sync.log.error("Entering com.pc.serviceareas.ServiceAreaPc.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function ServiceAreaPcSuccessCallback(){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.serviceareas.ServiceAreaPc.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, ServiceAreaPcTransactionCallback, ServiceAreaPcSuccessCallback, ServiceAreaPcErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes ServiceAreaPc(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. com.pc.serviceareas.ServiceAreaPc.remove("where assignedArea like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function ServiceAreaPc_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function ServiceAreaPc_removeSuccess(){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.remove->ServiceAreaPc_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, ServiceAreaPc_removeTransactioncallback, ServiceAreaPc_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes ServiceAreaPc using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	com.pc.serviceareas.ServiceAreaPc.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
com.pc.serviceareas.ServiceAreaPc.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(com.pc.serviceareas.ServiceAreaPc.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function ServiceAreaPcTransactionCallback(tx){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.removeDeviceInstanceByPK -> ServiceAreaPcTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function ServiceAreaPcErrorCallback(){
		sync.log.error("Entering com.pc.serviceareas.ServiceAreaPc.removeDeviceInstanceByPK -> ServiceAreaPcErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function ServiceAreaPcSuccessCallback(){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.removeDeviceInstanceByPK -> ServiceAreaPcSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.serviceareas.ServiceAreaPc.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, ServiceAreaPcTransactionCallback, ServiceAreaPcSuccessCallback, ServiceAreaPcErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes ServiceAreaPc(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function ServiceAreaPc_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function ServiceAreaPc_removeSuccess(){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.remove->ServiceAreaPc_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, ServiceAreaPc_removeTransactioncallback, ServiceAreaPc_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves ServiceAreaPc using primary key from the local Database. 
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	com.pc.serviceareas.ServiceAreaPc.getAllDetailsByPK(pks,successcallback,errorcallback);
};
com.pc.serviceareas.ServiceAreaPc.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var wcs = [];
	if(com.pc.serviceareas.ServiceAreaPc.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getAllDetailsByPK-> success callback function");
		successcallback(com.pc.serviceareas.ServiceAreaPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves ServiceAreaPc(s) using where clause from the local Database. 
* e.g. com.pc.serviceareas.ServiceAreaPc.find("where assignedArea like 'A%'", successcallback,errorcallback);
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, com.pc.serviceareas.ServiceAreaPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of ServiceAreaPc with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	com.pc.serviceareas.ServiceAreaPc.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.pc.serviceareas.ServiceAreaPc.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(com.pc.serviceareas.ServiceAreaPc.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of ServiceAreaPc matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering com.pc.serviceareas.ServiceAreaPc.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of ServiceAreaPc pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.serviceareas.ServiceAreaPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of ServiceAreaPc pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.serviceareas.ServiceAreaPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of ServiceAreaPc deferred for upload.
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.serviceareas.ServiceAreaPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to ServiceAreaPc in local database to last synced state
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to ServiceAreaPc's record with given primary key in local 
* database to last synced state
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	com.pc.serviceareas.ServiceAreaPc.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
com.pc.serviceareas.ServiceAreaPc.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var wcs = [];
	if(com.pc.serviceareas.ServiceAreaPc.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.serviceareas.ServiceAreaPc.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether ServiceAreaPc's record  
* with given primary key got deferred in last sync
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	com.pc.serviceareas.ServiceAreaPc.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
com.pc.serviceareas.ServiceAreaPc.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.serviceareas.ServiceAreaPc.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether ServiceAreaPc's record  
* with given primary key is pending for upload
*************************************************************************************/
com.pc.serviceareas.ServiceAreaPc.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.serviceareas.ServiceAreaPc.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	com.pc.serviceareas.ServiceAreaPc.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
com.pc.serviceareas.ServiceAreaPc.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.serviceareas.ServiceAreaPc.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.serviceareas.ServiceAreaPc.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.pc.serviceareas.ServiceAreaPc.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.removeCascade function");
	var tbname = com.pc.serviceareas.ServiceAreaPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


com.pc.serviceareas.ServiceAreaPc.convertTableToObject = function(res){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new com.pc.serviceareas.ServiceAreaPc();
			obj.assignedArea = res[i].assignedArea;
			obj.branchNumber = res[i].branchNumber;
			obj.businessCode = res[i].businessCode;
			obj.countryCode = res[i].countryCode;
			obj.email = res[i].email;
			obj.employeeforename1 = res[i].employeeforename1;
			obj.employeeSurname = res[i].employeeSurname;
			obj.errorDescription = res[i].errorDescription;
			obj.errorNumber = res[i].errorNumber;
			obj.languageCode = res[i].languageCode;
			obj.serviceAreaCode = res[i].serviceAreaCode;
			obj.serviceAreaDeleted = res[i].serviceAreaDeleted;
			obj.updateDateTime = res[i].updateDateTime;
			obj.serviceAreaDescription = res[i].serviceAreaDescription;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

com.pc.serviceareas.ServiceAreaPc.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.filterAttributes function");
	var attributeTable = {};
	attributeTable.assignedArea = "assignedArea";
	attributeTable.branchNumber = "branchNumber";
	attributeTable.businessCode = "businessCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.email = "email";
	attributeTable.employeeforename1 = "employeeforename1";
	attributeTable.employeeSurname = "employeeSurname";
	attributeTable.errorDescription = "errorDescription";
	attributeTable.errorNumber = "errorNumber";
	attributeTable.languageCode = "languageCode";
	attributeTable.serviceAreaCode = "serviceAreaCode";
	attributeTable.serviceAreaDeleted = "serviceAreaDeleted";
	attributeTable.updateDateTime = "updateDateTime";
	attributeTable.serviceAreaDescription = "serviceAreaDescription";

	var PKTable = {};
	PKTable.branchNumber = {}
	PKTable.branchNumber.name = "branchNumber";
	PKTable.branchNumber.isAutoGen = false;
	PKTable.businessCode = {}
	PKTable.businessCode.name = "businessCode";
	PKTable.businessCode.isAutoGen = false;
	PKTable.countryCode = {}
	PKTable.countryCode.name = "countryCode";
	PKTable.countryCode.isAutoGen = false;
	PKTable.serviceAreaCode = {}
	PKTable.serviceAreaCode.name = "serviceAreaCode";
	PKTable.serviceAreaCode.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject ServiceAreaPc. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject ServiceAreaPc. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject ServiceAreaPc. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

com.pc.serviceareas.ServiceAreaPc.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = com.pc.serviceareas.ServiceAreaPc.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

com.pc.serviceareas.ServiceAreaPc.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.prototype.getValuesTable function");
	var valuesTable = {};
	valuesTable.assignedArea = this.assignedArea;
	if(isInsert===true){
		valuesTable.branchNumber = this.branchNumber;
	}
	if(isInsert===true){
		valuesTable.businessCode = this.businessCode;
	}
	if(isInsert===true){
		valuesTable.countryCode = this.countryCode;
	}
	valuesTable.email = this.email;
	valuesTable.employeeforename1 = this.employeeforename1;
	valuesTable.employeeSurname = this.employeeSurname;
	valuesTable.errorDescription = this.errorDescription;
	valuesTable.errorNumber = this.errorNumber;
	valuesTable.languageCode = this.languageCode;
	if(isInsert===true){
		valuesTable.serviceAreaCode = this.serviceAreaCode;
	}
	valuesTable.serviceAreaDeleted = this.serviceAreaDeleted;
	valuesTable.updateDateTime = this.updateDateTime;
	valuesTable.serviceAreaDescription = this.serviceAreaDescription;
	return valuesTable;
};

com.pc.serviceareas.ServiceAreaPc.prototype.getPKTable = function(){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.prototype.getPKTable function");
	var pkTable = {};
	pkTable.branchNumber = {key:"branchNumber",value:this.branchNumber};
	pkTable.businessCode = {key:"businessCode",value:this.businessCode};
	pkTable.countryCode = {key:"countryCode",value:this.countryCode};
	pkTable.serviceAreaCode = {key:"serviceAreaCode",value:this.serviceAreaCode};
	return pkTable;
};

com.pc.serviceareas.ServiceAreaPc.getPKTable = function(){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getPKTable function");
	var pkTable = [];
	pkTable.push("branchNumber");
	pkTable.push("businessCode");
	pkTable.push("countryCode");
	pkTable.push("serviceAreaCode");
	return pkTable;
};

com.pc.serviceareas.ServiceAreaPc.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.pkCheck function");
	var wc = [];
	if(!kony.sync.isNull(pks.branchNumber)){
		if(!kony.sync.isNull(pks.branchNumber.value)){
			wc.key = "branchNumber";
			wc.value = pks.branchNumber.value;
		}
		else{
			wc.key = "branchNumber";
			wc.value = pks.branchNumber;
		}
	}else{
		sync.log.error("Primary Key branchNumber not specified in " + opName + " an item in ServiceAreaPc");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("branchNumber",opName,"ServiceAreaPc")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.businessCode)){
		if(!kony.sync.isNull(pks.businessCode.value)){
			wc.key = "businessCode";
			wc.value = pks.businessCode.value;
		}
		else{
			wc.key = "businessCode";
			wc.value = pks.businessCode;
		}
	}else{
		sync.log.error("Primary Key businessCode not specified in " + opName + " an item in ServiceAreaPc");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("businessCode",opName,"ServiceAreaPc")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.countryCode)){
		if(!kony.sync.isNull(pks.countryCode.value)){
			wc.key = "countryCode";
			wc.value = pks.countryCode.value;
		}
		else{
			wc.key = "countryCode";
			wc.value = pks.countryCode;
		}
	}else{
		sync.log.error("Primary Key countryCode not specified in " + opName + " an item in ServiceAreaPc");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("countryCode",opName,"ServiceAreaPc")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.serviceAreaCode)){
		if(!kony.sync.isNull(pks.serviceAreaCode.value)){
			wc.key = "serviceAreaCode";
			wc.value = pks.serviceAreaCode.value;
		}
		else{
			wc.key = "serviceAreaCode";
			wc.value = pks.serviceAreaCode;
		}
	}else{
		sync.log.error("Primary Key serviceAreaCode not specified in " + opName + " an item in ServiceAreaPc");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("serviceAreaCode",opName,"ServiceAreaPc")));
		return;
	}
	kony.table.insert(wcs,wc);
	return true;
};

com.pc.serviceareas.ServiceAreaPc.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.validateNull function");
	return true;
};

com.pc.serviceareas.ServiceAreaPc.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.validateNullInsert function");
	return true;
};

com.pc.serviceareas.ServiceAreaPc.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering com.pc.serviceareas.ServiceAreaPc.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


com.pc.serviceareas.ServiceAreaPc.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

com.pc.serviceareas.ServiceAreaPc.getTableName = function(){
	return "ServiceAreaPc";
};




// **********************************End ServiceAreaPc's helper methods************************