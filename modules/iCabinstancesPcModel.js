//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:02 UTC 2016iCabinstancesPc*******************
// **********************************Start iCabinstancesPc's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(com)=== "undefined"){ com = {}; }
if(typeof(com.pc)=== "undefined"){ com.pc = {}; }
if(typeof(com.pc.icabinstances)=== "undefined"){ com.pc.icabinstances = {}; }

/************************************************************************************
* Creates new iCabinstancesPc
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc = function(){
	this.businessCode = null;
	this.countryCode = null;
	this.email = null;
	this.iCABSServer = null;
	this.markForUpload = true;
};

com.pc.icabinstances.iCabinstancesPc.prototype = {
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get iCABSServer(){
		return this._iCABSServer;
	},
	set iCABSServer(val){
		this._iCABSServer = val;
	},
};

/************************************************************************************
* Retrieves all instances of iCabinstancesPc SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "businessCode";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "countryCode";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* com.pc.icabinstances.iCabinstancesPc.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	orderByMap = kony.sync.formOrderByClause("iCabinstancesPc",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getAll->successcallback");
		successcallback(com.pc.icabinstances.iCabinstancesPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of iCabinstancesPc present in local database.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getAllCount function");
	com.pc.icabinstances.iCabinstancesPc.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of iCabinstancesPc using where clause in the local Database
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of iCabinstancesPc in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.prototype.create function");
	var valuestable = this.getValuesTable(true);
	com.pc.icabinstances.iCabinstancesPc.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.icabinstances.iCabinstancesPc.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"iCabinstancesPc",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = com.pc.icabinstances.iCabinstancesPc.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "businessCode=" + valuestable.businessCode;
		pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
		errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
		pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
		errMsg = errMsg + ", email=" + valuestable.email;
		pks["email"] = {key:"email",value:valuestable.email};
		errMsg = errMsg + ", iCABSServer=" + valuestable.iCABSServer;
		pks["iCABSServer"] = {key:"iCABSServer",value:valuestable.iCABSServer};
		com.pc.icabinstances.iCabinstancesPc.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of iCabinstancesPc in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].email = "email_0";
*		valuesArray[0].iCABSServer = "iCABSServer_0";
*		valuesArray[1] = {};
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].email = "email_1";
*		valuesArray[1].iCABSServer = "iCABSServer_1";
*		valuesArray[2] = {};
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].email = "email_2";
*		valuesArray[2].iCABSServer = "iCABSServer_2";
*		com.pc.icabinstances.iCabinstancesPc.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"iCabinstancesPc",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "businessCode=" + valuestable.businessCode;
				pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
				errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
				pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
				errMsg = errMsg + ", email=" + valuestable.email;
				pks["email"] = {key:"email",value:valuestable.email};
				errMsg = errMsg + ", iCABSServer=" + valuestable.iCABSServer;
				pks["iCABSServer"] = {key:"iCABSServer",value:valuestable.iCABSServer};
				var wcs = [];
				if(com.pc.icabinstances.iCabinstancesPc.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = com.pc.icabinstances.iCabinstancesPc.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates iCabinstancesPc using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	com.pc.icabinstances.iCabinstancesPc.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.icabinstances.iCabinstancesPc.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(com.pc.icabinstances.iCabinstancesPc.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"iCabinstancesPc",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = com.pc.icabinstances.iCabinstancesPc.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates iCabinstancesPc(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"iCabinstancesPc",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.icabinstances.iCabinstancesPc.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = com.pc.icabinstances.iCabinstancesPc.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.icabinstances.iCabinstancesPc.getPKTable());
	}
};

/************************************************************************************
* Updates iCabinstancesPc(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].whereClause = "where businessCode = '0'";
*		inputArray[0].whereClause = "where countryCode = '0'";
*		inputArray[0].whereClause = "where email = '0'";
*		inputArray[0].whereClause = "where iCABSServer = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].whereClause = "where businessCode = '1'";
*		inputArray[1].whereClause = "where countryCode = '1'";
*		inputArray[1].whereClause = "where email = '1'";
*		inputArray[1].whereClause = "where iCABSServer = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].whereClause = "where businessCode = '2'";
*		inputArray[2].whereClause = "where countryCode = '2'";
*		inputArray[2].whereClause = "where email = '2'";
*		inputArray[2].whereClause = "where iCABSServer = '2'";
*		com.pc.icabinstances.iCabinstancesPc.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898512bd8be";
	var tbname = "iCabinstancesPc";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"iCabinstancesPc",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, com.pc.icabinstances.iCabinstancesPc.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, com.pc.icabinstances.iCabinstancesPc.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = com.pc.icabinstances.iCabinstancesPc.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes iCabinstancesPc using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.prototype.deleteByPK function");
	var pks = this.getPKTable();
	com.pc.icabinstances.iCabinstancesPc.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
com.pc.icabinstances.iCabinstancesPc.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(com.pc.icabinstances.iCabinstancesPc.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function iCabinstancesPcTransactionCallback(tx){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.deleteByPK->iCabinstancesPc_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function iCabinstancesPcErrorCallback(){
		sync.log.error("Entering com.pc.icabinstances.iCabinstancesPc.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function iCabinstancesPcSuccessCallback(){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.icabinstances.iCabinstancesPc.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, iCabinstancesPcTransactionCallback, iCabinstancesPcSuccessCallback, iCabinstancesPcErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes iCabinstancesPc(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. com.pc.icabinstances.iCabinstancesPc.remove("where businessCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function iCabinstancesPc_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function iCabinstancesPc_removeSuccess(){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.remove->iCabinstancesPc_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, iCabinstancesPc_removeTransactioncallback, iCabinstancesPc_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes iCabinstancesPc using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	com.pc.icabinstances.iCabinstancesPc.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
com.pc.icabinstances.iCabinstancesPc.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(com.pc.icabinstances.iCabinstancesPc.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function iCabinstancesPcTransactionCallback(tx){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.removeDeviceInstanceByPK -> iCabinstancesPcTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function iCabinstancesPcErrorCallback(){
		sync.log.error("Entering com.pc.icabinstances.iCabinstancesPc.removeDeviceInstanceByPK -> iCabinstancesPcErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function iCabinstancesPcSuccessCallback(){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.removeDeviceInstanceByPK -> iCabinstancesPcSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.icabinstances.iCabinstancesPc.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, iCabinstancesPcTransactionCallback, iCabinstancesPcSuccessCallback, iCabinstancesPcErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes iCabinstancesPc(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function iCabinstancesPc_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function iCabinstancesPc_removeSuccess(){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.remove->iCabinstancesPc_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, iCabinstancesPc_removeTransactioncallback, iCabinstancesPc_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves iCabinstancesPc using primary key from the local Database. 
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	com.pc.icabinstances.iCabinstancesPc.getAllDetailsByPK(pks,successcallback,errorcallback);
};
com.pc.icabinstances.iCabinstancesPc.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var wcs = [];
	if(com.pc.icabinstances.iCabinstancesPc.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getAllDetailsByPK-> success callback function");
		successcallback(com.pc.icabinstances.iCabinstancesPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves iCabinstancesPc(s) using where clause from the local Database. 
* e.g. com.pc.icabinstances.iCabinstancesPc.find("where businessCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, com.pc.icabinstances.iCabinstancesPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of iCabinstancesPc with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	com.pc.icabinstances.iCabinstancesPc.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.pc.icabinstances.iCabinstancesPc.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(com.pc.icabinstances.iCabinstancesPc.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of iCabinstancesPc matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering com.pc.icabinstances.iCabinstancesPc.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of iCabinstancesPc pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.icabinstances.iCabinstancesPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of iCabinstancesPc pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.icabinstances.iCabinstancesPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of iCabinstancesPc deferred for upload.
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.icabinstances.iCabinstancesPc.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to iCabinstancesPc in local database to last synced state
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to iCabinstancesPc's record with given primary key in local 
* database to last synced state
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	com.pc.icabinstances.iCabinstancesPc.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
com.pc.icabinstances.iCabinstancesPc.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var wcs = [];
	if(com.pc.icabinstances.iCabinstancesPc.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.icabinstances.iCabinstancesPc.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether iCabinstancesPc's record  
* with given primary key got deferred in last sync
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	com.pc.icabinstances.iCabinstancesPc.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
com.pc.icabinstances.iCabinstancesPc.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.icabinstances.iCabinstancesPc.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether iCabinstancesPc's record  
* with given primary key is pending for upload
*************************************************************************************/
com.pc.icabinstances.iCabinstancesPc.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.icabinstances.iCabinstancesPc.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	com.pc.icabinstances.iCabinstancesPc.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
com.pc.icabinstances.iCabinstancesPc.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.icabinstances.iCabinstancesPc.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.icabinstances.iCabinstancesPc.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.pc.icabinstances.iCabinstancesPc.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.removeCascade function");
	var tbname = com.pc.icabinstances.iCabinstancesPc.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


com.pc.icabinstances.iCabinstancesPc.convertTableToObject = function(res){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new com.pc.icabinstances.iCabinstancesPc();
			obj.businessCode = res[i].businessCode;
			obj.countryCode = res[i].countryCode;
			obj.email = res[i].email;
			obj.iCABSServer = res[i].iCABSServer;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

com.pc.icabinstances.iCabinstancesPc.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.filterAttributes function");
	var attributeTable = {};
	attributeTable.businessCode = "businessCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.email = "email";
	attributeTable.iCABSServer = "iCABSServer";

	var PKTable = {};
	PKTable.businessCode = {}
	PKTable.businessCode.name = "businessCode";
	PKTable.businessCode.isAutoGen = false;
	PKTable.countryCode = {}
	PKTable.countryCode.name = "countryCode";
	PKTable.countryCode.isAutoGen = false;
	PKTable.email = {}
	PKTable.email.name = "email";
	PKTable.email.isAutoGen = false;
	PKTable.iCABSServer = {}
	PKTable.iCABSServer.name = "iCABSServer";
	PKTable.iCABSServer.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject iCabinstancesPc. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject iCabinstancesPc. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject iCabinstancesPc. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

com.pc.icabinstances.iCabinstancesPc.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = com.pc.icabinstances.iCabinstancesPc.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

com.pc.icabinstances.iCabinstancesPc.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.prototype.getValuesTable function");
	var valuesTable = {};
	if(isInsert===true){
		valuesTable.businessCode = this.businessCode;
	}
	if(isInsert===true){
		valuesTable.countryCode = this.countryCode;
	}
	if(isInsert===true){
		valuesTable.email = this.email;
	}
	if(isInsert===true){
		valuesTable.iCABSServer = this.iCABSServer;
	}
	return valuesTable;
};

com.pc.icabinstances.iCabinstancesPc.prototype.getPKTable = function(){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.prototype.getPKTable function");
	var pkTable = {};
	pkTable.businessCode = {key:"businessCode",value:this.businessCode};
	pkTable.countryCode = {key:"countryCode",value:this.countryCode};
	pkTable.email = {key:"email",value:this.email};
	pkTable.iCABSServer = {key:"iCABSServer",value:this.iCABSServer};
	return pkTable;
};

com.pc.icabinstances.iCabinstancesPc.getPKTable = function(){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getPKTable function");
	var pkTable = [];
	pkTable.push("businessCode");
	pkTable.push("countryCode");
	pkTable.push("email");
	pkTable.push("iCABSServer");
	return pkTable;
};

com.pc.icabinstances.iCabinstancesPc.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.pkCheck function");
	var wc = [];
	if(!kony.sync.isNull(pks.businessCode)){
		if(!kony.sync.isNull(pks.businessCode.value)){
			wc.key = "businessCode";
			wc.value = pks.businessCode.value;
		}
		else{
			wc.key = "businessCode";
			wc.value = pks.businessCode;
		}
	}else{
		sync.log.error("Primary Key businessCode not specified in " + opName + " an item in iCabinstancesPc");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("businessCode",opName,"iCabinstancesPc")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.countryCode)){
		if(!kony.sync.isNull(pks.countryCode.value)){
			wc.key = "countryCode";
			wc.value = pks.countryCode.value;
		}
		else{
			wc.key = "countryCode";
			wc.value = pks.countryCode;
		}
	}else{
		sync.log.error("Primary Key countryCode not specified in " + opName + " an item in iCabinstancesPc");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("countryCode",opName,"iCabinstancesPc")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.email)){
		if(!kony.sync.isNull(pks.email.value)){
			wc.key = "email";
			wc.value = pks.email.value;
		}
		else{
			wc.key = "email";
			wc.value = pks.email;
		}
	}else{
		sync.log.error("Primary Key email not specified in " + opName + " an item in iCabinstancesPc");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("email",opName,"iCabinstancesPc")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.iCABSServer)){
		if(!kony.sync.isNull(pks.iCABSServer.value)){
			wc.key = "iCABSServer";
			wc.value = pks.iCABSServer.value;
		}
		else{
			wc.key = "iCABSServer";
			wc.value = pks.iCABSServer;
		}
	}else{
		sync.log.error("Primary Key iCABSServer not specified in " + opName + " an item in iCabinstancesPc");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("iCABSServer",opName,"iCabinstancesPc")));
		return;
	}
	kony.table.insert(wcs,wc);
	return true;
};

com.pc.icabinstances.iCabinstancesPc.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.validateNull function");
	return true;
};

com.pc.icabinstances.iCabinstancesPc.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.validateNullInsert function");
	return true;
};

com.pc.icabinstances.iCabinstancesPc.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering com.pc.icabinstances.iCabinstancesPc.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


com.pc.icabinstances.iCabinstancesPc.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

com.pc.icabinstances.iCabinstancesPc.getTableName = function(){
	return "iCabinstancesPc";
};




// **********************************End iCabinstancesPc's helper methods************************