//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:03 UTC 2016servicecover*******************
// **********************************Start servicecover's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(com)=== "undefined"){ com = {}; }
if(typeof(com.pc)=== "undefined"){ com.pc = {}; }
if(typeof(com.pc.servicecovers)=== "undefined"){ com.pc.servicecovers = {}; }

/************************************************************************************
* Creates new servicecover
*************************************************************************************/
com.pc.servicecovers.servicecover = function(){
	this.businessCode = null;
	this.contractCode = null;
	this.countryCode = null;
	this.propertyCode = null;
	this.serviceBranchNumber = null;
	this.serviceCoverDeleted = null;
	this.serviceCoverNumber = null;
	this.serviceCoverSequence = null;
	this.serviceLineCode = null;
	this.updateDateTime = null;
	this.serviceLineDescription = null;
	this.serviceSpecialInstructions = null;
	this.markForUpload = true;
};

com.pc.servicecovers.servicecover.prototype = {
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get contractCode(){
		return this._contractCode;
	},
	set contractCode(val){
		this._contractCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get propertyCode(){
		return this._propertyCode;
	},
	set propertyCode(val){
		this._propertyCode = val;
	},
	get serviceBranchNumber(){
		return this._serviceBranchNumber;
	},
	set serviceBranchNumber(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute serviceBranchNumber in servicecover.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._serviceBranchNumber = val;
	},
	get serviceCoverDeleted(){
		return kony.sync.getBoolean(this._serviceCoverDeleted)+"";
	},
	set serviceCoverDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute serviceCoverDeleted in servicecover.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._serviceCoverDeleted = val;
	},
	get serviceCoverNumber(){
		return this._serviceCoverNumber;
	},
	set serviceCoverNumber(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute serviceCoverNumber in servicecover.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._serviceCoverNumber = val;
	},
	get serviceCoverSequence(){
		return this._serviceCoverSequence;
	},
	set serviceCoverSequence(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute serviceCoverSequence in servicecover.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._serviceCoverSequence = val;
	},
	get serviceLineCode(){
		return this._serviceLineCode;
	},
	set serviceLineCode(val){
		this._serviceLineCode = val;
	},
	get updateDateTime(){
		return this._updateDateTime;
	},
	set updateDateTime(val){
		this._updateDateTime = val;
	},
	get serviceLineDescription(){
		return this._serviceLineDescription;
	},
	set serviceLineDescription(val){
		this._serviceLineDescription = val;
	},
	get serviceSpecialInstructions(){
		return this._serviceSpecialInstructions;
	},
	set serviceSpecialInstructions(val){
		this._serviceSpecialInstructions = val;
	},
};

/************************************************************************************
* Retrieves all instances of servicecover SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "businessCode";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "contractCode";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* com.pc.servicecovers.servicecover.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
com.pc.servicecovers.servicecover.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	orderByMap = kony.sync.formOrderByClause("servicecover",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.getAll->successcallback");
		successcallback(com.pc.servicecovers.servicecover.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of servicecover present in local database.
*************************************************************************************/
com.pc.servicecovers.servicecover.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.getAllCount function");
	com.pc.servicecovers.servicecover.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of servicecover using where clause in the local Database
*************************************************************************************/
com.pc.servicecovers.servicecover.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering com.pc.servicecovers.servicecover.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of servicecover in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.servicecovers.servicecover.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.servicecovers.servicecover.prototype.create function");
	var valuestable = this.getValuesTable(true);
	com.pc.servicecovers.servicecover.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.servicecovers.servicecover.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  com.pc.servicecovers.servicecover.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"servicecover",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.servicecovers.servicecover.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = com.pc.servicecovers.servicecover.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "businessCode=" + valuestable.businessCode;
		pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
		errMsg = errMsg + ", contractCode=" + valuestable.contractCode;
		pks["contractCode"] = {key:"contractCode",value:valuestable.contractCode};
		errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
		pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
		errMsg = errMsg + ", propertyCode=" + valuestable.propertyCode;
		pks["propertyCode"] = {key:"propertyCode",value:valuestable.propertyCode};
		errMsg = errMsg + ", serviceLineCode=" + valuestable.serviceLineCode;
		pks["serviceLineCode"] = {key:"serviceLineCode",value:valuestable.serviceLineCode};
		com.pc.servicecovers.servicecover.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of servicecover in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].contractCode = "contractCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].propertyCode = "propertyCode_0";
*		valuesArray[0].serviceBranchNumber = 0;
*		valuesArray[0].serviceCoverNumber = 0;
*		valuesArray[0].serviceCoverSequence = 0;
*		valuesArray[0].serviceLineCode = "serviceLineCode_0";
*		valuesArray[0].serviceLineDescription = "serviceLineDescription_0";
*		valuesArray[0].serviceSpecialInstructions = "serviceSpecialInstructions_0";
*		valuesArray[1] = {};
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].contractCode = "contractCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].propertyCode = "propertyCode_1";
*		valuesArray[1].serviceBranchNumber = 1;
*		valuesArray[1].serviceCoverNumber = 1;
*		valuesArray[1].serviceCoverSequence = 1;
*		valuesArray[1].serviceLineCode = "serviceLineCode_1";
*		valuesArray[1].serviceLineDescription = "serviceLineDescription_1";
*		valuesArray[1].serviceSpecialInstructions = "serviceSpecialInstructions_1";
*		valuesArray[2] = {};
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].contractCode = "contractCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].propertyCode = "propertyCode_2";
*		valuesArray[2].serviceBranchNumber = 2;
*		valuesArray[2].serviceCoverNumber = 2;
*		valuesArray[2].serviceCoverSequence = 2;
*		valuesArray[2].serviceLineCode = "serviceLineCode_2";
*		valuesArray[2].serviceLineDescription = "serviceLineDescription_2";
*		valuesArray[2].serviceSpecialInstructions = "serviceSpecialInstructions_2";
*		com.pc.servicecovers.servicecover.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
com.pc.servicecovers.servicecover.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"servicecover",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "businessCode=" + valuestable.businessCode;
				pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
				errMsg = errMsg + ", contractCode=" + valuestable.contractCode;
				pks["contractCode"] = {key:"contractCode",value:valuestable.contractCode};
				errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
				pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
				errMsg = errMsg + ", propertyCode=" + valuestable.propertyCode;
				pks["propertyCode"] = {key:"propertyCode",value:valuestable.propertyCode};
				errMsg = errMsg + ", serviceLineCode=" + valuestable.serviceLineCode;
				pks["serviceLineCode"] = {key:"serviceLineCode",value:valuestable.serviceLineCode};
				var wcs = [];
				if(com.pc.servicecovers.servicecover.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.servicecovers.servicecover.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.servicecovers.servicecover.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = com.pc.servicecovers.servicecover.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates servicecover using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.servicecovers.servicecover.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.servicecovers.servicecover.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	com.pc.servicecovers.servicecover.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.servicecovers.servicecover.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  com.pc.servicecovers.servicecover.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(com.pc.servicecovers.servicecover.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"servicecover",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = com.pc.servicecovers.servicecover.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates servicecover(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.servicecovers.servicecover.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"servicecover",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.servicecovers.servicecover.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.servicecovers.servicecover.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = com.pc.servicecovers.servicecover.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.servicecovers.servicecover.getPKTable());
	}
};

/************************************************************************************
* Updates servicecover(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.serviceBranchNumber = 0;
*		inputArray[0].changeSet.serviceCoverNumber = 0;
*		inputArray[0].changeSet.serviceCoverSequence = 0;
*		inputArray[0].changeSet.serviceLineDescription = "serviceLineDescription_updated0";
*		inputArray[0].whereClause = "where businessCode = '0'";
*		inputArray[0].whereClause = "where contractCode = '0'";
*		inputArray[0].whereClause = "where countryCode = '0'";
*		inputArray[0].whereClause = "where propertyCode = '0'";
*		inputArray[0].whereClause = "where serviceLineCode = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.serviceBranchNumber = 1;
*		inputArray[1].changeSet.serviceCoverNumber = 1;
*		inputArray[1].changeSet.serviceCoverSequence = 1;
*		inputArray[1].changeSet.serviceLineDescription = "serviceLineDescription_updated1";
*		inputArray[1].whereClause = "where businessCode = '1'";
*		inputArray[1].whereClause = "where contractCode = '1'";
*		inputArray[1].whereClause = "where countryCode = '1'";
*		inputArray[1].whereClause = "where propertyCode = '1'";
*		inputArray[1].whereClause = "where serviceLineCode = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.serviceBranchNumber = 2;
*		inputArray[2].changeSet.serviceCoverNumber = 2;
*		inputArray[2].changeSet.serviceCoverSequence = 2;
*		inputArray[2].changeSet.serviceLineDescription = "serviceLineDescription_updated2";
*		inputArray[2].whereClause = "where businessCode = '2'";
*		inputArray[2].whereClause = "where contractCode = '2'";
*		inputArray[2].whereClause = "where countryCode = '2'";
*		inputArray[2].whereClause = "where propertyCode = '2'";
*		inputArray[2].whereClause = "where serviceLineCode = '2'";
*		com.pc.servicecovers.servicecover.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
com.pc.servicecovers.servicecover.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering com.pc.servicecovers.servicecover.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898512bd8be";
	var tbname = "servicecover";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"servicecover",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, com.pc.servicecovers.servicecover.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.servicecovers.servicecover.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, com.pc.servicecovers.servicecover.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.servicecovers.servicecover.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = com.pc.servicecovers.servicecover.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes servicecover using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.servicecovers.servicecover.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.prototype.deleteByPK function");
	var pks = this.getPKTable();
	com.pc.servicecovers.servicecover.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
com.pc.servicecovers.servicecover.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(com.pc.servicecovers.servicecover.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function servicecoverTransactionCallback(tx){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.deleteByPK->servicecover_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function servicecoverErrorCallback(){
		sync.log.error("Entering com.pc.servicecovers.servicecover.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function servicecoverSuccessCallback(){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.servicecovers.servicecover.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, servicecoverTransactionCallback, servicecoverSuccessCallback, servicecoverErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes servicecover(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. com.pc.servicecovers.servicecover.remove("where businessCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
com.pc.servicecovers.servicecover.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function servicecover_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function servicecover_removeSuccess(){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.remove->servicecover_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, servicecover_removeTransactioncallback, servicecover_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes servicecover using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.servicecovers.servicecover.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	com.pc.servicecovers.servicecover.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
com.pc.servicecovers.servicecover.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(com.pc.servicecovers.servicecover.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function servicecoverTransactionCallback(tx){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.removeDeviceInstanceByPK -> servicecoverTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function servicecoverErrorCallback(){
		sync.log.error("Entering com.pc.servicecovers.servicecover.removeDeviceInstanceByPK -> servicecoverErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function servicecoverSuccessCallback(){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.removeDeviceInstanceByPK -> servicecoverSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.servicecovers.servicecover.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, servicecoverTransactionCallback, servicecoverSuccessCallback, servicecoverErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes servicecover(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.servicecovers.servicecover.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function servicecover_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function servicecover_removeSuccess(){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.remove->servicecover_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, servicecover_removeTransactioncallback, servicecover_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves servicecover using primary key from the local Database. 
*************************************************************************************/
com.pc.servicecovers.servicecover.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	com.pc.servicecovers.servicecover.getAllDetailsByPK(pks,successcallback,errorcallback);
};
com.pc.servicecovers.servicecover.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var wcs = [];
	if(com.pc.servicecovers.servicecover.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.getAllDetailsByPK-> success callback function");
		successcallback(com.pc.servicecovers.servicecover.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves servicecover(s) using where clause from the local Database. 
* e.g. com.pc.servicecovers.servicecover.find("where businessCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
com.pc.servicecovers.servicecover.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, com.pc.servicecovers.servicecover.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of servicecover with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.servicecovers.servicecover.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	com.pc.servicecovers.servicecover.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.pc.servicecovers.servicecover.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(com.pc.servicecovers.servicecover.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of servicecover matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.servicecovers.servicecover.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering com.pc.servicecovers.servicecover.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of servicecover pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
com.pc.servicecovers.servicecover.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.servicecovers.servicecover.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of servicecover pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
com.pc.servicecovers.servicecover.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.servicecovers.servicecover.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of servicecover deferred for upload.
*************************************************************************************/
com.pc.servicecovers.servicecover.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.servicecovers.servicecover.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to servicecover in local database to last synced state
*************************************************************************************/
com.pc.servicecovers.servicecover.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to servicecover's record with given primary key in local 
* database to last synced state
*************************************************************************************/
com.pc.servicecovers.servicecover.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	com.pc.servicecovers.servicecover.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
com.pc.servicecovers.servicecover.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var wcs = [];
	if(com.pc.servicecovers.servicecover.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.servicecovers.servicecover.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether servicecover's record  
* with given primary key got deferred in last sync
*************************************************************************************/
com.pc.servicecovers.servicecover.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.servicecovers.servicecover.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	com.pc.servicecovers.servicecover.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
com.pc.servicecovers.servicecover.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.servicecovers.servicecover.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether servicecover's record  
* with given primary key is pending for upload
*************************************************************************************/
com.pc.servicecovers.servicecover.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.servicecovers.servicecover.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	com.pc.servicecovers.servicecover.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
com.pc.servicecovers.servicecover.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.servicecovers.servicecover.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.servicecovers.servicecover.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.servicecovers.servicecover.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.pc.servicecovers.servicecover.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.removeCascade function");
	var tbname = com.pc.servicecovers.servicecover.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


com.pc.servicecovers.servicecover.convertTableToObject = function(res){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new com.pc.servicecovers.servicecover();
			obj.businessCode = res[i].businessCode;
			obj.contractCode = res[i].contractCode;
			obj.countryCode = res[i].countryCode;
			obj.propertyCode = res[i].propertyCode;
			obj.serviceBranchNumber = res[i].serviceBranchNumber;
			obj.serviceCoverDeleted = res[i].serviceCoverDeleted;
			obj.serviceCoverNumber = res[i].serviceCoverNumber;
			obj.serviceCoverSequence = res[i].serviceCoverSequence;
			obj.serviceLineCode = res[i].serviceLineCode;
			obj.updateDateTime = res[i].updateDateTime;
			obj.serviceLineDescription = res[i].serviceLineDescription;
			obj.serviceSpecialInstructions = res[i].serviceSpecialInstructions;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

com.pc.servicecovers.servicecover.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.filterAttributes function");
	var attributeTable = {};
	attributeTable.businessCode = "businessCode";
	attributeTable.contractCode = "contractCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.propertyCode = "propertyCode";
	attributeTable.serviceBranchNumber = "serviceBranchNumber";
	attributeTable.serviceCoverNumber = "serviceCoverNumber";
	attributeTable.serviceCoverSequence = "serviceCoverSequence";
	attributeTable.serviceLineCode = "serviceLineCode";
	attributeTable.serviceLineDescription = "serviceLineDescription";
	attributeTable.serviceSpecialInstructions = "serviceSpecialInstructions";

	var PKTable = {};
	PKTable.businessCode = {}
	PKTable.businessCode.name = "businessCode";
	PKTable.businessCode.isAutoGen = false;
	PKTable.contractCode = {}
	PKTable.contractCode.name = "contractCode";
	PKTable.contractCode.isAutoGen = false;
	PKTable.countryCode = {}
	PKTable.countryCode.name = "countryCode";
	PKTable.countryCode.isAutoGen = false;
	PKTable.propertyCode = {}
	PKTable.propertyCode.name = "propertyCode";
	PKTable.propertyCode.isAutoGen = false;
	PKTable.serviceLineCode = {}
	PKTable.serviceLineCode.name = "serviceLineCode";
	PKTable.serviceLineCode.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject servicecover. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject servicecover. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject servicecover. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

com.pc.servicecovers.servicecover.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = com.pc.servicecovers.servicecover.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

com.pc.servicecovers.servicecover.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.prototype.getValuesTable function");
	var valuesTable = {};
	if(isInsert===true){
		valuesTable.businessCode = this.businessCode;
	}
	if(isInsert===true){
		valuesTable.contractCode = this.contractCode;
	}
	if(isInsert===true){
		valuesTable.countryCode = this.countryCode;
	}
	if(isInsert===true){
		valuesTable.propertyCode = this.propertyCode;
	}
	valuesTable.serviceBranchNumber = this.serviceBranchNumber;
	valuesTable.serviceCoverNumber = this.serviceCoverNumber;
	valuesTable.serviceCoverSequence = this.serviceCoverSequence;
	if(isInsert===true){
		valuesTable.serviceLineCode = this.serviceLineCode;
	}
	valuesTable.serviceLineDescription = this.serviceLineDescription;
	valuesTable.serviceSpecialInstructions = this.serviceSpecialInstructions;
	return valuesTable;
};

com.pc.servicecovers.servicecover.prototype.getPKTable = function(){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.prototype.getPKTable function");
	var pkTable = {};
	pkTable.businessCode = {key:"businessCode",value:this.businessCode};
	pkTable.contractCode = {key:"contractCode",value:this.contractCode};
	pkTable.countryCode = {key:"countryCode",value:this.countryCode};
	pkTable.propertyCode = {key:"propertyCode",value:this.propertyCode};
	pkTable.serviceLineCode = {key:"serviceLineCode",value:this.serviceLineCode};
	return pkTable;
};

com.pc.servicecovers.servicecover.getPKTable = function(){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.getPKTable function");
	var pkTable = [];
	pkTable.push("businessCode");
	pkTable.push("contractCode");
	pkTable.push("countryCode");
	pkTable.push("propertyCode");
	pkTable.push("serviceLineCode");
	return pkTable;
};

com.pc.servicecovers.servicecover.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.pkCheck function");
	var wc = [];
	if(!kony.sync.isNull(pks.businessCode)){
		if(!kony.sync.isNull(pks.businessCode.value)){
			wc.key = "businessCode";
			wc.value = pks.businessCode.value;
		}
		else{
			wc.key = "businessCode";
			wc.value = pks.businessCode;
		}
	}else{
		sync.log.error("Primary Key businessCode not specified in " + opName + " an item in servicecover");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("businessCode",opName,"servicecover")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.contractCode)){
		if(!kony.sync.isNull(pks.contractCode.value)){
			wc.key = "contractCode";
			wc.value = pks.contractCode.value;
		}
		else{
			wc.key = "contractCode";
			wc.value = pks.contractCode;
		}
	}else{
		sync.log.error("Primary Key contractCode not specified in " + opName + " an item in servicecover");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("contractCode",opName,"servicecover")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.countryCode)){
		if(!kony.sync.isNull(pks.countryCode.value)){
			wc.key = "countryCode";
			wc.value = pks.countryCode.value;
		}
		else{
			wc.key = "countryCode";
			wc.value = pks.countryCode;
		}
	}else{
		sync.log.error("Primary Key countryCode not specified in " + opName + " an item in servicecover");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("countryCode",opName,"servicecover")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.propertyCode)){
		if(!kony.sync.isNull(pks.propertyCode.value)){
			wc.key = "propertyCode";
			wc.value = pks.propertyCode.value;
		}
		else{
			wc.key = "propertyCode";
			wc.value = pks.propertyCode;
		}
	}else{
		sync.log.error("Primary Key propertyCode not specified in " + opName + " an item in servicecover");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("propertyCode",opName,"servicecover")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.serviceLineCode)){
		if(!kony.sync.isNull(pks.serviceLineCode.value)){
			wc.key = "serviceLineCode";
			wc.value = pks.serviceLineCode.value;
		}
		else{
			wc.key = "serviceLineCode";
			wc.value = pks.serviceLineCode;
		}
	}else{
		sync.log.error("Primary Key serviceLineCode not specified in " + opName + " an item in servicecover");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("serviceLineCode",opName,"servicecover")));
		return;
	}
	kony.table.insert(wcs,wc);
	return true;
};

com.pc.servicecovers.servicecover.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.validateNull function");
	return true;
};

com.pc.servicecovers.servicecover.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.validateNullInsert function");
	return true;
};

com.pc.servicecovers.servicecover.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering com.pc.servicecovers.servicecover.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


com.pc.servicecovers.servicecover.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

com.pc.servicecovers.servicecover.getTableName = function(){
	return "servicecover";
};




// **********************************End servicecover's helper methods************************