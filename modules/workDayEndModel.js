//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:03 UTC 2016workDayEnd*******************
// **********************************Start workDayEnd's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}




/************************************************************************************
* Creates new workDayEnd
*************************************************************************************/
workDayEnd = function(){
	this.businessCode = null;
	this.countryCode = null;
	this.email = null;
	this.languageCode = null;
	this.workingDayDateEnd = null;
	this.workingDayTimeEnd = null;
	this.createDateTime = null;
	this.lastUpdatedTimeStamp = null;
	this.markForUpload = true;
};

workDayEnd.prototype = {
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get languageCode(){
		return this._languageCode;
	},
	set languageCode(val){
		this._languageCode = val;
	},
	get workingDayDateEnd(){
		return this._workingDayDateEnd;
	},
	set workingDayDateEnd(val){
		this._workingDayDateEnd = val;
	},
	get workingDayTimeEnd(){
		return this._workingDayTimeEnd;
	},
	set workingDayTimeEnd(val){
		this._workingDayTimeEnd = val;
	},
	get createDateTime(){
		return this._createDateTime;
	},
	set createDateTime(val){
		this._createDateTime = val;
	},
	get lastUpdatedTimeStamp(){
		return this._lastUpdatedTimeStamp;
	},
	set lastUpdatedTimeStamp(val){
		this._lastUpdatedTimeStamp = val;
	},
};

/************************************************************************************
* Retrieves all instances of workDayEnd SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "businessCode";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "countryCode";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* workDayEnd.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
workDayEnd.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering workDayEnd.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	orderByMap = kony.sync.formOrderByClause("workDayEnd",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering workDayEnd.getAll->successcallback");
		successcallback(workDayEnd.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of workDayEnd present in local database.
*************************************************************************************/
workDayEnd.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.getAllCount function");
	workDayEnd.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of workDayEnd using where clause in the local Database
*************************************************************************************/
workDayEnd.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering workDayEnd.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of workDayEnd in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
workDayEnd.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  workDayEnd.prototype.create function");
	var valuestable = this.getValuesTable(true);
	workDayEnd.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
workDayEnd.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  workDayEnd.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "workDayEnd.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"workDayEnd",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  workDayEnd.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = workDayEnd.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "businessCode=" + valuestable.businessCode;
		pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
		errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
		pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
		errMsg = errMsg + ", workingDayDateEnd=" + valuestable.workingDayDateEnd;
		pks["workingDayDateEnd"] = {key:"workingDayDateEnd",value:valuestable.workingDayDateEnd};
		errMsg = errMsg + ", workingDayTimeEnd=" + valuestable.workingDayTimeEnd;
		pks["workingDayTimeEnd"] = {key:"workingDayTimeEnd",value:valuestable.workingDayTimeEnd};
		workDayEnd.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of workDayEnd in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].email = "email_0";
*		valuesArray[0].languageCode = "languageCode_0";
*		valuesArray[0].workingDayDateEnd = "workingDayDateEnd_0";
*		valuesArray[0].workingDayTimeEnd = "workingDayTimeEnd_0";
*		valuesArray[0].createDateTime = "createDateTime_0";
*		valuesArray[0].lastUpdatedTimeStamp = 0;
*		valuesArray[1] = {};
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].email = "email_1";
*		valuesArray[1].languageCode = "languageCode_1";
*		valuesArray[1].workingDayDateEnd = "workingDayDateEnd_1";
*		valuesArray[1].workingDayTimeEnd = "workingDayTimeEnd_1";
*		valuesArray[1].createDateTime = "createDateTime_1";
*		valuesArray[1].lastUpdatedTimeStamp = 1;
*		valuesArray[2] = {};
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].email = "email_2";
*		valuesArray[2].languageCode = "languageCode_2";
*		valuesArray[2].workingDayDateEnd = "workingDayDateEnd_2";
*		valuesArray[2].workingDayTimeEnd = "workingDayTimeEnd_2";
*		valuesArray[2].createDateTime = "createDateTime_2";
*		valuesArray[2].lastUpdatedTimeStamp = 2;
*		workDayEnd.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
workDayEnd.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering workDayEnd.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"workDayEnd",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "businessCode=" + valuestable.businessCode;
				pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
				errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
				pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
				errMsg = errMsg + ", workingDayDateEnd=" + valuestable.workingDayDateEnd;
				pks["workingDayDateEnd"] = {key:"workingDayDateEnd",value:valuestable.workingDayDateEnd};
				errMsg = errMsg + ", workingDayTimeEnd=" + valuestable.workingDayTimeEnd;
				pks["workingDayTimeEnd"] = {key:"workingDayTimeEnd",value:valuestable.workingDayTimeEnd};
				var wcs = [];
				if(workDayEnd.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  workDayEnd.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  workDayEnd.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = workDayEnd.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates workDayEnd using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
workDayEnd.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  workDayEnd.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	workDayEnd.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
workDayEnd.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  workDayEnd.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(workDayEnd.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"workDayEnd",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = workDayEnd.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates workDayEnd(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
workDayEnd.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering workDayEnd.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"workDayEnd",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  workDayEnd.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, workDayEnd.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = workDayEnd.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, workDayEnd.getPKTable());
	}
};

/************************************************************************************
* Updates workDayEnd(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.email = "email_updated0";
*		inputArray[0].changeSet.languageCode = "languageCode_updated0";
*		inputArray[0].changeSet.createDateTime = "createDateTime_updated0";
*		inputArray[0].changeSet.lastUpdatedTimeStamp = "lastUpdatedTimeStamp_updated0";
*		inputArray[0].whereClause = "where businessCode = '0'";
*		inputArray[0].whereClause = "where countryCode = '0'";
*		inputArray[0].whereClause = "where workingDayDateEnd = '0'";
*		inputArray[0].whereClause = "where workingDayTimeEnd = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.email = "email_updated1";
*		inputArray[1].changeSet.languageCode = "languageCode_updated1";
*		inputArray[1].changeSet.createDateTime = "createDateTime_updated1";
*		inputArray[1].changeSet.lastUpdatedTimeStamp = "lastUpdatedTimeStamp_updated1";
*		inputArray[1].whereClause = "where businessCode = '1'";
*		inputArray[1].whereClause = "where countryCode = '1'";
*		inputArray[1].whereClause = "where workingDayDateEnd = '1'";
*		inputArray[1].whereClause = "where workingDayTimeEnd = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.email = "email_updated2";
*		inputArray[2].changeSet.languageCode = "languageCode_updated2";
*		inputArray[2].changeSet.createDateTime = "createDateTime_updated2";
*		inputArray[2].changeSet.lastUpdatedTimeStamp = "lastUpdatedTimeStamp_updated2";
*		inputArray[2].whereClause = "where businessCode = '2'";
*		inputArray[2].whereClause = "where countryCode = '2'";
*		inputArray[2].whereClause = "where workingDayDateEnd = '2'";
*		inputArray[2].whereClause = "where workingDayTimeEnd = '2'";
*		workDayEnd.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
workDayEnd.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering workDayEnd.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898512bd8be";
	var tbname = "workDayEnd";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"workDayEnd",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, workDayEnd.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  workDayEnd.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, workDayEnd.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  workDayEnd.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = workDayEnd.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes workDayEnd using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
workDayEnd.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.prototype.deleteByPK function");
	var pks = this.getPKTable();
	workDayEnd.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
workDayEnd.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering workDayEnd.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(workDayEnd.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function workDayEndTransactionCallback(tx){
		sync.log.trace("Entering workDayEnd.deleteByPK->workDayEnd_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function workDayEndErrorCallback(){
		sync.log.error("Entering workDayEnd.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function workDayEndSuccessCallback(){
		sync.log.trace("Entering workDayEnd.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering workDayEnd.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, workDayEndTransactionCallback, workDayEndSuccessCallback, workDayEndErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes workDayEnd(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. workDayEnd.remove("where businessCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
workDayEnd.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering workDayEnd.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function workDayEnd_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function workDayEnd_removeSuccess(){
		sync.log.trace("Entering workDayEnd.remove->workDayEnd_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering workDayEnd.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering workDayEnd.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, workDayEnd_removeTransactioncallback, workDayEnd_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes workDayEnd using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
workDayEnd.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	workDayEnd.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
workDayEnd.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(workDayEnd.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function workDayEndTransactionCallback(tx){
		sync.log.trace("Entering workDayEnd.removeDeviceInstanceByPK -> workDayEndTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function workDayEndErrorCallback(){
		sync.log.error("Entering workDayEnd.removeDeviceInstanceByPK -> workDayEndErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function workDayEndSuccessCallback(){
		sync.log.trace("Entering workDayEnd.removeDeviceInstanceByPK -> workDayEndSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering workDayEnd.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, workDayEndTransactionCallback, workDayEndSuccessCallback, workDayEndErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes workDayEnd(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
workDayEnd.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function workDayEnd_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function workDayEnd_removeSuccess(){
		sync.log.trace("Entering workDayEnd.remove->workDayEnd_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering workDayEnd.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering workDayEnd.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, workDayEnd_removeTransactioncallback, workDayEnd_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves workDayEnd using primary key from the local Database. 
*************************************************************************************/
workDayEnd.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	workDayEnd.getAllDetailsByPK(pks,successcallback,errorcallback);
};
workDayEnd.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var wcs = [];
	if(workDayEnd.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering workDayEnd.getAllDetailsByPK-> success callback function");
		successcallback(workDayEnd.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves workDayEnd(s) using where clause from the local Database. 
* e.g. workDayEnd.find("where businessCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
workDayEnd.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, workDayEnd.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of workDayEnd with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
workDayEnd.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering workDayEnd.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	workDayEnd.markForUploadbyPK(pks, successcallback, errorcallback);
};
workDayEnd.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering workDayEnd.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(workDayEnd.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of workDayEnd matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
workDayEnd.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering workDayEnd.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering workDayEnd.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering workDayEnd.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of workDayEnd pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
workDayEnd.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workDayEnd.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, workDayEnd.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of workDayEnd pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
workDayEnd.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering workDayEnd.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workDayEnd.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, workDayEnd.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of workDayEnd deferred for upload.
*************************************************************************************/
workDayEnd.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workDayEnd.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, workDayEnd.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to workDayEnd in local database to last synced state
*************************************************************************************/
workDayEnd.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering workDayEnd.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workDayEnd.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to workDayEnd's record with given primary key in local 
* database to last synced state
*************************************************************************************/
workDayEnd.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering workDayEnd.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	workDayEnd.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
workDayEnd.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering workDayEnd.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var wcs = [];
	if(workDayEnd.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workDayEnd.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering workDayEnd.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether workDayEnd's record  
* with given primary key got deferred in last sync
*************************************************************************************/
workDayEnd.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  workDayEnd.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	workDayEnd.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
workDayEnd.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering workDayEnd.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var wcs = [] ;
	var flag;
	if(workDayEnd.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workDayEnd.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether workDayEnd's record  
* with given primary key is pending for upload
*************************************************************************************/
workDayEnd.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  workDayEnd.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	workDayEnd.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
workDayEnd.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering workDayEnd.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workDayEnd.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workDayEnd.getTableName();
	var wcs = [] ;
	var flag;
	if(workDayEnd.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workDayEnd.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
workDayEnd.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering workDayEnd.removeCascade function");
	var tbname = workDayEnd.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


workDayEnd.convertTableToObject = function(res){
	sync.log.trace("Entering workDayEnd.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new workDayEnd();
			obj.businessCode = res[i].businessCode;
			obj.countryCode = res[i].countryCode;
			obj.email = res[i].email;
			obj.languageCode = res[i].languageCode;
			obj.workingDayDateEnd = res[i].workingDayDateEnd;
			obj.workingDayTimeEnd = res[i].workingDayTimeEnd;
			obj.createDateTime = res[i].createDateTime;
			obj.lastUpdatedTimeStamp = res[i].lastUpdatedTimeStamp;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

workDayEnd.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering workDayEnd.filterAttributes function");
	var attributeTable = {};
	attributeTable.businessCode = "businessCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.email = "email";
	attributeTable.languageCode = "languageCode";
	attributeTable.workingDayDateEnd = "workingDayDateEnd";
	attributeTable.workingDayTimeEnd = "workingDayTimeEnd";
	attributeTable.createDateTime = "createDateTime";
	attributeTable.lastUpdatedTimeStamp = "lastUpdatedTimeStamp";

	var PKTable = {};
	PKTable.businessCode = {}
	PKTable.businessCode.name = "businessCode";
	PKTable.businessCode.isAutoGen = false;
	PKTable.countryCode = {}
	PKTable.countryCode.name = "countryCode";
	PKTable.countryCode.isAutoGen = false;
	PKTable.workingDayDateEnd = {}
	PKTable.workingDayDateEnd.name = "workingDayDateEnd";
	PKTable.workingDayDateEnd.isAutoGen = false;
	PKTable.workingDayTimeEnd = {}
	PKTable.workingDayTimeEnd.name = "workingDayTimeEnd";
	PKTable.workingDayTimeEnd.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject workDayEnd. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject workDayEnd. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject workDayEnd. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

workDayEnd.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering workDayEnd.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = workDayEnd.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

workDayEnd.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering workDayEnd.prototype.getValuesTable function");
	var valuesTable = {};
	if(isInsert===true){
		valuesTable.businessCode = this.businessCode;
	}
	if(isInsert===true){
		valuesTable.countryCode = this.countryCode;
	}
	valuesTable.email = this.email;
	valuesTable.languageCode = this.languageCode;
	if(isInsert===true){
		valuesTable.workingDayDateEnd = this.workingDayDateEnd;
	}
	if(isInsert===true){
		valuesTable.workingDayTimeEnd = this.workingDayTimeEnd;
	}
	valuesTable.createDateTime = this.createDateTime;
	valuesTable.lastUpdatedTimeStamp = this.lastUpdatedTimeStamp;
	return valuesTable;
};

workDayEnd.prototype.getPKTable = function(){
	sync.log.trace("Entering workDayEnd.prototype.getPKTable function");
	var pkTable = {};
	pkTable.businessCode = {key:"businessCode",value:this.businessCode};
	pkTable.countryCode = {key:"countryCode",value:this.countryCode};
	pkTable.workingDayDateEnd = {key:"workingDayDateEnd",value:this.workingDayDateEnd};
	pkTable.workingDayTimeEnd = {key:"workingDayTimeEnd",value:this.workingDayTimeEnd};
	return pkTable;
};

workDayEnd.getPKTable = function(){
	sync.log.trace("Entering workDayEnd.getPKTable function");
	var pkTable = [];
	pkTable.push("businessCode");
	pkTable.push("countryCode");
	pkTable.push("workingDayDateEnd");
	pkTable.push("workingDayTimeEnd");
	return pkTable;
};

workDayEnd.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering workDayEnd.pkCheck function");
	var wc = [];
	if(!kony.sync.isNull(pks.businessCode)){
		if(!kony.sync.isNull(pks.businessCode.value)){
			wc.key = "businessCode";
			wc.value = pks.businessCode.value;
		}
		else{
			wc.key = "businessCode";
			wc.value = pks.businessCode;
		}
	}else{
		sync.log.error("Primary Key businessCode not specified in " + opName + " an item in workDayEnd");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("businessCode",opName,"workDayEnd")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.countryCode)){
		if(!kony.sync.isNull(pks.countryCode.value)){
			wc.key = "countryCode";
			wc.value = pks.countryCode.value;
		}
		else{
			wc.key = "countryCode";
			wc.value = pks.countryCode;
		}
	}else{
		sync.log.error("Primary Key countryCode not specified in " + opName + " an item in workDayEnd");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("countryCode",opName,"workDayEnd")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.workingDayDateEnd)){
		if(!kony.sync.isNull(pks.workingDayDateEnd.value)){
			wc.key = "workingDayDateEnd";
			wc.value = pks.workingDayDateEnd.value;
		}
		else{
			wc.key = "workingDayDateEnd";
			wc.value = pks.workingDayDateEnd;
		}
	}else{
		sync.log.error("Primary Key workingDayDateEnd not specified in " + opName + " an item in workDayEnd");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("workingDayDateEnd",opName,"workDayEnd")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.workingDayTimeEnd)){
		if(!kony.sync.isNull(pks.workingDayTimeEnd.value)){
			wc.key = "workingDayTimeEnd";
			wc.value = pks.workingDayTimeEnd.value;
		}
		else{
			wc.key = "workingDayTimeEnd";
			wc.value = pks.workingDayTimeEnd;
		}
	}else{
		sync.log.error("Primary Key workingDayTimeEnd not specified in " + opName + " an item in workDayEnd");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("workingDayTimeEnd",opName,"workDayEnd")));
		return;
	}
	kony.table.insert(wcs,wc);
	return true;
};

workDayEnd.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering workDayEnd.validateNull function");
	return true;
};

workDayEnd.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering workDayEnd.validateNullInsert function");
	return true;
};

workDayEnd.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering workDayEnd.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


workDayEnd.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

workDayEnd.getTableName = function(){
	return "workDayEnd";
};




// **********************************End workDayEnd's helper methods************************