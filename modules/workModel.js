//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:04 UTC 2016work*******************
// **********************************Start work's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(com)=== "undefined"){ com = {}; }
if(typeof(com.pc)=== "undefined"){ com.pc = {}; }
if(typeof(com.pc.workloads)=== "undefined"){ com.pc.workloads = {}; }

/************************************************************************************
* Creates new work
*************************************************************************************/
com.pc.workloads.work = function(){
	this.businessCode = null;
	this.countryCode = null;
	this.employeeCodePrimary = null;
	this.propertyCode = null;
	this.updateDateTime = null;
	this.workDateStartPlanned = null;
	this.workDeleted = null;
	this.workId = null;
	this.workStatus = null;
	this.customerApproval = null;
	this.customerApprovalName = null;
	this.customerApprovalSignature = null;
	this.issueCodeNoSignature = null;
	this.workDateStartActual = null;
	this.issueTypeCode = null;
	this.issueCode = null;
	this.issueTypeCodeNoSignature = null;
	this.emailDistributionList = null;
	this.serviceReceiptEmailSubject = null;
	this.outcard = null;
	this.outcardCode = null;
	this.outcardPropertyName = null;
	this.outcardPropertyAddressLine1 = null;
	this.outcardPropertyAddressLine2 = null;
	this.outcardPropertyAddressLine3 = null;
	this.outcardPropertyAddressLine4 = null;
	this.outcardPropertyAddressLine5 = null;
	this.outcardPropertyPostcode = null;
	this.outcardPropertyContactName = null;
	this.outcardPropertyContactTelephone = null;
	this.outcardOfficeRef = null;
	this.capturedPhoto = null;
	this.markForUpload = true;
};

com.pc.workloads.work.prototype = {
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get employeeCodePrimary(){
		return this._employeeCodePrimary;
	},
	set employeeCodePrimary(val){
		this._employeeCodePrimary = val;
	},
	get propertyCode(){
		return this._propertyCode;
	},
	set propertyCode(val){
		this._propertyCode = val;
	},
	get updateDateTime(){
		return this._updateDateTime;
	},
	set updateDateTime(val){
		this._updateDateTime = val;
	},
	get workDateStartPlanned(){
		return this._workDateStartPlanned;
	},
	set workDateStartPlanned(val){
		this._workDateStartPlanned = val;
	},
	get workDeleted(){
		return kony.sync.getBoolean(this._workDeleted)+"";
	},
	set workDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute workDeleted in work.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._workDeleted = val;
	},
	get workId(){
		return this._workId;
	},
	set workId(val){
		this._workId = val;
	},
	get workStatus(){
		return this._workStatus;
	},
	set workStatus(val){
		this._workStatus = val;
	},
	get customerApproval(){
		return kony.sync.getBoolean(this._customerApproval)+"";
	},
	set customerApproval(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute customerApproval in work.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._customerApproval = val;
	},
	get customerApprovalName(){
		return this._customerApprovalName;
	},
	set customerApprovalName(val){
		this._customerApprovalName = val;
	},
	get customerApprovalSignature(){
		return this._customerApprovalSignature;
	},
	set customerApprovalSignature(val){
		this._customerApprovalSignature = val;
	},
	get issueCodeNoSignature(){
		return this._issueCodeNoSignature;
	},
	set issueCodeNoSignature(val){
		this._issueCodeNoSignature = val;
	},
	get workDateStartActual(){
		return this._workDateStartActual;
	},
	set workDateStartActual(val){
		this._workDateStartActual = val;
	},
	get issueTypeCode(){
		return this._issueTypeCode;
	},
	set issueTypeCode(val){
		this._issueTypeCode = val;
	},
	get issueCode(){
		return this._issueCode;
	},
	set issueCode(val){
		this._issueCode = val;
	},
	get issueTypeCodeNoSignature(){
		return this._issueTypeCodeNoSignature;
	},
	set issueTypeCodeNoSignature(val){
		this._issueTypeCodeNoSignature = val;
	},
	get emailDistributionList(){
		return this._emailDistributionList;
	},
	set emailDistributionList(val){
		this._emailDistributionList = val;
	},
	get serviceReceiptEmailSubject(){
		return this._serviceReceiptEmailSubject;
	},
	set serviceReceiptEmailSubject(val){
		this._serviceReceiptEmailSubject = val;
	},
	get outcard(){
		return kony.sync.getBoolean(this._outcard)+"";
	},
	set outcard(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute outcard in work.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._outcard = val;
	},
	get outcardCode(){
		return this._outcardCode;
	},
	set outcardCode(val){
		this._outcardCode = val;
	},
	get outcardPropertyName(){
		return this._outcardPropertyName;
	},
	set outcardPropertyName(val){
		this._outcardPropertyName = val;
	},
	get outcardPropertyAddressLine1(){
		return this._outcardPropertyAddressLine1;
	},
	set outcardPropertyAddressLine1(val){
		this._outcardPropertyAddressLine1 = val;
	},
	get outcardPropertyAddressLine2(){
		return this._outcardPropertyAddressLine2;
	},
	set outcardPropertyAddressLine2(val){
		this._outcardPropertyAddressLine2 = val;
	},
	get outcardPropertyAddressLine3(){
		return this._outcardPropertyAddressLine3;
	},
	set outcardPropertyAddressLine3(val){
		this._outcardPropertyAddressLine3 = val;
	},
	get outcardPropertyAddressLine4(){
		return this._outcardPropertyAddressLine4;
	},
	set outcardPropertyAddressLine4(val){
		this._outcardPropertyAddressLine4 = val;
	},
	get outcardPropertyAddressLine5(){
		return this._outcardPropertyAddressLine5;
	},
	set outcardPropertyAddressLine5(val){
		this._outcardPropertyAddressLine5 = val;
	},
	get outcardPropertyPostcode(){
		return this._outcardPropertyPostcode;
	},
	set outcardPropertyPostcode(val){
		this._outcardPropertyPostcode = val;
	},
	get outcardPropertyContactName(){
		return this._outcardPropertyContactName;
	},
	set outcardPropertyContactName(val){
		this._outcardPropertyContactName = val;
	},
	get outcardPropertyContactTelephone(){
		return this._outcardPropertyContactTelephone;
	},
	set outcardPropertyContactTelephone(val){
		this._outcardPropertyContactTelephone = val;
	},
	get outcardOfficeRef(){
		return this._outcardOfficeRef;
	},
	set outcardOfficeRef(val){
		this._outcardOfficeRef = val;
	},
	get capturedPhoto(){
		return this._capturedPhoto;
	},
	set capturedPhoto(val){
		this._capturedPhoto = val;
	},
};

/************************************************************************************
* Retrieves all instances of work SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "businessCode";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "countryCode";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* com.pc.workloads.work.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
com.pc.workloads.work.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering com.pc.workloads.work.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	orderByMap = kony.sync.formOrderByClause("work",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.workloads.work.getAll->successcallback");
		successcallback(com.pc.workloads.work.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of work present in local database.
*************************************************************************************/
com.pc.workloads.work.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getAllCount function");
	com.pc.workloads.work.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of work using where clause in the local Database
*************************************************************************************/
com.pc.workloads.work.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering com.pc.workloads.work.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of work in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.work.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.workloads.work.prototype.create function");
	var valuestable = this.getValuesTable(true);
	com.pc.workloads.work.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.workloads.work.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  com.pc.workloads.work.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"work",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.workloads.work.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = com.pc.workloads.work.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "workId=" + valuestable.workId;
		pks["workId"] = {key:"workId",value:valuestable.workId};
		com.pc.workloads.work.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of work in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].employeeCodePrimary = "employeeCodePrimary_0";
*		valuesArray[0].propertyCode = "propertyCode_0";
*		valuesArray[0].workDateStartPlanned = "workDateStartPlanned_0";
*		valuesArray[0].workId = "workId_0";
*		valuesArray[0].workStatus = "workStatus_0";
*		valuesArray[0].customerApproval = true;
*		valuesArray[0].customerApprovalName = "customerApprovalName_0";
*		valuesArray[0].customerApprovalSignature = 0;
*		valuesArray[0].issueCodeNoSignature = "issueCodeNoSignature_0";
*		valuesArray[0].workDateStartActual = "workDateStartActual_0";
*		valuesArray[0].issueTypeCode = "issueTypeCode_0";
*		valuesArray[0].issueCode = "issueCode_0";
*		valuesArray[0].issueTypeCodeNoSignature = "issueTypeCodeNoSignature_0";
*		valuesArray[0].emailDistributionList = "emailDistributionList_0";
*		valuesArray[0].serviceReceiptEmailSubject = "serviceReceiptEmailSubject_0";
*		valuesArray[0].outcard = true;
*		valuesArray[0].outcardCode = "outcardCode_0";
*		valuesArray[0].outcardPropertyName = "outcardPropertyName_0";
*		valuesArray[0].outcardPropertyAddressLine1 = "outcardPropertyAddressLine1_0";
*		valuesArray[0].outcardPropertyAddressLine2 = "outcardPropertyAddressLine2_0";
*		valuesArray[0].outcardPropertyAddressLine3 = "outcardPropertyAddressLine3_0";
*		valuesArray[0].outcardPropertyAddressLine4 = "outcardPropertyAddressLine4_0";
*		valuesArray[0].outcardPropertyAddressLine5 = "outcardPropertyAddressLine5_0";
*		valuesArray[0].outcardPropertyPostcode = "outcardPropertyPostcode_0";
*		valuesArray[0].outcardPropertyContactName = "outcardPropertyContactName_0";
*		valuesArray[0].outcardPropertyContactTelephone = "outcardPropertyContactTelephone_0";
*		valuesArray[0].outcardOfficeRef = "outcardOfficeRef_0";
*		valuesArray[0].capturedPhoto = "capturedPhoto_0";
*		valuesArray[1] = {};
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].employeeCodePrimary = "employeeCodePrimary_1";
*		valuesArray[1].propertyCode = "propertyCode_1";
*		valuesArray[1].workDateStartPlanned = "workDateStartPlanned_1";
*		valuesArray[1].workId = "workId_1";
*		valuesArray[1].workStatus = "workStatus_1";
*		valuesArray[1].customerApproval = true;
*		valuesArray[1].customerApprovalName = "customerApprovalName_1";
*		valuesArray[1].customerApprovalSignature = 1;
*		valuesArray[1].issueCodeNoSignature = "issueCodeNoSignature_1";
*		valuesArray[1].workDateStartActual = "workDateStartActual_1";
*		valuesArray[1].issueTypeCode = "issueTypeCode_1";
*		valuesArray[1].issueCode = "issueCode_1";
*		valuesArray[1].issueTypeCodeNoSignature = "issueTypeCodeNoSignature_1";
*		valuesArray[1].emailDistributionList = "emailDistributionList_1";
*		valuesArray[1].serviceReceiptEmailSubject = "serviceReceiptEmailSubject_1";
*		valuesArray[1].outcard = true;
*		valuesArray[1].outcardCode = "outcardCode_1";
*		valuesArray[1].outcardPropertyName = "outcardPropertyName_1";
*		valuesArray[1].outcardPropertyAddressLine1 = "outcardPropertyAddressLine1_1";
*		valuesArray[1].outcardPropertyAddressLine2 = "outcardPropertyAddressLine2_1";
*		valuesArray[1].outcardPropertyAddressLine3 = "outcardPropertyAddressLine3_1";
*		valuesArray[1].outcardPropertyAddressLine4 = "outcardPropertyAddressLine4_1";
*		valuesArray[1].outcardPropertyAddressLine5 = "outcardPropertyAddressLine5_1";
*		valuesArray[1].outcardPropertyPostcode = "outcardPropertyPostcode_1";
*		valuesArray[1].outcardPropertyContactName = "outcardPropertyContactName_1";
*		valuesArray[1].outcardPropertyContactTelephone = "outcardPropertyContactTelephone_1";
*		valuesArray[1].outcardOfficeRef = "outcardOfficeRef_1";
*		valuesArray[1].capturedPhoto = "capturedPhoto_1";
*		valuesArray[2] = {};
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].employeeCodePrimary = "employeeCodePrimary_2";
*		valuesArray[2].propertyCode = "propertyCode_2";
*		valuesArray[2].workDateStartPlanned = "workDateStartPlanned_2";
*		valuesArray[2].workId = "workId_2";
*		valuesArray[2].workStatus = "workStatus_2";
*		valuesArray[2].customerApproval = true;
*		valuesArray[2].customerApprovalName = "customerApprovalName_2";
*		valuesArray[2].customerApprovalSignature = 2;
*		valuesArray[2].issueCodeNoSignature = "issueCodeNoSignature_2";
*		valuesArray[2].workDateStartActual = "workDateStartActual_2";
*		valuesArray[2].issueTypeCode = "issueTypeCode_2";
*		valuesArray[2].issueCode = "issueCode_2";
*		valuesArray[2].issueTypeCodeNoSignature = "issueTypeCodeNoSignature_2";
*		valuesArray[2].emailDistributionList = "emailDistributionList_2";
*		valuesArray[2].serviceReceiptEmailSubject = "serviceReceiptEmailSubject_2";
*		valuesArray[2].outcard = true;
*		valuesArray[2].outcardCode = "outcardCode_2";
*		valuesArray[2].outcardPropertyName = "outcardPropertyName_2";
*		valuesArray[2].outcardPropertyAddressLine1 = "outcardPropertyAddressLine1_2";
*		valuesArray[2].outcardPropertyAddressLine2 = "outcardPropertyAddressLine2_2";
*		valuesArray[2].outcardPropertyAddressLine3 = "outcardPropertyAddressLine3_2";
*		valuesArray[2].outcardPropertyAddressLine4 = "outcardPropertyAddressLine4_2";
*		valuesArray[2].outcardPropertyAddressLine5 = "outcardPropertyAddressLine5_2";
*		valuesArray[2].outcardPropertyPostcode = "outcardPropertyPostcode_2";
*		valuesArray[2].outcardPropertyContactName = "outcardPropertyContactName_2";
*		valuesArray[2].outcardPropertyContactTelephone = "outcardPropertyContactTelephone_2";
*		valuesArray[2].outcardOfficeRef = "outcardOfficeRef_2";
*		valuesArray[2].capturedPhoto = "capturedPhoto_2";
*		com.pc.workloads.work.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
com.pc.workloads.work.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.workloads.work.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"work",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "workId=" + valuestable.workId;
				pks["workId"] = {key:"workId",value:valuestable.workId};
				var wcs = [];
				if(com.pc.workloads.work.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.workloads.work.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.workloads.work.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = com.pc.workloads.work.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates work using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.work.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.workloads.work.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	com.pc.workloads.work.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.workloads.work.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  com.pc.workloads.work.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(com.pc.workloads.work.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"work",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = com.pc.workloads.work.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates work(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.work.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering com.pc.workloads.work.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"work",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.workloads.work.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.workloads.work.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = com.pc.workloads.work.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.workloads.work.getPKTable());
	}
};

/************************************************************************************
* Updates work(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.businessCode = "businessCode_updated0";
*		inputArray[0].changeSet.countryCode = "countryCode_updated0";
*		inputArray[0].changeSet.employeeCodePrimary = "employeeCodePrimary_updated0";
*		inputArray[0].changeSet.propertyCode = "propertyCode_updated0";
*		inputArray[0].whereClause = "where workId = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.businessCode = "businessCode_updated1";
*		inputArray[1].changeSet.countryCode = "countryCode_updated1";
*		inputArray[1].changeSet.employeeCodePrimary = "employeeCodePrimary_updated1";
*		inputArray[1].changeSet.propertyCode = "propertyCode_updated1";
*		inputArray[1].whereClause = "where workId = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.businessCode = "businessCode_updated2";
*		inputArray[2].changeSet.countryCode = "countryCode_updated2";
*		inputArray[2].changeSet.employeeCodePrimary = "employeeCodePrimary_updated2";
*		inputArray[2].changeSet.propertyCode = "propertyCode_updated2";
*		inputArray[2].whereClause = "where workId = '2'";
*		com.pc.workloads.work.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
com.pc.workloads.work.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering com.pc.workloads.work.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898512bd8be";
	var tbname = "work";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"work",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, com.pc.workloads.work.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.workloads.work.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, com.pc.workloads.work.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.workloads.work.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = com.pc.workloads.work.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes work using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.work.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.prototype.deleteByPK function");
	var pks = this.getPKTable();
	com.pc.workloads.work.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
com.pc.workloads.work.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.workloads.work.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(com.pc.workloads.work.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function workTransactionCallback(tx){
		sync.log.trace("Entering com.pc.workloads.work.deleteByPK->work_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
			if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, "", com.pc.workloads.workAttendance.removeCascade,"workAttendance",true, errorcallback, markForUpload, record, false)){
				isError = true;	
				kony.sync.rollbackTransaction(tx);
				return;
			}
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
			if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, "", com.pc.workloads.workOrder.removeCascade,"workOrder",true, errorcallback, markForUpload, record, false)){
				isError = true;	
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function workErrorCallback(){
		sync.log.error("Entering com.pc.workloads.work.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function workSuccessCallback(){
		sync.log.trace("Entering com.pc.workloads.work.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.workloads.work.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, workTransactionCallback, workSuccessCallback, workErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes work(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. com.pc.workloads.work.remove("where businessCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
com.pc.workloads.work.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.workloads.work.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function work_removeTransactioncallback(tx){
		wcs = " " + wcs;
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
		if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, wcs, com.pc.workloads.workAttendance.removeCascade, "workAttendance", true, errorcallback, markForUpload, null, false)){
			isError = true;	
			kony.sync.rollbackTransaction(tx);
			return;
		}
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
		if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, wcs, com.pc.workloads.workOrder.removeCascade, "workOrder", true, errorcallback, markForUpload, null, false)){
			isError = true;	
			kony.sync.rollbackTransaction(tx);
			return;
		}
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function work_removeSuccess(){
		sync.log.trace("Entering com.pc.workloads.work.remove->work_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.workloads.work.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.workloads.work.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, work_removeTransactioncallback, work_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes work using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.workloads.work.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	com.pc.workloads.work.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
com.pc.workloads.work.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(com.pc.workloads.work.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function workTransactionCallback(tx){
		sync.log.trace("Entering com.pc.workloads.work.removeDeviceInstanceByPK -> workTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
			if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, "", com.pc.workloads.workAttendance.removeCascade,"workAttendance",true, errorcallback, null, record, true)){
				isError = true;	
				kony.sync.rollbackTransaction(tx);
				return;
			}
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
			if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, "", com.pc.workloads.workOrder.removeCascade,"workOrder",true, errorcallback, null, record, true)){
				isError = true;	
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function workErrorCallback(){
		sync.log.error("Entering com.pc.workloads.work.removeDeviceInstanceByPK -> workErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function workSuccessCallback(){
		sync.log.trace("Entering com.pc.workloads.work.removeDeviceInstanceByPK -> workSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.workloads.work.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, workTransactionCallback, workSuccessCallback, workErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes work(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.workloads.work.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function work_removeTransactioncallback(tx){
		wcs = " " + wcs;
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
		if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, wcs, com.pc.workloads.workAttendance.removeCascade, "workAttendance", true, errorcallback, null, null, true)){
			isError = true;	
			kony.sync.rollbackTransaction(tx);
			return;
		}
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
		if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, wcs, com.pc.workloads.workOrder.removeCascade, "workOrder", true, errorcallback, null, null, true)){
			isError = true;	
			kony.sync.rollbackTransaction(tx);
			return;
		}
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function work_removeSuccess(){
		sync.log.trace("Entering com.pc.workloads.work.remove->work_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.workloads.work.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.workloads.work.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, work_removeTransactioncallback, work_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves work using primary key from the local Database. 
*************************************************************************************/
com.pc.workloads.work.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	com.pc.workloads.work.getAllDetailsByPK(pks,successcallback,errorcallback);
};
com.pc.workloads.work.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var wcs = [];
	if(com.pc.workloads.work.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.workloads.work.getAllDetailsByPK-> success callback function");
		successcallback(com.pc.workloads.work.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves work(s) using where clause from the local Database. 
* e.g. com.pc.workloads.work.find("where businessCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
com.pc.workloads.work.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.work.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of work with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.work.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	com.pc.workloads.work.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.pc.workloads.work.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(com.pc.workloads.work.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of work matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.work.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering com.pc.workloads.work.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering com.pc.workloads.work.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering com.pc.workloads.work.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of work pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
com.pc.workloads.work.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.work.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.work.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of work pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
com.pc.workloads.work.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.work.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.work.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of work deferred for upload.
*************************************************************************************/
com.pc.workloads.work.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.work.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.work.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to work in local database to last synced state
*************************************************************************************/
com.pc.workloads.work.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.work.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to work's record with given primary key in local 
* database to last synced state
*************************************************************************************/
com.pc.workloads.work.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	com.pc.workloads.work.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
com.pc.workloads.work.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var wcs = [];
	if(com.pc.workloads.work.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.work.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.workloads.work.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether work's record  
* with given primary key got deferred in last sync
*************************************************************************************/
com.pc.workloads.work.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.workloads.work.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	com.pc.workloads.work.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
com.pc.workloads.work.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.workloads.work.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.work.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether work's record  
* with given primary key is pending for upload
*************************************************************************************/
com.pc.workloads.work.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.workloads.work.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	com.pc.workloads.work.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
com.pc.workloads.work.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.work.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.workloads.work.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.work.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};



/************************************************************************************
* Retrieves instances of workAttendance related to work
* with given $relationship.getTargetObjectAttribute() from local database.
*************************************************************************************/

															
com.pc.workloads.work.prototype.getworkAttendanceWithworkIdANDbusinessCodeANDcountryCode  = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.prototype.getworkAttendanceWithworkIdANDbusinessCodeANDcountryCode function");
	var pks = this.getPKTable();
	com.pc.workloads.work.getworkAttendanceWithworkIdANDbusinessCodeANDcountryCode(pks,successcallback,errorcallback);
};
com.pc.workloads.work.getworkAttendanceWithworkIdANDbusinessCodeANDcountryCode = function(pks,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getworkAttendanceWithworkIdANDbusinessCodeANDcountryCode function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}		
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.getworkAttendanceWithworkIdANDbusinessCodeANDcountryCode",  "relationship", errorcallback)){
		return;
	}	
	function work_successcallback(res){
		if(null!==res && res.length>0) {
			var wcs = [];
			var targetKey_0 = res[0].workId;				
			wcs.push({key:"workId", value:targetKey_0});		
						var targetKey_1 = res[0].businessCode;				
			wcs.push({key:"businessCode", value:targetKey_1});		
						var targetKey_2 = res[0].countryCode;				
			wcs.push({key:"countryCode", value:targetKey_2});		
			
			var tbname = "workAttendance"
			var query = kony.sync.qb_createQuery();
			kony.sync.qb_select(query, null);
			kony.sync.qb_from(query, tbname);
			kony.sync.qb_where(query,wcs);
		
			var query_compile = kony.sync.qb_compile(query);
			var sql = query_compile[0];
			var params = query_compile[1];
			var dbname = kony.sync.getDBName();
		
			function mySuccCallback(res){
									kony.sync.verifyAndCallClosure(mySuccesscallback, com.pc.workloads.workAttendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
							}
		
			kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(successcallback);
			return;
		}	
	}
	
	function mySuccesscallback(res){
		var objMap = [];
		if(res!==null){
			for(var i in res){
				var obj = new com.pc.workloads.workAttendance();
				obj.businessCode = res[i].businessCode;
				obj.countryCode = res[i].countryCode;
				obj.employeeCodePrimary = res[i].employeeCodePrimary;
				obj.updateDateTime = res[i].updateDateTime;
				obj.workAttendanceDateTimeStart = res[i].workAttendanceDateTimeStart;
				obj.workAttendanceDeleted = res[i].workAttendanceDeleted;
				obj.workAttendanceId = res[i].workAttendanceId;
				obj.workAttendanceStatus = res[i].workAttendanceStatus;
				obj.workAttendanceTimeDuration = res[i].workAttendanceTimeDuration;
				obj.workId = res[i].workId;
				objMap[i] = obj;
			}
		}
		kony.sync.verifyAndCallClosure(successcallback, objMap);
	}
	
	com.pc.workloads.work.getAllDetailsByPK(pks, work_successcallback, errorcallback);
};

/************************************************************************************
* Retrieves number of instances of workAttendance related to work
* with given ${displayTargetAttribute} from local database.
*************************************************************************************/
com.pc.workloads.work.prototype.getCountOfworkAttendanceWithworkIdANDbusinessCodeANDcountryCode  = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.prototype.getCountOfworkAttendanceWithworkIdANDbusinessCodeANDcountryCode function");
	var pks = this.getPKTable();
	com.pc.workloads.work.getCountOfworkAttendanceWithworkIdANDbusinessCodeANDcountryCode(pks,successcallback,errorcallback);
};
com.pc.workloads.work.getCountOfworkAttendanceWithworkIdANDbusinessCodeANDcountryCode = function(pks,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getCountOfworkAttendanceWithworkIdANDbusinessCodeANDcountryCode function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.getCountOfworkAttendanceWithworkIdANDbusinessCodeANDcountryCode",  "relationship", errorcallback)){
		return;
	}
	function work_successcallback(res){
		if(null!==res && res.length>0) {
			var wcs = [];
				var targetAttributes = [];
													var targetKey_0 = res[0].workId;
					targetAttributes.push("workId");
					if(kony.type(targetKey_0)==="string") {
						wcs.push({"workId":"'"+targetKey_0+"'"});
					}else{
						wcs.push({"workId":targetKey_0});
					} 
														var targetKey_1 = res[0].businessCode;
					targetAttributes.push("businessCode");
					if(kony.type(targetKey_1)==="string") {
						wcs.push({"businessCode":"'"+targetKey_1+"'"});
					}else{
						wcs.push({"businessCode":targetKey_1});
					} 
														var targetKey_2 = res[0].countryCode;
					targetAttributes.push("countryCode");
					if(kony.type(targetKey_2)==="string") {
						wcs.push({"countryCode":"'"+targetKey_2+"'"});
					}else{
						wcs.push({"countryCode":targetKey_2});
					} 
														
			var wClause = "where ";
   			var i;
        	var len = wcs.length;
        	for (i = 0; i < len; i++) {
        		wClauseMap = wcs[i];
        		wClause += targetAttributes[i] + " = " + wClauseMap[targetAttributes[i]]
        		if(i != len-1)
        		{
            		 wClause += " AND "
        		}
    		}
		   com.pc.workloads.workAttendance.getCount(wClause, successcallback,errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(successcallback,{"count":0});
			return;
		}
	}
	
	com.pc.workloads.work.getAllDetailsByPK(pks, work_successcallback, errorcallback);
};
/************************************************************************************
* Retrieves instances of workOrder related to work
* with given $relationship.getTargetObjectAttribute() from local database.
*************************************************************************************/

															
com.pc.workloads.work.prototype.getworkOrderWithworkIdANDbusinessCodeANDcountryCode  = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.prototype.getworkOrderWithworkIdANDbusinessCodeANDcountryCode function");
	var pks = this.getPKTable();
	com.pc.workloads.work.getworkOrderWithworkIdANDbusinessCodeANDcountryCode(pks,successcallback,errorcallback);
};
com.pc.workloads.work.getworkOrderWithworkIdANDbusinessCodeANDcountryCode = function(pks,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getworkOrderWithworkIdANDbusinessCodeANDcountryCode function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}		
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.getworkOrderWithworkIdANDbusinessCodeANDcountryCode",  "relationship", errorcallback)){
		return;
	}	
	function work_successcallback(res){
		if(null!==res && res.length>0) {
			var wcs = [];
			var targetKey_0 = res[0].workId;				
			wcs.push({key:"workId", value:targetKey_0});		
						var targetKey_1 = res[0].businessCode;				
			wcs.push({key:"businessCode", value:targetKey_1});		
						var targetKey_2 = res[0].countryCode;				
			wcs.push({key:"countryCode", value:targetKey_2});		
			
			var tbname = "workOrder"
			var query = kony.sync.qb_createQuery();
			kony.sync.qb_select(query, null);
			kony.sync.qb_from(query, tbname);
			kony.sync.qb_where(query,wcs);
		
			var query_compile = kony.sync.qb_compile(query);
			var sql = query_compile[0];
			var params = query_compile[1];
			var dbname = kony.sync.getDBName();
		
			function mySuccCallback(res){
									kony.sync.verifyAndCallClosure(mySuccesscallback, com.pc.workloads.workOrder.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
							}
		
			kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(successcallback);
			return;
		}	
	}
	
	function mySuccesscallback(res){
		var objMap = [];
		if(res!==null){
			for(var i in res){
				var obj = new com.pc.workloads.workOrder();
				obj.businessCode = res[i].businessCode;
				obj.contractCode = res[i].contractCode;
				obj.countryCode = res[i].countryCode;
				obj.propertyCode = res[i].propertyCode;
				obj.propertyNumber = res[i].propertyNumber;
				obj.quantityPlanned = res[i].quantityPlanned;
				obj.sequenceNumber = res[i].sequenceNumber;
				obj.estimatedEffort = res[i].estimatedEffort;
				obj.serviceCoverNumber = res[i].serviceCoverNumber;
				obj.serviceLineCode = res[i].serviceLineCode;
				obj.updateDateTime = res[i].updateDateTime;
				obj.workId = res[i].workId;
				obj.workOrderDateStartPlanned = res[i].workOrderDateStartPlanned;
				obj.workOrderDeleted = res[i].workOrderDeleted;
				obj.workorderId = res[i].workorderId;
				obj.workOrderPlannerNotes = res[i].workOrderPlannerNotes;
				obj.workOrderStatus = res[i].workOrderStatus;
				obj.workOrderTypeCode = res[i].workOrderTypeCode;
				obj.workStatusLoc = res[i].workStatusLoc;
				obj.workLocOrder = res[i].workLocOrder;
				obj.isCompletedVisit = res[i].isCompletedVisit;
				obj.planVisitNumber = res[i].planVisitNumber;
				obj.issueTypeCode = res[i].issueTypeCode;
				obj.issueCode = res[i].issueCode;
				obj.quantityActual = res[i].quantityActual;
				obj.workOrderDateStartActual = res[i].workOrderDateStartActual;
				obj.workOrderVisitNotes = res[i].workOrderVisitNotes;
				obj.actualEffort = res[i].actualEffort;
				obj.csiVisitReference = res[i].csiVisitReference;
				obj.followUpDate = res[i].followUpDate;
				obj.followUpTime = res[i].followUpTime;
				obj.followUpDuration = res[i].followUpDuration;
				objMap[i] = obj;
			}
		}
		kony.sync.verifyAndCallClosure(successcallback, objMap);
	}
	
	com.pc.workloads.work.getAllDetailsByPK(pks, work_successcallback, errorcallback);
};

/************************************************************************************
* Retrieves number of instances of workOrder related to work
* with given ${displayTargetAttribute} from local database.
*************************************************************************************/
com.pc.workloads.work.prototype.getCountOfworkOrderWithworkIdANDbusinessCodeANDcountryCode  = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.prototype.getCountOfworkOrderWithworkIdANDbusinessCodeANDcountryCode function");
	var pks = this.getPKTable();
	com.pc.workloads.work.getCountOfworkOrderWithworkIdANDbusinessCodeANDcountryCode(pks,successcallback,errorcallback);
};
com.pc.workloads.work.getCountOfworkOrderWithworkIdANDbusinessCodeANDcountryCode = function(pks,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.getCountOfworkOrderWithworkIdANDbusinessCodeANDcountryCode function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.work.getCountOfworkOrderWithworkIdANDbusinessCodeANDcountryCode",  "relationship", errorcallback)){
		return;
	}
	function work_successcallback(res){
		if(null!==res && res.length>0) {
			var wcs = [];
				var targetAttributes = [];
													var targetKey_0 = res[0].workId;
					targetAttributes.push("workId");
					if(kony.type(targetKey_0)==="string") {
						wcs.push({"workId":"'"+targetKey_0+"'"});
					}else{
						wcs.push({"workId":targetKey_0});
					} 
														var targetKey_1 = res[0].businessCode;
					targetAttributes.push("businessCode");
					if(kony.type(targetKey_1)==="string") {
						wcs.push({"businessCode":"'"+targetKey_1+"'"});
					}else{
						wcs.push({"businessCode":targetKey_1});
					} 
														var targetKey_2 = res[0].countryCode;
					targetAttributes.push("countryCode");
					if(kony.type(targetKey_2)==="string") {
						wcs.push({"countryCode":"'"+targetKey_2+"'"});
					}else{
						wcs.push({"countryCode":targetKey_2});
					} 
														
			var wClause = "where ";
   			var i;
        	var len = wcs.length;
        	for (i = 0; i < len; i++) {
        		wClauseMap = wcs[i];
        		wClause += targetAttributes[i] + " = " + wClauseMap[targetAttributes[i]]
        		if(i != len-1)
        		{
            		 wClause += " AND "
        		}
    		}
		   com.pc.workloads.workOrder.getCount(wClause, successcallback,errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(successcallback,{"count":0});
			return;
		}
	}
	
	com.pc.workloads.work.getAllDetailsByPK(pks, work_successcallback, errorcallback);
};

/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.pc.workloads.work.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering com.pc.workloads.work.removeCascade function");
	var tbname = com.pc.workloads.work.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
		if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, wcs, com.pc.workloads.workAttendance.removeCascade, "workAttendance", true, errorcallback, markForUpload, null, isLocal)){
			return false;
		}
			var srcAttributes = [];
			var targetAttributes = [];
			srcAttributes.push("workId") ;
			targetAttributes.push("workId") ;
			srcAttributes.push("businessCode") ;
			targetAttributes.push("businessCode") ;
			srcAttributes.push("countryCode") ;
			targetAttributes.push("countryCode") ;
		if(!kony.sync.removeCascadeHelper(tx, srcAttributes, targetAttributes, tbname, wcs, com.pc.workloads.workOrder.removeCascade, "workOrder", true, errorcallback, markForUpload, null, isLocal)){
			return false;
		}
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


com.pc.workloads.work.convertTableToObject = function(res){
	sync.log.trace("Entering com.pc.workloads.work.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new com.pc.workloads.work();
			obj.businessCode = res[i].businessCode;
			obj.countryCode = res[i].countryCode;
			obj.employeeCodePrimary = res[i].employeeCodePrimary;
			obj.propertyCode = res[i].propertyCode;
			obj.updateDateTime = res[i].updateDateTime;
			obj.workDateStartPlanned = res[i].workDateStartPlanned;
			obj.workDeleted = res[i].workDeleted;
			obj.workId = res[i].workId;
			obj.workStatus = res[i].workStatus;
			obj.customerApproval = res[i].customerApproval;
			obj.customerApprovalName = res[i].customerApprovalName;
			obj.customerApprovalSignature = res[i].customerApprovalSignature;
			obj.issueCodeNoSignature = res[i].issueCodeNoSignature;
			obj.workDateStartActual = res[i].workDateStartActual;
			obj.issueTypeCode = res[i].issueTypeCode;
			obj.issueCode = res[i].issueCode;
			obj.issueTypeCodeNoSignature = res[i].issueTypeCodeNoSignature;
			obj.emailDistributionList = res[i].emailDistributionList;
			obj.serviceReceiptEmailSubject = res[i].serviceReceiptEmailSubject;
			obj.outcard = res[i].outcard;
			obj.outcardCode = res[i].outcardCode;
			obj.outcardPropertyName = res[i].outcardPropertyName;
			obj.outcardPropertyAddressLine1 = res[i].outcardPropertyAddressLine1;
			obj.outcardPropertyAddressLine2 = res[i].outcardPropertyAddressLine2;
			obj.outcardPropertyAddressLine3 = res[i].outcardPropertyAddressLine3;
			obj.outcardPropertyAddressLine4 = res[i].outcardPropertyAddressLine4;
			obj.outcardPropertyAddressLine5 = res[i].outcardPropertyAddressLine5;
			obj.outcardPropertyPostcode = res[i].outcardPropertyPostcode;
			obj.outcardPropertyContactName = res[i].outcardPropertyContactName;
			obj.outcardPropertyContactTelephone = res[i].outcardPropertyContactTelephone;
			obj.outcardOfficeRef = res[i].outcardOfficeRef;
			obj.capturedPhoto = res[i].capturedPhoto;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

com.pc.workloads.work.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering com.pc.workloads.work.filterAttributes function");
	var attributeTable = {};
	attributeTable.businessCode = "businessCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.employeeCodePrimary = "employeeCodePrimary";
	attributeTable.propertyCode = "propertyCode";
	attributeTable.workDateStartPlanned = "workDateStartPlanned";
	attributeTable.workId = "workId";
	attributeTable.workStatus = "workStatus";
	attributeTable.customerApproval = "customerApproval";
	attributeTable.customerApprovalName = "customerApprovalName";
	attributeTable.customerApprovalSignature = "customerApprovalSignature";
	attributeTable.issueCodeNoSignature = "issueCodeNoSignature";
	attributeTable.workDateStartActual = "workDateStartActual";
	attributeTable.issueTypeCode = "issueTypeCode";
	attributeTable.issueCode = "issueCode";
	attributeTable.issueTypeCodeNoSignature = "issueTypeCodeNoSignature";
	attributeTable.emailDistributionList = "emailDistributionList";
	attributeTable.serviceReceiptEmailSubject = "serviceReceiptEmailSubject";
	attributeTable.outcard = "outcard";
	attributeTable.outcardCode = "outcardCode";
	attributeTable.outcardPropertyName = "outcardPropertyName";
	attributeTable.outcardPropertyAddressLine1 = "outcardPropertyAddressLine1";
	attributeTable.outcardPropertyAddressLine2 = "outcardPropertyAddressLine2";
	attributeTable.outcardPropertyAddressLine3 = "outcardPropertyAddressLine3";
	attributeTable.outcardPropertyAddressLine4 = "outcardPropertyAddressLine4";
	attributeTable.outcardPropertyAddressLine5 = "outcardPropertyAddressLine5";
	attributeTable.outcardPropertyPostcode = "outcardPropertyPostcode";
	attributeTable.outcardPropertyContactName = "outcardPropertyContactName";
	attributeTable.outcardPropertyContactTelephone = "outcardPropertyContactTelephone";
	attributeTable.outcardOfficeRef = "outcardOfficeRef";
	attributeTable.capturedPhoto = "capturedPhoto";

	var PKTable = {};
	PKTable.workId = {}
	PKTable.workId.name = "workId";
	PKTable.workId.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject work. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject work. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject work. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

com.pc.workloads.work.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering com.pc.workloads.work.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = com.pc.workloads.work.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

com.pc.workloads.work.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering com.pc.workloads.work.prototype.getValuesTable function");
	var valuesTable = {};
	valuesTable.businessCode = this.businessCode;
	valuesTable.countryCode = this.countryCode;
	valuesTable.employeeCodePrimary = this.employeeCodePrimary;
	valuesTable.propertyCode = this.propertyCode;
	valuesTable.workDateStartPlanned = this.workDateStartPlanned;
	if(isInsert===true){
		valuesTable.workId = this.workId;
	}
	valuesTable.workStatus = this.workStatus;
	valuesTable.customerApproval = this.customerApproval;
	valuesTable.customerApprovalName = this.customerApprovalName;
	valuesTable.customerApprovalSignature = this.customerApprovalSignature;
	valuesTable.issueCodeNoSignature = this.issueCodeNoSignature;
	valuesTable.workDateStartActual = this.workDateStartActual;
	valuesTable.issueTypeCode = this.issueTypeCode;
	valuesTable.issueCode = this.issueCode;
	valuesTable.issueTypeCodeNoSignature = this.issueTypeCodeNoSignature;
	valuesTable.emailDistributionList = this.emailDistributionList;
	valuesTable.serviceReceiptEmailSubject = this.serviceReceiptEmailSubject;
	valuesTable.outcard = this.outcard;
	valuesTable.outcardCode = this.outcardCode;
	valuesTable.outcardPropertyName = this.outcardPropertyName;
	valuesTable.outcardPropertyAddressLine1 = this.outcardPropertyAddressLine1;
	valuesTable.outcardPropertyAddressLine2 = this.outcardPropertyAddressLine2;
	valuesTable.outcardPropertyAddressLine3 = this.outcardPropertyAddressLine3;
	valuesTable.outcardPropertyAddressLine4 = this.outcardPropertyAddressLine4;
	valuesTable.outcardPropertyAddressLine5 = this.outcardPropertyAddressLine5;
	valuesTable.outcardPropertyPostcode = this.outcardPropertyPostcode;
	valuesTable.outcardPropertyContactName = this.outcardPropertyContactName;
	valuesTable.outcardPropertyContactTelephone = this.outcardPropertyContactTelephone;
	valuesTable.outcardOfficeRef = this.outcardOfficeRef;
	valuesTable.capturedPhoto = this.capturedPhoto;
	return valuesTable;
};

com.pc.workloads.work.prototype.getPKTable = function(){
	sync.log.trace("Entering com.pc.workloads.work.prototype.getPKTable function");
	var pkTable = {};
	pkTable.workId = {key:"workId",value:this.workId};
	return pkTable;
};

com.pc.workloads.work.getPKTable = function(){
	sync.log.trace("Entering com.pc.workloads.work.getPKTable function");
	var pkTable = [];
	pkTable.push("workId");
	return pkTable;
};

com.pc.workloads.work.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering com.pc.workloads.work.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key workId not specified in  " + opName + "  an item in work");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("workId",opName,"work")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.workId)){
			if(!kony.sync.isNull(pks.workId.value)){
				wc.key = "workId";
				wc.value = pks.workId.value;
			}
			else{
				wc.key = "workId";
				wc.value = pks.workId;
			}
		}else{
			sync.log.error("Primary Key workId not specified in  " + opName + "  an item in work");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("workId",opName,"work")));
			return false;
		}
	}
	else{
		wc.key = "workId";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

com.pc.workloads.work.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.validateNull function");
	return true;
};

com.pc.workloads.work.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.workloads.work.validateNullInsert function");
	return true;
};

com.pc.workloads.work.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering com.pc.workloads.work.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


com.pc.workloads.work.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

com.pc.workloads.work.getTableName = function(){
	return "work";
};




// **********************************End work's helper methods************************