//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:04 UTC 2016workOrder*******************
// **********************************Start workOrder's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(com)=== "undefined"){ com = {}; }
if(typeof(com.pc)=== "undefined"){ com.pc = {}; }
if(typeof(com.pc.workloads)=== "undefined"){ com.pc.workloads = {}; }

/************************************************************************************
* Creates new workOrder
*************************************************************************************/
com.pc.workloads.workOrder = function(){
	this.businessCode = null;
	this.contractCode = null;
	this.countryCode = null;
	this.propertyCode = null;
	this.propertyNumber = null;
	this.quantityPlanned = null;
	this.sequenceNumber = null;
	this.estimatedEffort = null;
	this.serviceCoverNumber = null;
	this.serviceLineCode = null;
	this.updateDateTime = null;
	this.workId = null;
	this.workOrderDateStartPlanned = null;
	this.workOrderDeleted = null;
	this.workorderId = null;
	this.workOrderPlannerNotes = null;
	this.workOrderStatus = null;
	this.workOrderTypeCode = null;
	this.workStatusLoc = null;
	this.workLocOrder = null;
	this.isCompletedVisit = null;
	this.planVisitNumber = null;
	this.issueTypeCode = null;
	this.issueCode = null;
	this.quantityActual = null;
	this.workOrderDateStartActual = null;
	this.workOrderVisitNotes = null;
	this.actualEffort = null;
	this.csiVisitReference = null;
	this.followUpDate = null;
	this.followUpTime = null;
	this.followUpDuration = null;
	this.markForUpload = true;
};

com.pc.workloads.workOrder.prototype = {
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get contractCode(){
		return this._contractCode;
	},
	set contractCode(val){
		this._contractCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get propertyCode(){
		return this._propertyCode;
	},
	set propertyCode(val){
		this._propertyCode = val;
	},
	get propertyNumber(){
		return this._propertyNumber;
	},
	set propertyNumber(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute propertyNumber in workOrder.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._propertyNumber = val;
	},
	get quantityPlanned(){
		return this._quantityPlanned;
	},
	set quantityPlanned(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute quantityPlanned in workOrder.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._quantityPlanned = val;
	},
	get sequenceNumber(){
		return this._sequenceNumber;
	},
	set sequenceNumber(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute sequenceNumber in workOrder.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._sequenceNumber = val;
	},
	get estimatedEffort(){
		return this._estimatedEffort;
	},
	set estimatedEffort(val){
		this._estimatedEffort = val;
	},
	get serviceCoverNumber(){
		return this._serviceCoverNumber;
	},
	set serviceCoverNumber(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute serviceCoverNumber in workOrder.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._serviceCoverNumber = val;
	},
	get serviceLineCode(){
		return this._serviceLineCode;
	},
	set serviceLineCode(val){
		this._serviceLineCode = val;
	},
	get updateDateTime(){
		return this._updateDateTime;
	},
	set updateDateTime(val){
		this._updateDateTime = val;
	},
	get workId(){
		return this._workId;
	},
	set workId(val){
		this._workId = val;
	},
	get workOrderDateStartPlanned(){
		return this._workOrderDateStartPlanned;
	},
	set workOrderDateStartPlanned(val){
		this._workOrderDateStartPlanned = val;
	},
	get workOrderDeleted(){
		return kony.sync.getBoolean(this._workOrderDeleted)+"";
	},
	set workOrderDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute workOrderDeleted in workOrder.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._workOrderDeleted = val;
	},
	get workorderId(){
		return this._workorderId;
	},
	set workorderId(val){
		this._workorderId = val;
	},
	get workOrderPlannerNotes(){
		return this._workOrderPlannerNotes;
	},
	set workOrderPlannerNotes(val){
		this._workOrderPlannerNotes = val;
	},
	get workOrderStatus(){
		return this._workOrderStatus;
	},
	set workOrderStatus(val){
		this._workOrderStatus = val;
	},
	get workOrderTypeCode(){
		return this._workOrderTypeCode;
	},
	set workOrderTypeCode(val){
		this._workOrderTypeCode = val;
	},
	get workStatusLoc(){
		return this._workStatusLoc;
	},
	set workStatusLoc(val){
		this._workStatusLoc = val;
	},
	get workLocOrder(){
		return this._workLocOrder;
	},
	set workLocOrder(val){
		this._workLocOrder = val;
	},
	get isCompletedVisit(){
		return kony.sync.getBoolean(this._isCompletedVisit)+"";
	},
	set isCompletedVisit(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isCompletedVisit in workOrder.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isCompletedVisit = val;
	},
	get planVisitNumber(){
		return this._planVisitNumber;
	},
	set planVisitNumber(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute planVisitNumber in workOrder.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._planVisitNumber = val;
	},
	get issueTypeCode(){
		return this._issueTypeCode;
	},
	set issueTypeCode(val){
		this._issueTypeCode = val;
	},
	get issueCode(){
		return this._issueCode;
	},
	set issueCode(val){
		this._issueCode = val;
	},
	get quantityActual(){
		return this._quantityActual;
	},
	set quantityActual(val){
		this._quantityActual = val;
	},
	get workOrderDateStartActual(){
		return this._workOrderDateStartActual;
	},
	set workOrderDateStartActual(val){
		this._workOrderDateStartActual = val;
	},
	get workOrderVisitNotes(){
		return this._workOrderVisitNotes;
	},
	set workOrderVisitNotes(val){
		this._workOrderVisitNotes = val;
	},
	get actualEffort(){
		return this._actualEffort;
	},
	set actualEffort(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute actualEffort in workOrder.\nExpected:\"int\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._actualEffort = val;
	},
	get csiVisitReference(){
		return this._csiVisitReference;
	},
	set csiVisitReference(val){
		this._csiVisitReference = val;
	},
	get followUpDate(){
		return this._followUpDate;
	},
	set followUpDate(val){
		this._followUpDate = val;
	},
	get followUpTime(){
		return this._followUpTime;
	},
	set followUpTime(val){
		this._followUpTime = val;
	},
	get followUpDuration(){
		return this._followUpDuration;
	},
	set followUpDuration(val){
		this._followUpDuration = val;
	},
};

/************************************************************************************
* Retrieves all instances of workOrder SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "businessCode";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "contractCode";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* com.pc.workloads.workOrder.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
com.pc.workloads.workOrder.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering com.pc.workloads.workOrder.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	orderByMap = kony.sync.formOrderByClause("workOrder",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.workloads.workOrder.getAll->successcallback");
		successcallback(com.pc.workloads.workOrder.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of workOrder present in local database.
*************************************************************************************/
com.pc.workloads.workOrder.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.getAllCount function");
	com.pc.workloads.workOrder.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of workOrder using where clause in the local Database
*************************************************************************************/
com.pc.workloads.workOrder.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering com.pc.workloads.workOrder.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of workOrder in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.workOrder.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.workloads.workOrder.prototype.create function");
	var valuestable = this.getValuesTable(true);
	com.pc.workloads.workOrder.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.workloads.workOrder.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  com.pc.workloads.workOrder.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"workOrder",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.workloads.workOrder.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = com.pc.workloads.workOrder.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "workorderId=" + valuestable.workorderId;
		pks["workorderId"] = {key:"workorderId",value:valuestable.workorderId};
		com.pc.workloads.workOrder.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of workOrder in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].contractCode = "contractCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].propertyCode = "propertyCode_0";
*		valuesArray[0].propertyNumber = 0;
*		valuesArray[0].quantityPlanned = 0;
*		valuesArray[0].sequenceNumber = 0;
*		valuesArray[0].estimatedEffort = "estimatedEffort_0";
*		valuesArray[0].serviceCoverNumber = 0;
*		valuesArray[0].serviceLineCode = "serviceLineCode_0";
*		valuesArray[0].workId = "workId_0";
*		valuesArray[0].workOrderDateStartPlanned = "workOrderDateStartPlanned_0";
*		valuesArray[0].workorderId = "workorderId_0";
*		valuesArray[0].workOrderPlannerNotes = "workOrderPlannerNotes_0";
*		valuesArray[0].workOrderStatus = "workOrderStatus_0";
*		valuesArray[0].workOrderTypeCode = "workOrderTypeCode_0";
*		valuesArray[0].workStatusLoc = "workStatusLoc_0";
*		valuesArray[0].workLocOrder = "workLocOrder_0";
*		valuesArray[0].isCompletedVisit = true;
*		valuesArray[0].planVisitNumber = 0;
*		valuesArray[0].issueTypeCode = "issueTypeCode_0";
*		valuesArray[0].issueCode = "issueCode_0";
*		valuesArray[0].quantityActual = "quantityActual_0";
*		valuesArray[0].workOrderDateStartActual = "workOrderDateStartActual_0";
*		valuesArray[0].workOrderVisitNotes = "workOrderVisitNotes_0";
*		valuesArray[0].actualEffort = 0;
*		valuesArray[0].csiVisitReference = "csiVisitReference_0";
*		valuesArray[0].followUpDate = "followUpDate_0";
*		valuesArray[0].followUpTime = "followUpTime_0";
*		valuesArray[0].followUpDuration = "followUpDuration_0";
*		valuesArray[1] = {};
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].contractCode = "contractCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].propertyCode = "propertyCode_1";
*		valuesArray[1].propertyNumber = 1;
*		valuesArray[1].quantityPlanned = 1;
*		valuesArray[1].sequenceNumber = 1;
*		valuesArray[1].estimatedEffort = "estimatedEffort_1";
*		valuesArray[1].serviceCoverNumber = 1;
*		valuesArray[1].serviceLineCode = "serviceLineCode_1";
*		valuesArray[1].workId = "workId_1";
*		valuesArray[1].workOrderDateStartPlanned = "workOrderDateStartPlanned_1";
*		valuesArray[1].workorderId = "workorderId_1";
*		valuesArray[1].workOrderPlannerNotes = "workOrderPlannerNotes_1";
*		valuesArray[1].workOrderStatus = "workOrderStatus_1";
*		valuesArray[1].workOrderTypeCode = "workOrderTypeCode_1";
*		valuesArray[1].workStatusLoc = "workStatusLoc_1";
*		valuesArray[1].workLocOrder = "workLocOrder_1";
*		valuesArray[1].isCompletedVisit = true;
*		valuesArray[1].planVisitNumber = 1;
*		valuesArray[1].issueTypeCode = "issueTypeCode_1";
*		valuesArray[1].issueCode = "issueCode_1";
*		valuesArray[1].quantityActual = "quantityActual_1";
*		valuesArray[1].workOrderDateStartActual = "workOrderDateStartActual_1";
*		valuesArray[1].workOrderVisitNotes = "workOrderVisitNotes_1";
*		valuesArray[1].actualEffort = 1;
*		valuesArray[1].csiVisitReference = "csiVisitReference_1";
*		valuesArray[1].followUpDate = "followUpDate_1";
*		valuesArray[1].followUpTime = "followUpTime_1";
*		valuesArray[1].followUpDuration = "followUpDuration_1";
*		valuesArray[2] = {};
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].contractCode = "contractCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].propertyCode = "propertyCode_2";
*		valuesArray[2].propertyNumber = 2;
*		valuesArray[2].quantityPlanned = 2;
*		valuesArray[2].sequenceNumber = 2;
*		valuesArray[2].estimatedEffort = "estimatedEffort_2";
*		valuesArray[2].serviceCoverNumber = 2;
*		valuesArray[2].serviceLineCode = "serviceLineCode_2";
*		valuesArray[2].workId = "workId_2";
*		valuesArray[2].workOrderDateStartPlanned = "workOrderDateStartPlanned_2";
*		valuesArray[2].workorderId = "workorderId_2";
*		valuesArray[2].workOrderPlannerNotes = "workOrderPlannerNotes_2";
*		valuesArray[2].workOrderStatus = "workOrderStatus_2";
*		valuesArray[2].workOrderTypeCode = "workOrderTypeCode_2";
*		valuesArray[2].workStatusLoc = "workStatusLoc_2";
*		valuesArray[2].workLocOrder = "workLocOrder_2";
*		valuesArray[2].isCompletedVisit = true;
*		valuesArray[2].planVisitNumber = 2;
*		valuesArray[2].issueTypeCode = "issueTypeCode_2";
*		valuesArray[2].issueCode = "issueCode_2";
*		valuesArray[2].quantityActual = "quantityActual_2";
*		valuesArray[2].workOrderDateStartActual = "workOrderDateStartActual_2";
*		valuesArray[2].workOrderVisitNotes = "workOrderVisitNotes_2";
*		valuesArray[2].actualEffort = 2;
*		valuesArray[2].csiVisitReference = "csiVisitReference_2";
*		valuesArray[2].followUpDate = "followUpDate_2";
*		valuesArray[2].followUpTime = "followUpTime_2";
*		valuesArray[2].followUpDuration = "followUpDuration_2";
*		com.pc.workloads.workOrder.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
com.pc.workloads.workOrder.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.workloads.workOrder.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"workOrder",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "workorderId=" + valuestable.workorderId;
				pks["workorderId"] = {key:"workorderId",value:valuestable.workorderId};
				var wcs = [];
				if(com.pc.workloads.workOrder.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.workloads.workOrder.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.workloads.workOrder.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = com.pc.workloads.workOrder.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates workOrder using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.workOrder.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.workloads.workOrder.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	com.pc.workloads.workOrder.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
com.pc.workloads.workOrder.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  com.pc.workloads.workOrder.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(com.pc.workloads.workOrder.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"workOrder",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = com.pc.workloads.workOrder.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates workOrder(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.workOrder.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering com.pc.workloads.workOrder.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"workOrder",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  com.pc.workloads.workOrder.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.workloads.workOrder.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = com.pc.workloads.workOrder.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.pc.workloads.workOrder.getPKTable());
	}
};

/************************************************************************************
* Updates workOrder(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.businessCode = "businessCode_updated0";
*		inputArray[0].changeSet.contractCode = "contractCode_updated0";
*		inputArray[0].changeSet.countryCode = "countryCode_updated0";
*		inputArray[0].changeSet.propertyCode = "propertyCode_updated0";
*		inputArray[0].whereClause = "where workorderId = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.businessCode = "businessCode_updated1";
*		inputArray[1].changeSet.contractCode = "contractCode_updated1";
*		inputArray[1].changeSet.countryCode = "countryCode_updated1";
*		inputArray[1].changeSet.propertyCode = "propertyCode_updated1";
*		inputArray[1].whereClause = "where workorderId = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.businessCode = "businessCode_updated2";
*		inputArray[2].changeSet.contractCode = "contractCode_updated2";
*		inputArray[2].changeSet.countryCode = "countryCode_updated2";
*		inputArray[2].changeSet.propertyCode = "propertyCode_updated2";
*		inputArray[2].whereClause = "where workorderId = '2'";
*		com.pc.workloads.workOrder.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
com.pc.workloads.workOrder.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering com.pc.workloads.workOrder.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898512bd8be";
	var tbname = "workOrder";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"workOrder",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, com.pc.workloads.workOrder.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  com.pc.workloads.workOrder.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, com.pc.workloads.workOrder.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.pc.workloads.workOrder.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = com.pc.workloads.workOrder.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes workOrder using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.workOrder.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.prototype.deleteByPK function");
	var pks = this.getPKTable();
	com.pc.workloads.workOrder.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
com.pc.workloads.workOrder.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.workloads.workOrder.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(com.pc.workloads.workOrder.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function workOrderTransactionCallback(tx){
		sync.log.trace("Entering com.pc.workloads.workOrder.deleteByPK->workOrder_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function workOrderErrorCallback(){
		sync.log.error("Entering com.pc.workloads.workOrder.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function workOrderSuccessCallback(){
		sync.log.trace("Entering com.pc.workloads.workOrder.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.workloads.workOrder.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, workOrderTransactionCallback, workOrderSuccessCallback, workOrderErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes workOrder(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. com.pc.workloads.workOrder.remove("where businessCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
com.pc.workloads.workOrder.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.pc.workloads.workOrder.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function workOrder_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function workOrder_removeSuccess(){
		sync.log.trace("Entering com.pc.workloads.workOrder.remove->workOrder_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.workloads.workOrder.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.workloads.workOrder.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, workOrder_removeTransactioncallback, workOrder_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes workOrder using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.workloads.workOrder.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	com.pc.workloads.workOrder.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
com.pc.workloads.workOrder.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(com.pc.workloads.workOrder.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function workOrderTransactionCallback(tx){
		sync.log.trace("Entering com.pc.workloads.workOrder.removeDeviceInstanceByPK -> workOrderTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function workOrderErrorCallback(){
		sync.log.error("Entering com.pc.workloads.workOrder.removeDeviceInstanceByPK -> workOrderErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function workOrderSuccessCallback(){
		sync.log.trace("Entering com.pc.workloads.workOrder.removeDeviceInstanceByPK -> workOrderSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.workloads.workOrder.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, workOrderTransactionCallback, workOrderSuccessCallback, workOrderErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes workOrder(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.pc.workloads.workOrder.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function workOrder_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function workOrder_removeSuccess(){
		sync.log.trace("Entering com.pc.workloads.workOrder.remove->workOrder_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.pc.workloads.workOrder.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.pc.workloads.workOrder.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, workOrder_removeTransactioncallback, workOrder_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves workOrder using primary key from the local Database. 
*************************************************************************************/
com.pc.workloads.workOrder.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	com.pc.workloads.workOrder.getAllDetailsByPK(pks,successcallback,errorcallback);
};
com.pc.workloads.workOrder.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var wcs = [];
	if(com.pc.workloads.workOrder.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering com.pc.workloads.workOrder.getAllDetailsByPK-> success callback function");
		successcallback(com.pc.workloads.workOrder.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves workOrder(s) using where clause from the local Database. 
* e.g. com.pc.workloads.workOrder.find("where businessCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
com.pc.workloads.workOrder.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.workOrder.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of workOrder with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.workOrder.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	com.pc.workloads.workOrder.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.pc.workloads.workOrder.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(com.pc.workloads.workOrder.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of workOrder matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.pc.workloads.workOrder.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering com.pc.workloads.workOrder.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering com.pc.workloads.workOrder.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering com.pc.workloads.workOrder.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of workOrder pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
com.pc.workloads.workOrder.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.workOrder.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.workOrder.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of workOrder pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
com.pc.workloads.workOrder.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.workOrder.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.workOrder.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of workOrder deferred for upload.
*************************************************************************************/
com.pc.workloads.workOrder.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.workOrder.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.pc.workloads.workOrder.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to workOrder in local database to last synced state
*************************************************************************************/
com.pc.workloads.workOrder.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.workOrder.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to workOrder's record with given primary key in local 
* database to last synced state
*************************************************************************************/
com.pc.workloads.workOrder.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	com.pc.workloads.workOrder.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
com.pc.workloads.workOrder.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var wcs = [];
	if(com.pc.workloads.workOrder.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.workOrder.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.pc.workloads.workOrder.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether workOrder's record  
* with given primary key got deferred in last sync
*************************************************************************************/
com.pc.workloads.workOrder.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.workloads.workOrder.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	com.pc.workloads.workOrder.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
com.pc.workloads.workOrder.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.workloads.workOrder.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.workOrder.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether workOrder's record  
* with given primary key is pending for upload
*************************************************************************************/
com.pc.workloads.workOrder.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.pc.workloads.workOrder.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	com.pc.workloads.workOrder.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
com.pc.workloads.workOrder.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.pc.workloads.workOrder.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.pc.workloads.workOrder.getTableName();
	var wcs = [] ;
	var flag;
	if(com.pc.workloads.workOrder.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.pc.workloads.workOrder.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.pc.workloads.workOrder.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering com.pc.workloads.workOrder.removeCascade function");
	var tbname = com.pc.workloads.workOrder.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


com.pc.workloads.workOrder.convertTableToObject = function(res){
	sync.log.trace("Entering com.pc.workloads.workOrder.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new com.pc.workloads.workOrder();
			obj.businessCode = res[i].businessCode;
			obj.contractCode = res[i].contractCode;
			obj.countryCode = res[i].countryCode;
			obj.propertyCode = res[i].propertyCode;
			obj.propertyNumber = res[i].propertyNumber;
			obj.quantityPlanned = res[i].quantityPlanned;
			obj.sequenceNumber = res[i].sequenceNumber;
			obj.estimatedEffort = res[i].estimatedEffort;
			obj.serviceCoverNumber = res[i].serviceCoverNumber;
			obj.serviceLineCode = res[i].serviceLineCode;
			obj.updateDateTime = res[i].updateDateTime;
			obj.workId = res[i].workId;
			obj.workOrderDateStartPlanned = res[i].workOrderDateStartPlanned;
			obj.workOrderDeleted = res[i].workOrderDeleted;
			obj.workorderId = res[i].workorderId;
			obj.workOrderPlannerNotes = res[i].workOrderPlannerNotes;
			obj.workOrderStatus = res[i].workOrderStatus;
			obj.workOrderTypeCode = res[i].workOrderTypeCode;
			obj.workStatusLoc = res[i].workStatusLoc;
			obj.workLocOrder = res[i].workLocOrder;
			obj.isCompletedVisit = res[i].isCompletedVisit;
			obj.planVisitNumber = res[i].planVisitNumber;
			obj.issueTypeCode = res[i].issueTypeCode;
			obj.issueCode = res[i].issueCode;
			obj.quantityActual = res[i].quantityActual;
			obj.workOrderDateStartActual = res[i].workOrderDateStartActual;
			obj.workOrderVisitNotes = res[i].workOrderVisitNotes;
			obj.actualEffort = res[i].actualEffort;
			obj.csiVisitReference = res[i].csiVisitReference;
			obj.followUpDate = res[i].followUpDate;
			obj.followUpTime = res[i].followUpTime;
			obj.followUpDuration = res[i].followUpDuration;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

com.pc.workloads.workOrder.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering com.pc.workloads.workOrder.filterAttributes function");
	var attributeTable = {};
	attributeTable.businessCode = "businessCode";
	attributeTable.contractCode = "contractCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.propertyCode = "propertyCode";
	attributeTable.propertyNumber = "propertyNumber";
	attributeTable.quantityPlanned = "quantityPlanned";
	attributeTable.sequenceNumber = "sequenceNumber";
	attributeTable.estimatedEffort = "estimatedEffort";
	attributeTable.serviceCoverNumber = "serviceCoverNumber";
	attributeTable.serviceLineCode = "serviceLineCode";
	attributeTable.workId = "workId";
	attributeTable.workOrderDateStartPlanned = "workOrderDateStartPlanned";
	attributeTable.workorderId = "workorderId";
	attributeTable.workOrderPlannerNotes = "workOrderPlannerNotes";
	attributeTable.workOrderStatus = "workOrderStatus";
	attributeTable.workOrderTypeCode = "workOrderTypeCode";
	attributeTable.workStatusLoc = "workStatusLoc";
	attributeTable.workLocOrder = "workLocOrder";
	attributeTable.isCompletedVisit = "isCompletedVisit";
	attributeTable.planVisitNumber = "planVisitNumber";
	attributeTable.issueTypeCode = "issueTypeCode";
	attributeTable.issueCode = "issueCode";
	attributeTable.quantityActual = "quantityActual";
	attributeTable.workOrderDateStartActual = "workOrderDateStartActual";
	attributeTable.workOrderVisitNotes = "workOrderVisitNotes";
	attributeTable.actualEffort = "actualEffort";
	attributeTable.csiVisitReference = "csiVisitReference";
	attributeTable.followUpDate = "followUpDate";
	attributeTable.followUpTime = "followUpTime";
	attributeTable.followUpDuration = "followUpDuration";

	var PKTable = {};
	PKTable.workorderId = {}
	PKTable.workorderId.name = "workorderId";
	PKTable.workorderId.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject workOrder. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject workOrder. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject workOrder. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

com.pc.workloads.workOrder.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering com.pc.workloads.workOrder.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = com.pc.workloads.workOrder.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

com.pc.workloads.workOrder.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering com.pc.workloads.workOrder.prototype.getValuesTable function");
	var valuesTable = {};
	valuesTable.businessCode = this.businessCode;
	valuesTable.contractCode = this.contractCode;
	valuesTable.countryCode = this.countryCode;
	valuesTable.propertyCode = this.propertyCode;
	valuesTable.propertyNumber = this.propertyNumber;
	valuesTable.quantityPlanned = this.quantityPlanned;
	valuesTable.sequenceNumber = this.sequenceNumber;
	valuesTable.estimatedEffort = this.estimatedEffort;
	valuesTable.serviceCoverNumber = this.serviceCoverNumber;
	valuesTable.serviceLineCode = this.serviceLineCode;
	valuesTable.workId = this.workId;
	valuesTable.workOrderDateStartPlanned = this.workOrderDateStartPlanned;
	if(isInsert===true){
		valuesTable.workorderId = this.workorderId;
	}
	valuesTable.workOrderPlannerNotes = this.workOrderPlannerNotes;
	valuesTable.workOrderStatus = this.workOrderStatus;
	valuesTable.workOrderTypeCode = this.workOrderTypeCode;
	valuesTable.workStatusLoc = this.workStatusLoc;
	valuesTable.workLocOrder = this.workLocOrder;
	valuesTable.isCompletedVisit = this.isCompletedVisit;
	valuesTable.planVisitNumber = this.planVisitNumber;
	valuesTable.issueTypeCode = this.issueTypeCode;
	valuesTable.issueCode = this.issueCode;
	valuesTable.quantityActual = this.quantityActual;
	valuesTable.workOrderDateStartActual = this.workOrderDateStartActual;
	valuesTable.workOrderVisitNotes = this.workOrderVisitNotes;
	valuesTable.actualEffort = this.actualEffort;
	valuesTable.csiVisitReference = this.csiVisitReference;
	valuesTable.followUpDate = this.followUpDate;
	valuesTable.followUpTime = this.followUpTime;
	valuesTable.followUpDuration = this.followUpDuration;
	return valuesTable;
};

com.pc.workloads.workOrder.prototype.getPKTable = function(){
	sync.log.trace("Entering com.pc.workloads.workOrder.prototype.getPKTable function");
	var pkTable = {};
	pkTable.workorderId = {key:"workorderId",value:this.workorderId};
	return pkTable;
};

com.pc.workloads.workOrder.getPKTable = function(){
	sync.log.trace("Entering com.pc.workloads.workOrder.getPKTable function");
	var pkTable = [];
	pkTable.push("workorderId");
	return pkTable;
};

com.pc.workloads.workOrder.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering com.pc.workloads.workOrder.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key workorderId not specified in  " + opName + "  an item in workOrder");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("workorderId",opName,"workOrder")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.workorderId)){
			if(!kony.sync.isNull(pks.workorderId.value)){
				wc.key = "workorderId";
				wc.value = pks.workorderId.value;
			}
			else{
				wc.key = "workorderId";
				wc.value = pks.workorderId;
			}
		}else{
			sync.log.error("Primary Key workorderId not specified in  " + opName + "  an item in workOrder");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("workorderId",opName,"workOrder")));
			return false;
		}
	}
	else{
		wc.key = "workorderId";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

com.pc.workloads.workOrder.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.validateNull function");
	return true;
};

com.pc.workloads.workOrder.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering com.pc.workloads.workOrder.validateNullInsert function");
	return true;
};

com.pc.workloads.workOrder.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering com.pc.workloads.workOrder.getRelationshipMap function");
	var r1 = {};
	r1 = {};
	r1.sourceAttribute = [];
	r1.foreignKeyAttribute = [];
	r1.targetAttributeValue  = [];
		
	if (!kony.sync.isNullOrUndefined(valuestable.workId)){
		r1.sourceAttribute.push("workId");
		r1.foreignKeyAttribute.push("workId");
		r1.targetAttributeValue.push("'" + valuestable.workId + "'");
	}
	if (!kony.sync.isNullOrUndefined(valuestable.businessCode)){
		r1.sourceAttribute.push("businessCode");
		r1.foreignKeyAttribute.push("businessCode");
		r1.targetAttributeValue.push("'" + valuestable.businessCode + "'");
	}
	if (!kony.sync.isNullOrUndefined(valuestable.countryCode)){
		r1.sourceAttribute.push("countryCode");
		r1.foreignKeyAttribute.push("countryCode");
		r1.targetAttributeValue.push("'" + valuestable.countryCode + "'");
	}
	if(r1.targetAttributeValue.length > 0){
		if(relationshipMap.work===undefined){
			relationshipMap.work = [];
		}
		relationshipMap.work.push(r1);
	}
	

	return relationshipMap;
};


com.pc.workloads.workOrder.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

com.pc.workloads.workOrder.getTableName = function(){
	return "workOrder";
};




// **********************************End workOrder's helper methods************************