//****************Sync Version:MobileFabricInstaller-DEV-7.2.1_v201611220827_r47*******************
// ****************Generated On Thu Dec 15 11:54:01 UTC 2016workOrderType*******************
// **********************************Start workOrderType's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}




/************************************************************************************
* Creates new workOrderType
*************************************************************************************/
workOrderType = function(){
	this.isInstallation = null;
	this.isFollowUp = null;
	this.updateStateOfService = null;
	this.isCallOut = null;
	this.businessCode = null;
	this.countryCode = null;
	this.WorkOrderTypeDescription = null;
	this.WorkOrderTypeCode = null;
	this.updateDateTime = null;
	this.isRemoval = null;
	this.markForUpload = true;
};

workOrderType.prototype = {
	get isInstallation(){
		return kony.sync.getBoolean(this._isInstallation)+"";
	},
	set isInstallation(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isInstallation in workOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isInstallation = val;
	},
	get isFollowUp(){
		return kony.sync.getBoolean(this._isFollowUp)+"";
	},
	set isFollowUp(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isFollowUp in workOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isFollowUp = val;
	},
	get updateStateOfService(){
		return kony.sync.getBoolean(this._updateStateOfService)+"";
	},
	set updateStateOfService(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute updateStateOfService in workOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._updateStateOfService = val;
	},
	get isCallOut(){
		return kony.sync.getBoolean(this._isCallOut)+"";
	},
	set isCallOut(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isCallOut in workOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isCallOut = val;
	},
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get WorkOrderTypeDescription(){
		return this._WorkOrderTypeDescription;
	},
	set WorkOrderTypeDescription(val){
		this._WorkOrderTypeDescription = val;
	},
	get WorkOrderTypeCode(){
		return this._WorkOrderTypeCode;
	},
	set WorkOrderTypeCode(val){
		this._WorkOrderTypeCode = val;
	},
	get updateDateTime(){
		return this._updateDateTime;
	},
	set updateDateTime(val){
		this._updateDateTime = val;
	},
	get isRemoval(){
		return kony.sync.getBoolean(this._isRemoval)+"";
	},
	set isRemoval(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isRemoval in workOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isRemoval = val;
	},
};

/************************************************************************************
* Retrieves all instances of workOrderType SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "isInstallation";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "isFollowUp";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* workOrderType.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
workOrderType.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering workOrderType.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	orderByMap = kony.sync.formOrderByClause("workOrderType",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering workOrderType.getAll->successcallback");
		successcallback(workOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of workOrderType present in local database.
*************************************************************************************/
workOrderType.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.getAllCount function");
	workOrderType.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of workOrderType using where clause in the local Database
*************************************************************************************/
workOrderType.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering workOrderType.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of workOrderType in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
workOrderType.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  workOrderType.prototype.create function");
	var valuestable = this.getValuesTable(true);
	workOrderType.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
workOrderType.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  workOrderType.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "workOrderType.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"workOrderType",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  workOrderType.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = workOrderType.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "businessCode=" + valuestable.businessCode;
		pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
		errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
		pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
		errMsg = errMsg + ", WorkOrderTypeCode=" + valuestable.WorkOrderTypeCode;
		pks["WorkOrderTypeCode"] = {key:"WorkOrderTypeCode",value:valuestable.WorkOrderTypeCode};
		workOrderType.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of workOrderType in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].isInstallation = true;
*		valuesArray[0].isFollowUp = true;
*		valuesArray[0].updateStateOfService = true;
*		valuesArray[0].isCallOut = true;
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].WorkOrderTypeDescription = "WorkOrderTypeDescription_0";
*		valuesArray[0].WorkOrderTypeCode = "WorkOrderTypeCode_0";
*		valuesArray[1] = {};
*		valuesArray[1].isInstallation = true;
*		valuesArray[1].isFollowUp = true;
*		valuesArray[1].updateStateOfService = true;
*		valuesArray[1].isCallOut = true;
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].WorkOrderTypeDescription = "WorkOrderTypeDescription_1";
*		valuesArray[1].WorkOrderTypeCode = "WorkOrderTypeCode_1";
*		valuesArray[2] = {};
*		valuesArray[2].isInstallation = true;
*		valuesArray[2].isFollowUp = true;
*		valuesArray[2].updateStateOfService = true;
*		valuesArray[2].isCallOut = true;
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].WorkOrderTypeDescription = "WorkOrderTypeDescription_2";
*		valuesArray[2].WorkOrderTypeCode = "WorkOrderTypeCode_2";
*		workOrderType.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
workOrderType.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering workOrderType.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"workOrderType",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "businessCode=" + valuestable.businessCode;
				pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
				errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
				pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
				errMsg = errMsg + ", WorkOrderTypeCode=" + valuestable.WorkOrderTypeCode;
				pks["WorkOrderTypeCode"] = {key:"WorkOrderTypeCode",value:valuestable.WorkOrderTypeCode};
				var wcs = [];
				if(workOrderType.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  workOrderType.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  workOrderType.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = workOrderType.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates workOrderType using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
workOrderType.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  workOrderType.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	workOrderType.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
workOrderType.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  workOrderType.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(workOrderType.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"workOrderType",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = workOrderType.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates workOrderType(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
workOrderType.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering workOrderType.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"workOrderType",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  workOrderType.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, workOrderType.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = workOrderType.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, workOrderType.getPKTable());
	}
};

/************************************************************************************
* Updates workOrderType(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.isInstallation = true;
*		inputArray[0].changeSet.isFollowUp = true;
*		inputArray[0].changeSet.updateStateOfService = true;
*		inputArray[0].changeSet.isCallOut = true;
*		inputArray[0].whereClause = "where businessCode = '0'";
*		inputArray[0].whereClause = "where countryCode = '0'";
*		inputArray[0].whereClause = "where WorkOrderTypeCode = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.isInstallation = true;
*		inputArray[1].changeSet.isFollowUp = true;
*		inputArray[1].changeSet.updateStateOfService = true;
*		inputArray[1].changeSet.isCallOut = true;
*		inputArray[1].whereClause = "where businessCode = '1'";
*		inputArray[1].whereClause = "where countryCode = '1'";
*		inputArray[1].whereClause = "where WorkOrderTypeCode = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.isInstallation = true;
*		inputArray[2].changeSet.isFollowUp = true;
*		inputArray[2].changeSet.updateStateOfService = true;
*		inputArray[2].changeSet.isCallOut = true;
*		inputArray[2].whereClause = "where businessCode = '2'";
*		inputArray[2].whereClause = "where countryCode = '2'";
*		inputArray[2].whereClause = "where WorkOrderTypeCode = '2'";
*		workOrderType.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
workOrderType.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering workOrderType.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898512bd8be";
	var tbname = "workOrderType";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"workOrderType",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, workOrderType.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  workOrderType.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, workOrderType.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  workOrderType.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = workOrderType.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes workOrderType using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
workOrderType.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.prototype.deleteByPK function");
	var pks = this.getPKTable();
	workOrderType.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
workOrderType.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering workOrderType.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(workOrderType.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function workOrderTypeTransactionCallback(tx){
		sync.log.trace("Entering workOrderType.deleteByPK->workOrderType_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function workOrderTypeErrorCallback(){
		sync.log.error("Entering workOrderType.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function workOrderTypeSuccessCallback(){
		sync.log.trace("Entering workOrderType.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering workOrderType.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, workOrderTypeTransactionCallback, workOrderTypeSuccessCallback, workOrderTypeErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes workOrderType(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. workOrderType.remove("where businessCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
workOrderType.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering workOrderType.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function workOrderType_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function workOrderType_removeSuccess(){
		sync.log.trace("Entering workOrderType.remove->workOrderType_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering workOrderType.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering workOrderType.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, workOrderType_removeTransactioncallback, workOrderType_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes workOrderType using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
workOrderType.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	workOrderType.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
workOrderType.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(workOrderType.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function workOrderTypeTransactionCallback(tx){
		sync.log.trace("Entering workOrderType.removeDeviceInstanceByPK -> workOrderTypeTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function workOrderTypeErrorCallback(){
		sync.log.error("Entering workOrderType.removeDeviceInstanceByPK -> workOrderTypeErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function workOrderTypeSuccessCallback(){
		sync.log.trace("Entering workOrderType.removeDeviceInstanceByPK -> workOrderTypeSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering workOrderType.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, workOrderTypeTransactionCallback, workOrderTypeSuccessCallback, workOrderTypeErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes workOrderType(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
workOrderType.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function workOrderType_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function workOrderType_removeSuccess(){
		sync.log.trace("Entering workOrderType.remove->workOrderType_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering workOrderType.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering workOrderType.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, workOrderType_removeTransactioncallback, workOrderType_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves workOrderType using primary key from the local Database. 
*************************************************************************************/
workOrderType.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	workOrderType.getAllDetailsByPK(pks,successcallback,errorcallback);
};
workOrderType.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var wcs = [];
	if(workOrderType.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering workOrderType.getAllDetailsByPK-> success callback function");
		successcallback(workOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves workOrderType(s) using where clause from the local Database. 
* e.g. workOrderType.find("where businessCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
workOrderType.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, workOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of workOrderType with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
workOrderType.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering workOrderType.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	workOrderType.markForUploadbyPK(pks, successcallback, errorcallback);
};
workOrderType.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering workOrderType.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(workOrderType.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of workOrderType matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
workOrderType.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering workOrderType.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering workOrderType.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering workOrderType.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of workOrderType pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
workOrderType.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workOrderType.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, workOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of workOrderType pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
workOrderType.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering workOrderType.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workOrderType.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, workOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of workOrderType deferred for upload.
*************************************************************************************/
workOrderType.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workOrderType.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, workOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to workOrderType in local database to last synced state
*************************************************************************************/
workOrderType.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering workOrderType.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workOrderType.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to workOrderType's record with given primary key in local 
* database to last synced state
*************************************************************************************/
workOrderType.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering workOrderType.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	workOrderType.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
workOrderType.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering workOrderType.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var wcs = [];
	if(workOrderType.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workOrderType.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering workOrderType.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether workOrderType's record  
* with given primary key got deferred in last sync
*************************************************************************************/
workOrderType.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  workOrderType.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	workOrderType.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
workOrderType.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering workOrderType.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var wcs = [] ;
	var flag;
	if(workOrderType.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workOrderType.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether workOrderType's record  
* with given primary key is pending for upload
*************************************************************************************/
workOrderType.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  workOrderType.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	workOrderType.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
workOrderType.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering workOrderType.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "workOrderType.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = workOrderType.getTableName();
	var wcs = [] ;
	var flag;
	if(workOrderType.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering workOrderType.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
workOrderType.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering workOrderType.removeCascade function");
	var tbname = workOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


workOrderType.convertTableToObject = function(res){
	sync.log.trace("Entering workOrderType.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new workOrderType();
			obj.isInstallation = res[i].isInstallation;
			obj.isFollowUp = res[i].isFollowUp;
			obj.updateStateOfService = res[i].updateStateOfService;
			obj.isCallOut = res[i].isCallOut;
			obj.businessCode = res[i].businessCode;
			obj.countryCode = res[i].countryCode;
			obj.WorkOrderTypeDescription = res[i].WorkOrderTypeDescription;
			obj.WorkOrderTypeCode = res[i].WorkOrderTypeCode;
			obj.updateDateTime = res[i].updateDateTime;
			obj.isRemoval = res[i].isRemoval;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

workOrderType.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering workOrderType.filterAttributes function");
	var attributeTable = {};
	attributeTable.isInstallation = "isInstallation";
	attributeTable.isFollowUp = "isFollowUp";
	attributeTable.updateStateOfService = "updateStateOfService";
	attributeTable.isCallOut = "isCallOut";
	attributeTable.businessCode = "businessCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.WorkOrderTypeDescription = "WorkOrderTypeDescription";
	attributeTable.WorkOrderTypeCode = "WorkOrderTypeCode";

	var PKTable = {};
	PKTable.businessCode = {}
	PKTable.businessCode.name = "businessCode";
	PKTable.businessCode.isAutoGen = false;
	PKTable.countryCode = {}
	PKTable.countryCode.name = "countryCode";
	PKTable.countryCode.isAutoGen = false;
	PKTable.WorkOrderTypeCode = {}
	PKTable.WorkOrderTypeCode.name = "WorkOrderTypeCode";
	PKTable.WorkOrderTypeCode.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject workOrderType. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject workOrderType. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject workOrderType. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

workOrderType.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering workOrderType.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = workOrderType.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

workOrderType.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering workOrderType.prototype.getValuesTable function");
	var valuesTable = {};
	valuesTable.isInstallation = this.isInstallation;
	valuesTable.isFollowUp = this.isFollowUp;
	valuesTable.updateStateOfService = this.updateStateOfService;
	valuesTable.isCallOut = this.isCallOut;
	if(isInsert===true){
		valuesTable.businessCode = this.businessCode;
	}
	if(isInsert===true){
		valuesTable.countryCode = this.countryCode;
	}
	valuesTable.WorkOrderTypeDescription = this.WorkOrderTypeDescription;
	if(isInsert===true){
		valuesTable.WorkOrderTypeCode = this.WorkOrderTypeCode;
	}
	return valuesTable;
};

workOrderType.prototype.getPKTable = function(){
	sync.log.trace("Entering workOrderType.prototype.getPKTable function");
	var pkTable = {};
	pkTable.businessCode = {key:"businessCode",value:this.businessCode};
	pkTable.countryCode = {key:"countryCode",value:this.countryCode};
	pkTable.WorkOrderTypeCode = {key:"WorkOrderTypeCode",value:this.WorkOrderTypeCode};
	return pkTable;
};

workOrderType.getPKTable = function(){
	sync.log.trace("Entering workOrderType.getPKTable function");
	var pkTable = [];
	pkTable.push("businessCode");
	pkTable.push("countryCode");
	pkTable.push("WorkOrderTypeCode");
	return pkTable;
};

workOrderType.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering workOrderType.pkCheck function");
	var wc = [];
	if(!kony.sync.isNull(pks.businessCode)){
		if(!kony.sync.isNull(pks.businessCode.value)){
			wc.key = "businessCode";
			wc.value = pks.businessCode.value;
		}
		else{
			wc.key = "businessCode";
			wc.value = pks.businessCode;
		}
	}else{
		sync.log.error("Primary Key businessCode not specified in " + opName + " an item in workOrderType");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("businessCode",opName,"workOrderType")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.countryCode)){
		if(!kony.sync.isNull(pks.countryCode.value)){
			wc.key = "countryCode";
			wc.value = pks.countryCode.value;
		}
		else{
			wc.key = "countryCode";
			wc.value = pks.countryCode;
		}
	}else{
		sync.log.error("Primary Key countryCode not specified in " + opName + " an item in workOrderType");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("countryCode",opName,"workOrderType")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.WorkOrderTypeCode)){
		if(!kony.sync.isNull(pks.WorkOrderTypeCode.value)){
			wc.key = "WorkOrderTypeCode";
			wc.value = pks.WorkOrderTypeCode.value;
		}
		else{
			wc.key = "WorkOrderTypeCode";
			wc.value = pks.WorkOrderTypeCode;
		}
	}else{
		sync.log.error("Primary Key WorkOrderTypeCode not specified in " + opName + " an item in workOrderType");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("WorkOrderTypeCode",opName,"workOrderType")));
		return;
	}
	kony.table.insert(wcs,wc);
	return true;
};

workOrderType.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering workOrderType.validateNull function");
	return true;
};

workOrderType.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering workOrderType.validateNullInsert function");
	return true;
};

workOrderType.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering workOrderType.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


workOrderType.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

workOrderType.getTableName = function(){
	return "workOrderType";
};




// **********************************End workOrderType's helper methods************************