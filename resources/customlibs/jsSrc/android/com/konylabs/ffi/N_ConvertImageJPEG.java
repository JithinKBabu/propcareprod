package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.cognizant.convertimage.ConvertImage;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_ConvertImageJPEG extends JSLibrary {

 
 
	public static final String convertImageType = "convertImageType";
 
	String[] methods = { convertImageType };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_ConvertImageJPEG(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 try {
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String base64PNG0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 base64PNG0 = (java.lang.String)params[0];
 }
 ret = this.convertImageType( base64PNG0 );
 
 			break;
 		default:
			break;
		}
 }catch (Exception e){
			ret = new Object[]{e.getMessage(), new Double(101), e.getMessage()};
		}
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "ConvertImageJPEG";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] convertImageType( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 java.lang.String val = com.cognizant.convertimage.ConvertImage.convertImageToJPEG( inputKey0
 );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};
