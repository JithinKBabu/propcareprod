package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.sampleimageFile.ImageCompress;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_compressImage extends JSLibrary {

 
 
	public static final String compressImageToBase64 = "compressImageToBase64";
 
	String[] methods = { compressImageToBase64 };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_compressImage(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 try {
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String base640 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 base640 = (java.lang.String)params[0];
 }
 ret = this.compressImageToBase64( base640 );
 
 			break;
 		default:
			break;
		}
 }catch (Exception e){
			ret = new Object[]{e.getMessage(), new Double(101), e.getMessage()};
		}
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "compressImage";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] compressImageToBase64( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 java.lang.String val = com.sampleimageFile.ImageCompress.getComprBase64( inputKey0
 );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};
