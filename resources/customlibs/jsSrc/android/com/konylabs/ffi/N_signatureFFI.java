package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.Sign;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_signatureFFI extends JSLibrary {

 
 
	public static final String getSignature = "getSignature";
 
	String[] methods = { getSignature };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_signatureFFI(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 try {
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 5){ return new Object[] {new Double(100),"Invalid Params"}; }
 com.konylabs.vm.Function signCallBack0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 signCallBack0 = (com.konylabs.vm.Function)params[0];
 }
 java.lang.String strimg0 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 strimg0 = (java.lang.String)params[1];
 }
 java.lang.String SignHere0 = null;
 if(params[2] != null && params[2] != LuaNil.nil) {
 SignHere0 = (java.lang.String)params[2];
 }
 java.lang.String Clear0 = null;
 if(params[3] != null && params[3] != LuaNil.nil) {
 Clear0 = (java.lang.String)params[3];
 }
 java.lang.String NewBtn0 = null;
 if(params[4] != null && params[4] != LuaNil.nil) {
 NewBtn0 = (java.lang.String)params[4];
 }
 ret = this.getSignature( signCallBack0, strimg0, SignHere0, Clear0, NewBtn0 );
 
 			break;
 		default:
			break;
		}
 }catch (Exception e){
			ret = new Object[]{e.getMessage(), new Double(101), e.getMessage()};
		}
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "signatureFFI";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] getSignature( com.konylabs.vm.Function inputKey0, java.lang.String inputKey1, java.lang.String inputKey2, java.lang.String inputKey3, java.lang.String inputKey4 ){
 
		Object[] ret = null;
 com.kony.Sign.getSign( (com.konylabs.vm.Function)inputKey0
 , inputKey1
 , inputKey2
 , inputKey3
 , inputKey4
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
};
